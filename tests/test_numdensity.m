N=1e5;
Res=2048;
NSearch=32;

if(~isoctave)
% savedState was created via:
%  Stream=RandStream.getGlobalStream;
%  save State Stream
load State
PrevStream = RandStream.setGlobalStream(Stream);
end

x=rand(N,1);
y=rand(N,1);
tic
A=numdensity(x,y,NSearch,Res,[0 1 0 1]);
toc

load Ao_numdensity
dA = A-Ao;
disp([' <A-Ao>=',num2str(mean(dA(:))),' std(A-Ao)=',num2str(std(dA(:)))])

