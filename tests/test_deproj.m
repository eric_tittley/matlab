load deproj_input.mat

[X,Y,Z,tol]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol);

load deproj_output.mat
mean(abs(X-X_o))
mean(abs(Y-Y_o))
mean(abs(Z-Z_o))
mean(abs(tol-tol_o))
