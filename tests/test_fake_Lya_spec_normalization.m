% Column density of the absorber
Ncol = 2e16; %  m^-2
T=100; % K
m_p = 1.6726217e-27; % proton mass (kg)
mass = m_p;
nu_0 = 2.466068e15; % Hz

Su = 1.1045987e-06; % m^2 s^-1

% Doppler broadening 
b_inv = sqrt(3.62148e+28 * mass / T ); % Doppler width [km/s]^-1
Const = 169139.782029778 / nu_0; % km

ddv = 0.01;
Sum=0;
EW=0;
figure(1)
clf reset
hold on
for dv=[-4:ddv:4];
 exponent = (dv * b_inv)^2.0;
 sig = Const * exp( -exponent ) * b_inv;
 tau = Su * sig * Ncol;
 Sum = Sum + tau;
 EW = EW + (1-exp(-tau));
 plot(dv,tau,'.');
end
hold off
Sum=Sum*ddv;
EW = EW * ddv



kms=[-4:ddv:4];
v=0;
flux_HI_test  =fake_Lya_spec(v, Ncol,  T, kms);

figure(2)
clf reset
plot(kms, flux_HI_test);

disp(['Difference in Equivalent Width from analytic: ',num2str(EW-sum((1-flux_HI_test))*ddv)])
