% Generate a field of two Gaussian distributions.
if( ~ exist('test_PlotWithDensity.mat') )
 N=1000000;
 Ordinate=[-1:0.01:1];
 Dist=gauss(10,0.1,Ordinate,0);
 X=randdist(Ordinate,Dist,N/2)+0.2;
 Y=randdist(Ordinate,Dist,N/2);
 X=[X; randdist(Ordinate,Dist,N/2)];
 Y=[Y; randdist(Ordinate,Dist,N/2)+0.3];
 save test_PlotWithDensity.mat X Y 
else
 load test_PlotWithDensity.mat
end

figure(1)
clf reset
plot(X,Y,'.','markersize',1)
XLim=get(gca,'XLim')
YLim=get(gca,'YLim')

figure(2)
clf reset
Handle=PlotWithDensity(X,Y,'.','markersize',1);
H=colorbar;
CLim=get(gca,'CLim')
set(H,'YLim',-0.5+CLim) ; % Adjusts the scale (number positions)
H_im=get(H,'child');
set(H_im,'YData',CLim-1); % Adjusts the colour bar colours
