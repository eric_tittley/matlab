N=1e5;

% savedState was created via:
%  Stream=RandStream.getGlobalStream;
%  save State Stream
load State
PrevStream = RandStream.setGlobalStream(Stream);

x=rand(N,1);
y=rand(N,1);
z=ones(N,1);
h=0.1*z;
tic
[A,unresolved,resolved]=h_proj(x,y,z,h,100,[0 1 0 1],1);
% The following should give a nice error message. Not crash.
% [A,unresolved,resolved]=h_proj(single(x),y,z,h,100,[0 1 0 1],1);
toc

load Ao_h_proj
dA = A-Ao;

disp([' <A-Ao>=',num2str(mean(dA(:))),' std(A-Ao)=',num2str(std(dA(:)))])
