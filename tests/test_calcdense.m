N=10000;

% savedState was created via:
%  Stream=RandStream.getDefaultStream;
%  save State Stream
load State
PrevStream = RandStream.setGlobalStream(Stream);

load A_calcdens

r=rand(3,N);

disp('single...')
rs=single(r);
[h_s,dn_s]=calcdens(rs,32,0.1);
disp([' mean error in h : ',num2str(mean(abs(h_s-h_s_o)))]);
disp([' mean error in dn: ',num2str(mean(abs(dn_s-dn_s_o)))]);

disp('double...')
[h_d,dn_d]=calcdens_d(r,32,0.1);
disp([' mean error in h : ',num2str(mean(abs(h_d-h_d_o)))]);
disp([' mean error in dn: ',num2str(mean(abs(dn_d-dn_d_o)))]);
