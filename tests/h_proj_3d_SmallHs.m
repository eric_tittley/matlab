% Put a particle in the centre of a box
% Set h = 0.5/3 to 0.75/3
% Integrate h on a 96^3 grid
% Rebin down to a 3^3 grid.
% Check the integral in each cell.

more off

r=[1 1 1]'/2;

hs=[0.50:0.0001:0.75]/3; % In units of volume edge length.

L = 2*96;
Limits=[0 1 0 1 0 1];
z=1;
As=zeros(3,3,3,length(hs));
for i=1:length(hs)
 h=hs(i);
 A=h_proj_3d(r,z,h,L,Limits);
 while(length(A)>6)
  A=RebinCubeToHalf(A);
 end
 As(:,:,:,i)=RebinCubeToHalf(A)*(L/3)^3; % 3^3
end

hGridSpacing = hs*3;

[P111,YFitted]=curvefit2(hGridSpacing,squeeze(As(1,1,1,:)),3 );
[P112,YFitted]=curvefit2(hGridSpacing,squeeze(As(1,1,2,:)),3 );
[P122,YFitted]=curvefit2(hGridSpacing,squeeze(As(1,2,2,:)),3 );
[P222,YFitted]=curvefit2(hGridSpacing,squeeze(As(2,2,2,:)),3 );

save Ps.mat P111 P112 P122 P222
