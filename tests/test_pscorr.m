N=1e6;

r=rand(3,N);
L=5; % i.e. to 1/5 = 0.2
nbins=32;
rllw=-3;

corr = zeros(nbins,length(rllw));
rad  = zeros(nbins,length(rllw));
for i=1:length(rllw)
 tic
 [rad(:,i),ncorr,nlt,corr(:,i),J3]=pscorr(r,L,nbins,rllw(i));
 toc
end

R=(rad(1:end-1)+rad(2:end))/2;
CF=RBC_CorrelationCorrectionFactor(R);

plot(R,(corr(1:end-1)+1)./CF);
lh=line(get(gca,'Xlim'),[1 1]);
set(lh,'color',[0 0 0])
