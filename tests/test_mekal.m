bins=10.^[-1:0.01:1];
CEm = 1.0;
H=10;
T=2;
Abund=ones(15,1);

[flux,ed]=mekal(bins,CEm,H,T,Abund);

E=(bins(1:end-1)+bins(2:end))/2;
loglog(E,flux)

