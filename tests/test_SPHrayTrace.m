clear

L=40;

NParticles = L^3;
NRays      = 2^16;
Nsph       = 32;
r = zeros(3,NParticles);
dr=1/L;
for i = 1:L
 for j = 1:L
  for k = 1:L
  r(:,k+(j-1)*L+(i-1)*L*L)=[i-0.5 j-0.5 k-0.5]*dr;
  end
 end
end
h_guess=0.05;

disp('Calculating h')
tic
[h,dn]=calcdens(single(r),Nsph,h_guess);
% calcdens takes the kernel to be compact over 2*h
toc

h=double(h);
%z=1./dn;
z=ones(NParticles,1)/NParticles;

mean(h)

Rays=zeros(3,2,NRays);

for i=1:NRays
 Rays(:,1,i)=[0.5 0.5 0.5];
 Rays(:,2,i)=rand(3,1);
end

disp('Calculating Integrals')
tic
RayIntegrals=SPHrayTrace(r,z,2*h,Rays);
toc
RayIntegrals=squeeze(RayIntegrals);

figure(1)
disp('Calculating histogram')
hist(RayIntegrals,100)

RayLength=squeeze(sqrt(sum((Rays(:,2,:)-Rays(:,1,:)).^2)));

figure(2)
plot(RayLength,RayIntegrals,'.')

