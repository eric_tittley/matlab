nTrials=100;
dh=0.0001;
hs=[dh:dh:0.02];

more off
L=128;
Limits=[0 1 0 1 0 1];
z=1;

results=zeros(nTrials, length(hs));

tic
for ih=1:length(hs)
 h=hs(ih)
 for iTrial=1:nTrials
  r=2*h+(1-4*h)*rand(3,1);
%  A=h_proj_3d(r,z,h,L,Limits);
  A=h_proj_3d_parallel(r,z,h,L,Limits);
%  A=h_proj_3d_PR(r,z,h,L,Limits);
%  if(abs(sum(A(:))-1)>0.0001)
%   disp(r)
%   blah
%  end
  results(iTrial,ih) = sum(A(:));
 end
end
toc

meanError=mean(abs(results-1));
clf reset
GridSpacing=1/L;
plot(hs/GridSpacing,100*meanError,'r')
xlabel('h / GridSpacing')
ylabel('mean absolute error %')
