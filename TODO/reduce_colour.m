function [image_out,map_out]=reduce_colour(image,map,tol)

list=fof(map,tol);

N_colours=max(list);

map_out=zeros(N_colours,3);
image_out=image;

for i=1:N_colours
 similar_colours=find(list==i);
 if(length(similar_colours)>1)
  map_out(i,:)=mean(map(similar_colours,:));
 else
  map_out(i,:)=map(similar_colours,:);
 end
 N_similar_colours=length(similar_colours);
 for j=1:N_similar_colours
  image_out(image==similar_colours(j))=uint8(i);
 end
end
