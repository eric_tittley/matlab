/* function smooth=smooth_kernel(Image,level) */
/* AUTHOR: Eric Tittley */
/* HISTORY
 *  03 03 17 Added comments.
 *	Clarified error messages.
 *	Removed unused variables.
 *	Removed diagnostic output in smoothkernel().
*/

#include <math.h>
#include <stdio.h> /* only for debugging messages */
#include "mex.h"

int smoothkernel(double *, int, int, double, int*, double *); 
double sum_over_radius(double *, int, int, int, int, int);

/* ************************************************** */
void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

/* mxArray *smooth,*Image; */
 double *smooth,*Image;
 double *mask_dbl;
 int *mask_int;
 double *level;
 size_t mrows,ncols;
 size_t mrows_dum,ncols_dum;
 int status;
 int i,j;

/* Check for the proper number of arguments */
 if( (nrhs!=3) || (nlhs!=1) ) {
  mexErrMsgTxt("Syntax: smooth=smooth_kernel(Image,level,mask)");
 }

/* The inputs must be of the type double */
 if( !mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]) || !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("Image, level, and mask must be type double");
 }

/* The second argument must be scalar */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("level must be scalar");
 }

/* The first and third arguments must be the same size */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 mrows_dum = mxGetM(prhs[2]);
 ncols_dum = mxGetN(prhs[2]);
 if(mrows!=mrows_dum || ncols !=ncols_dum) {
  mexErrMsgTxt("Image and mask must be the same size");
 }

/* Create matrix for the return argument and assign a pointer */
 plhs[0]=mxCreateDoubleMatrix(mrows,ncols, mxREAL);
 smooth = mxGetPr(plhs[0]);

/* Assign pointers to the input */
 Image   = mxGetPr(prhs[0]);
 level   = mxGetPr(prhs[1]);
 mask_dbl= mxGetPr(prhs[2]);

/* we are going to pass integers to the subroutine for the mask */
 mask_int=(int*) malloc(ncols*mrows*sizeof(int));
 if(mask_int==NULL) {
  mexErrMsgTxt("Could not allocate memory for the mask");
 }
 for(i=0;i<ncols;i++){
  for(j=0;j<mrows;j++){
   mask_int[j + mrows*i]=floor(mask_dbl[j + mrows*i]);
  }
 }
 
 /* Call the subroutine */
 status = smoothkernel(Image,(int)mrows,(int)ncols,*level,mask_int,smooth);
}

/* ************************************************** */

int smoothkernel(
 double *Image, /* Image is 2-d data stored column after column */
 int mrows,
 int ncols,
 double level,
 int *mask,  /* The do-this-cell mask */
 double *smooth /* empty matrix size of Image */
) {
 
 int ix,iy;
/* int jx,jy; */
/* double dist; */
 int radius;
 double sum;
/* int xlo,xhi,ylo,yhi; */

 radius=1;
 for(ix=0; ix<ncols; ix++) {
  printf("column: %d of %d \n",ix,ncols-1);
  for(iy=0; iy<mrows; iy++) {

   if(mask[iy + ix*mrows]) {

    /* On first pass, use the previously found search radius */  
    sum=sum_over_radius(Image,ncols,mrows,ix,iy,radius);
    if(sum<level) {
     /* increase radius monotonically until sum is greater than level */
     while(sum<level) {
      radius++;
      sum=sum_over_radius(Image,ncols,mrows,ix,iy,radius);
     }
    } /* end if initial radius was too low */
    else { /* initial guess at radius was too high */
     /* decrease radius monotonically until sum is greater than level */
     while(sum>level) {
      radius--;
      sum=sum_over_radius(Image,ncols,mrows,ix,iy,radius);
     }
     /* Now that we've found the appropriate radius-1, we have to increment radius by 1 */
     /* and recaluclate sum */
     radius++;
     sum=sum_over_radius(Image,ncols,mrows,ix,iy,radius);
    } /* end if initial guess at radius was too high */
    smooth[iy + mrows*ix]=sum/(3.141*pow((double)radius,2.0));
   } /* end if mask */
   else {
    smooth[iy + mrows*ix]=0.;
   }
  } /* end for iy */
 } /* end for ix */
 return(0);
}

/* ************************************************** */

double sum_over_radius(
 double *Image,        /* The input array */
 int ncols, int mrows, /* The size of the array */
 int ix, int iy,       /* The point in the array */
 int radius            /* The search radius */
) {
 double sum,dist;
 int xlo, xhi, ylo, yhi;
 int jx, jy;

 sum=0.;
 xlo=ix-radius;
 if(xlo<0) {xlo=0;}
 xhi=ix+radius;
 if(xhi>ncols) {xhi=ncols;}
 ylo=iy-radius;
 if(ylo<0) {ylo=0;}
 yhi=iy+radius;
 if(yhi>mrows) {yhi=mrows;}

 for(jx=xlo; jx<xhi; jx++) {
  for(jy=ylo; jy<yhi; jy++) {
   dist=sqrt( pow((double)(jx-ix),2.) + pow((double)(jy-iy),2.) );
   if(dist<(double)radius) {
    sum=sum+Image[jy + mrows*jx];
   }
  }
 }
 return(sum);
}
