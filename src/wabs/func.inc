c Include file for FUNCTION object

c  CHARACTER*4       solar      The Solar abundance table in use.
c  CHARACTER*4       xsect      The photoelectric cross-sections in use.
c  CHARACTER*128     datdir     Directory for data files used.
c  REAL              solfil(18) Solar abundance table read in from a file

      REAL      solfil(18)
      CHARACTER datdir*128
      CHARACTER solar*4
      CHARACTER xsect*4
      parameter(solar='feld')
      parameter(xsect='vern')
 
      COMMON /funcco1/ solfil
C      COMMON /funcco2/ solar, xsect, datdir
      COMMON /funcco2/ datdir
