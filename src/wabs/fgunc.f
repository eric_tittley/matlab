c Output routines for the FUNCTION object.

c Data types
c  CHARACTER*4        solar       The Solar abundance table in use.
c  CHARACTER*4        xsect       The photoelectric cross-sections in use.
c  CHARACTER*128      datdir      Directory for data files used.
c  DOUBLE PRECISION               solfil      Solar abundance table read in from a file

c Output routines
c       fgsolr          function returns the Solar abundance table in use.
c       fgxsct          function returns cross-sections in use.
c       fgabnd          function returns the Solar abundance for the input elt.
c       fgdatd          function returns the data file directory.

c *****************************************************************************
      FUNCTION fgsolr()

      INCLUDE 'func.inc'

      CHARACTER fgsolr*4

c Returns the Solar abundance table in use
c Arguments :
c      fgsolr   C*4    r: The Solar abundance table

      fgsolr = solar

      RETURN
      END

c *****************************************************************************
      FUNCTION fgxsct()

      INCLUDE 'func.inc'

      CHARACTER fgxsct*4

c Returns the photoelectric cross-sections in use.
c Arguments :
c      fgxsct   C*4    r: The cross-sections

      fgxsct = xsect

      RETURN
      END

c *****************************************************************************
      FUNCTION fgabnd(element)

      INCLUDE 'func.inc'

      DOUBLE PRECISION      fgabnd
      CHARACTER element*2

c Returns the Solar abundance for the input element
c Arguments :
c      fgabnd   R    r: The Solar abundance for the input element

      INTEGER NELTS
      PARAMETER(NELTS=18)

      DOUBLE PRECISION        feld(NELTS), angr(NELTS), aneb(NELTS)
      CHARACTER*2 elts(NELTS)

      INTEGER i

      CHARACTER fgsolr*4
      EXTERNAL  fgsolr

c Solar abundance tables. 'feld' is from Feldman, U., 1992. Physica Scripta,
c 46, 202. 'angr' is from Anders, E. & Grevesse, N., 1989. Geochimica and
c Cosmochimica Acta 53, 197. 'aneb' is from Anders, E. & Ebihara, 1982. 
c Geochimica and Cosmochimica Acta 46, 2363. 'file' is from a file read in
c by the user.

      DATA elts/'H ', 'He', 'C ', 'N ', 'O ', 'Ne', 'Na', 'Mg',
     &          'Al', 'Si', 'S ', 'Cl', 'Ar', 'Ca', 'Cr', 'Fe',
     &          'Co', 'Ni'/
      DATA feld/1.00e+0, 9.77e-2, 3.98e-4, 1.00e-4, 8.51e-4,
     &          1.29e-4, 2.14e-6, 3.80e-5, 2.95e-6, 3.55e-5, 
     &          1.62e-5, 1.88e-7, 4.47e-6, 2.29e-6, 4.84e-7, 
     &          3.24e-5, 8.60e-8, 1.78e-6/
      DATA angr/1.00e+0, 9.77e-2, 3.63e-4, 1.12e-4, 8.51e-4,
     &          1.23e-4, 2.14e-6, 3.80e-5, 2.95e-6, 3.55e-5, 
     &          1.62e-5, 1.88e-7, 3.63e-6, 2.29e-6, 4.84e-7, 
     &          4.68e-5, 8.60e-8, 1.78e-6/
      DATA aneb/1.00e+0, 8.01e-2, 4.45e-4, 9.12e-5, 7.39e-4,
     &          1.38e-4, 2.10e-6, 3.95e-5, 3.12e-6, 3.68e-5, 
     &          1.89e-5, 1.93e-7, 3.82e-6, 2.25e-6, 4.93e-7, 
     &          3.31e-5, 8.27e-8, 1.81e-6/

      fgabnd = 0.

      DO i = 1, NELTS
         IF ( elts(i) .EQ. element ) THEN
            IF ( fgsolr() .EQ. 'feld' ) THEN
               fgabnd = feld(i)
            ELSEIF ( fgsolr() .EQ. 'angr' ) THEN
               fgabnd = angr(i)
            ELSEIF ( fgsolr() .EQ. 'aneb' ) THEN
               fgabnd = aneb(i)
            ELSEIF ( fgsolr() .EQ. 'file' ) THEN
               fgabnd = solfil(i)
            ENDIF
          RETURN
         ENDIF
      ENDDO

      RETURN
      END

c *****************************************************************************
      FUNCTION fgdatd()

      INCLUDE 'func.inc'

      CHARACTER fgdatd*(*)

c Returns the data file directory in use
c Arguments :
c      fgdatd   C*(*)    r: The data file directory

      fgdatd = datdir

      RETURN
      END

