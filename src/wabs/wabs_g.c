/* wabs_g: Gateway function for WABS
 *
 * MATLAB sytax: trans=wabs(bins,nH,Abund,z)
 */

#include <mex.h>

#include <stdio.h>

#define N_ABUNDANCES    18
#define N_PARAMS        19

void xszvab_(double *bins, int *NBins, double *param, double *trans);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 mwSize binsM,binsN,nHM,nHN,AbundM,AbundN,zM,zN;
 int NBins;

 double param[N_PARAMS];
      
 int i;
      
 double *trans;
 double *bins;
 double *nH;
 double *Abund;
 double *z;

 /* Check for the proper number of arguments */
 if( (nlhs!=1) || (nrhs!=4) ) {
  mexErrMsgTxt("Syntax: trans=wabs(bins,nH,Abund,z)");
 }

 /* Get the sizes of the input arguments */
 binsM =mxGetM(prhs[0]);
 binsN =mxGetN(prhs[0]);
 nHM   =mxGetM(prhs[1]);
 nHN   =mxGetN(prhs[1]);
 AbundM=mxGetM(prhs[2]);
 AbundN=mxGetN(prhs[2]);
 zM    =mxGetM(prhs[3]);
 zN    =mxGetN(prhs[3]);

 /* Check sizes */
 if( (binsM != 1) && (binsN != 1) ) {
  mexErrMsgTxt("bins must be a vector");
 }
 if(binsM > binsN) {
  binsN=binsM;
 }
 if(binsN < 2) {
  mexErrMsgTxt("Must be 2 or more bin edges in bins");
 }
 if((nHM != 1) || (nHN != 1)) {
  mexErrMsgTxt("nH must be a scalar");
 }
 if((AbundM != 1) && (AbundN != 1)) {
  mexErrMsgTxt("Abund must be a vector with 17 elements");
 }
 if(AbundM > AbundN) {
  AbundN=AbundM;
 }
 if(AbundN != 17) {
  mexErrMsgTxt("Abund must be a vector with 17 elements");
 }
 if((zM != 1) || (zN != 1)) {
  mexErrMsgTxt("z must be a scalar");
 }

 /* Assign pointers to the input arguments */
 bins  = mxGetPr(prhs[0]);
 nH    = mxGetPr(prhs[1]);
 Abund = mxGetPr(prhs[2]);
 z     = mxGetPr(prhs[3]);

 /* Create a matrix for return argument */
 plhs[0] = mxCreateDoubleMatrix(binsN-1,1,mxREAL);
 trans   = mxGetPr(plhs[0]);

 /* Set the abundances */
 param[0]=*nH;
 for(i=1;i<18;++i) {
  param[i]=*nH*Abund[i-1];
 }
 param[18]=*z;

 /* We need an integer to pass to xszvab */
 NBins=(int)binsN;

 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 xszvab_(bins, &NBins, param, trans);

}       
