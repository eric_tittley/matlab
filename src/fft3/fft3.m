% transform = fft3(data)
%
% 3-D FFT.
%
% Powered by the FFTW suite of tools (www.fftw.org)
%
% SEE ALSO:
% ifft3

% AUTHOR
%  Eric Tittley

% HISTORY
%  01-10-12 First version

