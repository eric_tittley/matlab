/* SUBROUTINE: fit_gaussian */

/* Includes the support routines:
* boxcar()
* gaussian()
* frame_error()
* make_gaussian()
*/

/* AUTHOR: Eric Tittley */

/*  HISTORY  */
/*
* 01 11 28 Cosmetic changes to the comments.
*
* 01 08 16 Implemented new method for minimisation.  Now it is
*	done in a grid-like fashion.
*
* 00 12 14 Fixed a bug in boxcar, where I was multiplied by *N
*	instead of *M.
*
* 00 12 13  I don't know how it ever worked before 
*      Fixed numerous bugs.                    
*      Added boxcar averaging of the frame to permit
*      a discriminant for cosmic arrays        
*      when finding the guess at the centre of the
*      star.                                   
*/


#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "fit_gaussian.h"

/* ********************************************************************* */
/* BOXCAR AVERAGE AN ARRAY */
void boxcar( const int * const Array,
                   int * const Array_out,
             const size_t * const N,
             const size_t * const M ) {
/* Memory for Array_out must already have been allocated */

/* M is the inner array dimension, N the outer i.e. (0->M-1) loops N-1 times */
/* This is because Matlab stores arrays one column at a time */
/* A byproduct of this is that x corresponds to the N and y corresponds to M */
/* If you like, the data is stored one RA at a time, cylcing through */
/* declination */


 long i,j;
 long di,dj;
 long ii,jj;
 long sum;

 for(i=0;i<*N;i++){
  for(j=0;j<*M;j++){
   sum=0;
   for(di=-1;di<=1;di++){
    ii=i+di;
    if(ii==-1) {ii=*N-1;}
    if(ii==*N) {ii=0;}
    for(dj=-1;dj<=1;dj++){
     jj=j+dj;
     if(jj==-1) {jj=*M-1;}
     if(jj==*M) {jj=0;}
     sum+=(long)Array[ii*(*M)+jj];
    }
   }
   Array_out[i*(*M)+j]=sum/(long)9;
  }
 }

}
/* END OF BOXCAR AVERAGE   */
/* ********************************************************************* */

float gaussian( const float * const x,
                const float * const sigma,
                const float * const ampl  ){
 return( (*ampl)/(*sigma)*exp(-1.0*((*x)/(*sigma))*((*x)/(*sigma))/2.0) ); 
}

/* ********************************************************************* */

double frame_error( const int * const frame_int,
                    const float * const frame_float,
                    const size_t * const ArrLen ) {
 double err;
 size_t i;
 
 err=0.0;
 for( i=0; i < *ArrLen; i++ ) {
/*  printf("%f ",pow( (double)((float)(*(frame_int+i)) - *(frame_float+i)), 2.0 )); */
  err += pow( (double)(((float)frame_int[i])-frame_float[i]), 2.0 );
 }
 err = sqrt(err)/(*ArrLen);
 return(err);
}

/* ********************************************************************* */

void make_gaussian(
/* Input arguments, not modified */
/* M is the inner array dimension, N the outer i.e. (0->M-1) loops N-1 times */
/* This is because Matlab stores arrays one column at a time */
/* A byproduct of this is that x corresponds to the N and y corresponds to M */
/* If you like, the data is stored one RA at a time, cylcing through */
/* declination */
const size_t * const M, const size_t * const N,
const float * const bias, const float * const sigma, const float * const ampl,
const float * const x, const float * const y,
/* Output argument (space must already have been allocated! */
/* ( a "int * const" is a pointer whose value cannot change, but the contents
     at the address to which it points may change) */
float * const frame ) { 

 size_t i,j;
 float rx,ry,rx2;
 float dist;
 
/*
 printf("bias=%f, sigma=%f, ampl=%f, x=%f, y=%f\n", *bias, *sigma, *ampl, *x, *y );
*/

/* x corresponds to N corresponds to i */
/* y corresponds to M corresponds to j */

 for(i = 0; i < *N ; i++) {
  rx = (float)i - *x;
  rx2 = rx*rx;
  for(j = 0; j < *M ; j++) {
   ry = (float)j - *y;
   dist = sqrt( rx2 + ry*ry );
   frame[i*(*M) + j] = gaussian( &dist, sigma, ampl) + (*bias) ;
  }
 }
} /* end of make_gaussian */

/* ********************************************************************* */

int fit_gaussian(
/* Input arguments */
const int * const frame, const size_t * const N, const size_t * const M,
/* output arguments */
float * const bias, float * const sigma, float * const ampl,
float * const x, float * const y ) {

/* given an image, frame, of a star,
   fit a gaussian to it and return the fit */

/* History */
/* First Version: Eric Tittley 00 09 23 */

 size_t i;
 long int is; /* a signed array index.  Use with caution. */
 int sum, max, index_max;
 int iters1;
 const int ITER_MAX=100;
 float *frame_approx;
 const size_t ArrLen = *N * *M ;
 double err;
 double TOL = 0.0000001;
 float Delta = 0.01;
 int *frame_smooth;
 float Sum;
 double min_bias,max_bias,min_ampl,max_ampl;
 double min_sigma,max_sigma,min_x,max_x,min_y,max_y;
 double dbias,dampl,dsigma,dx,dy;
 int min_index,BreakFlag;
 float test,errs[5],min_err;
 
 /* Allocate Memory for the box-car-smoothed image */
 frame_smooth=(int *)malloc( ArrLen * sizeof(int) );
 if(frame_smooth==NULL) {
  printf("fit_gaussian: failed to allocate memory for frame_smooth\n");
  return(-1);
 }

 /* Smooth the image */
 boxcar(frame,frame_smooth,N,M);

 /* make a guess at the bias using the first 10 points */
 sum=0;
 for( i=0;i<10;i++ ) {
  sum=sum+frame_smooth[i];
 }
 *bias = ((float)sum)/10.0;

 /* find the approximate centre */
 /* The criterion for Cosmic ray rejection is if the pixel value less the */
 /* bias is greater than twice the smoothed pixel value less the bias */
 /* i.e. frame-bias > 2*(frame_smooth-bias) */
 /* which is the same thing as: frame > 2*frame_smooth-bias */
 max=0;
 index_max=-1;
 for( i=0;i<ArrLen;i++ ) {
  if(frame[i]>max) {
   if( frame[i] < (2*frame_smooth[i]-((int) *bias)) ) {
    max=frame[i];
    index_max=i;
   }
  }  
 }
 if(index_max==-1) {
  printf("error in fit_gaussian: No maximum found\n");
  return(-1);
 }
 
 /* guess at x and y */
 /* this is very important, since convergence is worse for this parameter */
 /* Convergence to a valid set of parameters is fairly sensitive to
    getting x and y centred on the star  (within +- 10 pixels) */
 *x = (float)(floor( ((double)index_max)/((double)(*M)) ));
 *y = (float)(index_max-(int)(*x)*(int)(*M)) ;

 /* guess at sigma */
 *sigma = 5.0;

 /* guess at ampl */
 *ampl = 5 * ( max - *bias );

 /* allocated space for the calculated field */
 frame_approx = (float *)malloc( ArrLen*sizeof(float) );
 if(frame_approx==NULL) {
  printf("fit_gaussian: failed to allocate memory for frame_approx\n");
  return(-1);
 }

 /* initial guess at frame */
 make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );

 /* initial error */
 err = frame_error( frame, frame_approx, &ArrLen );

 /* The sum of the frame */
 Sum=0.0;
 for(i=0;i<ArrLen;i++) {
  Sum += (float)frame[i];
 }

 /* the minimum and maximum values */
 min_bias=0.0;
 max_bias=4294967296.0;
 min_ampl=0.0;
 max_ampl=4294967296.0;
 min_sigma=0.0;
 max_sigma=4294967296.0;
 min_x=0.0;
 max_x=(float)(*N)-1.0;
 min_y=0.0;
 max_y=(float)(*M)-1.0;

 /* Now loop through, trying to minimise the err */
 /* The variables that can change are bias, ampl, sigma, x, and y */
 /* For each of these, there is a dxxxx */
 iters1=0;
 BreakFlag=0;
 do {
  iters1++;
  if(iters1>ITER_MAX) {
   printf("warning fit_gaussian: failed to converge (iters1)\n");
   BreakFlag=1;
  }
  
  /* Calculate the dxxx's */
  dbias=Delta * (*bias);
  dampl=Delta * (*ampl);
  dsigma=Delta* (*sigma);
  dx   =Delta * (*x);
  dy   =Delta * (*y);
  
  /* The ampl */
/*  printf("The Ampl\n"); */
  for(is=-2;is<=2;is++) {
   test = (*ampl) + ((double)is)*dampl;
   make_gaussian( M, N, bias, sigma, &test, x, y, frame_approx );
   errs[is+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
/*  printf("ampl=%f min_index=%i dampl=%f\n",*ampl,min_index,dampl); */
  *ampl = (*ampl) + ((double)min_index-2.0)*dampl;
  if(*ampl<min_ampl) {
   *ampl = min_ampl;
  }
  if(*ampl>max_ampl) {
   *ampl = max_ampl;
  }
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */ 
  
  /* The x */
/*  printf("x\n"); */
  for(is=-2;is<=2;is++) {
   test = (*x) + ((double)is)*dx;
   make_gaussian( M, N, bias, sigma, ampl, &test, y, frame_approx );
   errs[is+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *x = (*x) + ((double)min_index-2.0)*dx;
  if(*x<min_x) {
   *x = min_x;
  }
  if(*x>max_x) {
   *x = max_x;
  }
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  /* The y */
/*  printf("y\n"); */
  for(is=-2;is<=2;is++) {
   test = (*y) + ((double)is)*dy;
   make_gaussian( M, N, bias, sigma, ampl, x, &test, frame_approx );
   errs[is+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *y = (*y) + ((double)min_index-2.0)*dy;
  if(*y<min_y) {
   *y = min_y;
  }
  if(*y>max_y) {
   *y = max_y;
  }
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */

  /* The Bias */
/*  printf("The bias\n"); */
  for(is=-2;is<=2;is++) {
   test = (*bias) + ((double)is)*dbias;
   make_gaussian( M, N, &test, sigma, ampl, x, y, frame_approx );
   errs[is+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *bias = (*bias) + ((double)min_index-2.0)*dbias;
  if(*bias<min_bias) {
   *bias = min_bias;
  }
  if(*bias>max_bias) {
   *bias = max_bias;
  }
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  /* The sigma */
/*  printf("Sigma\n"); */
  for(is=-2;is<=2;is++) {
   test = (*sigma) + ((double)is)*dsigma;
   make_gaussian( M, N, bias, &test, ampl, x, y, frame_approx );
   errs[is+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *sigma = (*sigma) + ((double)min_index-2.0)*dsigma;
  if(*sigma<min_sigma) {
   *sigma = min_sigma;
  }
  if(*sigma>max_sigma) {
   *sigma = max_sigma;
  }
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
  err = frame_error( frame, frame_approx, &ArrLen );
/*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */

  Delta=Delta*0.7071;
  
/*  printf("fabs(err)/Sum=%f TOL=%f\n",fabs(err)/Sum,TOL); */
  
 } while( (fabs(err)/Sum>TOL) & (BreakFlag!=1) ); /* the percentage error is > TOL */

 /* free up allocated space */
 free( (void *) frame_approx);
 free( (void *) frame_smooth);
 
 /* increment x and y by 1 to translate from the [0..N-1] indexing of */
 /* C to the [1..N] indexing of Matlab */
 *x = *x +1;
 *y = *y +1;
 return(0);
} /* end of fit_gaussian */
