#ifndef _FIT_GAUSSIAN_H_
#define _FIT_GAUSSIAN_H_

#include <math.h>
#include <stdlib.h> /* for the memory allocation */
#include <stdio.h>
#include "mex.h"

int fit_gaussian(
/* Input arguments */
const int * const frame, const size_t * const N, const size_t * const M,
/* output arguments */
float * const bias, float * const sigma, float * const ampl,
float * const x, float * const y );

#endif
