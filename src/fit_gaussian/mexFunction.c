/* function [bias,sigma,ampl,x,y]=fit_gaussian(frame) */

#include <mex.h>

#include "fit_gaussian.h"

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 size_t i;
 int *frame_int;
 size_t mrows,ncols;
 int status;
 float bias_float, sigma_float, ampl_float, x_float, y_float;
 double *bias_double, *sigma_double, *ampl_double, *x_double, *y_double;
 double *frame;

 /* Check for the proper number of arguments */
 if( (nrhs!=1) || (nlhs!=5) ) {
  mexErrMsgTxt("Syntax: [bias,sigma,ampl,x,y]=fit_gaussian(frame)");
 }

 /* The argument must be matrix */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows==1 || ncols ==1) {
  mexErrMsgTxt("frame must be a matrix");
 }

 /* frame will be a double matrix, we want it to be an integer matrix */
 frame_int = (int *)malloc(mrows*ncols*sizeof(int));
 if(frame_int==NULL) {
  mexErrMsgTxt("Unable to create space");
 }
 frame=mxGetPr(prhs[0]);
 
 for(i=0;i<mrows*ncols;i++) {
  frame_int[i]=(int)rint(frame[i]);
 }

  /* Call the subroutine */						
    /* input */
    /* output */
  status = fit_gaussian(
              frame_int, &ncols, &mrows,
              &bias_float, &sigma_float, &ampl_float, &x_float, &y_float );
  if(status==-1) {                                                     
   mexErrMsgTxt("core subroutine returned bad status");                
  }                                                                    

 plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);


 bias_double =mxGetPr(plhs[0]);
 sigma_double=mxGetPr(plhs[1]);
 ampl_double =mxGetPr(plhs[2]);
 x_double    =mxGetPr(plhs[3]);
 y_double    =mxGetPr(plhs[4]);

 *bias_double  = (double)bias_float;
 *sigma_double = (double)sigma_float;
 *ampl_double  = (double)ampl_float;
 *x_double     = (double)x_float;
 *y_double     = (double)y_float;

 free(frame_int);
}
