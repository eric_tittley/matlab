#pragma once

#include "Spheres.h"

struct SEDMDataset {
    void getSpheres(Spheres&) const;
};

void parseSEDMDataset(std::string&, SEDMDataset&);
