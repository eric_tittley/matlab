#pragma once

#include <stack>
#include <vector>

#include "Common.h"
#include "BVHNode.h"
#include "Spheres.h"
#include "Ray.h"
#include "Packet.h"
#include "Weight.h"

//#define LEAFSIZE 32
#define LEAFSIZE 1

struct BVH {
    //access required for building
    Spheres spheres;
    std::vector<uint32_t> map;
    std::vector<BVHNode> nodes;

    void weightRay(const Ray&, const size_t, const real*, real*) const;
    void weightRay(const Ray&, real*) const;

    #ifdef SIMD
    void weightPacket(const Packet&, const size_t, const real*, real[PACKETSIZE]) const;
    void weightPacket(const Packet&, real[PACKETSIZE]) const;
    #endif

    /// \brief Create packet from the target to the source.
    /// 
    /// Must be leaf nodes.
    int makeRays(const size_t, const real[3], Ray*) const;
    #ifdef SIMD
    int makePackets(const size_t, const real[3], Packet*) const;
    #endif
};

/// \brief Prototype of SAH BVH build factory.
///
/// This function builds a SAH BVH out of a collection of spheres.
//void mkSAHBVH(const Spheres&, BVH&);

/// /brief Prototype of Object Median build algorithm.
///
///
void mkOBVH(const Spheres&, const real*, BVH&);

/// /brief Prototype of flat BVH tree construction.
///
/// 
//void mkNBVH(const Spheres&, BVH&);
