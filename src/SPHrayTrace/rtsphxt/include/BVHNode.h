#pragma once

struct BVHNode {
    //bounding box
    float min[3];
    float max[3];

    //internal pointer
    uint32_t right;
    //leaf data
    uint32_t offset;

    BVHNode() {
        min[0] =  real_inf;    
        min[1] =  real_inf;    
        min[2] =  real_inf;    
        max[0] = -real_inf;    
        max[1] = -real_inf;    
        max[2] = -real_inf;    
    }

    inline bool isLeaf() const {
        return (0 == right);
    }

    inline bool isInternal() const {
        return (0 != right);
    }

    inline void setLeaf() {
        right = 0;
    }
};
