#pragma once
/// \file Spheres.h
/// \brief Sphere(SOA) structure

#include "Common.h"
#include "Ray.h"
#include "Packet.h"

/// \brief Spheres data as Structure of Arrays
///
/// This structure contains the sphere data which is loaded from
/// Gadget2 datasets. 
struct Spheres {
    /// \brief Number of spheres in arrays.
    size_t count;

    /// \brief Arrays containing position data of spheres.
    real *c[3];

    /// \brief Array which contains inverse radius squared. (1/h2)
    real *invh2;

    /// \brief Allocate all sphere data memory.
    void allocate(size_t size) {
        count = size;
        
        real* tmp = new real[size*4];

        c[0] = tmp;
        c[1] = &(tmp[size]);
        c[2] = &(tmp[size*2]);
        invh2 = &(tmp[size*3]);
    }

    /// \brief Deallocate all sphere data memory.
    void deallocate() {
        delete[] c[0];
    }

    /// \brief Intersect the indexed sphere with ray.
    void intersect(size_t, const Ray&, real&) const;

    #ifdef SIMD
    /// \brief 
    ///
    ///
    void intersect(size_t, const Packet&, doublew[PACKETWIDTH]) const;
    #endif
};
