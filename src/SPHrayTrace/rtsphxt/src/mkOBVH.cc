#include "BVH.h"

#include <stack>
#include <algorithm>
#include <iostream>

#include "Packet.h"

struct Item {
    uint32_t off, run, parent;
    bool right, root;
};

struct Comparator {
    Comparator(const Spheres& sphs)
    : spheres(sphs) {}

    bool operator()(uint32_t i, uint32_t j) {
        real ip, jp;
        ip = spheres.c[axis][i];
        jp = spheres.c[axis][j];
        return ip < jp;
    }

    const Spheres& spheres;
    uint32_t axis;
};

void mkOBVH(const Spheres& sphs, const real* hs, BVH& tree) {
    //reference spheres
    std::vector<uint32_t> refs;
    for(uint32_t i = 0; i < sphs.count; ++i)
        refs.push_back(i);
    
    //allocate memory
    tree.spheres.allocate(sphs.count);

    //comparison object using references
    Comparator com(sphs); 

    //construct SAH BVH
    std::stack<Item> stack;
   
    //insert root item
    Item rtitem;
    rtitem.off = 0; rtitem.run = sphs.count;
    rtitem.right = false; rtitem.root = true; rtitem.parent = 0;
    stack.push(rtitem);

    //build all items
    while(!stack.empty()) {
        //get build item
        Item item = stack.top(); 
        stack.pop();

        //create node
        uint32_t inode = (uint32_t)tree.nodes.size();
        BVHNode node;

        //calculate bounding box
        for(uint32_t inc = 0; inc < item.run; ++inc) {
            uint32_t idx = item.off + inc;
            uint32_t ref = refs[idx];
            
            //expand
            node.min[0] = MIN(node.min[0], sphs.c[0][ref]-hs[ref]);
            node.min[1] = MIN(node.min[1], sphs.c[1][ref]-hs[ref]);
            node.min[2] = MIN(node.min[2], sphs.c[2][ref]-hs[ref]);

            node.max[0] = MAX(node.max[0], sphs.c[0][ref]+hs[ref]);
            node.max[1] = MAX(node.max[1], sphs.c[1][ref]+hs[ref]);
            node.max[2] = MAX(node.max[2], sphs.c[2][ref]+hs[ref]);
        }

        //set parent indices
        if(!item.root) {
            if(item.right)
                tree.nodes[item.parent].right = inode;
        }

        //leaf generation
        if(item.run == LEAFSIZE) {
            node.setLeaf();

            //create leaf
            node.offset = item.off;

            //copy data in required order
            for(uint32_t i = 0; i < item.run; ++i) {
                uint32_t idx = item.off + i;
                uint32_t ref = refs[idx];

                //reorder copy
                tree.spheres.c[0][idx] = sphs.c[0][ref];
                tree.spheres.c[1][idx] = sphs.c[1][ref];
                tree.spheres.c[2][idx] = sphs.c[2][ref];
                tree.spheres.invh2[idx] = sphs.invh2[ref];
            }
        }
        else if(item.run < LEAFSIZE) {
            std::cout << "Uneven split, not power of 2!!!" << std::endl;
            assert(false);
        }
        //split item
        else {
            //partition list, object median
            real d[3];
            d[0] = node.max[0] - node.min[0];
            d[1] = node.max[1] - node.min[1];
            d[2] = node.max[2] - node.min[2];

            //sort objects in longest axis
            if((d[0] >= d[1]) && (d[0] >= d[2])) {
                com.axis = 0;
            }
            else if((d[1] >= d[0]) && (d[1] >= d[2])) {
                com.axis = 1;
            }
            else {
                com.axis = 2;
            }
           
            //sort node objects
            std::sort(refs.begin()+item.off, 
                      refs.begin()+item.off+item.run,
                      com); 

            //split on median object
            uint32_t k = item.off + item.run/2;
            
            //create child items
            Item litem, ritem;
            litem.off = item.off; litem.run = item.run/2;
            ritem.off = k; ritem.run = item.run/2; 
            
            //left item
            litem.root = false;
            litem.parent = inode; 
            litem.right = false;

            //right item
            ritem.root = false;
            ritem.parent = inode; 
            ritem.right = true;

            stack.push(ritem); stack.push(litem);
        }

        //write node
        tree.nodes.push_back(node);
    }

    //copy mapping
    for(size_t i = 0; i < sphs.count; ++i) {
        tree.map.push_back(refs[i]);
    }
}
