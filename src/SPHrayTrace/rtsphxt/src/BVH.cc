#include "BVH.h"

#include <cmath>

bool intersect(const BVHNode& node, const Ray& ray) {
    float t0, t1;
    float tymin, tymax, tzmin, tzmax;

    if(ray.v[0] >= 0) {
        t0 = (node.min[0] - ray.o[0]) * ray.v[0];
        t1 = (node.max[0] - ray.o[0]) * ray.v[0];
    }
    else {
        t0 = (node.max[0] - ray.o[0]) * ray.v[0];
        t1 = (node.min[0] - ray.o[0]) * ray.v[0];
    }

    if(ray.v[1] >= 0) {
        tymin = (node.min[1] - ray.o[1]) * ray.v[1];
        tymax = (node.max[1] - ray.o[1]) * ray.v[1];
    }
    else {
        tymin = (node.max[1] - ray.o[1]) * ray.v[1];
        tymax = (node.min[1] - ray.o[1]) * ray.v[1];
    }

    //early exit 1
    if((t0 > tymax) || (tymin > t1))
        return false;

    t0 = MAX(t0, tymin);
    t1 = MIN(t1, tymax);

    if(ray.v[2] >= 0) {
        tzmin = (node.min[2] - ray.o[2]) * ray.v[2];
        tzmax = (node.max[2] - ray.o[2]) * ray.v[2];
    }
    else {
        tzmin = (node.max[2] - ray.o[2]) * ray.v[2];
        tzmax = (node.min[2] - ray.o[2]) * ray.v[2];
    }
    
    //early exit 2
    if((t0 > tzmax) || (tzmin > t1))
        return false;

    t0 = MAX(t0, tzmin);
    t1 = MIN(t1, tzmax);

    //ray limits
    return (t0 <= ray.tmax) && (0.0 <= t1);
}

void BVH::weightRay(const Ray& ray, const size_t dim, const real* qs, real* weight) const {
    size_t count = spheres.count;

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //push root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(intersect(node, ray)) {
            //process internal
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }

            //process leaf
            else {
                //for every sphere in leaf
                for(size_t inc = 0; inc < LEAFSIZE; ++inc) {
                    size_t isph = node.offset + inc;
                     
                    //geometric weight
                    real nb2; spheres.intersect(isph, ray, nb2);
                    real k2; kernelnb2(nb2, k2);
                    real gweight = k2 * spheres.invh2[isph];

                    //combine geometry with physics
                    for(size_t idim = 0; idim < dim; ++idim) {
                        //block indexed
                        size_t iweight = isph + idim * count;

                        weight[idim] += qs[iweight] * gweight;
                    }
                }
            }
        }
    }
}

void BVH::weightRay(const Ray& ray, real* weight) const {
    //stack for tree traversal
    std::stack<uint32_t> stack;
    //push root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(intersect(node, ray)) {
            //process internal
            if(node.isInternal()) {
                //push right node
                stack.push(node.right);
                //push left node
                stack.push(inode+1);
            }
            //process leaf
            else {
                //for every sphere in leaf
                for(size_t inc = 0; inc < LEAFSIZE; ++inc) {
                    size_t isph = node.offset + inc;
        
                    //geometric weight
                    real nb2, k2;
                    spheres.intersect(isph, ray, nb2);

                    if((0.0f <= nb2) && (nb2 < 1.0f)) {
                        kernelnb2(nb2, k2);
                    
                        //combine geometry with physics
                        real gweight = k2 * spheres.invh2[isph];

                        *weight += gweight;
                    }
                }
            }
        }
    }
}

/*void BVH::weightPacket(const Packet& packet, const size_t dim, const real* qs, real weights[PACKETSIZE]) const {

}

void BVH::weightPacket(const Packet& packet, real weight[PACKETSIZE]) const {
    //stack for tree traversal
    std::stack<uint32_t> stack;
    //push root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(packet)) {
            //process internal
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }

            //process leaf
            else {
                //for every sphere in leaf
                for(uint32_t inc = 0; inc < node.run; ++inc) {
                    uint32_t isph = node.offset + inc;
        
                    //geometric weight
                    doublew nb2[PACKETWIDTH];
                    spheres.intersect(isph, packet, nb2);

                    doublew k2[PACKETWIDTH];
                    kernelnb2(nb2, k2);

                    //double geomweight = k2 * spheres.invh2[isph];
                    doublew gweight[PACKETWIDTH];
                    doublew invh2 = set1pd(spheres.invh2[isph]);
                    for(size_t i = 0; i < PACKETWIDTH; ++i) {
                        gweight[i] = k2[i] * invh2;
                    }

                    //combine geometry with physics
                    //(*weight) += geomweight;
                    for(size_t i = 0; i < PACKETWIDTH; ++i) {
                        double extract[4]; 
                        storeupd(extract, gweight[i]);

                        weight[i*4+0] += extract[0]; 
                        weight[i*4+1] += extract[1];
                        weight[i*4+2] += extract[2];
                        weight[i*4+3] += extract[3];
                    }
                }
            }
        }
    }
}*/

/*void BVH::weightRay(const Ray& ray, double& rayweight) const {
    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        //XXX node memory fetch before intersect calculation
        if(node.bbox.intersect(ray)) {
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
            else {
                double nodeweight = 0.0; 
                weightNode(ray, inode, nodeweight);

                rayweight += nodeweight;
            }
        }
    }
}*/

/*void BVH::weightNode(const Ray& ray, size_t inode, const double* qs) const {
    double nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        double nb2 = spheres.intersect(isph, ray);

        //weight
        nodeweight += kernelnb2(nb2)*spheres.invh2[isph]*qs[isph];
    }

    return nodeweight;
}

void BVH::weightNode(const Ray& ray, const size_t inode, double& nodeweight) const {
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        double nb2 = spheres.intersect(isph, ray);

        //value checks
        assert(0.0 <= nb2);
        assert(nb2 <= 1.0);

        //weight
        nodeweight += kernelnb2(nb2) * spheres.invh2[isph];
    }
}*/

/*bool BVH::makeRays(const size_t inode, const float target[3], Ray* rays) const {
    //check inode in range
    assert(inode < nodes.size());

    //
    const BVHNode& node = nodes[inode];
    bool isLeaf = node.isLeaf();

    if(isLeaf) {
        for(size_t inc = 0; inc < LEAFSIZE; ++inc) {
            size_t isph = node.offset + inc;

            Ray& ray = rays[inc];

            float d[3];
            //ray origin at sphere
            ray.o[0] = spheres.cx[isph];
            ray.o[1] = spheres.cy[isph];
            ray.o[2] = spheres.cz[isph];

            //direction to target
            d[0] = target[0] - ray.o[0];
            d[1] = target[1] - ray.o[1];
            d[2] = target[2] - ray.o[2];

            //ray length
            float length2 = 0.0;
            for(size_t dim = 0; dim < 3; ++dim) {
                length2 += (d[dim]*d[dim]);
            }
            ray.tmax = sqrt(length2);

            for(size_t dim = 0; dim < 3; ++dim) {
                //normalized direction
                ray.d[dim] = d[dim] / ray.tmax;
            
                //inverse direction
                ray.v[dim] = ray.tmax / d[dim];
            }
        }
    }

    return isLeaf;
}

bool BVH::makePackets(const size_t inode, const float target[3], Packet* packets) const {
    //check inode in range
    assert(inode < nodes.size());

    //leaf check
    const BVHNode& node = nodes[inode];
    bool isLeaf = node.isLeaf();

    if(isLeaf) {
        //construct packet from node to target
        for(size_t ipac = 0; ipac < (LEAFSIZE/PACKETSIZE); ++ipac) {
            Packet& packet = packets[ipac];

            for(size_t inc = 0; inc < PACKETSIZE; ++inc) {
                size_t idx = (ipac * PACKETSIZE) + (node.offset + inc);

                Vector3 center; spheres.getCenter(idx, center);

                //direction at target
                Vector3 d = target - center;
                packet.tmax[inc] = d.length();
                double invtmax = 1.0 / packet.tmax[inc];
                packet.dx[inc] = d.x() * invtmax;
                packet.dy[inc] = d.y() * invtmax;
                packet.dz[inc] = d.z() * invtmax;

                //origin at sphere center
                packet.ox[inc] = center.x();
                packet.oy[inc] = center.y();
                packet.oz[inc] = center.z();

                //inverse directions
                packet.vdx[inc] = 1.0 / packet.dx[inc];
                packet.vdy[inc] = 1.0 / packet.dy[inc];
                packet.vdz[inc] = 1.0 / packet.dz[inc];
            }
        //}
    }

    return isLeaf;
}*/
