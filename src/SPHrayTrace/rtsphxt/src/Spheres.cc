#include "Spheres.h"

void Spheres::intersect(size_t isph, const Ray& ray, real& nb2) const {
    //input check
    //assert(isph < count);

    //local space
    real l[3];
    l[0] = c[0][isph] - ray.o[0];
    l[1] = c[1][isph] - ray.o[1];
    l[2] = c[2][isph] - ray.o[2];
    real l2 = (l[0]*l[0]) + (l[1]*l[1]) + (l[2]*l[2]);

    //project onto direction
    real tb = (ray.d[0]*l[0]) + (ray.d[1]*l[1]) + (ray.d[2]*l[2]);
    
    //ray limits check
    if((tb<0.0f) || (ray.tmax<tb)) {
        nb2 = 1.0f;
    }
    else {
        //pythagoras
        real tb2 = tb*tb;
        real b2 = l2 - tb2;

        //square normalized impact parameter
        nb2 = b2 * invh2[isph];
        nb2 = MIN(nb2, 1.0f);
    }
}

#ifdef SIMD
void Spheres::intersect(size_t isph, const Packet& packet, doublew nb2[PACKETWIDTH]) const {
    //input check
    assert(isph < count);

    //cache
    doublew cx    = set1pd(cx[isph]);
    doublew cy    = set1pd(cy[isph]);
    doublew cz    = set1pd(cz[isph]);
    doublew invh2 = set1pd(invh2[isph]);

    for(size_t i = 0; i < PACKETWIDTH; ++i) {
        //object space
        // l = ray.o - sphere.c
        doublew lx = subpd(cx, packet.ox[i]);
        doublew ly = subpd(cy, packet.oy[i]);
        doublew lz = subpd(cz, packet.oz[i]);

        // l2 = l . l
        doublew lx2 = mulpd(lx, lx);
        doublew ly2 = mulpd(ly, ly);
        doublew lz2 = mulpd(lz, lz);
        doublew l2 = addpd(lx2, addpd(ly2, lz2));

        // tb = direction . l
        doublew dxlx = mulpd(packet.dx[i], lx);
        doublew dyly = mulpd(packet.dy[i], ly);
        doublew dzlz = mulpd(packet.dz[i], lz);
        doublew tb = addpd(dxlx, addpd(dyly, dzlz));

        //ray limit masking
        doublew zeromask = cmplepd(zero, tb);
        doublew tmaxmask = cmplepd(tb, packet.tmax[i]);
        doublew combmask = andpd(zeromask, tmaxmask);

        //pythagoras
        doublew tb2 = mulpd(tb, tb);
        doublew b2 = subpd(l2, tb2);

        //square normalized impact parameter
        nb2[i] = mulpd(b2, invh2);
        nb2[i] = minpd(nb2[i], one);

        //mask out
        nb2[i] = andpd(nb2[i], combmask);
        doublew inv = andnotpd(combmask, one);
        nb2[i] = orpd(nb2[i], inv);
    }
}
#endif
