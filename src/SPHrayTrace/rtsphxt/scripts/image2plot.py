#! /usr/bin/python

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.colors import LogNorm

fid = open('im.cds','rb')

nx = unpack('L', fid.read(8))[0]
ny = unpack('L', fid.read(8))[0]

#img = mpimg.imread("test.png")
img = np.fromfile(fid, dtype='double', count=nx*ny)
img = np.reshape(img, (nx, ny))

img = np.transpose(img)

#plt.imshow(img, norm=LogNorm(), cmap='jet', interpolation='nearest')
plt.imshow(img, norm=LogNorm(), cmap='jet')
#plt.imshow(img, cmap='jet', interpolation='nearest')
#plt.imshow(img, cmap='jet')

plt.colorbar()
plt.show()
#plt.savefig("out.png",format="png")
