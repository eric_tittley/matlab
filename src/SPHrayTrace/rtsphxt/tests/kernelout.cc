#include "Weight.h"
#include <iostream>
#include <cmath>

int main() {
    for(double nb2 = 0.0; nb2 <= 1.0; nb2 += 0.001) {
    //for(float nb = 0.0; nb <= 1.0; nb += 0.001) {
        float k2d; kernelnb2(nb2, k2d);

        std::cout << sqrt(nb2) << " ";
        std::cout << nb2 << " ";
        std::cout << k2d << std::endl;
    }
}
