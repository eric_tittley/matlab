/* Gateway function for v_disp
 *
 * MATLAB sytax: sigma=v_disp(r,v,h);
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  1999-06-03 Mature function
 *  2002-10-16 Modified for [3xN] format for r & v for new hydra
 *  2010-06-05 Made 64-bit
 *  2011-06-27 Converted MEX function to C.  Sick of Fortran's memory issues.
 */

#include <mex.h>

#include <stdio.h>
#include <string.h>
#include <math.h>

void v_disp_(double *r, double *v, double *h, int *Nparticles, int *Lmax, double *sigma,
             int *box_index, int *ll, int *elements, int *done,
             int *ll_start, int *ll_holder);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 double *r;
 double *v;
 double *h;
 double *sigma;
 double *sigma_matlab;

 int NParticlesINT;
 size_t mrows, ncols, NParticles;

 /* Argument Checks */

 /* Check for the proper number of arguments */
 if( nlhs!=1 || nrhs!=3 ) {
  mexErrMsgTxt("Syntax: sigma=v_disp(r,v,h)");
 }
 
 /* r must be of the type double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("r must be double.");
 }

 /* v must be of the type double */
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("r must be double.");
 }

 /* h must be of the type double */
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("r must be double.");
 }

 /* r must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=3 || ncols<11) {
  mexErrMsgTxt("r must be a 3xN array with N>10");
 }

 /* v must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=3 || ncols<11) {
  mexErrMsgTxt("v must be a 3xN array with N>10");
 }

 /* h must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if(mrows!=1 && ncols!=1) {
  mexErrMsgTxt("h must be a vector");
 }
 
 /* Get the number of particles */
 if((int)mrows == 1) {
  NParticles = ncols;
 } else {
  NParticles = mrows;
 }

 /* Check that r & v have the same number of elements */
 if( mxGetN(prhs[0]) != NParticles ) {
  mexErrMsgTxt("r, v, & h must the same length");
 }
 if( mxGetN(prhs[1]) != NParticles ) {
  mexErrMsgTxt("r, v, & h must the same length");
 }

 /* Finished argument checks */

 /* Assign pointers to the input */
 r = (double *)mxGetPr(prhs[0]);
 v = (double *)mxGetPr(prhs[1]);
 h = (double *)mxGetPr(prhs[2]);

 /* Allocate memory for the output */
 sigma = (double *)malloc(NParticles*sizeof(double));
 if(sigma==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for sigma.\n");
  return;
 }

 /* The maximum grid size needed is L=1/(4h_min)
  * But enforce a hard limit. */
#define LMAX 256
 double hmin=h[0];
 size_t i;
 for (i=1;i<NParticles;i++) {
  if(h[i]<hmin) {
   hmin=h[i];
  }
 }
 int Lmax = (int)ceil(1.0/(4.0*hmin));
 if(Lmax>LMAX) {
  Lmax=LMAX;
 }

 /* Allocate memory for temporary variables */
 int *box_index = (int *)malloc((size_t)3*NParticles*sizeof(int));
 if(box_index==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for box_index.\n");
  return;
 }
 int *ll        = (int *)malloc(NParticles*sizeof(int));
 if(ll==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for ll.\n");
  return;
 }
 int *elements  = (int *)malloc(NParticles*sizeof(int));
 if(elements==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for elements.\n");
  return;
 }
 int *done      = (int *)malloc(NParticles*sizeof(int));
 if(done==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for done.\n");
  return;
 }
 int *ll_start  = (int *)malloc(Lmax*Lmax*Lmax*sizeof(int));
 if(ll_start==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for ll_start.\n");
  return;
 }
 int *ll_holder  = (int *)malloc(Lmax*Lmax*Lmax*sizeof(int));
 if(ll_start==NULL) {
  printf("ERROR: v_disp: Unable to allocate memory for ll_holder.\n");
  return;
 }

 /* Call the subroutine */
 NParticlesINT = (int)NParticles;
 v_disp_(r, v, h, &NParticlesINT, &Lmax, sigma,
         box_index, ll, elements, done, ll_start, ll_holder);

 /* Create matrix for the return argument and assign a pointer.  Then
  * copy the contents and release the memory. */
 plhs[0]=mxCreateDoubleMatrix(NParticles, 1, mxREAL);
 sigma_matlab = mxGetPr(plhs[0]);
 memcpy(sigma_matlab,sigma,NParticles*sizeof(double));

 free(sigma);
 free(box_index);
 free(ll);
 free(elements);
 free(done);
 free(ll_start);
 free(ll_holder);
}
