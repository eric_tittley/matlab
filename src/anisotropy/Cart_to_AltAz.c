/*
 Converts Cartesian (x,y,z) coordinates into Altitude and Aximuth.
 Assumes (0,0,0) is the origin (duh!)
*/
#include <math.h>
#include <float.h>

#include "anisotropy.h"

int Cart_to_AltAz(
/* input */
 double x,
 double y,
 double z,
/* output */
 double *Alt, /* Have to pass pointers, or we loose the information */
 double *Az
) {
/* The Azimuth */
 if(x>DBL_EPSILON){
  if(y>=DBL_EPSILON){ *Az=atan(y/x); }
  else{ *Az=2.*M_PI+atan(y/x); }
 }
 else{
  if(fabs(x)<=DBL_EPSILON){ *Az=0.; }
  else{ *Az=M_PI+atan(y/x); }
 }
/* The Altitude */
 if(  (fabs(x)<=DBL_EPSILON)
    & (fabs(y)<=DBL_EPSILON)
    & (fabs(z)<=DBL_EPSILON)
   ) { *Alt=0.; }
 else{ *Alt=asin( z/sqrt( x*x + y*y + z*z) ); }
 return(0);
}
