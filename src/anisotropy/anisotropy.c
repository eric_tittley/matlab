#include "mex.h"

#include "anisotropy.h"

#include <stdio.h>
#include <float.h>

int anisotropy(
 double *r, /* r is 3xN data stored column after column */
 int N,
 double length,
 double *aniso /* empty matrix 12xN */
) {
 
/* return status of the main subroutine */
 int status;

/* boundaries */
 double min[3],max[3],span,spanx,spany,spanz;
 int Nnodes,Nnodes2;

/* Link list arrays */
 int *ll, *ll_start;

/* list array */
 int *elements;

/* Main loop */
 int i,j,k,m,neighbour,part;
 int MaxFacet,facet;
 int count;
 double dx,dy,dz;
 double dist;
 double MaxAngDist, AngDist;
 double Alt, Az;
 double rx,ry,rz;
#ifdef SD_HACK
 double alpha1,dec1, alpha2,dec2;
#endif

 double Direction[2][12]; /* There are 12 faces in a dodecaheron */
 /* Each facet's direction is listed as (Az, Alt) */
 Direction[0][ 0]= 0.0000;  Direction[1][ 0]= 1.5708;  
 Direction[0][ 1]= 0.0000;  Direction[1][ 1]= 0.4637;  
 Direction[0][ 2]= 1.2566;  Direction[1][ 2]= 0.4637;
 Direction[0][ 3]= 2.5133;  Direction[1][ 3]= 0.4637;
 Direction[0][ 4]= 3.7699;  Direction[1][ 4]= 0.4637;
 Direction[0][ 5]= 5.0265;  Direction[1][ 5]= 0.4637;
 Direction[0][ 6]= 0.6283;  Direction[1][ 6]=-0.4637;
 Direction[0][ 7]= 1.8850;  Direction[1][ 7]=-0.4637;
 Direction[0][ 8]= 3.1416;  Direction[1][ 8]=-0.4637;
 Direction[0][ 9]= 4.3982;  Direction[1][ 9]=-0.4637;
 Direction[0][10]= 5.6549;  Direction[1][10]=-0.4637;
 Direction[0][11]= 0.0000;  Direction[1][11]=-1.5708;

/* clear the values in aniso */
 for(i=0;i<12*N;i++) { aniso[i]=0.0; }

/* find the boundaries of the distribution of particles */
 max[0]=r[0]; max[1]=r[1]; max[2]=r[2];
 min[0]=r[0]; min[1]=r[1]; min[2]=r[2];
 for(i=1;i<N;i++) {
  if(r[3*i+0]>max[0]) {max[0]=r[3*i+0];}
  if(r[3*i+1]>max[1]) {max[1]=r[3*i+1];}
  if(r[3*i+2]>max[2]) {max[2]=r[3*i+2];}
  if(r[3*i+0]<min[0]) {min[0]=r[3*i+0];}
  if(r[3*i+1]<min[1]) {min[1]=r[3*i+1];}
  if(r[3*i+2]<min[2]) {min[2]=r[3*i+2];}
 }

/* our mesh will be have lengths span on each side */
 spanx=max[0]-min[0];
 spany=max[1]-min[1];
 spanz=max[2]-min[2];
 span=spanx; if(spany>span){span=spany;} if(spanz>span){span=spanz;}
 span=span+span/1000.; /* add just a bit so the particle at max[] doesn't seg fault */

/* number of nodes in the mesh, per side */
 Nnodes=floor(span/length);
 if(Nnodes<3){
  mexErrMsgTxt("Nnodes < 3. The scale length is larger than the sample.");
 }
 Nnodes2=Nnodes*Nnodes;

/* allocate memory for the link list arrays */
 ll=(int *) mxMalloc(sizeof(int)*N);
 if(ll==NULL){ mexErrMsgTxt("Unable to allocate memory for ll");}

 ll_start=(int *) mxMalloc(sizeof(int)*Nnodes*Nnodes*Nnodes);
 if(ll_start==NULL){
  mxFree(ll_start);
  mexErrMsgTxt("Unable to allocate memory for ll_start");
 }

/* allocate memory for the list of particles in the 27 boxes of influence */
 elements=(int *) mxMalloc(sizeof(int)*N);
 if(elements==NULL){
  mxFree(ll);
  mxFree(ll_start);
  mexErrMsgTxt("Unable to allocate memory for elements");
 }
 
#define CLEAR_MEMORY { mxFree(ll); mxFree(ll_start); mxFree(elements); }

 status=boxlist(r,N,min,Nnodes,span,0,Nnodes,ll,ll_start);
 if(status!=0){
  CLEAR_MEMORY
  mexErrMsgTxt("boxlist returned a non-zero exit code");
 }

/********************** MAIN LOOP ****************/

/* In the following, we will:
    1) move through each box
    2) find all the particles in the box and the 8 surrounding boxes
    3) for each particle in the box,
      3a) loop through all the neighbours
      3b) find those within length
      3c) find the direction of the anisotropy array closest to that of the neighbour
      3c) sum the anisotropy array for the particle
*/

/* 1 */
 for(i=0;i<Nnodes;i++){
  printf("\nX Node: %d",i); fflush(stdout);
  for(j=0;j<Nnodes;j++) {
   printf(": %d",j); fflush(stdout);
   for(k=0;k<Nnodes;k++) {
/* 2 */
    status=neighbourlist(Nnodes,i,j,k,N,ll_start,ll, elements,&count);
    if(status!=0){
     CLEAR_MEMORY
     mexErrMsgTxt("neighbourlist returned a non-zero exit code");
    }
    if(count>N){
     CLEAR_MEMORY
     mexErrMsgTxt("neighbourlist a list of more than N particles");
    }
/* 3 */
    part=ll_start[i*Nnodes2+j*Nnodes+k];
    while(part!=-1) {
     rx=r[part*3+0];
     ry=r[part*3+1];
     rz=r[part*3+2];
/* 3a */
     for(m=0;m<count;m++) {
      neighbour=elements[m];
      if(neighbour!=part){
/* 3b */
       dx=rx-r[neighbour*3+0];
       if(fabs(dx)>(2.*length)){
        if(dx<DBL_EPSILON) {dx=dx+1.;}
        else{dx=dx-1.;}
       }
       dy=ry-r[neighbour*3+1];
       if(fabs(dy)>(2.*length)){
        if(dy<DBL_EPSILON) {dy=dy+1.;}
        else{dy=dy-1.;}
       }
       dz=rz-r[neighbour*3+2];
       if(fabs(dz)>(2.*length)){
        if(dz<DBL_EPSILON) {dz=dz+1.;}
        else{dz=dz-1.;}
       }
       dist=sqrt( dx*dx + dy*dy + dz*dz );
       if(dist<length) {
        status=Cart_to_AltAz(dx,dy,dz,&Alt,&Az);
        if(status!=0){
         CLEAR_MEMORY
         mexErrMsgTxt("Cart_to_AltAz returned a non-zero exit code");
        }
/* 3c */
        MaxAngDist=M_PI;
        MaxFacet=13;
        for(facet=0;facet<12;facet++){
/* For some reason, I can't get the next line to work on Floodlands */
#ifndef SD_HACK
        AngDist=SphericalDistance(Direction[0][facet],Direction[1][facet],Az,Alt);
#else
/* start Hack */
         alpha1=Direction[0][facet];
         dec1  =Direction[1][facet];
         alpha2=Az;
         dec2  =Alt;
         AngDist=acos( cos( M_PI_2 - dec1) * cos( M_PI_2 -dec2)
                      +sin( M_PI_2 - dec1) * sin( M_PI_2 -dec2) * cos(alpha1-alpha2)
                      );
/* end of hack */
#endif
         if(AngDist<MaxAngDist) {
          MaxAngDist=AngDist;
          MaxFacet=facet;
         }
         if(MaxFacet==13) {
          CLEAR_MEMORY
          mexErrMsgTxt("MaxFacet was not set properly");
         }
        } /* Finished looping through the possible directions it could be closest to */
/* 3d */
        aniso[part*12+MaxFacet]=aniso[part*12+MaxFacet]+1.0;
       } /* end if the neighbour particle is within length */
      } /* end if the neighbour particle is NOT the particle itself */
     } /* finished looping through all the neighbour particles */
     part=ll[part];
    } /* finished looping through the link list of all the particles in the cell (i,j,k) */
   }
  }
 } /* finished looping through all the cells */

 /* We now have the anisotropy vector for each particle */
/********************** END OF MAIN LOOP ****************/

 CLEAR_MEMORY
 return(0);

}
