/*
 Finds the distance (in radian) between two points on a sphere whose
 cooridnates are given (alpha1,dec1) and (alpha2,dec2).
 with alpha=[0,2pi] and dec=[-pi/2,pi/2]
*/

#include <math.h>
double SphericalDistance(
 double alpha1,
 double dec1,
 double alpha2,
 double dec2
) {
/* M_PI_2 is defined in math.h */
return acos( cos( M_PI_2 - dec1) * cos( M_PI_2 -dec2)
             +sin( M_PI_2 - dec1) * sin( M_PI_2 -dec2) * cos(alpha1-alpha2)
            );
}
