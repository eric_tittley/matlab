/* function Dims=DistributedGridDimensions(nNodes,nDims)
*
* Splits a nDims grid into nNode cells
*
* Taken from Enzo2 (Mpich_V1_Dims_create.c which probably came from MPICH V1
*
*/

/* AUTHOR: Eric Tittley (port to Matlab) */

#include <mex.h>

int XXMPI_Dims_create(
	int nnodes, 
	int ndims, 
	int *dims);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {
 double dnNodes, dnDims;
 double *dDims;
 int nNodes, nDims;
 int *Dims;

 int j;
 int status;
 size_t mrows,ncols;
 
 /* Check for the proper number of arguments */
 if( nlhs!=1 || nrhs!=2 ) {
  mexErrMsgTxt("Dims=DistributedGridDimensions(nNodes,nDims)");
 }

 /* The inputs must be of the type double */
 if(   !mxIsDouble(prhs[0])
    || !mxIsDouble(prhs[1])
   ) {
  mexErrMsgTxt("all arguments must be double");
 }

 /* The arguments must be scalar */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("nNodes must be scalar");
 }
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("nDims must be scalar");
 }

 /* Assign pointers to the input */
 dnNodes = *(mxGetPr(prhs[0]));
 dnDims  = *(mxGetPr(prhs[1]));
 
 /* Convert to integer */
 nNodes = (int)dnNodes;
 nDims  = (int)dnDims;
 
 Dims=(int *)calloc(nDims,sizeof(int));

/* the MAIN SUBROUTINE */
 status = XXMPI_Dims_create(nNodes,nDims,Dims);
 if(status!=0) { mexErrMsgTxt("fof returned non-zero status"); }

 /* Create matrix for the return arguments and assign a pointer */
 plhs[0] = mxCreateDoubleMatrix(1,nDims, mxREAL);
 dDims   = mxGetPr(plhs[0]);

 for (j=0;j<nDims;j++) { dDims[j] = (double)Dims[j]; }

 free(Dims);

}
