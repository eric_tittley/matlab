% Dims=DistributedGridDimensions(nNodes,nDims)
%
% Calculates the grid dimensions for a nDims grid split into nNode cells
%
% ARGUMENTS
%
%  nNodes  The number of grids
%
%  nDims   The number of dimensions
%
% OUTPUT
%
%  Dims    The number of grids in each dimension
%
% NOTES
%  Uses the algorithm in MPICH V1 for MPI_Dims_create
