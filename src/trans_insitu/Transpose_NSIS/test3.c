#include <stdlib.h>
#include <stdio.h>

#include "Transpose_NSIS.h"

int main(void) {
 const unsigned int N=12;
 double A[] = {11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43};
 double B[] = {11, 21, 31, 41, 12, 22, 32, 42, 13, 23, 33, 43};
 unsigned int i;
 
 printf("Before: ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 
 Transpose_NSIS_ViaFile_d(A, 3, 4);
 
 printf("After : ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 printf("Expect: ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",B[i]);
 }
 printf("\n");
 
 return EXIT_SUCCESS;
}
