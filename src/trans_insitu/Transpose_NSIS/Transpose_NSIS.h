#ifndef _TRANSPOSE_NSIS_H_
#define _TRANSPOSE_NSIS_H_

#include "TNSIS_LinkList.h"

unsigned int SwapWith(const unsigned int index, const unsigned int N,
                      const unsigned int M);

LL_node * LL_Search_a(unsigned int j, LL_node * node);
LL_node * LL_Search_b(unsigned int j, LL_node * node);
LL_node * CheckIfMoved( unsigned int *j, LL_node * node);
LL_node * LL_Compact(LL_node * node);

void Transpose_NSIS_d(double * A, const int N, const int M);
void Transpose_NSIS_f(float  * A, const int N, const int M);
void Transpose_NSIS_ViaFile_d(double * A, const int N, const int M);
void Transpose_NSIS_ViaFile_f(float  * A, const int N, const int M);


#endif
