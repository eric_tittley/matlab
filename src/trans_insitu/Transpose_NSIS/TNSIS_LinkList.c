#include <stdlib.h>
#include <stdio.h>

#include "TNSIS_LinkList.h"

LL_node * LL_Insert( LL_node * const node, const int a, const int b ) {
 LL_node * new_node = (LL_node *) malloc(sizeof(LL_node));
 new_node->a = a;
 new_node->b = b;
 if(node != NULL) {
  if(node->prev != NULL) {
   /* Insert between two nodes */
   new_node->next = node;
   new_node->prev = node->prev;
   node->prev = new_node;
   (new_node->prev)->next = new_node;
  } else {
   /* No previous node, so append */
   new_node->next = node;
   new_node->prev = NULL;
   node->prev = new_node;
  }
 } else {
  /* Only node in list */
  new_node->prev = NULL;
  new_node->next = NULL;
 }
 return new_node;
}

LL_node * LL_Append( LL_node * node, const int a, const int b ) {
 LL_node * new_node = (LL_node *) malloc(sizeof(LL_node));
 new_node->a = a;
 new_node->b = b;
 node = LL_Start(node);
 new_node->prev = NULL;
 new_node->next = node;
 return new_node;
}

LL_node * LL_Prepend( LL_node * node, const int a, const int b ) {
 LL_node * new_node = (LL_node *) malloc(sizeof(LL_node));
 new_node->a = a;
 new_node->b = b;
 node = LL_End(node);
 new_node->prev = node;
 new_node->next = NULL;
 return new_node;
}

LL_node * LL_Remove( LL_node * node ) {
 LL_node * remember;
 if(node != NULL ) {
  if(node->next != NULL) {
   remember=node->next;
  } else {
   remember=node->prev;
  }
  if( (node->prev != NULL) && (node->next != NULL) ) {
   node->prev->next = node->next;
   node->next->prev = node->prev;
  } else {
   if( (node->prev == NULL) && (node->next !=NULL) ) node->next->prev = NULL;
   if( (node->next == NULL) && (node->prev !=NULL) ) node->prev->next = NULL;
  }
  free(node);
 } else {
  return NULL ;
 }
 return remember;
}

LL_node * LL_End( LL_node * node ) {
 if(node != NULL) {
  while(node->next != NULL) node=node->next;
 }
 return node;
}

LL_node * LL_Start( LL_node * node ) {
 if(node != NULL) {
  while(node->prev != NULL) node=node->prev;
 }
 return node;
}

void LL_Print( LL_node * node ) {
 if( node != NULL ) {
  while(node != NULL) {
   printf("(%i,%i) ",node->a, node->b);
   node = node->next;
  }
 } else {
  printf("NULL ");
 }
}
