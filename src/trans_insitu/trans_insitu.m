% transpose: Transpose an array in-situ.
%
% function A=trans_insitu(A)
%
% Transpose an array in-situ.
% If the array is square, no extra memory is required.
% If the array is not square, extra memory is required, but it is kept to a
% minimum.
%
% ARGUMENTS
%  Input and Modified
%   A  Array to transpose
%
% RETURNS
%   A  The same array
%
% SEE ALSO
%  ' operator, transpose
