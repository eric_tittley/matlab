/* function A=trans_insitu(A)
 *
 * Transpose an array in-situ.
 * If the array is square, no extra memory is required.
 * If the array is not square, extra memory is required, but it is kept to a
 * minimum.
 *
 * ARGUMENTS
 *  Input and Modified
 *   A  Array to transpose
 *
 * RETURNS
 *   A  The same array
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  08 10 20 First version
 *
 * TODO
 *  Deal with the easier case of a square matrix separately.
 *  Deal with doubles and floats separately.
 */

#include <stdlib.h>
#include <stdio.h>

#include "mex.h"

#include "Transpose_NSIS/Transpose_NSIS.h"

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[] ) {

 double *Ad=NULL;
 float  *Af=NULL;
 size_t mrows,ncols;
 /* int i; */

 /* Check for the proper number of arguments */
 if( (nrhs!=1) || (nlhs!=1) ) mexErrMsgTxt("Syntax: A=transpose(A)");

 /* The mandatory inputs must be of the type double */
 if( (!mxIsDouble(prhs[0])) && (!mxIsSingle(prhs[0])) ) {
  mexErrMsgTxt("Argument must be double");
 }
 
 /* Get pointer to A */
 if(mxIsDouble(prhs[0])) {
  Ad = (double *)mxGetPr(prhs[0]);
 } else {
  Af = (float  *)mxGetPr(prhs[0]);
 }

 /* Get size of A */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);

 /*
 printf("Before: ");
 for(i=0;i<(mrows*ncols);i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 */

 /* Perform the transpose */
 if( (mrows*ncols) < 1009 ) { /* 1009 is prime */
  if(mxIsDouble(prhs[0])) {
   Transpose_NSIS_d(Ad, (int)mrows, (int)ncols);
  } else {
   Transpose_NSIS_f(Af, (int)mrows, (int)ncols);
  }
 } else {
  if(mxIsDouble(prhs[0])) {
   Transpose_NSIS_ViaFile_d(Ad, (int)mrows, (int)ncols);
  } else {
   Transpose_NSIS_ViaFile_f(Af, (int)mrows, (int)ncols);
  }
 }

 /*
 printf("After : ");
 for(i=0;i<(mrows*ncols);i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 */

 /* Assign the input to the output */
 plhs[0] = prhs[0];
 mxSetM(plhs[0],ncols);
 mxSetN(plhs[0],mrows);

}
