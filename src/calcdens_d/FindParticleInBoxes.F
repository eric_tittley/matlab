      SUBROUTINE FindParticleInBoxes(N,Ldim,L,ll,ll_start,particle)
      integer*8 N
      integer*8 Ldim
      integer*8 L
      integer*8 particle
      integer ll(N)
      integer ll_start(Ldim,Ldim,Ldim)
      
      integer*8 L1,L2,L3,i
      
      do L3=1,L
       do L2=1,L
        do L1=1,L
         i=int(ll_start(L1,L2,L3),8)
         do while(i.ne.0)
          if(i.eq.particle) then
           print *,'particle ',particle,' is in cell',L1,L2,L3
          endif
          i=ll(i)
         enddo
        enddo
       enddo
      enddo
      
      END
