      FUNCTION Vol(x,rL)
      IMPLICIT NONE
      
      real*8 Vol
      real*8 x,rL
c Local variable
      real*8 pi,rL2,x1,x2,x3,ah,xc,rt
            
      parameter(pi=3.14159265358979323846d0)
      Vol=0.0d0
      if(x.gt.0.0d0)then
       rL2=rL/2.0d0
       x1=rL2
       x2=rL2*dsqrt(2.0d0)
       x3=rL2*dsqrt(3.0d0)
       ah=rL2/x
       xc=x**3
       Vol=4.0d0*pi*xc/3.0d0
       if(x.gt.x1 .and. x.lt.x2)then
          Vol=2.0d0*pi*xc*(ah*(3.0d0-ah*ah)-4.0d0/3.0d0)
       else if(x.eq.x2)then
          Vol=2.0d0*pi*xc*(5.0d0/2.0d0/dsqrt(2.0d0)-4.0d0/3.0d0)
       else if(x.gt.x2 .and. x.lt.x3)then
        rt=dsqrt(1.0d0-2.0d0*ah*ah)
        Vol=(8.0d0/3.0d0)*xc*( 2.0d0*datan(rt)
     &                  -2.0d0*datan(ah*ah/rt)
     &                  +3.0d0*ah*ah*rt
     &  +ah*(3.0d0-ah*ah)*(pi/4.0d0+datan(ah/rt)-2.0d0*datan(rt/ah)) )
       else if(x.ge.x3)then
          Vol=rL**3
       end if
      end if
      RETURN
      END
