      SUBROUTINE list(N,r,ll,Ls,ihc,rca,rcb)
C Lists the particles in r into the mesh boxes.
C The size of the mesh is given by Ls
C The list is returned in ll.
C ihc stores what? The first particle in the list for each box?
      IMPLICIT NONE
c Arguments input, not modified
      integer N,Ls
      real*8 r(3,N),rca,rcb
c Agruments output, modified
      integer ihc(Ls**3),ll(N)
c Local Variables
      integer Ls2,Ls3,i,ix,iy,iz,ibox,inst,ins,insp
      real*8 rx,ry,rz,r3i

      Ls3=Ls**3

      do i=1,Ls3
       ihc(i)=0
      enddo

      do i=1,N
       ll(i)=0
      enddo

      Ls2=Ls**2

      do i=1,N
       rx=rca*r(1,i)+rcb
       ry=rca*r(2,i)+rcb
       r3i=r(3,i)
       rz=rca* r3i + rcb
       ix= int(rx)
       iy= int(ry)
       iz= int(rz)
       ibox=ix+Ls*(iy-1)+Ls2*(iz-1)

       inst=ihc(ibox)
       ins=inst
       insp=inst

 222   if(ins.gt.0)then
        if(r3i.lt.r(ins,3))then
         insp=ins
         ins=ll(ins)
         goto 222
        end if
       end if

       ll(i)=ins
       if(ins.eq.inst)then
        ihc(ibox)=i
       else
        ll(insp)=i
       end if

      enddo

      return
      end
