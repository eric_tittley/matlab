% syntax: smooth=SmoothOverRadius(Image,radius);
%
% Given an Image (MxN array), the image is smoothed over the
% radii given in the MxN array, radius.  That is, the value
% for smooth(i,j)=mean( pixels of Image within radius(i,j) of (i,j) )
%
% See also: smoothimage
