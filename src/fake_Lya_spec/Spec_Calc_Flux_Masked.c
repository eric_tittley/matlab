/* Calculate the optical depth for a line of sight at a variety of velocities.
 * Normalize the scaling of tau by requiring a mean tau of tau_eff.
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_los_cells        Number of cells in the skewer
 *   N_spec_cells       Number of cells in the output spectrum
 *   v          Velocity in each cell of the skewer (Hubble + peculiar)
 *              (km/s)
 *   nH1        Column density of HI in each cell (m^-2).
 *              Usually this is the number density * dx, since this is an
 *              integral.
 *   T          Temperature in each cell of the skewer (K)
 *   mask_in    Mask to propogate through the integration [int 0 or 1]
 *   kms        Velocity of the flux bin (km/s)
 *   tau_eff    The mean optical depth in each velocity bin (wavelength) by
 *              which to normalise the transmission spectrum.
 *              If tau_eff < 0, it is ignored as is A.
 *   psi_method Line profile shape.  0=> Doppler, 1=>Voigt.
 *   species    Atomic species. 0=> HI, 2=>HeII
 *  Input, modified
 *   A          Depends on tau_eff.
 *              if tau_eff>0
 *               The initial upper bracket of the normalization factor.  Modified to
 *               the normalization factor found on exit.
 *              else
 *               The normalization to use. Not modified. Set to 1 if no normalization. 
 *  Input, initialized
 *   flux       Transmission factor at that velocity
 *   mask_out   Contributions of the mask_in to each flux point.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if
 *                The normalization coefficient fails to fall within the
 *                initial bracket.
 *                The choice of psi is unsupported.
 *
 * AUTHOR: Eric Tittley based on code by Martin White
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "psi_doppler.h"
#include "voigt/voigt.h"

#include "species.h"

//#define DEBUG
//#define VERBOSE

/* SAFE_SUM is probably not thread-safe */
//#define SAFE_SUM

double SCF_SafeSum(double *Array, const size_t count);

int Spec_Calc_Flux_Masked(const size_t * const N_los_cells,
                   const size_t * const N_spec_cells,
                   const double * const v,
                   const double * const nH1,
                   const double * const T,
                   const int * const mask_in,
                   const double * const kms,
                   double * const flux,
                   double * const mask_out,
                   const double * const tau_eff, 
                   double * const A,
                   enum psi_Methods *psi_Method,
                   enum Species *species) {
 /* The initial brackets for the normalization factor */
 const double A_LO=0.0;
 const double A_HI=*A;
 /* Constants */
 const double m_p = 1.6726217e-27;  /* proton mass (kg) */
 /* Precalculate C
  * C = pi^0.5 * e^2 * f_Lya / (m_e * nu_Lya)
  *  f_Lya = 0.4162 (Allen's Astrophysical Constants)
  *  nu_Lya = 2.466068e15 Hz
  *  e = 4.8032068e-19 C [ C^2 has units kg m^3 s^-2 ]
  *  m_e = 9.1093897e-31 kg
  * C has units m^3 s^-1
  * Actually, you need to do this in CGS units, then convert to MKS since
  * the original formulas were CGS.  Otherwise you get 7.6e-23.
  * In CGS, C = 7.576092e-8 cm^3 s^-1 */
 /* const double C = 7.576092e-14 * 1.0e-3; */ /* m^3 s^-1 * km/m */
 /* Su (Spitzer p.36 to 38)
  * Su = pi * e^2 * f_Lya / (m_e * c) */
 const double Su_f = 2.6540093705e-06; /* Su/f_Lya  m^2 s^-1 */
 double Su;
 const double TOL = 0.01; /* Units of tau */
  /* Used to prevent underflows in exp(-tau); MaxTau must <= 709 but can be
   * much less in practice.  40 is conservative.  25 is more appropriate. 
   * Note: the limit of 709 is for the normal doubles.  For subnormal doubles
   * it is 745, but they will still produce underflows when you add them to
   * normals. */
 const double MaxTau = 40.;
 const int MAX_ITER = 100; /* Maximum number of iterations */

 /* Repeated, for convenience */
 /* enum Species { HI_Lya, HeI, HeII_Lya, HI_Lyb, HI_Lyg, HeII_Lyb, HeII_Lyg}; */

 /* The Oscillator Strengths, matching the enumerated Species */
 const double f[] = {0.41620, 0., 0.41620, 0.07910, 0.02899, 0.07910, 0.02899};
 
 /* The line-centre rest-frame frequencies */
 /* const double nu_Lya = 2.466068e15; */ /* For HI Hz */
 /* For HeII: nu_Lyb = c/256.317*10^10
  *           nu_Lyg = c/256.317*10^10 */
 const double nus[] = {2.466068e15, 0, 4*2.466068e15, 2.9240e+15, 3.0839e+15,
                     4.0*2.9240e+15, 4.0*3.0839e+15};

 int ix,iz,ierr;
 int nn=0;
 /* Alo, Ahi, Amid are brackets for the normalization factor which has
  * units cm^2 km s^-1 */
 double Alo = A_LO;
 double Ahi = *A;
 double Amid; /* Guess at the normalization factor */
 double tau_error;
 double tau_error_lo = 0.0 - *tau_eff;
 double trans;
 double u,tau,sig;
 double nu,mass;
#ifdef SAFE_SUM
 double *Taus;
#endif
 
 double mask_sum, mask_norm;

 /* Get the correct parameters for the species.
  * Also provides a check that the species is valid. */
 switch(*species) {
  case HI_Lya:
   mass = m_p; /* kg */
   break;
  case HI_Lyb:
   mass = m_p; /* kg */
   break;
  case HI_Lyg:
   mass = m_p; /* kg */
   break;
  case HeII_Lya:
   mass = 4*m_p; /* kg */
   break;
  case HeII_Lyb:
   mass = 4*m_p; /* kg */
   break;
  case HeII_Lyg:
   mass = 4*m_p; /* kg */
   break;
  default:
   printf("ERROR: %s: %i: Invalid species: %i\n",__FILE__,__LINE__,*species);
   return EXIT_FAILURE;
 }
 /* Get the rest-frame line-centre frequency */
 nu=nus[*species]; /* Hz */

 /* Set Su, which incorporates the cross-section constants */
 Su = Su_f * f[*species];

#ifdef SAFE_SUM
 Taus=malloc(*N_los_cells*sizeof(double));
#endif
 if(*tau_eff <= 0) { /* Ignore tau_eff */
  trans = 0.;
 #pragma omp parallel for \
  default(none) \
  private(ix,u,tau,iz,sig,mask_sum,mask_norm) \
  shared(psi_Method,nu,mass,Su,N_spec_cells,kms,T,v,nH1,mask_in,N_los_cells,A,flux,mask_out)
  for(ix=0;ix<*N_spec_cells;ix++) {
   u = kms[ix]; /* km/s */
   tau = 0.;
   mask_sum = 0.;
   mask_norm = 0.;
   switch(*psi_Method) {
    case Doppler:
     for(iz=0;iz<*N_los_cells;iz++) {
      sig = psi_doppler(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
     #ifdef SAFE_SUM
      Taus[iz]=sig * nH1[iz];
     #else
      tau += sig * nH1[iz]; /* s m^-2 */
     #endif
      mask_sum  += sig * mask_in[iz];
      mask_norm += sig;
     }
     break;
    case Voigt:
     for(iz=0;iz<*N_los_cells;iz++) {
      sig = psi_voigt(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
     #ifdef DEBUG
      if(!isfinite(sig)) {
       printf("WARNING: %s: %i: sig=%5.3e; u=%5.3e; v[%i]=%5.3e; nu=%5.3e; T=%5.3e; mass=%5.3e\n",
              __FILE__,__LINE__,sig,u,iz,v[iz],nu,T[iz],mass);
       break;
      }
     #endif
     #ifdef SAFE_SUM
      Taus[iz]=sig * nH1[iz];
     #else
      tau += sig * nH1[iz]; /* s m^-2 */
     #endif
     #ifdef DEBUG
      if(!isfinite(tau)) {
       printf("WARNING: %s: %i: tau=%5.3e; sig=%5.3e; nH1[%i]=%5.3e; N_los_cells=%i\n",
              __FILE__,__LINE__,tau,sig,iz,nH1[iz],(int)(*N_los_cells));
       break;
      }
     #endif
      mask_sum  += sig * mask_in[iz];
      mask_norm += sig;
     }
     break;
    default:
     printf("WARNING: %s: Invalid psi form: %i\n",__FILE__,*psi_Method);
     printf("         Using Doppler\n");
     for(iz=0;iz<*N_los_cells;iz++) {
      sig = psi_doppler(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
     #ifdef SAFE_SUM
      Taus[iz]=sig * nH1[iz];
     #else
      tau += sig * nH1[iz]; /* s m^-2 */
     #endif
      mask_sum  += sig * mask_in[iz];
      mask_norm += sig;
     }
   }
  #ifdef SAFE_SUM
   tau=SCF_SafeSum(Taus,*N_los_cells);
  #endif
   tau = (*A) * Su * tau; /* Scale by the absorption cross section. */
   if(tau<MaxTau) {
    flux[ix] = exp(-tau); /* Normalised flux. */
   } else {
    flux[ix] = 0.0;
   }
   if(mask_norm > 0.) {
    mask_out[ix] = mask_sum / mask_norm;
   } else {
    mask_out[ix] = 1.;
   }
  }
 } else { /* We are using tau_eff. */
  /* The following uses the bisection method to determine the amplitude A such
   * that the mean optical depth is tau_eff.
   * Amid is the guess at A. */
  do {
   nn++;
   Amid = (Ahi+Alo)/2.;
   trans = 0.;
  #pragma omp parallel for \
   default(none) \
   private(ix,u,tau,iz,sig,mask_sum,mask_norm) \
   shared(psi_Method,nu,mass,Amid,Su,N_spec_cells,kms,T,v,nH1,mask_in,N_los_cells,flux,mask_out) \
   reduction(+:trans)
   for(ix=0;ix<*N_spec_cells;ix++) {
    u = kms[ix]; /* km/s */
    tau = 0.;
    mask_sum = 0.;
    mask_norm = 0.;
    switch(*psi_Method) {
     case Doppler:
      for(iz=0;iz<*N_los_cells;iz++) {
       sig = psi_doppler(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
       tau += sig * nH1[iz]; /* s m^-2 */
       mask_sum  += sig * mask_in[iz];
       mask_norm += sig;
      }
      break;
     case Voigt:
      for(iz=0;iz<*N_los_cells;iz++) {
       sig = psi_voigt(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
       tau += sig * nH1[iz]; /* s m^-2 */
       mask_sum  += sig * mask_in[iz];
       mask_norm += sig;
      }
      break;
     default:
      printf("WARNING: %s: Invalid psi form: %i\n",__FILE__,*psi_Method);
      printf("         Using Doppler\n");
      for(iz=0;iz<*N_los_cells;iz++) {
       sig = psi_doppler(u-v[iz],nu,T[iz],mass); /* dpsi [Hz^-1] */
       tau += sig * nH1[iz]; /* s m^-2 */
       mask_sum  += sig * mask_in[iz];
       mask_norm += sig;
      }      
    }
    tau = Amid * Su * tau; /* Normalise tau */
    if(tau<MaxTau) {
   #ifndef NOFLUX
     flux[ix] = exp(-tau); /* Normalised flux. */
     trans += flux[ix]; /* Accumulate */
    } else {
     flux[ix] = 0.0;
   #else
     trans += exp(-tau); /* Accumulate */
   #endif
    }
    if(mask_norm > 0.) {
     mask_out[ix] = mask_sum / mask_norm;
    } else {
     mask_out[ix] = 1.;
    }
   } /* Loop over ix, the counter for the velocity bins */
   trans = trans/(double)(*N_spec_cells); /* Take the average */
   /* Should have an assert(trans > 0) here */
   if(trans < 0.0 || trans > 1.0 ) {
    printf("ERROR: %s: %i: require 0 < trans < 1: trans=%f\n",
           __FILE__,__LINE__,trans);
    fflush(stdout);
    return EXIT_FAILURE;
   }
   tau_error = -log(trans) / *tau_eff - 1.0;
   if (tau_error*tau_error_lo>0.) {
    Alo = Amid;
    tau_error_lo = tau_error;
   } else {
    Ahi = Amid;
   }
  #ifdef DEBUG
   printf("%s: nn=%i; A=%5.2e; tau error=%5.2e;\n",
          __FILE__,nn,Amid,tau_error);
   fflush(stdout);
  #endif
   if(!isfinite(Amid)) {
    printf("ERROR: %s: %i: A=%f\n",__FILE__,__LINE__,Amid);
    return EXIT_FAILURE;
   }
   if(!isfinite(tau_error)) {
    printf("ERROR: %s: %i: tau_error=%f\n",__FILE__,__LINE__,tau_error);
    return EXIT_FAILURE;
   }
   /* Three conditions to terminate:
    *  1) Exceeded the maximum number of iterations
    *  2) We've achieved the error we want
    *  3) Abutting the upper limit */
  } while( (nn<MAX_ITER) && (fabs(tau_error)>TOL) && (Amid < 0.99*A_HI) );

  /* Check to see if a solution was found.  If no solution, decide what to do
   * next.  Otherwise.  YAY!
   * There are 4 conditions which will prevent a solution from being found
   *  0) There is no solution.  Sorta fucked if that happens.
   *  1) Minimum falls within bounds, but error is everywhere greater than TOL
   *  2) Minimum falls within bounds and exists with error < TOL, but we have reached
   *     nn==MAX_ITER because the bounds were too large to begin with.
   *  3) Minimum falls above the upper bound
   *  4) Minimum falls below the lower bound
   * Currently I can only handle the last two cases, of which the last is unphysical.
   */   
  if( fabs(tau_error)>TOL ) { /* Solution not found */
  #ifdef VERBOSE
   printf("WARNING: %s: Failed to converge within bracket.\n",__FILE__);
   printf("                         A_LO=%5.2e; A_HI=%5.2e; Amid=%5.2e;\n",
          A_LO,A_HI,Amid);
   fflush(stdout);
   if(!isfinite(A_LO)) {
    printf("ERROR: %s: %i: A_LO=%f\n",__FILE__,__LINE__,A_LO);
    fflush(stdout);
    return EXIT_FAILURE;
   }
   if(!isfinite(A_HI)) {
    printf("ERROR: %s: %i: A_HI=%f\n",__FILE__,__LINE__,A_HI);
    fflush(stdout);
    return EXIT_FAILURE;
   }
   if(!isfinite(Amid)) {
    printf("ERROR: %s: %i: Amid=%f\n",__FILE__,__LINE__,Amid);
    fflush(stdout);
    return EXIT_FAILURE;
   }
  #endif
   /* The following assumes A_LO==0 */
   if( Amid < A_HI/2.0 ) {
    /* A started too high */
    *A = Ahi*2.0;
   } else {
    /* we started with A too small */
    *A = A_HI * 10.0;
   }
  #ifdef VERBOSE
   printf(" %s: Recursively calling Spec_Calc_Flux_Masked\n",__FILE__);
  #endif
   ierr = Spec_Calc_Flux_Masked( N_los_cells, N_spec_cells, v, nH1, T, mask_in, kms,
                                flux, mask_out, tau_eff, A, psi_Method, species );
   if(ierr!=EXIT_SUCCESS) {
    printf("ERROR: %s: Recursive call to Spec_Calc_Flux_Masked failed.\n",__FILE__);
    fflush(stdout);
    return EXIT_FAILURE;
   }
  } else { /* The solution was within the bracket */
  #ifdef VERBOSE
   printf("%s: A=%5.2e; error=%5.2e;\n",__FILE__,Amid,tau_error);
   fflush(stdout);
  #endif
   /* Return the normalization factor found. */
   *A = Amid;
  }
 } /* End using tau_eff */
 return EXIT_SUCCESS;
}
