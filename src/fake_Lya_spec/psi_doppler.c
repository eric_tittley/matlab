/* Doppler line profile
 *
 * psi = psi_doppler(dv,nu_0,T)
 *
 * ARGUMENTS
 *  dv		Velocity relative to line centre. [km/s]
 *  nu_0	Frequency of the line centre. [Hz]
 *  T		Temperature. [K]
 *  mass	Mass per particle.
 *
 * RETURNS
 *  The line profile at dv. [Hz^-1]
 *
 * NOTES
 *  integral(psi_doppler, nu=0 to inf [dv = c(nu/nu_0 -1)] = 1
 *
 * AUTHOR
 *  Eric Tittley
 *
 * HISTORY
 *  05 10 17 First version
 *
 */
#include <math.h>

#include "psi_doppler.h"

double psi_doppler(const double dv, const double nu_0, const double T,
                   const double mass) {
 const double MaxTau = 40.;
 /* Doppler velocity width (inverse)
  * b = sqrt(2.0 * k_B * T / mass * 1.0e-6); */
 const double b_inv = sqrt(3.62148e+28 * mass / T ); /* Doppler width [km/s]^-1 */
 /* Const = c / ( pi^0.5 * nu_0) */
 const double Const = 169139.782029778 / nu_0; /* km */
 const double exponent = pow(dv * b_inv,2.0); /* unitless */
 if (exponent<MaxTau) {
  return Const * exp( -exponent ) * b_inv;
 } else {
  return 0;
 }
}
