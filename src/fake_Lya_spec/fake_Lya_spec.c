/*
 function Trans=fake_Lya_spec(v,n,T,kms,[psi_shape],[species],[tau_eff],[Amp])

% [Trans,[mask_out]]=fake_Lya_spec(v, n, T, kms, [psi_shape], [species], [tau_eff], [Amp], [mask_in])
%
% ARGUMENTS
%  Input
%   v   N-vector of line-of-sight velocities (peculiar + hubble flow)(km/s)
%   n   N-vector of the HI or HeII column density in each cell (m^-2)
%   T   N-vector of temperatures (K)
%   kms M-vector of velocity bins for output spectrum (km/s)
%  Optional
%   psi_shape The line profile.
%               0=>Doppler [Default]
%               1=>Voigt
%   species   The species.
%               0=> HI Lya [Default]
%               1=> HeI (Currently not supported)
%               2=> HeII Lya
%               3=> HI Ly-beta
%               4=> HI Ly-gamma
%               5=> HeII Ly-beta
%               6=> HeII Ly-gamma
%   tau_eff   An effective mean optical depth to normalize to. Ignored if <= 0.
%
%   Amp    The nomalization amplitude to use.
%       OR The guess at the normalization amplitude required to achieve tau_eff.
%        
%        If tau >0 then used as a first guess on the amplitude, then modified
%                  to be the normalization required to achieve tau_eff.
%        If tau <0 then the normalization to use. Left unmodified.
%
%   mask_in A vector of 0's and 1's that flag those bins corresponding to
%           those in (v, n, & T) that should be ignored.
%
%  Output
%   Trans    M-vector of transmission fractions at the velocities given by kms
%   mask_out M-vector of values between 0 and 1 corresponding to the degree
%             of contamination from cells that should be ignored.
%   Amp      If tau_eff>0, returns the normalization amplitude.
%
%  NOTES
%   Normalization of the spectrum can be done with tau_eff or Amp.
%   tau_eff defined:
%    The routine searches for a solution such that the mean optical depth is
%    tau_eff.  'Amp' is the normalization amplitude.  A bracketing algorithm
%    searches between 0 and Amp for the value of 'Amp'.
%    If the top end of the bracket is reached, then Amp is multiplied by
%    10 and the bracketing starts over.  Hence, 1) you can save yourself some
%    time if you know approxiamately what Amp is choose Amp to be twice
%    that.  2) If Amp is too large (>2^14 times), the bracketing algorithm
%    gives up in despair.  So set Amp low on the first call, then use 2*Amp on
%    subsequent calls.

 HISTORY
  080821 First version
  110727 Include species.h, where they are enumerated, and provide better
        generality.
*/
   

/* #include <math.h> */
/* #include <stdio.h> */ /* only for debugging messages */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "mex.h"

#include "species.h"

int Spec_Calc_Flux(const size_t *N_los_cells,
                   const size_t *N_spec_cells,
                   const double *v,
                   const double *nH1,
                   const double *T,
                   const double *kms,
                   double *flux,
                   const double *tau_eff, 
                   double *A,
                   enum psi_Methods *psi_Method,
                   enum Species *species);

int Spec_Calc_Flux_Masked(const size_t * const N_los_cells,
                   const size_t * const N_spec_cells,
                   const double * const v,
		   const double * const nH1,
		   const double * const T,
                   const int * const mask_in,
		   const double * const kms,
		   double * const flux,
		   double * const mask_out,
		   const double * const tau_eff, 
                   double * const A,
		   enum psi_Methods *psi_Method,
                   enum Species *species);

/* ************************************************** */
void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[] ) {

 double *v;
 double *n;
 double *T;
 double *kms;
 enum psi_Methods psi_shape;
 enum Species species;
 double *tau_eff;
 double *Amp;
 int *mask_in;
 double *mask_in_D;

 double *Trans;
 double *mask_out=NULL;

 size_t mrows,ncols;
 size_t N_los_cells, N_spec_cells;
 int status;
 double *pointer;
 unsigned int No_Flag_tau_eff; /* 1 if tau_eff was not passed in */
 
 int i;

 /* Check for the proper number of arguments */
 if( (nrhs<4) || (nrhs>9) || (nlhs!=1 && nlhs!=2) ) {
  mexErrMsgTxt("Syntax: [Trans,[mask_out]]=fake_Lya_spec(v,n,T,kms,[psi_shape],[species],[tau_eff],[Amp],[mask_in])");
 }

 if( ((nlhs==2) && (nrhs!=9)) || ((nlhs==1) && (nrhs==9)) ) {
  mexErrMsgTxt("Syntax: if either mask_in or mask_out is provided, so must be the other");
 }

 /* The mandatory inputs must be of the type double */
 if(   (!mxIsDouble( prhs[0])) || (!mxIsDouble(prhs[1]))
    || (!mxIsDouble( prhs[2])) || (!mxIsDouble(prhs[3])) ) {
  mexErrMsgTxt("Arguments must be double");
 }

 /* Get pointer to v */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 N_los_cells = mrows * ncols;
 v = mxGetPr(prhs[0]);

 /* Get pointer to n */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if( N_los_cells != (mrows * ncols) ) {
  mexErrMsgTxt("v, n, & T must have the same number of elements.");
 }
 n = mxGetPr(prhs[1]);

 /* Get pointer to T */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if( N_los_cells != (mrows * ncols) ) {
  mexErrMsgTxt("v, n, & T must have the same number of elements.");
 }
 T = mxGetPr(prhs[2]);

 /* Get pointer to kms */
 mrows = mxGetM(prhs[3]);
 ncols = mxGetN(prhs[3]);
 N_spec_cells = mrows * ncols;
 kms = mxGetPr(prhs[3]);

 /* Set defaults for the optional parameters */
 psi_shape=0;
 species=0;

 /* Set psi_shape */
 if( nrhs > 4 ) {
  mrows = mxGetM(prhs[4]);
  ncols = mxGetN(prhs[4]);
  if( (mrows!=1) || (ncols!=1) ) {
   mexErrMsgTxt("psi_shape must be scalar");
  }
  pointer=mxGetPr(prhs[4]);
  psi_shape=(int)floor(*pointer+0.5);
  if( (psi_shape!=Doppler) && (psi_shape!=Voigt) ) {
   mexErrMsgTxt("psi_shape must be 0 or 1");
  }
 }

 /* Set species */
 if( nrhs > 5 ) {
  mrows = mxGetM(prhs[5]);
  ncols = mxGetN(prhs[5]);
  if(mrows!=1 || ncols !=1) {
   mexErrMsgTxt("species must be scalar");
  }
  pointer=mxGetPr(prhs[5]);
  species=(int)floor(*pointer+0.5);
  if( (species<HI_Lya) || (species>=SpeciesTerminator) ) {
   mexErrMsgTxt("species must out of range");
  }
 }

 /* Set tau_eff & Amp */
 if( nrhs > 6 ) {
  No_Flag_tau_eff = 0;
  if( !( nrhs > 7)  ) {
   mexErrMsgTxt("If tau_eff is set, so must Amp");
  }
  mrows = mxGetM(prhs[6]);
  ncols = mxGetN(prhs[6]);
  if(mrows!=1 || ncols !=1) {
   mexErrMsgTxt("tau_eff must be scalar");
  }
  mrows = mxGetM(prhs[7]);
  ncols = mxGetN(prhs[7]);
  if( (mrows!=1) || (ncols !=1) ) {
   mexErrMsgTxt("Amp must be scalar");
  }
  if( (!mxIsDouble( prhs[6])) || (!mxIsDouble(prhs[7])) ) {
   mexErrMsgTxt("tau_eff and Amp must be doubles.");
  }
  tau_eff = mxGetPr(prhs[6]);
  Amp     = mxGetPr(prhs[7]);
 } else {
  No_Flag_tau_eff = 1;
  tau_eff = (double *)malloc(sizeof(double));
  Amp     = (double *)malloc(sizeof(double));
  *tau_eff=-1.;
  *Amp    = 1.;
 }

 if( nlhs != 1 ) {
  /* Get pointer to mask_in */
  mrows = mxGetM(prhs[8]);
  ncols = mxGetN(prhs[8]);
  if( N_los_cells != (mrows * ncols) ) {
   mexErrMsgTxt("v, n, T, & mask_in must have the same number of elements.");
  }
  mask_in_D = mxGetPr(prhs[8]);
  mask_in = (int *)malloc(N_los_cells*sizeof(int));
  for(i=0;i<N_los_cells;i++) {
   mask_in[i]=(int)mask_in_D[i];
  }
 }

 /* Assign pointers to the arguments */
 
 /* Allocate the output */
 plhs[0]=mxCreateDoubleMatrix(N_spec_cells,1,mxREAL);
 Trans = mxGetPr(plhs[0]);
 if(nlhs>1) {
  plhs[1]=mxCreateDoubleMatrix(N_spec_cells,1,mxREAL);
  mask_out = mxGetPr(plhs[1]);
 }

 /* Call the subroutine */
 if(nlhs==1) {
  status = Spec_Calc_Flux(&N_los_cells, &N_spec_cells, v, n, T, kms,
  			  Trans, tau_eff, Amp, &psi_shape, &species);
 } else {
  status = Spec_Calc_Flux_Masked(&N_los_cells, &N_spec_cells, v, n, T, mask_in, kms,
                                 Trans, mask_out, tau_eff, Amp, &psi_shape, &species);
 }
 if(status == EXIT_FAILURE) {
  mexErrMsgTxt("Spec_Calc_Flux returned in error.");
 }

 if(No_Flag_tau_eff) {
  free(tau_eff);
  free(Amp);
 }

}
