        double precision function Dawsonf(x)
* Function to provide Dawson's function D evaluated at x.
        implicit double precision (a-h,o-z)
        include 'dawson_coef.f'
        dimension du(310),dD(310)
        data isave /0/
        save
        if(isave.eq.0) call SPLINI
        if(x.lt.1.d-3) then
            Dawsonf = x*(1.d0 - 2.d0*x*x/ 3.d0)
            return
        endif
        if(x.gt.5.d0) then
            Dawsonf = (0.5d0/ x)*(1.d0 + 0.5d0/ x/ x)
            return
        endif
        if(isave.eq.0) then
            isave = 1
            call SPLDER(u,du,nfit)
            call SPLDER(D,dD,nfit)
        endif
        call SPEVSP(u,D,du,dD,nfit,x,Dawsonf)
        return
        end
