cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine quadls(t,f,f0,f0d,f0dd,n)
c  Quadls calculates a n-point least squares quadratic fit to f(t),
c  giving the output coefficients f0=f, f0d=df/dt, and f0dd=d2f/dt2,
c  evaluated at t=0.
c
        implicit double precision (a-h,o-z)
        dimension t(n),f(n)
c
        st=0.0d0
        st2=0.0d0
        st3=0.0d0
        st4=0.0d0
        sf=0.0d0
        sft=0.0d0
        sft2=0.0d0
          do 10 i=1,n
          t2=t(i)*t(i)
          st=st+t(i)
          st2=st2+t2
          st3=st3+t2*t(i)
          st4=st4+t2*t2
          sf=sf+f(i)
          sft=sft+f(i)*t(i)
          sft2=sft2+f(i)*t2
10      continue
        s6=st2*st4-st3*st3
        s5=st*st4-st2*st3
        s41=st*st3-st2*st2
        s42=n*st4-st2*st2
        s3=n*st3-st*st2
        s2=n*st2-st*st
        det=n*s6-st*s5+st2*s41
        if (det.eq.0.0d0) det=1.0d-16
        f0=(sf*s6-sft*s5+sft2*s41)/det
        f0d=(-sf*s5+sft*s42-sft2*s3)/det
        f0dd=2.0d0*(sf*s41-sft*s3+sft2*s2)/det
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine splder(y,dy,n)
c  Splder fits a cubic spline to y and returns the first derivatives at
c  the grid points in dy.  Dy is equivalent to a 4th-order Pade
c  difference formula for dy/di.
c
        implicit double precision (a-h,o-z)
        dimension f(1001),g(1001),y(n),dy(n)
        common /spline/ g
c
        n1=n-1
        if (n1.gt.1000) then
         write(*,*) 'Spline array overflow!!! n1=',n1,'>1000'
        endif
c  Quartic fit to dy/di at boundaries, assuming d3y/di3=0.
        f(1)=(-10.0d0*y(1)+15.0d0*y(2)-6.0d0*y(3)+y(4))/6.0d0
        f(n)=(10.0d0*y(n)-15.0d0*y(n1)+6.0d0*y(n-2)-y(n-3))/6.0d0
c  Solve the tridiagonal system
c  dy(i-1)+4*dy(i)+dy(i+1)=3*(y(i+1)-y(i-1)), i=2,3,...,n1,
c  with dy(1)=f(1), dy(n)=f(n).
          do 10 i=2,n1
          f(i)=g(i)*(3.0d0*(y(i+1)-y(i-1))-f(i-1))
10      continue
        dy(n)=f(n)
          do 20 i=n1,1,-1
          dy(i)=f(i)-g(i)*dy(i+1)
20      continue
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine spldrv(y,dy,n)
c  Splder fits a cubic spline to y and returns the first derivatives at
c  the grid points in dy.  Dy is equivalent to a 4th-order Pade
c  difference formula for dy/di.
c  N.B. This version is for functions with vector symmetry at r=0.
c
        implicit double precision (a-h,o-z)
        dimension f(1001),g(1001),y(n),dy(n)
        common /spline/ g
c
        n1=n-1
        if (n1.gt.1000) then
         write(*,*) 'Spline array overflow!!! n1=',n1,'>1000'
        endif
c  Cubic fit to dy/di at boundaries, assuming d2y/di2=0 or d4y/di4=0.
c       f(1)=(-85.0d0*y(1)+108.0d0*y(2)-27.0d0*y(3)+4.0d0*y(4))/66.0d0
c       f(n)=(85.0d0*y(n)-108.0d0*y(n1)+27.0d0*y(n-2)-4.0d0*y(n-3))/66.0d0
        f(1)=(-11.0d0*y(1)+18.0d0*y(2)-9.0d0*y(3)+2.0d0*y(4))/6.0d0
        f(n)=(11.0d0*y(n)-18.0d0*y(n1)+9.0d0*y(n-2)-2.0d0*y(n-3))/6.0d0
c  Solve the tridiagonal system
c  dy(i-1)+4*dy(i)+dy(i+1)=3*(y(i+1)-y(i-1)), i=2,3,...,n1,
c  with dy(1)=f(1), dy(n)=f(n).
          do 10 i=2,n1
          f(i)=g(i)*(3.0d0*(y(i+1)-y(i-1))-f(i-1))
10      continue
        dy(n)=f(n)
          do 20 i=n1,1,-1
          dy(i)=f(i)-g(i)*dy(i+1)
20      continue
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine splini
c  Splini must be called before splder to initialize array g in common.
c
        implicit double precision (a-h,o-z)
        dimension g(1001)
        common /spline/ g
        save /spline/
c
        g(1)=0.0d0
          do 10 i=2,1001
          g(i)=1.0d0/(4.0d0-g(i-1))
10      continue
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine splint(y,z,n)
c  Splint integrates a cubic spline, providing the ouput value
c  z = integral from 1 to n of s(i)di, where s(i) is the spline fit
c  to y(i).
c
        implicit double precision (a-h,o-z)
        dimension y(n)
c
        n1=n-1
c  Cubic fit to dy/di at boundaries.
        dy1=(-11.0d0*y(1)+18.0d0*y(2)-9.0d0*y(3)+2.0d0*y(4))/6.0d0
        dyn=(11.0d0*y(n)-18.0d0*y(n1)+9.0d0*y(n-2)-2.0d0*y(n-3))/6.0d0
c
        z=0.5d0*(y(1)+y(n))+(dy1-dyn)/12.0d0
          do 10 i=2,n1
          z=z+y(i)
10      continue
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine spintn(y,dy,z,n)
c  Spintn calculates z(i) = the integral from 1 to i of s(j)dj, for 1 <= i <= n,
c  where s is the spline fit to y, and dy = ds/di.
c
        implicit double precision (a-h,o-z)
        dimension y(n),dy(n),z(n)
c
        ztemp=0.0d0
          do 10 i=2,n
          ztemp1=ztemp+0.5d0*(y(i-1)+y(i))+(dy(i-1)-dy(i))/12.0d0
          z(i-1)=ztemp
          ztemp=ztemp1
10      continue
        z(n)=ztemp
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c       subroutine spevsp(x,y,dx,dy,n,t,u)
c  Provided by J. J. Goodman, 1982.
c
c  Evaluate a cubic spline at a single point.
c  Input:
c       x,y = Arrays of length n containing abcissae and ordinates
c         (N.B. abcissae must be in increasing order).
c       dx,dy = Arrays of length n containing dx/di and dy/di.
c       n = Number of (x,y) pairs in input.
c       t = Interpolation point.
c  Output:
c       u = Interpolated value at t.
c
        subroutine spevsp(x,y,dx,dy,n,t,u)
        integer n
        double precision x(n),y(n),dx(n),dy(n),t,u
c
        integer i,j,k
        double precision d,yp0,yp1,delx
c
        i=1
        j=n
10      if (j.le.i+1) go to 20
          k=(i+j)/2
          if (t.gt.x(k)) then
            i=k
          else
            j=k
          end if
        go to 10
c
20      delx=x(i+1)-x(i)
        d=(t-x(i))/delx
        yp0=dy(i)/dx(i)*delx
        yp1=dy(i+1)/dx(i+1)*delx
        if (t.le.x(1)) then
          u=y(1)+yp0*d
        else if (t.gt.x(n)) then
          u=y(n)+yp1*(d-1.0d0)
        else
          u=y(i)+d*(yp0+d*(3.0d0*(y(i+1)-y(i))
     2        -2.0d0*yp0-yp1+d*(yp0+yp1+2.0d0*(y(i)-y(i+1)))))
        end if
c
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c       subroutine spevar(x,y,dx,dy,n,t,u,m)
c  Provided by J. J. Goodman, 1982.
c
c  Evaluate a cubic spline at a list of points.
c  Input:
c       x,y = Arrays of length n containing abcissae and ordinates
c         (N.B. abcissae must be in increasing order).
c       dx,dy = Arrays of length n containing dx/di and dy/di.
c       n = Number of (x,y) pairs in input.
c       t = Array of length m containing interpolation points
c         (N.B. points must be in increasing order).
c       m = Number of interpolation points.
c  Output:
c       u = Array of length m containing interpolated values at t.
c
        subroutine spevar(x,y,dx,dy,n,t,u,m)
        integer n,m
        double precision x(n),y(n),dx(n),dy(n),t(m),u(m)
c
        integer i,k
        double precision d,yp0,yp1,delx
c
        k=1
          do 30 i=1,m
10          if (x(k+1).gt.t(i).or.k.eq.n-1) go to 20
              k=k+1
            go to 10
c
20          delx=x(k+1)-x(k)
            d=(t(i)-x(k))/delx
            yp0=dy(k)/dx(k)*delx
            yp1=dy(k+1)/dx(k+1)*delx
            if (t(i).le.x(1)) then
              u(i)=y(1)+yp0*d
            else if (t(i).gt.x(n)) then
              u(i)=y(n)+yp1*(d-1.0d0)
            else
              u(i)=y(k)+d*(yp0+d*(3.0d0*(y(k+1)-y(k))
     2            -2.0d0*yp0-yp1+d*(yp0+yp1+2.0d0*(y(k)-y(k+1)))))
            end if
30      continue
c
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine coolini
c  Coolini must be called before coolf to initialize the spline fit to
c  the cooling curve.
        implicit double precision (a-h,o-z)
        dimension alt0(50),all0(50),dalt0(50),dall0(50)
        common /coolfun/ alt0,all0,dalt0,dall0,nspl0
        save /coolfun/
c
c  Zmet gives the fractional metal abundance (relative to solar).
        zmet=0.5d0
        open(10,file='coolfun.dat',status='old')
        rewind 10
        i=0
10        i=i+1
c  The second column of coolfun.dat gives Lambda for solar metal abundances;
c  The third column gives Lambda for half-solar metal abundances.
          read(10,*,end=20) alt0(i),altot,alhalf
          altot=10.0d0**altot
          almet=2.0d0*(altot-10.0d0**alhalf)
          all0(i)=log10(altot-(1.0d0-zmet)*almet)
          go to 10
20      nspl0=i-1
        close(10)
        call splini
        call splder(alt0,dalt0,nspl0)
        call splder(all0,dall0,nspl0)
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        double precision function coolf(hden,tempk)
c  Coolf computes the optically thin radiative cooling loss (erg/cm**3/s)
c  for a cosmic gas in ionization equilibrium with hydrogen number density hden
c  (cm**-3) and temperature tempk (K).  The Gaetz and Salpeter (1983) cooling
c  curve is used, with added forbidden line cooling of [OII].  Coolf must be
c  preceded by a call to coolini to initialize the spline fit.
c
        implicit double precision (a-h,o-z)
        dimension alt0(50),all0(50),dalt0(50),dall0(50)
        common /coolfun/ alt0,all0,dalt0,dall0,nspl0
        save /coolfun/
c
        if (tempk.le.0.0d0) then
          coolf=0.0d0
          return
        end if
c
        alt=log10(tempk)
        if (alt.lt.4.0d0) then
c  No cooling for T < 10**4 K.
          alambda=0.0d0
        else if (alt.gt.8.0d0) then
c  Bremsstrahlung cooling for T > 10**8 K.
          alambda=2.0d-27*sqrt(tempk)
        else
          call spevsp(alt0,all0,dalt0,dall0,nspl0,alt,all)
          alambda=10.0d0**all
        end if
c
c  Assume fully ionized H, He; n(He)/n(H)=0.10.
        coolf=1.2d0*hden*hden*alambda
        return
        end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        double precision function lambda(tempk)
c  Lambda computes the optically thin radiative cooling loss (erg*cm**3/s)
c  for a cosmic gas in ionization equilibrium with
c  temperature tempk (K).  The Gaetz and Salpeter (1983) cooling
c  curve is used, with added forbidden line cooling of [OII].  Lambda must be
c  preceded by a call to coolini to initialize the spline fit.
c
        implicit double precision (a-h,o-z)
        dimension alt0(50),all0(50),dalt0(50),dall0(50)
        common /coolfun/ alt0,all0,dalt0,dall0,nspl0
        save /coolfun/
c
        if (tempk.le.0.0d0) then
          lambda=0.0d0
          return
        end if
c
        alt=log10(tempk)
        if (alt.lt.4.0d0) then
c  No cooling for T < 10**4 K.
          lambda=0.0d0
        else if (alt.gt.8.0d0) then
c  Bremsstrahlung cooling for T > 10**8 K.
          lambda=2.0d-27*sqrt(tempk)
        else
          call spevsp(alt0,all0,dalt0,dall0,nspl0,alt,all)
          lambda=10.0d0**all
        end if
        return
        end
