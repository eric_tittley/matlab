#ifndef _VOIGT_H_
#define _VOIGT_H_
 double voigt_(double *a, double *u);
 double psi_voigt(const double dv, const double nu_0, const double T,
                  const double mass);
#endif
