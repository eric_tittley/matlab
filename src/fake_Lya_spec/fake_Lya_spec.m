% fake_Lya_spec: Create a fake Lymann-alpha transmission spectrum
%
% [Trans,[mask_out]]=fake_Lya_spec(v, n, T, kms, [psi_shape], [species], [tau_eff], [Amp], [mask_in])
%
% ARGUMENTS
%  Input
%   v   N-vector of line-of-sight velocities (peculiar + hubble flow)(km/s)
%   n   N-vector of the HI or HeII column density in each cell (m^-2)
%   T   N-vector of temperatures (K)
%   kms M-vector of velocity bins for output spectrum (km/s)
%  Optional
%   psi_shape The line profile.
%               0=>Doppler [Default]
%               1=>Voigt
%   species   The species.
%               0=> HI Lya [Default]
%               1=> HeI (Currently not supported)
%               2=> HeII Lya
%               3=> HI Ly-beta
%               4=> HI Ly-gamma
%               5=> HeII Ly-beta
%               6=> HeII Ly-gamma
%   tau_eff   An effective mean optical depth to normalize to. Ignored if <= 0.
%
%   Amp    The nomalization amplitude to use.
%       OR The guess at the normalization amplitude required to achieve tau_eff.
%        
%        If tau >0 then used as a first guess on the amplitude, then modified
%                  to be the normalization required to achieve tau_eff.
%        If tau <0 then the normalization to use. Left unmodified.
%
%   mask_in A vector of 0's and 1's that flag those bins corresponding to
%           those in (v, n, & T) that should be ignored.
%
%  Output
%   Trans    M-vector of transmission fractions at the velocities given by kms
%   mask_out M-vector of values between 0 and 1 corresponding to the degree
%             of contamination from cells that should be ignored.
%   Amp      If tau_eff>0, returns the normalization amplitude.
%
%  NOTES
%   Normalization of the spectrum can be done with tau_eff or Amp.
%   tau_eff defined:
%    The routine searches for a solution such that the mean optical depth is
%    tau_eff.  'Amp' is the normalization amplitude.  A bracketing algorithm
%    searches between 0 and Amp for the value of 'Amp'.
%    If the top end of the bracket is reached, then Amp is multiplied by
%    10 and the bracketing starts over.  Hence, 1) you can save yourself some
%    time if you know approxiamately what Amp is choose Amp to be twice
%    that.  2) If Amp is too large (>2^14 times), the bracketing algorithm
%    gives up in despair.  So set Amp low on the first call, then use 2*Amp on
%    subsequent calls.

% AUTHOR: Eric Tittley
%
% HISTORY
%  080821 First version
%  130416 Merged fake_Lya_spec & fake_Lya_spec_masked
