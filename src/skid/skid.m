% A group finder for N-body simulations. (skid)
%
% This is a port of SKID v1.4.1: Joachim Stadel, (Dec. 2000) to Matlab
% (http://www-hpcc.astro.washington.edu/tools/skid/)
%
% GroupNum=skid(rm,r,Period,Center,tau);
%
% ARGUMENTS
%  rm		Particle masses (Nx1)
%  r		Particle positions (3xN)
%  Period	The periodicity of the particle positions.  If the simulation 
%		does not have periodic boundary conditions, set this to a large
%		number.
%  Center	The centre of the simulation volume.
%  tau		The linking length at the end of the shifting of the positions.
%
% OUTPUT
%  GroupNum	The group number of each particle.
%
% METHOD
%  1) The local densities of the particles are determined.
%  2) The particles are moved down the density gradient until they
%     oscillate in a minimum.
%  3) A friends-of-friends method is called to group the particles.
%
% NOTES
%  This routine is based on skid, by Joachim Stadel.  It differs from skid
%  in that it simply groups the particles by moving them to positions of
%  maximum density.  It does not impose a minimum density cut-off (the user
%  can do that in matlab before calling skid) nor does it attempt to determine
%  if the particles are bound or not.  To determine if the particles are bound
%  would require too much user input in regards to the physical units of the
%  simulation.
%
%
% SEE ALSO
%  fof

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 05 06 First version
