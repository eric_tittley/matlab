/* function GroupNum=skid(rm,r,Period,Center,tau)
*
* A group finder for N-body simulations
*
*
* This is a port of SKID v1.4.1: Joachim Stadel, (Dec. 2000) to Matlab
*
* (http://www-hpcc.astro.washington.edu/tools/skid/)
*/

/* AUTHOR: Eric Tittley (port to Matlab) */

/******** HISTORY ****************************************************
  02 05 06 First version                                            
 *********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mex.h>

int skid(
 /* Input arguments, not modified */
 const int N,			/* The number of particles */
 const double * const rm,	/* Particle mass (Nx1 vector) */ /* Is this used? */
 const double * const r,         /* Particle positions (Nx3 array) */
 const double * const fPeriod,   /* Periodicity (1x3 vector) */
 const double * const fCenter,   /* box centre (1x3 vector) */ /* Is this used? */
 const double fTau,              /* The linking length */
 /* Output, initialised and modified */
 int *const Group		/* Group number for each particle (Nx1 vector pre-allocated) */
);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {
 double *rm, *r, *fPeriod, *fCenter, fTau, *fTau_p;
 size_t N;

 double *GroupNum;
 int *GroupNum_INT;

 int i;
 int status;
 size_t mrows,ncols;
 
 /* Check for the proper number of arguments */
 if(nrhs!=5 || nlhs!=1 ) {
  mexErrMsgTxt("GroupNum=skid(rm,r,Period,Center,tau)");
 }

 /* The inputs must be of the type double */
 if(   !mxIsDouble(prhs[0])
    || !mxIsDouble(prhs[1])
    || !mxIsDouble(prhs[2])
    || !mxIsDouble(prhs[3])
    || !mxIsDouble(prhs[4])
   ) {
  mexErrMsgTxt("All arguments must be double");
 }

 /* The 1st argument must be N rows by 1 columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 N = mxGetN(prhs[0]);
 if(mrows<11 || N!=1) {
  mexErrMsgTxt("rm must be a [Nx1] array with N>10");
 }
 /* The 2nd argument must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[1]);
 N = mxGetN(prhs[1]);
 if(mrows!=3 || N<11) {
  mexErrMsgTxt("r must be a 3xN array with N>10");
 }
 /* The 3rd & 4th arguments must be [1x3] */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if(mrows!=1 || ncols !=3) {
  mexErrMsgTxt("Period must be [1x3]");
 }
 mrows = mxGetM(prhs[3]);
 ncols = mxGetN(prhs[3]);
 if(mrows!=1 || ncols !=3) {
  mexErrMsgTxt("Center must be [1x3]");
 }
 /* The 5th arguments must be scalar */
 mrows = mxGetM(prhs[4]);
 ncols = mxGetN(prhs[4]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("tau must be scalar");
 }

 /* Create matrix for the return argument and assign a pointer */
 plhs[0]=mxCreateDoubleMatrix(1,N, mxREAL);
 GroupNum = mxGetPr(plhs[0]);
 
 /* Allocate memory for GroupNum_INT */
 GroupNum_INT = (int*)mxCalloc(N,sizeof(int));
 if(GroupNum_INT==NULL) {
  mexErrMsgTxt("Failed to allocate memory for GroupNum_INT");
 }

 /* Assign pointers to the input */
 rm      = mxGetPr(prhs[0]);
 r       = mxGetPr(prhs[1]);
 fPeriod = mxGetPr(prhs[2]);
 fCenter = mxGetPr(prhs[3]);
 fTau_p  = mxGetPr(prhs[4]);

 /* get the value at the pointer's address */
 fTau=*fTau_p;
 
/* the MAIN SUBROUTINE */
 status=skid(
  /* Input */
  (int)N, rm, r, fPeriod, fCenter, fTau,
  /* Output */
  GroupNum_INT
 );
 if(status!=0) { mexErrMsgTxt("skid returned non-zero status"); }

 for(i=0;i<N;++i) {
  GroupNum[i]=(float)GroupNum_INT[i];
 }

}
