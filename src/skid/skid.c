// #define VERBOSE

#include <stdio.h>

#include <stdlib.h>
#include <math.h>
#include <limits.h>

#ifdef MEX
#include <mex.h>
#endif

#include "kd.h"
#include "smooth1.h"

#define PRUNE_STEPS	5
#define MICRO_STEP	0.1


int skid(
 /* Input arguments, not modified */
 const int N,			/* The number of particles */
 const double * const rm,	/* Particle mass (Nx1 vector) */
 const double * const r,	/* Particle positions (Nx3 array) */
 const double * const fPeriod,	/* Periodicity (1x3 vector) */
 const double * const fCenter,	/* box centre (1x3 vector) */
 const double fTau,		/* The linking length */
 /* Output, initialised and modified */
 int *const Group		/* Group number for each particle (Nx1 vector pre-allocated) */
) {

 /* Structure used by the Kraft Dinner routines */
 KD kd;

 /* Structure containing parameters for the smoothing */
 SMX smx;

 /* Working variables for SKID mainline. */
 int i,j,nActive,nIttr;
#ifdef VERBOSE
 int nScat;
#endif

 /* Control variables */
 const int bGasAndDark=0;
 const int bGasOnly=0;
 const int bPeriodic=1;
 const int nBucket = 16;
 /* verbosity level */
 const int bOutDiag=0;

 /* Where does this come from? The authors' suggest 64 */
 /* const int nSmooth = 200; */
 const int nSmooth = 64;

 float fCvg;
 float fStep;
 fCvg = 0.5*fTau;
 fStep = 0.5*fCvg;

 /* Initialise the KD structure */
 kdInit(&kd,nBucket,fPeriod,fCenter,bOutDiag);

 /* Copy the input data to local kd arrays */
 kd->nParticles = N;
 kd->nDark = N;
 kd->nGas = 0;
 kd->nStar = 0;
 kd->fTime = 1;
 kd->nActive = N;
 kd->inType=DARK;
 /* Allocate memory for particles */
 kd->pInit = (PINIT *)malloc(kd->nActive*sizeof(PINIT));
 if(kd->pInit == NULL) {
#ifdef MEX
  mexErrMsgTxt("skid: failed to allocate memory for kd.pInit");
#else
  printf("skid: failed to allocate memory for kd.pInit\n");
  return(-1);
#endif
 }
 /* Copy the positions into Kraft Dinner format (a wasteful step) */
 for (i=0;i<N;++i) {
  kd->pInit[i].iOrder = i;
  for (j=0;j<3;++j) {
   kd->pInit[i].r[j] = r[i*3+j];
  }
  kd->pInit[i].fMass = rm[i];
 }

 kdScatterActive(kd,bGasAndDark,bGasOnly);
 kdBuildTree(kd);

 /* Smooth the data onto a grid */
 smInit(&smx,kd,nSmooth);
 smDensityInit(smx,bPeriodic);

/*
 ** Find initial moving particles.
 ** Move the moving particles for one step and reduce the
 ** number of scatterers if possible.
 */
 /* Set the number of particles to move */
 nActive=N;
 kd->nMove=N;
 kd->nActive = kd->nMove;
 /* Allocate memory for the moving particles */
 kd->pMove = (PMOVE *)malloc(kd->nMove*sizeof(PMOVE));
 if( kd->pMove==NULL ) {
#ifdef MEX
  mexErrMsgTxt("skid: Failed to allocate memory for the moving particles.");
#else
  printf("skid: Failed to allocate memory for the moving particles.\n");
#endif
  return(-1);
 }
 /* Initialise the moving particles */
 for (i=0;i<kd->nParticles;++i) {
  for (j=0;j<3;++j) {
   kd->pMove[i].r[j]    = kd->pInit[i].r[j];
   kd->pMove[i].rOld[j] = kd->pInit[i].r[j];
  }
  kd->pMove[i].iOrder = kd->pInit[i].iOrder;
 }
 
 /* Build the move tree (whatever that is) */
 kdBuildMoveTree(kd);

 /* Accumulate the densities (?) */
 /* Are the densities accumulated here, or in smDensityInit() which is timed */
 /* The 2nd argument is bInitial, which, on the initial iteration, removes
  * "all potential high density scatterers which did not scatter."
  * I don't know what it should be. */

#ifdef VERBOSE
 nScat = smAccDensity(smx,0);
 printf("a Ittr:%d nActive:%d nScatter:%d\n",0,nActive,nScat);
 fflush(stdout);
#endif
 kdMoveParticles(kd,fStep);
 /*
  ** Do the main "flow" loop for the moving particles.
  */
 nIttr = 1;
 while (nActive) {
  for (i=0;i<PRUNE_STEPS;++i) {
   kdBuildMoveTree(kd);
#ifdef VERBOSE
   nScat = smAccDensity(smx,0);
   printf("Ittr:%d nActive:%d nScatter:%d\n",nIttr,nActive,nScat);
   fflush(stdout);
#endif
   kdMoveParticles(kd,fStep);
  }
  nActive = kdPruneInactive(kd,fCvg);
  ++nIttr;
 }

 /*
  ** Assign groups using the friends-of-friends algorithm
  */
 kdFoF(kd,fTau);

 /*
  ** Micro-stepping phase.
  */
 kdReactivateMove(kd);
 for (i=0;i<PRUNE_STEPS;++i) {
  kdBuildMoveTree(kd);
  kdMoveParticles(kd,MICRO_STEP*fStep);
#ifdef VERBOSE
  nScat = smAccDensity(smx,0);
  printf("Microstep:%d nScatter:%d\n",i+1,nScat);
  fflush(stdout);
#endif
 }

 /* Free memory used for smoothing */
 smFinish(smx);

 /* Copy the contents of the kd.piGroup array to our output array */
 for (i=0;i<N;i++) {
  Group[i]=kd->piGroup[i]; /* This is the primary product */
 }

 /* Clean up allocated memory */
 kdFinish(kd);

 return 0;
}
	

