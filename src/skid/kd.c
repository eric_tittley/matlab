#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
/*#include <rpc/types.h> */
#include <assert.h>
#include <limits.h>
#include "kd.h"

#ifdef VERBOSE
void kdTime(KD kd, int *puSecond, int *puMicro)
{
 struct rusage ru;

 getrusage(0, &ru);
 *puMicro = ru.ru_utime.tv_usec - kd->uMicro;
 *puSecond = ru.ru_utime.tv_sec - kd->uSecond;
 if (*puMicro < 0) {
  *puMicro += 1000000;
  *puSecond -= 1;
 }
 kd->uSecond = ru.ru_utime.tv_sec;
 kd->uMicro = ru.ru_utime.tv_usec;
}
#endif

int kdInit(KD * const pkd,
           const int nBucket,
           const double *const fPeriod,
           const double *const fCenter, const int bOutDiag)
{
 KD kd;
 int j;

 kd = (KD) malloc(sizeof(struct kdContext));
 assert(kd != NULL);
 kd->nBucket = nBucket;
 for (j = 0; j < 3; ++j) {
  kd->fPeriod[j] = fPeriod[j];
  kd->fCenter[j] = fCenter[j];
 }
 kd->bOutDiag = bOutDiag;
 kd->G = 1.0;
//      kd->csm = NULL;
 kd->z = 0.0;
 kd->nParticles = 0;
 kd->nDark = 0;
 kd->nGas = 0;
 kd->nStar = 0;
 kd->inType = 0;
 kd->fTime = 0.0;
 kd->nLevels = 0;
 kd->nNodes = 0;
 kd->nSplit = 0;
 kd->nMove = 0;
 kd->nActive = 0;
 kd->nInitActive = 0;
 kd->pMove = NULL;
 kd->pInit = NULL;
 kd->pGroup = NULL;
 kd->kdNodes = NULL;
 kd->piGroup = NULL;
 kd->nGroup = 0;
 *pkd = kd;
 return (1);
}


int kdParticleType(KD kd, int iOrder)
{
 if (iOrder < kd->nGas)
  return (GAS);
 else if (iOrder < (kd->nGas + kd->nDark))
  return (DARK);
 else if (iOrder < kd->nParticles)
  return (STAR);
 else
  return (0);
}


void kdSelectInit(KD kd, int d, int k, int l, int r)
{
 PINIT *p, t;
 double v;
 int i, j;

 p = kd->pInit;
 while (r > l) {
  v = p[k].r[d];
  t = p[r];
  p[r] = p[k];
  p[k] = t;
  i = l - 1;
  j = r;
  while (1) {
   while (i < j)
    if (p[++i].r[d] >= v)
     break;
   while (i < j)
    if (p[--j].r[d] <= v)
     break;
   t = p[i];
   p[i] = p[j];
   p[j] = t;
   if (j <= i)
    break;
  }
  p[j] = p[i];
  p[i] = p[r];
  p[r] = t;
  if (i >= k)
   r = i - 1;
  if (i <= k)
   l = i + 1;
 }
}


void kdSelectMove(KD kd, int d, int k, int l, int r)
{
 PMOVE *p, t;
 double v;
 int i, j;

 p = kd->pMove;
 while (r > l) {
  v = p[k].r[d];
  t = p[r];
  p[r] = p[k];
  p[k] = t;
  i = l - 1;
  j = r;
  while (1) {
   while (i < j)
    if (p[++i].r[d] >= v)
     break;
   while (i < j)
    if (p[--j].r[d] <= v)
     break;
   t = p[i];
   p[i] = p[j];
   p[j] = t;
   if (j <= i)
    break;
  }
  p[j] = p[i];
  p[i] = p[r];
  p[r] = t;
  if (i >= k)
   r = i - 1;
  if (i <= k)
   l = i + 1;
 }
}


void Combine(KDN * p1, KDN * p2, KDN * pOut)
{
 int j;

 /*
  ** Combine the bounds.
  */
 for (j = 0; j < 3; ++j) {
  if (p2->bnd.fMin[j] < p1->bnd.fMin[j])
   pOut->bnd.fMin[j] = p2->bnd.fMin[j];
  else
   pOut->bnd.fMin[j] = p1->bnd.fMin[j];
  if (p2->bnd.fMax[j] > p1->bnd.fMax[j])
   pOut->bnd.fMax[j] = p2->bnd.fMax[j];
  else
   pOut->bnd.fMax[j] = p1->bnd.fMax[j];
 }
}


void UpPassInit(KD kd, int iCell)
{
 KDN *c;
 int l, u, pj, j;

 c = kd->kdNodes;
 if (c[iCell].iDim != -1) {
  l = LOWER(iCell);
  u = UPPER(iCell);
  UpPassInit(kd, l);
  UpPassInit(kd, u);
  Combine(&c[l], &c[u], &c[iCell]);
 } else {
  l = c[iCell].pLower;
  u = c[iCell].pUpper;
  for (j = 0; j < 3; ++j) {
   c[iCell].bnd.fMin[j] = kd->pInit[u].r[j];
   c[iCell].bnd.fMax[j] = kd->pInit[u].r[j];
  }
  for (pj = l; pj < u; ++pj) {
   for (j = 0; j < 3; ++j) {
    if (kd->pInit[pj].r[j] < c[iCell].bnd.fMin[j])
     c[iCell].bnd.fMin[j] = kd->pInit[pj].r[j];
    if (kd->pInit[pj].r[j] > c[iCell].bnd.fMax[j])
     c[iCell].bnd.fMax[j] = kd->pInit[pj].r[j];
   }
  }
 }
}


void UpPassMove(KD kd, int iCell)
{
 KDN *c;
 int l, u, pj, j;

 c = kd->kdNodes;
 if (c[iCell].iDim != -1) {
  l = LOWER(iCell);
  u = UPPER(iCell);
  UpPassMove(kd, l);
  UpPassMove(kd, u);
  Combine(&c[l], &c[u], &c[iCell]);
 } else {
  l = c[iCell].pLower;
  u = c[iCell].pUpper;
  for (j = 0; j < 3; ++j) {
   c[iCell].bnd.fMin[j] = kd->pMove[u].r[j];
   c[iCell].bnd.fMax[j] = kd->pMove[u].r[j];
  }
  for (pj = l; pj < u; ++pj) {
   for (j = 0; j < 3; ++j) {
    if (kd->pMove[pj].r[j] < c[iCell].bnd.fMin[j])
     c[iCell].bnd.fMin[j] = kd->pMove[pj].r[j];
    if (kd->pMove[pj].r[j] > c[iCell].bnd.fMax[j])
     c[iCell].bnd.fMax[j] = kd->pMove[pj].r[j];
   }
  }
 }
}


int kdBuildTree(KD kd)
{
 int l, n, i, d, m, j, diff;
 KDN *c;
 BND bnd;

 if (kd->bOutDiag)
  puts(">> kdBuildTree()");
 fflush(stdout);
 if (kd->nInitActive == 0) {
  if (kd->kdNodes)
   free(kd->kdNodes);
  kd->kdNodes = NULL;
  return (1);
 }
 assert(kd->nInitActive > 0);
 n = kd->nInitActive;
 kd->nLevels = 1;
 l = 1;
 while (n > kd->nBucket) {
  n = n >> 1;
  l = l << 1;
  ++kd->nLevels;
 }
 kd->nSplit = l;
 kd->nNodes = l << 1;
 if (kd->kdNodes) {
  free(kd->kdNodes);
  kd->kdNodes = NULL;
 }
 kd->kdNodes = (KDN *) malloc(kd->nNodes * sizeof(KDN));
 assert(kd->kdNodes != NULL);
 /*
  ** Calculate Bounds.
  */
 for (j = 0; j < 3; ++j) {
  bnd.fMin[j] = kd->pInit[0].r[j];
  bnd.fMax[j] = kd->pInit[0].r[j];
 }
 for (i = 1; i < kd->nInitActive; ++i) {
  for (j = 0; j < 3; ++j) {
   if (bnd.fMin[j] > kd->pInit[i].r[j])
    bnd.fMin[j] = kd->pInit[i].r[j];
   else if (bnd.fMax[j] < kd->pInit[i].r[j])
    bnd.fMax[j] = kd->pInit[i].r[j];
  }
 }
 /*
  ** Set up ROOT node
  */
 c = kd->kdNodes;
 c[ROOT].pLower = 0;
 c[ROOT].pUpper = kd->nInitActive - 1;
 c[ROOT].bnd = bnd;
 i = ROOT;
 while (1) {
  assert(c[i].pUpper - c[i].pLower + 1 > 0);
  if (i < kd->nSplit && (c[i].pUpper - c[i].pLower) > 0) {
   d = 0;
   for (j = 1; j < 3; ++j) {
    if (c[i].bnd.fMax[j] - c[i].bnd.fMin[j] >
        c[i].bnd.fMax[d] - c[i].bnd.fMin[d])
     d = j;
   }
   c[i].iDim = d;

   m = (c[i].pLower + c[i].pUpper) / 2;
   kdSelectInit(kd, d, m, c[i].pLower, c[i].pUpper);

   c[i].fSplit = kd->pInit[m].r[d];
   c[LOWER(i)].bnd = c[i].bnd;
   c[LOWER(i)].bnd.fMax[d] = c[i].fSplit;
   c[LOWER(i)].pLower = c[i].pLower;
   c[LOWER(i)].pUpper = m - 1;
   c[UPPER(i)].bnd = c[i].bnd;
   c[UPPER(i)].bnd.fMin[d] = c[i].fSplit;
   c[UPPER(i)].pLower = m;
   c[UPPER(i)].pUpper = c[i].pUpper;
   diff = (m - c[i].pLower + 1) - (c[i].pUpper - m);
   assert(diff == 0 || diff == 1);
   i = LOWER(i);
  } else {
   c[i].iDim = -1;
   SETNEXT(i);
   if (i == ROOT)
    break;
  }
 }
 UpPassInit(kd, ROOT);
 if (kd->bOutDiag)
  puts("<< kdBuildTree()");
 fflush(stdout);
 return (1);
}


int kdBuildMoveTree(KD kd)
{
 int l, n, i, d, m, j, diff;
 KDN *c;
 BND bnd;

 if (kd->bOutDiag)
  puts(">> kdBuildMoveTree()");
 fflush(stdout);
 if (kd->nActive == 0) {
  if (kd->kdNodes)
   free(kd->kdNodes);
  kd->kdNodes = NULL;
  return (1);
 }
 assert(kd->nActive > 0);
 n = kd->nActive;
 kd->nLevels = 1;
 l = 1;
 while (n > kd->nBucket) {
  n = n >> 1;
  l = l << 1;
  ++kd->nLevels;
 }
 kd->nSplit = l;
 kd->nNodes = l << 1;
 if (kd->kdNodes) {
  free(kd->kdNodes);
  kd->kdNodes = NULL;
 }
 kd->kdNodes = (KDN *) malloc(kd->nNodes * sizeof(KDN));
 assert(kd->kdNodes != NULL);
 /*
  ** Calculate Bounds.
  */
 for (j = 0; j < 3; ++j) {
  bnd.fMin[j] = kd->pMove[0].r[j];
  bnd.fMax[j] = kd->pMove[0].r[j];
 }
 for (i = 1; i < kd->nMove; ++i) {
  for (j = 0; j < 3; ++j) {
   if (bnd.fMin[j] > kd->pMove[i].r[j])
    bnd.fMin[j] = kd->pMove[i].r[j];
   else if (bnd.fMax[j] < kd->pMove[i].r[j])
    bnd.fMax[j] = kd->pMove[i].r[j];
  }
 }
 /*
  ** Set up ROOT node
  */
 c = kd->kdNodes;
 c[ROOT].pLower = 0;
 c[ROOT].pUpper = kd->nActive - 1;
 c[ROOT].bnd = bnd;
 i = ROOT;
 while (1) {
  assert(c[i].pUpper - c[i].pLower + 1 > 0);
  if (i < kd->nSplit && (c[i].pUpper - c[i].pLower) > 0) {
   d = 0;
   for (j = 1; j < 3; ++j) {
    if (c[i].bnd.fMax[j] - c[i].bnd.fMin[j] >
        c[i].bnd.fMax[d] - c[i].bnd.fMin[d])
     d = j;
   }
   c[i].iDim = d;

   m = (c[i].pLower + c[i].pUpper) / 2;
   kdSelectMove(kd, d, m, c[i].pLower, c[i].pUpper);

   c[i].fSplit = kd->pMove[m].r[d];
   c[LOWER(i)].bnd = c[i].bnd;
   c[LOWER(i)].bnd.fMax[d] = c[i].fSplit;
   c[LOWER(i)].pLower = c[i].pLower;
   c[LOWER(i)].pUpper = m - 1;
   c[UPPER(i)].bnd = c[i].bnd;
   c[UPPER(i)].bnd.fMin[d] = c[i].fSplit;
   c[UPPER(i)].pLower = m;
   c[UPPER(i)].pUpper = c[i].pUpper;
   diff = (m - c[i].pLower + 1) - (c[i].pUpper - m);
   assert(diff == 0 || diff == 1);
   i = LOWER(i);
  } else {
   c[i].iDim = -1;
   SETNEXT(i);
   if (i == ROOT)
    break;
  }
 }
 UpPassMove(kd, ROOT);
 if (kd->bOutDiag)
  puts("<< kdBuildMoveTree()");
 fflush(stdout);
 return (1);
}


int ScatterCriterion(KD kd, int pi, int bGasAndDark, int bGasOnly)
{
 switch (kd->inType) {
  case (DARK):
   return (1);
  case (GAS):
  case (DARK | GAS):
   if (bGasAndDark)
    return (1);
   else if (kdParticleType(kd, kd->pInit[pi].iOrder) == GAS) {
    return (1);
   }
   break;
  case (STAR):
  case (DARK | STAR):
   if (kdParticleType(kd, kd->pInit[pi].iOrder) == STAR)
    return (1);
   break;
  case (GAS | STAR):
  case (DARK | GAS | STAR):
   if (bGasAndDark)
    return (1);
   if (kdParticleType(kd, kd->pInit[pi].iOrder) == GAS) {
    return (1);
   } else if (kdParticleType(kd, kd->pInit[pi].iOrder) == STAR && !bGasOnly) {
    return (1);
   }
 }
 return (0);
}


int kdScatterActive(KD kd, int bGasAndDark, int bGasOnly)
{
 PINIT *p, t;
 int i, j;

 if (kd->bOutDiag)
  puts(">> kdScatterActive()");
 fflush(stdout);
 p = kd->pInit;
 i = 0;
 j = kd->nParticles - 1;
 while (1) {
  while (ScatterCriterion(kd, i, bGasAndDark, bGasOnly))
   if (++i > j)
    goto done;
  while (!ScatterCriterion(kd, j, bGasAndDark, bGasOnly))
   if (i > --j)
    goto done;
  t = p[i];
  p[i] = p[j];
  p[j] = t;
 }
done:
 kd->nInitActive = i;
 if (kd->bOutDiag)
  puts("<< kdScatterActive()");
 fflush(stdout);
 return (i);
}


void kdMoveParticles(KD kd, float fStep)
{
 PMOVE *p;
 int i, j;
 float ax, ay, az, ai;

 if (kd->bOutDiag)
  puts(">> kdMoveParticles()");
 fflush(stdout);
 p = kd->pMove;
 for (i = 0; i < kd->nActive; ++i) {
  ax = p[i].a[0];
  ay = p[i].a[1];
  az = p[i].a[2];
  ai = sqrt(ax * ax + ay * ay + az * az);
  if (ai > 0.0)
   ai = fStep / sqrt(ax * ax + ay * ay + az * az);
  else
   ai = 0.0;
  p[i].r[0] -= ai * ax;
  p[i].r[1] -= ai * ay;
  p[i].r[2] -= ai * az;
  for (j = 0; j < 3; ++j) {
   if (p[i].r[j] > kd->fCenter[j] + 0.5 * kd->fPeriod[j])
    p[i].r[j] -= kd->fPeriod[j];
   if (p[i].r[j] <= kd->fCenter[j] - 0.5 * kd->fPeriod[j])
    p[i].r[j] += kd->fPeriod[j];
  }
 }
 if (kd->bOutDiag)
  puts("<< kdMoveParticles()");
 fflush(stdout);
}


int kdPruneInactive(KD kd, float fCvg)
{
 PMOVE *p, t;
 int i, j;
 float dx, dy, dz, dr2, fCvg2, hx, hy, hz;

 if (kd->bOutDiag)
  puts(">> kdPruneInactive()");
 fflush(stdout);
 p = kd->pMove;
 hx = 0.5 * kd->fPeriod[0];
 hy = 0.5 * kd->fPeriod[1];
 hz = 0.5 * kd->fPeriod[2];
 i = 0;
 j = kd->nActive - 1;
 fCvg2 = fCvg * fCvg;
 while (1) {
  while (1) {
   dx = p[i].r[0] - p[i].rOld[0];
   dy = p[i].r[1] - p[i].rOld[1];
   dz = p[i].r[2] - p[i].rOld[2];
   if (dx > hx)
    dx -= 2 * hx;
   if (dx <= -hx)
    dx += 2 * hx;
   if (dy > hy)
    dy -= 2 * hy;
   if (dy <= -hy)
    dy += 2 * hy;
   if (dz > hz)
    dz -= 2 * hz;
   if (dz <= -hz)
    dz += 2 * hz;
   dr2 = dx * dx + dy * dy + dz * dz;
   if (dr2 < fCvg2)
    break;
   if (++i > j)
    goto done;
  }
  while (1) {
   dx = p[j].r[0] - p[j].rOld[0];
   dy = p[j].r[1] - p[j].rOld[1];
   dz = p[j].r[2] - p[j].rOld[2];
   if (dx > hx)
    dx -= 2 * hx;
   if (dx <= -hx)
    dx += 2 * hx;
   if (dy > hy)
    dy -= 2 * hy;
   if (dy <= -hy)
    dy += 2 * hy;
   if (dz > hz)
    dz -= 2 * hz;
   if (dz <= -hz)
    dz += 2 * hz;
   dr2 = dx * dx + dy * dy + dz * dz;
   if (dr2 >= fCvg2)
    break;
   if (i > --j)
    goto done;
  }
  t = p[i];
  p[i] = p[j];
  p[j] = t;
 }
done:
 kd->nActive = i;
 for (i = 0; i < kd->nActive; ++i) {
  for (j = 0; j < 3; ++j) {
   p[i].rOld[j] = p[i].r[j];
  }
 }
 if (kd->bOutDiag)
  puts("<< kdPruneInactive()");
 fflush(stdout);
 return (i);
}


void kdReactivateMove(KD kd)
{
 kd->nActive = kd->nMove;
}


void kdFoF(KD kd, float fTau)
{
 PMOVE *p;
 KDN *c;
 int pi, pj, pn, cp;
 int *Group, iGroup;
 int *Fifo, iHead, iTail, nFifo;
 float fTau2;
 float dx, dy, dz, x, y, z, lx, ly, lz, sx, sy, sz, fDist2;

 if (kd->bOutDiag)
  puts(">> kdFoF()");
 fflush(stdout);
 kd->nActive = kd->nMove;
 if (kd->nActive == 0) {
  kd->nGroup = 1;
  kd->piGroup = (int *) malloc(kd->nParticles * sizeof(int));
  assert(kd->piGroup);
  for (pi = 0; pi < kd->nParticles; ++pi) {
   kd->piGroup[pi] = 0;
  }
  return;
 }
 kdBuildMoveTree(kd);
 p = kd->pMove;
 c = kd->kdNodes;
 lx = kd->fPeriod[0];
 ly = kd->fPeriod[1];
 lz = kd->fPeriod[2];
 fTau2 = fTau * fTau;
 Group = (int *) malloc(kd->nActive * sizeof(int));
 assert(Group != NULL);
 for (pn = 0; pn < kd->nActive; ++pn)
  Group[pn] = 0;
 nFifo = kd->nActive;
 Fifo = (int *) malloc(nFifo * sizeof(int));
 assert(Fifo != NULL);
 iHead = 0;
 iTail = 0;
 iGroup = 0;
 for (pn = 0; pn < kd->nActive; ++pn) {
  if (Group[pn])
   continue;
  ++iGroup;
  /*
   ** Mark it and add to the do-fifo.
   */
  Group[pn] = iGroup;
  Fifo[iTail++] = pn;
  if (iTail == nFifo)
   iTail = 0;
  while (iHead != iTail) {
   pi = Fifo[iHead++];
   if (iHead == nFifo)
    iHead = 0;
   /*
    ** Now do an fEps-Ball Gather!
    */
   x = p[pi].r[0];
   y = p[pi].r[1];
   z = p[pi].r[2];
   cp = ROOT;
   while (1) {
    INTERCONT(c, cp, fTau2, lx, ly, lz, x, y, z, sx, sy, sz);
    /*
     ** We have an intersection to test.
     */
    if (c[cp].iDim >= 0) {
     cp = LOWER(cp);
     continue;
    } else {
     for (pj = c[cp].pLower; pj <= c[cp].pUpper; ++pj) {
      if (Group[pj])
       continue;
      dx = sx - p[pj].r[0];
      dy = sy - p[pj].r[1];
      dz = sz - p[pj].r[2];
      fDist2 = dx * dx + dy * dy + dz * dz;
      if (fDist2 < fTau2) {
       /*
        ** Mark it and add to the do-fifo.
        */
       Group[pj] = iGroup;
       Fifo[iTail++] = pj;
       if (iTail == nFifo)
        iTail = 0;
      }
     }
     SETNEXT(cp);
     if (cp == ROOT)
      break;
     continue;
    }
  ContainedCell:
    for (pj = c[cp].pLower; pj <= c[cp].pUpper; ++pj) {
     if (Group[pj])
      continue;
     /*
      ** Mark it and add to the do-fifo.
      */
     Group[pj] = iGroup;
     Fifo[iTail++] = pj;
     if (iTail == nFifo)
      iTail = 0;
    }
  GetNextCell:
    SETNEXT(cp);
    if (cp == ROOT)
     break;
   }
  }
 }
 free(Fifo);
 kd->nGroup = iGroup + 1;
 kd->piGroup = (int *) malloc(kd->nParticles * sizeof(int));
 assert(kd->piGroup);
 for (pi = 0; pi < kd->nParticles; ++pi) {
  kd->piGroup[pi] = 0;
 }
 for (pi = 0; pi < kd->nActive; ++pi) {
  kd->piGroup[p[pi].iOrder] = Group[pi];
 }
 free(Group);
 if (kd->bOutDiag)
  puts("<< kdFoF()");
 fflush(stdout);
}



void kdFinish(KD kd)
{
 if (kd->pMove)
  free(kd->pMove);
 if (kd->pInit)
  free(kd->pInit);
 if (kd->pGroup)
  free(kd->pGroup);
 if (kd->kdNodes)
  free(kd->kdNodes);
 if (kd->piGroup)
  free(kd->piGroup);
 free(kd);
}
