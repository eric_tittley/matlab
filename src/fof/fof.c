#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <mex.h>

#include "kd.h"

/* HISTORY ***************************************************
 * 02 02 06 Eric Tittley                                     *
 *  Variable Ngroups changed to nGroup, since nGroup is the  *
 *  variable returned by kdFoF                               *
 *  Also had to make nGroup a pointer, otherwise we lose the *
 *  contents.
 *************************************************************/

void usage(void) {
 printf("USAGE:\n");
 mexErrMsgTxt("fof(r,LinkLen,nMembers,Period,v_flag,nGroup,GroupNum)");
}

int fof(
 double *r,    /* particle positions (3xN) */
 int N,        /* number of particles */
 double fEps, /* Link Length */
 int nMembers, /* Minimum number of members */
 int bVerbose, /* Verbose Flag */
 double *fPeriod, /* Periodicity (1x3 vector) */
 int *nGroup, /* The number of groups found */
 double *GroupNum /* The group number of each particle */
) {

/* the structure used by the Kraft Dinner routines */
 KD kd;

/* for kdInit */
 int nBucket;

/* for the replacement of kdReadTipsy */
 int i,j,nCnt;

/* returned by kdFoF */
/* int nGroup; */ /* Don't need to re-define it. ET */

/* used by kdTime */
 int sec,usec;

/*  */ 
 nBucket = 16;
 kdInit(&kd,nBucket,fPeriod);

/* kdReadTipsy(kd,stdin,bDark,bGas,bStar); */
/* the following replaces the previous line */
 kd->nParticles = N;
 kd->nDark = N;
 kd->nGas = 0;
 kd->nStar = 0;
 kd->fTime = 1;
 kd->nActive = 0;
 kd->nActive = N;
 kd->bDark = 1;
 kd->bGas = 0;
 kd->bStar = 0;
 /* Copy the positions into Kraft Dinner format (a wasteful step) */
 /* Allocate particles */
 kd->p = (PARTICLE *)malloc(kd->nActive*sizeof(PARTICLE));
 if(kd->p == NULL) { mexErrMsgTxt("fof: failed to allocate memory for kd->p"); }
 nCnt = 0;
 for (i=0;i<N;++i) {
  kd->p[nCnt].iOrder = nCnt;
  for (j=0;j<3;++j) {
   kd->p[nCnt].r[j] = r[i*3+j];
  }
  ++nCnt;
 }
/* end replace */

 kdBuildTree(kd);
 kdTime(kd,&sec,&usec);
 *nGroup = kdFoF(kd,fEps);
 kdTime(kd,&sec,&usec);
 if (bVerbose) { printf("Number of initial groups:%d\n",*nGroup); }
 *nGroup = kdTooSmall(kd,nMembers);
 if (bVerbose) {
  printf("Number of groups:%d\n",*nGroup);
  printf("FOF CPU TIME: %d.%06d secs\n",sec,usec);
 }
 kdOrder(kd);
/*  kdOutGroup(kd,ach); */
/* the following replaces the previous line */
 nCnt = 0;
 for (i=0;i<N;++i) {
  GroupNum[i]=(double)kd->p[nCnt++].iGroup;
 }
/* end replacement */

 kdFinish(kd);
 return(0);
}
