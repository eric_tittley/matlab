/* function [GroupNum,NGroups]=fof(r,eps,MinMembers,Period,Verb_flag)
*
* A Group Finder for N-body Simulations
*
*
* This is a port of FOF v1.1 (1994) to Matlab
*
* (http:www-hpcc.astro.washington.edu/tools/FOF/)
* 
*
*
*/

/* AUTHOR: Eric Tittley (port to Matlab) */

/******** HISTORY ****************************************************
 * 02 02 06 Changed Ngroups into a pointer.  Otherwise, the value is *
 *          lost.                                                    *
 *          Cast Ngroups_INT as a (float).  Was 1.0*Ngroups_INT      *
 *********************************************************************/

#include <mex.h>
#include <stdio.h>
#include <math.h>

int fof(
 double *r,    /* particle positions (3xN) */
 int N,        /* number of particles */
 double fEps, /* Link Length */
 int nMembers, /* Minimum number of members */
 int bVerbose, /* Verbose Flag */
 double *fPeriod, /* Periodicity */
 int *Ngroups, /* The number of groups found */
 double *GroupNum /* The group number of each particle */
);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {
 double *r;
 size_t N;
 double fEps;
 int nMembers;
 int bVerbose;
 double Period,fPeriod[3];
 double *Ngroups;
 int Ngroups_INT;
 double *GroupNum;

 int j;
 int status;
 size_t mrows,ncols;
 
 /* Check for the proper number of arguments */
 if(nrhs!=5 || nlhs!=2 ) {
  mexErrMsgTxt("[GroupNum,NGroups]=fof(r,eps,MinMembers,Period,Verb_flag)");
 }

 /* The inputs must be of the type double */
 if(   !mxIsDouble(prhs[0])
    || !mxIsDouble(prhs[1])
    || !mxIsDouble(prhs[2])
    || !mxIsDouble(prhs[3])
    || !mxIsDouble(prhs[4])
   ) {
  mexErrMsgTxt("all arguments must be double");
 }

 /* The 2nd, 3rd, 4th, and 5th arguments must be scalar */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("eps must be scalar");
 }
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("MinMembers must be scalar");
 }
 mrows = mxGetM(prhs[3]);
 ncols = mxGetN(prhs[3]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("Period must be scalar");
 }
 mrows = mxGetM(prhs[4]);
 ncols = mxGetN(prhs[4]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("Verb_flag must be scalar");
 }

 /* The first argument must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 N = mxGetN(prhs[0]);
 if(mrows!=3 || N<11) {
  mexErrMsgTxt("r must be a 3xN array with N>10");
 }

 /* Create matrix for the return arguments and assign a pointer */
 plhs[0]=mxCreateDoubleMatrix(1,N, mxREAL);
 GroupNum = mxGetPr(plhs[0]);
 plhs[1]=mxCreateDoubleMatrix(1,1, mxREAL);
 Ngroups = mxGetPr(plhs[1]);

 /* Assign pointers to the input */
 r        =          mxGetPr(prhs[0]);
 fEps     =        *(mxGetPr(prhs[1]));
 nMembers = (long)(*(mxGetPr(prhs[2])));
 Period   =        *(mxGetPr(prhs[3]));
 bVerbose = (long)(*(mxGetPr(prhs[4])));
 
 for (j=0;j<3;j++) { fPeriod[j] = Period; }

/* the MAIN SUBROUTINE */
 status=fof(
  /* Input */
  r, (int)N, fEps, nMembers, bVerbose, fPeriod,
  /* Output */
  &Ngroups_INT, GroupNum
 );
 if(status!=0) { mexErrMsgTxt("fof returned non-zero status"); }

 *Ngroups=(double)Ngroups_INT;

}
