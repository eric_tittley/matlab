% [GroupNum,NGroups]=fof(r,eps,MinMembers,Period,Verb_flag)
%
% A Group Finder for N-body Simulations
% This is a port of FOF v1.1 (1994) to Matlab
% (http:www-hpcc.astro.washington.edu/tools/FOF/)
%
% INPUT ARGUMENTS:
%
% r	     A (3xN) array of values representing the particle positions
%
% eps	     This argument specifies  the  linking  length  used  by  the
%            friends-of-friends method.
%
% MinMembers The  minimum number  of  members  (particles)  per  group. All
%            groups with less than this value will be discarded.
%
% Period     The periodicity of the particle positions.  If the simulation 
%            does not have periodic boundary conditions, set this to a large
%            number.
%
% Verb_flag  Verbose flag. Allows the user to enable  diagnostic  output.
%            This  includes the number of groups   found by the
%            friends-of-friends method, the number of groups meeting  the
%            minimum  members  criterion  and also the CPU time needed to
%            find the groups.
%
% OUTPUT ARGUMENTS:
%
% GroupNum   A vector of length N containing the group number to which each
%            particle belongs.
%
% NGroups    The number of groups.
