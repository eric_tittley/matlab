% powerspec: The powerspectrum of a 3-d distribution of points
%
% syntax: [k,p]=powerspec(r,L);
%
% ARGUMENTS
%  r	The point positions [3,N] real*8
%  L    The resolution (cells/side) of the grid on which to find the wavenumbers
%
% RETURNS
%  k	The wave number [L^3], real*8
%  p	The power at that wave number [L^3] real*8
%
% Note, this is a MEX-file, and must be compiled before it can
% be run.

% AUTHOR: Eric Tittley
%
% HISTORY
%  Written some time in the late 90s
