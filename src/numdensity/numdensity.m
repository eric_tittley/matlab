function NumDens=numdensity(x,y,Nsrch,Res,Limits)
% syntax: NumDens=numdensity(x,y,Nsrch,Res,Limits);
%
% ARGUMENTS
%  x,y          List of points in the plane used to contribute to the density
%  Nsrch        Number of points to contribute to the density estimate
%  Res          Requested size of the array [Res x Res] to return
%  Limits       Limits to the node positions, as
%                Limits = [x_lo x_hi y_lo y_hi]
%
% RETURNS
%  A [Res x Res] array, NumDens, corresponding to the number density near the
%  nodes of the array of the data points distributed in x and y.
%  Numberdensity is in units of number of points per area defined by Limits.
%  So, if a value is N at position i & j, then if all values were N,
%  everywhere, then the area contained within Limits would have N points.
%
% The number density is simply defined as:
%  if kernel weighting has been compiled in (numdensity):
%   Sum( W(r_i,Radius) )/(pi * Radius^2)
%  But you can think of it as:
%   Nsrch/(pi * Radius^2)
% where Radius is the radius of a circle about a node point which
% contains Nsrch points.
%
% Nsrch is typically 10 or more.
%
% This calls a MEX routine
