      real*8 FUNCTION kernel_2d(x)
C Retuns the profile of the Hydra spline kernel, projected onto a plane.
C
C x == r/h where h is the smoothing length.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  01 11 01  A mature code.  Fixed a bug in which kernel_2d was <0 for
C       1.989 < x < 2.0.  Increased p2(1) to 0.98100114077244 from
C       0.98098128434920.  The correction makes for a smoother transition
C       at x=1.0.
C       The integral is now 1.00069978 (should be 1)

      IMPLICIT NONE
      
      real*8 x
      real*8 p1(4)
      real*8 p2(4)
      

      data p1/0.476717909787056d0,  0.020248192026407d0,
     &       -0.799825478212311d0,  0.414198993939548d0 /
      data p2/0.980295287490319d0, -1.517509906222201d0,
     &        0.783354388139954d0, -0.134839108767931d0/
     
      if( x.ge.0.0d0 .and. x.le.1.0d0 ) then
       kernel_2d = p1(1) + p1(2)*x + p1(3)*x*x + p1(4)*x*x*x
      elseif( x.lt.2.0d0 ) then
       kernel_2d = p2(1) + p2(2)*x + p2(3)*x*x + p2(4)*x*x*x
      else
       kernel_2d = 0.0d0
      endif
      
      RETURN
      END
