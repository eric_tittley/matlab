/* Gateway function for H_PROJ_CORE
 *
 * MATLAB sytax: [A,unresolved,resolved]=h_proj_core(x,y,z,h,L,N,RBCFlag)
 */

#include <mex.h>

#include <stdio.h>

void h_proj_core_(double *x, double *y, double *z, double *h,
                  int *L, int *N, int *RBCFlag,
                  double *A, int *unresolved, int *resolved);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 /* Pointers to Left and Right hand side arguments */
  double *A;
  double *unresolvedr;
  double *resolvedr;
  double *x;
  double *y;
  double *z;
  double *h;
  double *Lr;
  double *Nr;
  double *RBCFlagr;
  
 /* Temporary local variables */
 int L,N,unresolved,resolved;
 mwSize nobjx,nobjy,nobjz,nobjh;
 mwSize dumx,dumy,dumz,dumh;
 int RBCFlag;

 /* Check for proper number of arguments */
 if( (nrhs!=7) || (nlhs>3) ){
  mexErrMsgTxt("syntax: [A,unresolved,resolved]=h_proj_core(x,y,z,h,L,N,RBCFlag)");
 }

 /* All arguments must be double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("x must be a double.");
 }
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("y must be a double.");
 }
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("z must be a double.");
 }
 if( !mxIsDouble(prhs[3]) ) {
  mexErrMsgTxt("h must be a double.");
 }
 if( !mxIsDouble(prhs[4]) ) {
  mexErrMsgTxt("L must be a double.");
 }
 if( !mxIsDouble(prhs[5]) ) {
  mexErrMsgTxt("N must be a double.");
 }
 if( !mxIsDouble(prhs[6]) ) {
  mexErrMsgTxt("RBCFlag must be a double.");
 }

 /* Get the dimensions of x,y,z, and h */
 nobjx = mxGetN(prhs[0]);
 dumx  = mxGetM(prhs[0]);
 nobjy = mxGetN(prhs[1]);
 dumy  = mxGetM(prhs[1]);
 nobjz = mxGetN(prhs[2]);
 dumz  = mxGetM(prhs[2]);
 nobjh = mxGetN(prhs[3]);
 dumh  = mxGetM(prhs[3]);
 if(nobjx>1 && dumx>1) {
  mexErrMsgTxt("x must be a vector, not an array");
 }
 if(nobjy>1 && dumy>1) {
  mexErrMsgTxt("y must be a vector, not an array");
 }
 if(nobjz>1 && dumz>1) {
  mexErrMsgTxt("z must be a vector, not an array");
 }
 if(nobjh>1 && dumh>1) {
  mexErrMsgTxt("h must be a vector, not an array");
 }

 /* Get the number of objects in x */
 if( nobjx < dumx ) nobjx=dumx;

 /* Get the number of objects in y,z, and h */
 if( nobjy < dumy ) nobjy=dumy;
 if( nobjz < dumz ) nobjz=dumz;
 if( nobjh < dumh ) nobjh=dumh;
 if( nobjy!=nobjx || nobjz!=nobjx || nobjh!=nobjx ) {
  mexErrMsgTxt("x,y,z, and h must have the same number of elements");
 }

 /* Assign pointers to the arguments */
 x  = mxGetPr(prhs[0]);
 y  = mxGetPr(prhs[1]);
 z  = mxGetPr(prhs[2]);
 h  = mxGetPr(prhs[3]);
 Lr = mxGetPr(prhs[4]);
 Nr = mxGetPr(prhs[5]);
 RBCFlagr = mxGetPr(prhs[6]);

 /* Copy right hand arguments to local arrays or variables */
 L=(int)(*Lr);
 N=(int)(*Nr);
 RBCFlag = (int)(*RBCFlagr);

 /* Create a matrices for return arguments */
 plhs[0] = mxCreateDoubleMatrix((mwSize)L,(mwSize)L,mxREAL);
 A = mxGetPr(plhs[0]);
 plhs[1] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
 unresolvedr = mxGetPr(plhs[1]);
 plhs[2] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
 resolvedr = mxGetPr(plhs[2]);

 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 h_proj_core_(x,y,z,h,&L,&N,&RBCFlag,A,&unresolved,&resolved);

 /* COPY OUTPUT WHICH IS STORED IN LOCAL ARRAY TO MATRIX OUTPUT */
 *resolvedr   = (double)resolved;
 *unresolvedr = (double)unresolved;

}
