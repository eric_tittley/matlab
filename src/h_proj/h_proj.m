function [A,unresolved,resolved]=h_proj(x,y,z,h,L,Limits,RBCFlag)
% Project the value of z at x,y over a range of cells using a smoothing kernel.
%
% A=h_proj(x,y,z,h,L,Limits,RBCFlag)
% ARGUMENTS
%  Input
%   x	N-vector of x positions.
%   y	N-vector of y positions.
%   z	N-vector of z values.  Can be mass, temperature, velocity, ...
%   h	N-vector of smoothing radii.
%   L	The number of cells per side in the final image, A.
%   Limits (optional)	4-vector. The limits of the final image.
%	Limits = [x_min x_max y_min y_max]
%	Eg [0 1 0 1], [0.3 0.6 0.4 0.7]. 
%	Important condition: Limits(3)-Limits(2) == Limits(4)-Limits(3)
%	Otherwise, the routine will use flattened smoothing kernels
%	with potentially unwanted effects.
%   RBCFlag (optional) Repeating Boundary COnditions flag. 0=>off, otherwise on.
%	Default off.
%  Output
%   A		LxL array storing the sum of the projected values of z.
%   unresolved	Number of particles with h small enough that z fits in 1 cell.
%   resolved	Number of particles with h big enough that z is spread over
%		more than 1 cell.
%
% SEE ALSO
%  h_proj_3d

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 11 12 Removed A=flipud(A) line
%  02 10 31 Added comments
%  07 04 10 Added RBCFlag

resolved=0; unresolved=0;

%normalize x and y to be on 0:1
if(nargin<6)
 max_x=max(x); min_x=min(x);
 max_y=max(y); min_y=min(y);
else
 min_x=Limits(1); max_x=Limits(2);
 min_y=Limits(3); max_y=Limits(4);
end
if(nargin<7)
 RBCFlag = 0;
end

dx=1.000001*(max_x-min_x); dy=1.000001*(max_y-min_y);
if(dx==0) disp('dx=0')
else x=(x-min_x)/dx;
end
if(dy==0) disp('dy=0')
else y=(y-min_y)/dy;
end

%put x and y on 0:L
x=x*L; y=y*L;

%put h on the same scale. Note, we are assuming that dx=dy.
h=h/mean([dx,dy])*L;

%This next for loop sucks up all the time.
N=length(x);
[A,unresolved,resolved]=h_proj_core(x,y,z,h,L,N,RBCFlag);
disp(['efficiency=',num2str(resolved/(resolved+unresolved))])
