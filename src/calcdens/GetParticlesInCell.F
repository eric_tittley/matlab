      SUBROUTINE GetParticlesInCell(L,N,ix,iy,iz,ll,ll_start,
     &                              Ncounts,counter,sorted_index)
C Find the particles in a cell given a link-list containing the cells for
C each particle.
C
C ARGUMENTS
C  Input, not modified
C    L  The number of cells per dimension in the base mesh.
C    N  The number of particles (the length of the link-list)
C    ix,iy,iz   The position of the cell.  ix,iy,iz span 0 to L-1
C    ll(N)      The link-list.  Each element of ll contains the index
C               of the next element in the list.  0 => end of list.
C    ll_start(0:L-1,0:L-1,0:L-1)
C               For each cell, the first element in link-list
C  Input, modified
C    Ncounts    The expected number of elements in the cell.
C               Set to 0 after completion.
C  Output, no initialisation required
C    counter    The number of elements in the cell.
C    sorted_index(counter)      The elements in the cell.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  01 06 08 First version ?
C  02 09 10 Removed declaration of unused variable 'k'
C       Added comments

      IMPLICIT NONE
c Input, not modified
#if defined (__x86_64) || defined (__amd64__)
      integer*8 N
      integer*8 L
#else
      integer N
      integer L
#endif
      integer ix,iy,iz
      integer ll(N)
      integer ll_start(0:L-1,0:L-1,0:L-1)
c Input, modified
      integer Ncounts(0:L-1,0:L-1,0:L-1)
c Output, no initialisation required
      integer counter
      integer sorted_index(N)

c Local variables
      integer i,j

c linked-list loop through particles in the cell, adding them to the list
      j=ll_start(ix,iy,iz)
      do i=1,Ncounts(ix,iy,iz)
#ifdef DEBUG
       if(j.eq.0) then
        print *,i,ix,iy,iz,Ncounts(ix,iy,iz)
        stop 'GetParticlesInCell: j==0 means inconsistency with Ncounts'
       endif
#endif
       counter=counter+1
       sorted_index(counter)=j
       j=ll(j)
      enddo

c     Mark cell as done.
      Ncounts(ix,iy,iz)=0

      RETURN
      END
