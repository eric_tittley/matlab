      SUBROUTINE ApplyIndex(Ndim,N,index,A,TempStorage)
C Re-indexes the A(N) or A(3,N) array using index
C
C ARGUMENTS
C  Input, not modified
C   Ndim      The number if dimensions.  If 1, then A(N) assumed.
C              Otherwise A(3,N)
C   N         The number of elements in the list.
C   index(N)  The index to resort by.
C  Input, modified
C   A(N) or A(3,N)      The array to re-index.
C
C AUTHOR: Eric Tittley

      IMPLICIT NONE

c Input, not modified
      integer*8 N
      integer Ndim
      integer index(N)
c Input, modified
c Note that we are over-dimensioning things here if A is an A(N) vector
      real A(3*N)

c Pre-allocated local storage
      real TempStorage(3*N)

c Local variables
      integer*8 i,j

      if(Ndim.eq.1) then

!$OMP PARALLEL
!$OMP+DEFAULT(none)
!$OMP+SHARED(TempStorage,index,A,N)
!$OMP+PRIVATE(i)
!$OMP DO
       do i=1,N
        TempStorage(i)=A(index(i))
       enddo
!$OMP END DO
!$OMP END PARALLEL

!$OMP PARALLEL
!$OMP+DEFAULT(none)
!$OMP+SHARED(TempStorage,A,N)
!$OMP+PRIVATE(i)
!$OMP DO
       do i=1,N
        A(i)=TempStorage(i)
       enddo
!$OMP END DO
!$OMP END PARALLEL

      else

!$OMP PARALLEL
!$OMP+DEFAULT(none)
!$OMP+SHARED(TempStorage,index,A,N)
!$OMP+PRIVATE(i,j)
!$OMP DO
       do i=1,N
        do j=1,3
         TempStorage((i-1)*3+j)=A((index(i)-1)*3+j)
        enddo
       enddo
!$OMP END DO
!$OMP END PARALLEL

!$OMP PARALLEL
!$OMP+DEFAULT(none)
!$OMP+SHARED(TempStorage,A,N)
!$OMP+PRIVATE(i)
!$OMP DO
       do i=1,3*N
        A(i)=TempStorage(i)
       enddo
!$OMP END DO
!$OMP END PARALLEL

      endif

      RETURN
      END
