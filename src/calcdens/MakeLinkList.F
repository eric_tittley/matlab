      SUBROUTINE MakeLinkList(N,cell_index,L,ll_count,ll_start,ll,
     &                        ll_holder)
C Given the cell index of each particle, cell_index, (expressed
C as a 3-digit base-L integer) find the number of particles in
C each of the cells and compile a link list.
C
C ARGUMENTS
C  Input, not modified
C   N   The number of particles
C   L   The number of mesh cells per dimension
C   cell_index(N)       The cell index of each particle, expressed
C       as a 3-digit base-L integer
C  Output
C   ll_count(0:L-1,0:L-1,0:L-1) The number of elements in each cell
C   ll_start(0:L-1,0:L-1,0:L-1) The starting element in each cell
C   ll(N)       The link list, itself.  Each element of the link-list contains
C                the index to the next element.  If ll=0, then it is the last
C                element in the list for that cell.
C  Pre-allocated storage
C   ll_holder(L,L,L)
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  02-11-11
C       Let compiler find psize.inc
C       Add IMPLICIT NONE
C  04 11 09 Checked with ftnchek. Initialised ll_holder
C  07 01 23 Remove need for psize.inc.  Use calcdens.inc
C  13 05 20 Removed need for calcdens.inc.  Big arrays must be pre-allocated.

      IMPLICIT NONE
c Input, not modified
      integer*8 N
      integer*8 L
      integer*8 cell_index(N)
c Output, modified
      integer ll_count(0:L-1,0:L-1,0:L-1)
      integer ll_start(0:L-1,0:L-1,0:L-1)
      integer ll(N)

c Pre-allocated local storage
      integer ll_holder(0:L-1,0:L-1,0:L-1)

c local variables
      integer*8 L2,ix,iy,iz,i,dummy,particle

      L2=L*L
c Initialize arrays
      do iz=0,L-1
       do iy=0,L-1
        do ix=0,L-1
         ll_count(ix,iy,iz)=0
        enddo
       enddo
      enddo
      do iz=0,L-1
       do iy=0,L-1
        do ix=0,L-1
         ll_start(ix,iy,iz)=0
        enddo
       enddo
      enddo
      do iz=0,L-1
       do iy=0,L-1
        do ix=0,L-1
         ll_holder(ix,iy,iz)=-1
        enddo
       enddo
      enddo
      do i=1,N
       ll(i)=0
      enddo
      
      do i=1,N
       dummy=cell_index(i)
       iz=int(dummy/L2)
       if( (iz.lt.0 ) .or. (iz.gt.(L-1)) ) then
        print *,'cell_index(',i,')=',cell_index(i),'; iz=',iz
       endif
       iy=int((dummy-iz*L2)/L)
       if( (iy.lt.0 ) .or. (iy.gt.(L-1)) ) then
        print *,'cell_index(',i,')=',cell_index(i),'; iy=',iy
       endif
       ix=dummy-iz*L2-iy*L
       if( (ix.lt.0 ) .or. (ix.gt.(L-1)) ) then
        print *,'cell_index(',i,')=',cell_index(i),'; ix=',ix
       endif
       if(ll_start(ix,iy,iz).gt.0) then
        particle=ll_holder(ix,iy,iz)
       if( (particle.lt.1 ) .or. (particle.gt.N) ) then
        print *,'particle=',particle
       endif
        ll(particle)=int(i)
       else
        ll_start(ix,iy,iz)=int(i)
       endif
       ll_count(ix,iy,iz)=ll_count(ix,iy,iz)+1
       ll_holder(ix,iy,iz)=int(i)
      enddo

      RETURN
      END
