      SUBROUTINE ListNSmallest(NumNeighbours,Nsph,dist,indx)
C Find index to the Nsph smallest values of dist.
C
C ARGUMENTS
C  Input, not modified
C   NumNeighbours	The number of elements in dist
C   Nsph		The number of smallest elements to index
C   dist		The elements to sort
C
C  Input and initialised
C   indx		The indices of the Nsph smallest values of dist.
C			indx must be allocated previously.
C
C AUTHOR: Eric Tittley
C
C HISTORY: 02-02-19 First version

      IMPLICIT NONE
      integer NumNeighbours,Nsph,indx(NumNeighbours)
      real dist(NumNeighbours)
      
      real max
      integer i,himarker,lomarker
      
      do i=1,NumNeighbours
       indx(i)=i
      enddo
      
c     In the 1st Nsph elements, find the largest value.      
      max=dist(1)
      do i=2,Nsph
       if(dist(i).gt.max)) then
        max=dist(i)
       endif
      enddo
c     Necessarily, the smallest Nsph will all have to be less than max.

      himarker=NumNeighbours
      lomarker=Nsph+1

c     Move all the indices for values bigger or equal to max to the end of
c     the list swapping them with indices of values less than max.
      do while(lomarker.lt.himarker)
       do while(dist(himarker).ge.max .and. himarker.gt.lomarker)
        himarker=himarker-1
       enddo
c      himarker points to an element < max
       do while(dist(lomarker).lt.max .and. lomarker.lt.himarker)
        lomarker=lomarker+1
       enddo
c      lomarker points to an element >= max
       if(lomarker.lt.himarker) then
        indxt=indx(himarker)
        indx(himarker)=indx(lomarker)
        indx(lomarker)=indxt
        himarker=himarker-1
        lomarker=lomarker+1
       endif
      enddo

----- not done ----
