      SUBROUTINE FindStartPosition(L,Ncounts,indx,indx_counter,ix,iy,iz)
C Given the counts in the cells, Ncounts, find the cell with the
C peak number.
C ix == -1 if no peak is found

C HISTORY:
C  01 05 14 1st version
C  01 05 15 Modified to use a index into the Ncounts, sorted by number

      IMPLICIT NONE
c input, not modified
#if defined (__x86_64) || defined (__amd64__)
      integer*8 L
#else
      integer L
#endif
      integer Ncounts(0:L-1,0:L-1,0:L-1)
      integer*8 indx(L*L*L)
c output, modified
      integer*8 indx_counter
      integer ix,iy,iz
      
c local variables
      integer L2,i
      integer*8 j,dummy
      
      L2=L*L

c set ix to -1, which is the indicator to the outside world that
c no maximum was found
      ix=-1

c Loop through the index, starting where we last left off, looking
c for the first non-zero entry
      do j=indx_counter,L*L*L
       dummy=indx(j)
       iz=int(dummy/L2)
       iy=int((dummy-iz*L2)/L)
       i =int(dummy-iz*L2-iy*L)
       if(Ncounts(i,iy,iz).gt.0) then
        ix=i
        goto 1
       endif
      enddo
1     continue
      indx_counter=j

      RETURN
      END
