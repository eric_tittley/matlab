      SUBROUTINE neighbourlist(N,Ldim,L,L1,L2,L3,ll_start,ll,
     &                         elements,count)
C Finds the list of particles in a box and surrounding boxes.
C
C ARGUMENTS
C  Input, not modified
C   N
C   Ldim
C   L                           The number of cells per dimension in the mesh
C   L1,L2,L3                    The index of the cell in question
C   ll_start(Ldim,Ldim,Ldim)    The list of starting elements for each cell
C   ll(Nmax)                    The link-list
C  Output
C   elements(N)                 The list of particles in the cell and           C                               neigbouring cells.
C   count                       The number of particles in the list, elements.
C
C AUTHOR: Eric Tittley

      IMPLICIT NONE
c Arguments input, not modified
      integer*8 N
      integer*8 Ldim
      integer*8 L,L1,L2,L3
      integer ll_start(Ldim,Ldim,Ldim),ll(N)
c Arguments output, modified
      integer elements(N,1),count

c local variables
      integer*8 i
      integer   j
      integer*8 dL1,dL2,dL3
      integer*8 bL1,bL2,bL3

      count=0
      if(L.ge.4) then
c      We are looking for a subset of the entire set of particles
c      Loop over surrounding boxes to find neighbours
       do dL1=-1,1
        bL1=L1+dL1
        if(bL1.eq.0) then 
         bL1=L
        elseif(bL1.eq.(L+1)) then
         bL1=1
        endif
        do dL2=-1,1
         bL2=L2+dL2
         if(bL2.eq.0) then
          bL2=L
         elseif(bL2.eq.(L+1)) then
          bL2=1
         endif
         do dL3=-1,1
          bL3=L3+dL3
          if(bL3.eq.0) then
           bL3=L
          elseif(bL3.eq.(L+1)) then
           bL3=1
          endif
c         Loop over particles in a surrounding box
          j=ll_start(bL1,bL2,bL3)
          do while(j.ne.0)
           count=count+1
           elements(count,1)=j
           j=ll(j)
          enddo
c         Finshed looping over particles in a surrounding box
         enddo
        enddo
       enddo
c      End looping over 27 surrounding boxes
      else ! elements will contain all the particles
C Could not this be done simply by:
       do i=1,N
        elements(i,1)=int(i)
       enddo
       count=int(N)
C       do bL1=1,L
C        do bL2=1,L
C         do bL3=1,L
Cc         Loop over particles in a surrounding box
C          j=ll_start(bL1,bL2,bL3)
C          do while(j.ne.0)
C           count=count+1
C           elements(count)=j
C           j=ll(j)
C          enddo
Cc         Finshed looping over particles in a surrounding box
C         enddo
C        enddo
C       enddo
Cc      End looping over 27 surrounding boxes
      endif ! if L>=4, else, end

      RETURN
      END
