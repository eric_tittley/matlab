function A=h_proj_3d(r,z,h,L,Limits)
% A = h_proj_3d(r,z,h,L,Limits)
%
% ARGUMENTS
%  r       positions of the particles. [3,N]
%  z       parameter to be projected.  [N]
%  h       smoothing length.           [N]
%  L       grid size.                  [scalar]
%  Limits  Range of r to be gridded.   [xlo xhi ylo yhi zlo zhi]
%
% RETURNS
%  A          The grid of the sum of the projections of the individual
%             particles projected onto the grid by their smoothing lengths.
%             [L,L,L] (for units, see NOTES)
%
% NOTES
%  Particles should be from a box in which dx=dy=dz.
%  That is, it should be square in the x-y direction.
%
%  Units:
%   If all particles are within the Limits, then sum(A(:))=sum(z)
%
% SEE ALSO
%  h_proj, h_proj_multi, h_proj_2d, h_proj_3d

% AUTHOR: Eric Tittley

%normalize x and y to be on 0:1
if(nargin<5)
 min_r=min(r); max_r=max(r)*1.000001;
else
 min_r=Limits([1 3 5]); max_r=Limits([2 4 6]);
end

dr=(max_r-min_r);
if(sum(dr==0)>0)
 disp('dr=0')
else
 r(1,:)=(r(1,:)-min_r(1))/dr(1);
 r(2,:)=(r(2,:)-min_r(2))/dr(2);
 r(3,:)=(r(3,:)-min_r(3))/dr(3);
end

% Check to see that r is between 0 and 1
if(sum(sum(r<0 | r>=1)))
 error('Not all r fit within Limits')
end

% Put r on 0:L
r=r*L;

% Put h on the same scale. Note, we hope that dx=dy=dz.
h=h/mean(dr)*L;

% Calculate the number of particles
[dummy,N]=size(r);
if( (dummy~=3) && (N==3) )
 r=r';
end
N=size(r,2);

% The next line does all the work
A=h_proj_3d_core(double(r),double(z),double(h), double(L),double(N));

% A is a vector A(1:L^3).  Make it A(L,L,L)
A=reshape(A,L,L,L);
