#include <mex.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "deproj.h"

/* GATEWAY ROUTINE */
/* function [X,Y,Z,tolerance]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol) */
/* Note: All arguments are integer values, but matlab passes only doubles */
void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
)
{
 /* Output */
 double *Xr, *Yr, *Zr, *tolerance_r;
 /* Input */
 double *X1r, *Y1r, *X2r, *Z2r, *Y3r, *Z3r, *max_tol_r;

 /* Local Variables */
 int max_tol;
 int count=0; /* The number of halos found my deproj() */
 int i;
 const mwSize * lengthVector;
 mwSize length1; /* The length of X1 & Y1 */
 mwSize length2; /* The length of X2 & Z2 */
 mwSize length3; /* The length of Y3 & Z3 */
 /* These will be mxMalloc'ed */
 int *X1, *Y1, *X2, *Z2, *Y3, *Z3 ; // # of elements: length
 int *X, *Y, *Z, *tolerance; // # of elements: MAX_CLUSTERS

 /* Check input arguments */
 if(nrhs!=7) {
  mexErrMsgTxt("Seven input arguments required in call to deproj()");
 }
 if(nlhs!=4) {
  mexErrMsgTxt("Four output arguments required in call to deproj()");
 }

 /* Get the length of the X1,Y1,etc vectors */
 lengthVector = (mwSize *)mxGetDimensions(prhs[0]);
 /* if N>M, then set M to be N's memory address (N's value). */
 if(lengthVector[0] > lengthVector[1]) {
  length1 = lengthVector[0];
 } else {
  length1 = lengthVector[1];
 }
 /* Get the length of the X1,Y1,etc vectors */
 lengthVector = (mwSize *)mxGetDimensions(prhs[2]);
 /* if N>M, then set M to be N's memory address (N's value). */
 if(lengthVector[0] > lengthVector[1]) {
  length2 = lengthVector[0];
 } else {
  length2 = lengthVector[1];
 }
 /* Get the length of the X1,Y1,etc vectors */
 lengthVector = (mwSize *)mxGetDimensions(prhs[4]);
 /* if N>M, then set M to be N's memory address (N's value). */
 if(lengthVector[0] > lengthVector[1]) {
  length3 = lengthVector[0];
 } else {
  length3 = lengthVector[1];
 }

 /* Associate the input matrices with the function arguments */ 
 X1r=mxGetPr(prhs[0]);
 Y1r=mxGetPr(prhs[1]);
 X2r=mxGetPr(prhs[2]);
 Z2r=mxGetPr(prhs[3]);
 Y3r=mxGetPr(prhs[4]);
 Z3r=mxGetPr(prhs[5]);
 max_tol_r=mxGetPr(prhs[6]);

 /* Allocate space for the incoming vectors */
 X1 = (int *) mxMalloc( length1 *sizeof(int));
 if(X1==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Y1 = (int *) mxMalloc( length1 *sizeof(int));
 if(Y1==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 X2 = (int *) mxMalloc( length2 *sizeof(int));
 if(X2==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Z2 = (int *) mxMalloc( length2 *sizeof(int));
 if(X2==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Y3 = (int *) mxMalloc( length3 *sizeof(int));
 if(Y3==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Z3 = (int *) mxMalloc( length3 *sizeof(int));
 if(Z3==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }

 /* store the (real) input arguments into the local input arguments */
 for(i=0;i<length1;i++) {
  X1[i]=floor(X1r[i]);
  Y1[i]=floor(Y1r[i]);
 }
 for(i=0;i<length2;i++) {
  X2[i]=floor(X2r[i]);
  Z2[i]=floor(Z2r[i]);
 }
 for(i=0;i<length3;i++) {
  Y3[i]=floor(Y3r[i]);
  Z3[i]=floor(Z3r[i]);
 }
 max_tol = floor(*max_tol_r);

 /* Allocate memory for output vectors */
 X = (int *) mxMalloc( MAX_CLUSTERS *sizeof(int));
 if(X==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Y = (int *) mxMalloc( MAX_CLUSTERS *sizeof(int));
 if(Y==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Z = (int *) mxMalloc( MAX_CLUSTERS *sizeof(int));
 if(Z==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 tolerance = (int *) mxMalloc( MAX_CLUSTERS *sizeof(int));
 if(tolerance==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 
 /* call the core routine */
 deproj( X, Y, Z, tolerance, &count, X1, Y1, X2, Z2, Y3, Z3, max_tol, length1, length2, length3);

 /* Free mxMalloc'ed memory */
 /* I do this here, since we will be requiring a pile of memory shortly */
 mxFree(X1);
 mxFree(Y1);
 mxFree(X2);
 mxFree(Z2);
 mxFree(Y3);
 mxFree(Z3);

 /* Create the output matrices */
 plhs[0]=mxCreateDoubleMatrix(count+1,1,mxREAL);
 plhs[1]=mxCreateDoubleMatrix(count+1,1,mxREAL);
 plhs[2]=mxCreateDoubleMatrix(count+1,1,mxREAL);
 plhs[3]=mxCreateDoubleMatrix(count+1,1,mxREAL);

 /* Associate the output matrices with the function arguments */
 Xr=mxGetPr(plhs[0]);
 Yr=mxGetPr(plhs[1]);
 Zr=mxGetPr(plhs[2]);
 tolerance_r=mxGetPr(plhs[3]);

 /* store output as a dbl */
 for(i=0;i<=count;i++) {
  Xr[i]=(double)X[i];
  Yr[i]=(double)Y[i];
  Zr[i]=(double)Z[i];
  tolerance_r[i]=(double)tolerance[i];
 }

 /* Free mxMalloc'ed memory */
 mxFree(X);
 mxFree(Y);
 mxFree(Z);
 mxFree(tolerance);

} /* End of GATEWAY function: mexFunction*/
