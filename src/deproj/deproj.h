/*
 AUTHOR: Eric Tittley

 HISTORY
 02-09-16 Mature version
 	Added #define MAX_CLUSTERS 524288
*/

#define MAX_CLUSTERS 1048576
// #define MAX_CLUSTERS 524288
// #define MAX_CLUSTERS 262144 /* Use this if you have 1Gb */
// #define MAX_CLUSTERS 32768 /* use this for hoth, which has only 256 Mb */
// #define MAX_CLUSTERS 65536

void deproj(
 /* Ouput */
 int *X, int *Y, int *Z, int *tolerance, int *c,
 /* Input */
 int *X1, int *Y1, int *X2, int *Z2, int *Y3, int *Z3, int max_tol,
 const int length1, const int length2, const int length3
);
