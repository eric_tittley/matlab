      SUBROUTINE rho_grid(N,r,rm,L,ltab,d3)
C  2008-03-25 Loop over inner indices first.Attempt to remove nonexistent passive grab

c input and not modified
      integer*4 N,L
      real*8 rm(N),r(N,3)
c output modified
      real*8 d3(L,L,L)

c Local variables
      integer*4 i,j,k,ix,iy,iz
      real*8 rmass,rrx,rry,rrz,hx,hy,hz
      real*8 hx0,hxp1,hxm1,hy0,hyp1,hym1,hz0,hzp1,hzm1
      integer*4 ixp1,iyp1,izp1,ixm1,iym1,izm1
      integer*4 ltab(-1:L+2)
      real*8 maxd3,mind3,rL

      rL=real(L)

c calculate ltab
      ltab(-1)=L-1
      ltab(0)=L
      do i=1,L
       ltab(i)=i
      enddo
      ltab(L+1)=1
      ltab(L+2)=2

c clear d3
      do k=1,L
       do j=1,L
        do i=1,L
         d3(i,j,k)=0.
        enddo
       enddo
      enddo

c core loop
      do i=1,N
       rmass=rm(i)
       rrx=r(i,1)*rL
       rry=r(i,2)*rL
       rrz=r(i,3)*rL
       ix= int(rrx+0.5)
       iy= int(rry+0.5)
       iz= int(rrz+0.5)
       hx = rrx -ix
       hy = rry -iy
       hz = rrz -iz
       ix=ltab(ix)
       iy=ltab(iy)
       iz=ltab(iz)

       hx0=0.75 - hx*hx
       hxp1=0.5* (0.5 + hx)**2
       hxm1=0.5* (0.5 - hx)**2
       hy0=0.75 - hy*hy
       hyp1=0.5* (0.5 + hy)**2
       hym1=0.5* (0.5 - hy)**2
       hz0=(0.75 - hz*hz)      *rmass
       hzp1=0.5* (0.5 + hz)**2 *rmass
       hzm1=0.5* (0.5 - hz)**2 *rmass

       ixp1=ltab(ix+1)
       iyp1=ltab(iy+1)
       izp1=ltab(iz+1)
       ixm1=ltab(ix-1)
       iym1=ltab(iy-1)
       izm1=ltab(iz-1)
       d3(ixm1,iym1,izm1)   = d3(ixm1,iym1,izm1)+ hxm1*hym1 *hzm1
       d3(ix  ,iym1,izm1)   = d3(ix  ,iym1,izm1)+ hx0 *hym1 *hzm1
       d3(ixp1,iym1,izm1)   = d3(ixp1,iym1,izm1)+ hxp1*hym1 *hzm1
       d3(ixm1,iy  ,izm1)   = d3(ixm1,iy  ,izm1)+ hxm1*hy0  *hzm1
       d3(ix  ,iy  ,izm1)   = d3(ix  ,iy  ,izm1)+ hx0 *hy0  *hzm1
       d3(ixp1,iy  ,izm1)   = d3(ixp1,iy  ,izm1)+ hxp1*hy0  *hzm1
       d3(ixm1,iyp1,izm1)   = d3(ixm1,iyp1,izm1)+ hxm1*hyp1 *hzm1
       d3(ix  ,iyp1,izm1)   = d3(ix  ,iyp1,izm1)+ hx0 *hyp1 *hzm1
       d3(ixp1,iyp1,izm1)   = d3(ixp1,iyp1,izm1)+ hxp1*hyp1 *hzm1
       d3(ixm1,iym1,iz  )   = d3(ixm1,iym1,iz  )+ hxm1*hym1 *hz0
       d3(ix  ,iym1,iz  )   = d3(ix  ,iym1,iz  )+ hx0 *hym1 *hz0
       d3(ixp1,iym1,iz  )   = d3(ixp1,iym1,iz  )+ hxp1*hym1 *hz0
       d3(ixm1,iy  ,iz  )   = d3(ixm1,iy  ,iz  )+ hxm1*hy0  *hz0
       d3(ix  ,iy  ,iz  )   = d3(ix  ,iy  ,iz  )+ hx0 *hy0  *hz0
       d3(ixp1,iy  ,iz  )   = d3(ixp1,iy  ,iz  )+ hxp1*hy0  *hz0
       d3(ixm1,iyp1,iz  )   = d3(ixm1,iyp1,iz  )+ hxm1*hyp1 *hz0
       d3(ix  ,iyp1,iz  )   = d3(ix  ,iyp1,iz  )+ hx0 *hyp1 *hz0
       d3(ixp1,iyp1,iz  )   = d3(ixp1,iyp1,iz  )+ hxp1*hyp1 *hz0
       d3(ixm1,iym1,izp1)   = d3(ixm1,iym1,izp1)+ hxm1*hym1 *hzp1
       d3(ix  ,iym1,izp1)   = d3(ix  ,iym1,izp1)+ hx0 *hym1 *hzp1
       d3(ixp1,iym1,izp1)   = d3(ixp1,iym1,izp1)+ hxp1*hym1 *hzp1
       d3(ixm1,iy  ,izp1)   = d3(ixm1,iy  ,izp1)+ hxm1*hy0  *hzp1
       d3(ix  ,iy  ,izp1)   = d3(ix  ,iy  ,izp1)+ hx0 *hy0  *hzp1
       d3(ixp1,iy  ,izp1)   = d3(ixp1,iy  ,izp1)+ hxp1*hy0  *hzp1
       d3(ixm1,iyp1,izp1)   = d3(ixm1,iyp1,izp1)+ hxm1*hyp1 *hzp1
       d3(ix  ,iyp1,izp1)   = d3(ix  ,iyp1,izp1)+ hx0 *hyp1 *hzp1
       d3(ixp1,iyp1,izp1)   = d3(ixp1,iyp1,izp1)+ hxp1*hyp1 *hzp1
      enddo

c Diagnostics
      maxd3=-9999.9
      mind3= 9999.9
      do k=1,L
       do j=1,L
        do i=1,L
        maxd3=max(maxd3,d3(i,j,k))
        mind3=min(mind3,d3(i,j,k))
       enddo
       enddo
      enddo

      END
