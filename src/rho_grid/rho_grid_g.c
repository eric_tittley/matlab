/* MEX Gateway funtion for rho_grid
 *
 * MATLAB sytax: rho=rho_grid(r,rm,L)
 * where r = [n x 3]
 *       rm= [n x 1]
 *       L = [1 x 1]
 */
#include <stdlib.h>

#include "mex.h"


void rho_grid_(const int *N,
               const double *r,
               const double *rm,
               const int *L,
               int *ltab,
               double *rho);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {
 double *r;
 double *rm;
 double *Lfloat;
 int    L;
 double *rho;

 size_t M,N;
 int N_int;

 /* Check for the proper number of arguments */
 if( (nrhs!=3) || (nlhs!=1) ) {
  mexErrMsgTxt("Syntax: rho=rho_grid(r,rm,L)");
 }

 /* The inputs must be of the type double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("r must be double");
 }
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("rm must be double");
 }
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("L must be double");
 }

 /* The second argument (L) must be scalar */
 M = mxGetM(prhs[2]);
 N = mxGetN(prhs[2]);
 if( (M !=1) || (N != 1) ) {
  mexErrMsgTxt("L must be scalar");
 }

 /* Check the dimensions of r and rm */
 /* I use N to be the length of the vector, which is the number of rows,
  * commonly called "M".  Hence the next line gets "N" and assigns to "M" */
 M = mxGetN(prhs[0]);
 if(M != 3) {
  mexErrMsgTxt("rho_grid: r must be [n x 3]\n");
 }
 M=mxGetM(prhs[1]);
 N=mxGetN(prhs[1]);
 M = (M>N) ? M : N; /* Max of M & N */
 N = mxGetM(prhs[0]); /* Length of r */
 if(N != M)  {
  mexErrMsgTxt("rho_grid: r and rm must be the same length\n");
 }

 /* Assign pointers to the arguments */
 r      = mxGetPr(prhs[0]);
 rm     = mxGetPr(prhs[1]);
 Lfloat = mxGetPr(prhs[2]);

 /* Copy right hand arguments to local arrays or variables */
 L = (int)(*Lfloat);

 /* Create matrix for the return argument and assign a pointer */
 const mwSize dims[3]={L, L, L};
 plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
 rho     = mxGetPr(plhs[0]);

 /* Temporary storage required in rho_grid */
 int *ltab = (int *)malloc(sizeof(int)*(size_t)(L+4));

 /* Do the actual computations in a subroutine */
 N_int = (int)N;
 rho_grid_(&N_int,r,rm,&L,ltab,rho);
 
 /* Free temporary storage */
 free(ltab);
}
