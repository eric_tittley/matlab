% Syntax: rho=rho_grid(r,rm,L)
%
% Grids the values rm for the particles at positions r onto a
% grid of dimensions LxLxL.
%
%       r = [n x 3] must span [0:1]
%       rm= [n x 1]
%       L = [1 x 1]
%
% sum(sum(sum(rho)))==sum(rm)
%
% Repeating boundary conditions are assumed.
%
% See also
%  max_den, h_proj_3d
