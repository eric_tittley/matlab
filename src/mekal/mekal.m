% [flux,ed]=mekal(bins,CEm,H,T,Abund)
%
% Computes the flux from a hot plasma using the mekal code.
%
% input: bins(nbin+1) lower and upper boundaries of energy bins in keV
%        CEm         -    emitting volume / distance**2
%                         (units: 1e50 cm**3 / pc**2 = 1.0503e11 m)
%        H           - hydrogen density (in cm**-3)
%        T           - temperature in keV
%        Abund(15)   - elemental abundances w.r.t. solar values
%
% output:flux(nbin)  - spectrum (phot/cm**2/s/keV)
%        ed          - electron density w.r.t. hydrogen
%
% The Solar values of the Abundances are presumed to be (log_10) [units?]
%
%   12.00, 10.99, 8.56, 8.05, 8.93, 8.09, 6.33, 7.58
%    6.47,  7.55, 7.21, 6.56, 6.36, 7.67, 6.25
%
% For the elements:
%       H,    He,    C,    N,    O,   Ne,   Na,   Mg,
%      Al,    Si,    S,   Ar,   Ca,   Fe,   Ni 

% HISTORY:
%  00 11 06 First version
%	Based on the core routine fmekal.f taken from FTOOLS v5.02.
