/* gaussian_fit_2d : Fit a Gaussian to a 2D distribution.
 * 
 * Includes the support routines:
 * boxcar()
 * gaussian()
 * frame_error()
 * make_gaussian()
 *
 * AUTHOR: Eric Tittley 
 *
 * HISTORY
 *  05 03 03: First version, based on fit_gaussian, which was tailored for
 *	stellar images.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

double gaussian( const double * const x,
                 const double * const sigma,
                 const double * const ampl  ){
 return( (*ampl)/(*sigma)*exp(-1.0*((*x)/(*sigma))*((*x)/(*sigma))/2.0) ); 
}

/* ********************************************************************* */

double frame_error( const double * const frame_1,
                    const double * const frame_2,
                    const int * const ArrLen ) {
 double err;
 int i;
 
 err=0.0;
 for( i=0; i < *ArrLen; i++ ) {
  err += pow( frame_1[i]-frame_2[i], 2.0 );
 }
 err = sqrt(err)/(*ArrLen);
 return(err);
}

/* ********************************************************************* */

void make_gaussian(
/* Input arguments, not modified */
/* M is the inner array dimension, N the outer i.e. (0->M-1) loops N-1 times */
/* This is because Matlab stores arrays one column at a time */
/* A byproduct of this is that x corresponds to the N and y corresponds to M */
/* If you like, the data is stored one RA at a time, cycling through */
/* declination */
const int * const M, const int * const N,
const double * const bias, const double * const sigma,
const double * const ampl,
const double * const x, const double * const y,
/* Output argument (space must already have been allocated! */
/* ( a "int * const" is a pointer whose value cannot change, but the contents
     at the address to which it points may change) */
double * const frame ) { 

 int i,j;
 double rx,ry,rx2;
 double dist;
 
/*
 printf("bias=%f, sigma=%f, ampl=%f, x=%f, y=%f\n", *bias, *sigma, *ampl, *x, *y );
*/

/* x corresponds to N corresponds to i */
/* y corresponds to M corresponds to j */

 for(i = 0; i < *N ; i++) {
  rx = (double)i - *x;
  rx2 = rx*rx;
  for(j = 0; j < *M ; j++) {
   ry = (double)j - *y;
   dist = sqrt( rx2 + ry*ry );
   frame[i*(*M) + j] = gaussian( &dist, sigma, ampl) + (*bias) ;
  }
 }
} /* end of make_gaussian */

/* ********************************************************************* */

int gaussian_fit_2d(
/* Input arguments */
const double * const frame, const int * const N, const int * const M,
/* output arguments */
double * const bias, double * const sigma, double * const ampl,
double * const x, double * const y ) {

 const int ITER_MAX=1000;
 const int ArrLen = *N * *M ;
 const double TOL = 1e-6;
 int i;
 int index_max;
 int iters;
 int min_index,BreakFlag;
 double err, err_0;
 double Delta = 0.01;
 double Sum;
 double min_bias,max_bias,min_ampl,max_ampl;
 double min_sigma,max_sigma,min_x,max_x,min_y,max_y;
 double dbias,dampl,dsigma,dx,dy;
 double test,min_err;
 double max;
 double errs[5];
 double *frame_approx=NULL;
 
 /* make a guess at the bias using the first 10 points */
 Sum=0.;
 for( i=0;i<10;i++ ) {
  Sum=Sum+frame[i];
 }
 *bias = Sum/10.0;

 /* find the approximate centre */
 /* The criterion for Cosmic ray rejection is if the pixel value less the */
 /* bias is greater than twice the smoothed pixel value less the bias */
 /* i.e. frame-bias > 2*(frame_smooth-bias) */
 /* which is the same thing as: frame > 2*frame_smooth-bias */
 max=frame[0];
 index_max=0;
 for( i=1;i<ArrLen;i++ ) {
  if(frame[i]>max) {
   max = frame[i];
   index_max=i;
  }  
 }
 
 /* guess at x and y */
 /* this is very important, since convergence is worse for this parameter */
 /* Convergence to a valid set of parameters is fairly sensitive to
    getting x and y centred on the star  (within +- 10 pixels) */
 *x = floor( ((double)index_max)/((double)(*M)) );
 *y = (double)(index_max-(int)(*x)*(*M)) ;

 /* guess at sigma */
 *sigma = 5.0;

 /* guess at ampl */
 *ampl = 5. * ( max - *bias );

 /* allocated space for the calculated field */
 frame_approx = (double *)malloc( ArrLen*sizeof(double) );
 if(frame_approx==NULL) {
  printf("gaussian_fit_2d: failed to allocate memory for frame_approx\n");
  return(-1);
 }

 /* initial guess at frame */
 make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );

 /* initial error */
 err = frame_error( frame, frame_approx, &ArrLen );

 /* The sum of the frame */
 Sum=0.0;
 for(i=0;i<ArrLen;i++) {
  Sum += (double)frame[i];
 }

 /* the minimum and maximum values */
 min_bias=0.0;
 max_bias=4294967296.0;
 min_ampl=0.0;
 max_ampl=4294967296.0;
 min_sigma=0.0;
 max_sigma=4294967296.0;
 min_x=0.0;
 max_x=(double)(*N)-1.0;
 min_y=0.0;
 max_y=(double)(*M)-1.0;

 /* Now loop through, trying to minimise the err */
 /* The variables that can change are bias, ampl, sigma, x, and y */
 /* For each of these, there is a dxxxx */
 iters=0;
 BreakFlag=0;
 do {
  iters++;
  if(iters>ITER_MAX) {
   printf("warning gaussian_fit_2d: failed to converge (iters)\n");
   BreakFlag=1;
  }
  
  /* Calculate the dxxx's */
  dbias=Delta * (*bias);
  if(dbias < 0.1*Delta) { dbias=0.1*Delta; }
  dampl=Delta * (*ampl);
  dsigma=Delta* (*sigma);
  dx   =Delta * (*x);
  dy   =Delta * (*y);
  
  /* The ampl */
  /*  printf("The Ampl\n"); */
  for(i=-2;i<=2;i++) {
   test = (*ampl) + ((double)i)*dampl;
   make_gaussian( M, N, bias, sigma, &test, x, y, frame_approx );
   errs[i+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  /*  printf("ampl=%f min_index=%i dampl=%f\n",*ampl,min_index,dampl); */
  *ampl = (*ampl) + ((double)min_index-2.0)*dampl;
  if(*ampl<min_ampl) {
   *ampl = min_ampl;
  }
  if(*ampl>max_ampl) {
   *ampl = max_ampl;
  }
  /*  printf("%f %f %f %f %f %e\n",*bias,*sigma,*ampl,*x,*y,err); */
  fflush(stdout);
  
  /* The x */
  /*  printf("x\n"); */
  for(i=-2;i<=2;i++) {
   test = (*x) + ((double)i)*dx;
   make_gaussian( M, N, bias, sigma, ampl, &test, y, frame_approx );
   errs[i+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *x = (*x) + ((double)min_index-2.0)*dx;
  if(*x<min_x) {
   *x = min_x;
  }
  if(*x>max_x) {
   *x = max_x;
  }
  /*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  /* The y */
  /*  printf("y\n"); */
  for(i=-2;i<=2;i++) {
   test = (*y) + ((double)i)*dy;
   make_gaussian( M, N, bias, sigma, ampl, x, &test, frame_approx );
   errs[i+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *y = (*y) + ((double)min_index-2.0)*dy;
  if(*y<min_y) {
   *y = min_y;
  }
  if(*y>max_y) {
   *y = max_y;
  }
  /*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */

  /* The Bias */
  /*  printf("The bias\n"); */
  for(i=-2;i<=2;i++) {
   test = (*bias) + ((double)i)*dbias;
   make_gaussian( M, N, &test, sigma, ampl, x, y, frame_approx );
   errs[i+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *bias = (*bias) + ((double)min_index-2.0)*dbias;
  if(*bias<min_bias) {
   *bias = min_bias;
  }
  if(*bias>max_bias) {
   *bias = max_bias;
  }
  /*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  /* The sigma */
  /*  printf("Sigma\n"); */
  for(i=-2;i<=2;i++) {
   test = (*sigma) + ((double)i)*dsigma;
   make_gaussian( M, N, bias, &test, ampl, x, y, frame_approx );
   errs[i+2] = frame_error( frame, frame_approx, &ArrLen );
  }
  min_err=errs[0];
  min_index=0;
  for(i=1;i<5;i++) {
   if(errs[i]<min_err) {
    min_err=errs[i];
    min_index=i;
   }
  }
  *sigma = (*sigma) + ((double)min_index-2.0)*dsigma;
  if(*sigma<min_sigma) {
   *sigma = min_sigma;
  }
  if(*sigma>max_sigma) {
   *sigma = max_sigma;
  }
  /*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */
  
  err_0 = err;
  make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
  err = frame_error( frame, frame_approx, &ArrLen );
  /*  printf("%f %f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y,err); */

  /* Decrease delta if we have bottomed out at this resolution */
  if(err >= err_0) {
   Delta=Delta*0.7071;
   /*  printf("Delta=%e\n",Delta); */
  }
  
  /*  printf("fabs(err)/Sum=%f TOL=%f\n",fabs(err)/Sum,TOL); */
  
 } while( (fabs(err)/Sum>TOL) & (BreakFlag!=1) ); /* the percentage error is > TOL */

 /* free up allocated space */
 free(frame_approx);
 
 /* increment x and y by 1 to translate from the [0..N-1] indexing of */
 /* C to the [1..N] indexing of Matlab */
 *x = *x +1;
 *y = *y +1;
 return(0);
} /* end of gaussian_fit_2d */
