/* function [bias,sigma,ampl,x,y]=gaussian_fit_2d(frame) */

#include <mex.h>

#include "gaussian_fit_2d.h"

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 size_t mrows,ncols;
 int status,N,M;
 double *bias, *sigma, *ampl, *x, *y;
 double *frame;

 /* Check for the proper number of arguments */
 if( (nrhs!=1) || (nlhs!=5) ) {
  mexErrMsgTxt("Syntax: [bias,sigma,ampl,x,y]=gaussian_fit_2d(frame)");
 }

 /* The argument must be matrix */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows==1 || ncols ==1) {
  mexErrMsgTxt("frame must be a matrix");
 }

 frame=mxGetPr(prhs[0]);

 /* The output */
 plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL);
 plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);

 bias =mxGetPr(plhs[0]);
 sigma=mxGetPr(plhs[1]);
 ampl =mxGetPr(plhs[2]);
 x    =mxGetPr(plhs[3]);
 y    =mxGetPr(plhs[4]);

 N = (int)ncols;
 M = (int)mrows;
  /* Call the subroutine */						
    /* input */
    /* output */
 status = gaussian_fit_2d(frame, &N, &M, bias, sigma, ampl, x, y );
 if(status==-1) {                                                     
  mexErrMsgTxt("core subroutine returned bad status");                
 }                                                                    

}
