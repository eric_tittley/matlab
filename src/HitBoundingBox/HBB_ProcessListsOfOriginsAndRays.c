/* HBB_ProcessListsOfOriginsAndRays
 *  Process lists of origins and directions by HitBoundingBox to return lists
 *  of impact coordinates
 *
 * ARGUMENTS
 *  minB[NDIM]      The minumum boundary of the volume, in each of the dimensions
 *  maxB[NDIM]      The maximum boundary of the volume, in each of the dimensions
 *  origin[N*NDIM]  The list of ray origins
 *  dir[N*NDIM]     The list of ray directions (should be normalized)
 *
 * RETURNS
 *  coord[N*NDIM]   The coordinates of where the ray hits the volume.
 */

#include <stddef.h>

int HBB_ProcessListsOfOriginsAndRays(const double *const minB,
                                     const double *const maxB,
                                     const double *const origin,
                                     const double *const dir,
                                           double *const coord) {


 return(EXIT_SUCCESS);
}
                                        
