/* 
Fast Ray-Box Intersection
by Andrew Woo
from "Graphics Gems", Academic Press, 1990
Modified by Eric Tittley to deal with rays inside boxes.
*/

#include <math.h>
#include <float.h>

#include "GraphicsGems.h"

#define NUMDIM  3
#define RIGHT   0
#define LEFT    1
#define MIDDLE  2

/*
 * char HitBoundingBox(minB,maxB, origin, dir,coord)
 * double minB[NUMDIM], maxB[NUMDIM];
 * double origin[NUMDIM], dir[NUMDIM];
 * double coord[NUMDIM];
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    *//* hit point */
unsigned int
HitBoundingBox(const double *const minB,
               const double *const maxB,
               const double *const origin,
               const double *const dir,
                     double *const coord)
{
 unsigned int inside = TRUE;
 unsigned int quadrant[NUMDIM];
 register int i;
 int whichPlane;
 double maxT[NUMDIM];
 double candidatePlane[NUMDIM];

 /* Find candidate planes; this loop can be avoided if
  * rays cast all from the eye (assume perspective view) */
 for (i = 0; i < NUMDIM; i++) {
  candidatePlane[i] = maxB[i];
  if (origin[i] < minB[i]) {
   quadrant[i] = LEFT;
   candidatePlane[i] = minB[i];
   inside = FALSE;
  } else if (origin[i] > maxB[i]) {
   quadrant[i] = RIGHT;
   inside = FALSE;
  } else {
   quadrant[i] = MIDDLE;
  }
 }

 /* Ray origin inside bounding box */
 if (inside == TRUE) {

  /* Find candidate planes in box */
  for (i = 0; i < NUMDIM; i++) {
   if (dir[i] < 0.0) {
    quadrant[i] = LEFT;
    candidatePlane[i] = minB[i];
   } else if (dir[i] > 0.0) {
    quadrant[i] = RIGHT;
    candidatePlane[i] = maxB[i];
   } else {                     /* dir[i]==0, parallel to planes */
    quadrant[i] = MIDDLE;
   }
  }

  /* Calculate T distances to candidate planes */
  for (i = 0; i < NUMDIM; i++) {
   if (quadrant[i] != MIDDLE) {
    maxT[i] = (candidatePlane[i] - origin[i]) / dir[i];
   } else {
    maxT[i] = -1.;
   }
  }

  /* Get smallest of the maxT's for final choice of intersection */
  whichPlane = 0;
  for (i = 1; i < NUMDIM; i++) {
   if (maxT[whichPlane] < maxT[i]) {
    whichPlane = i;
   }
  }

  /* Find the other coordinates */
  for (i = 0; i < NUMDIM; i++) {
   if (whichPlane != i) {
    coord[i] = origin[i] + maxT[whichPlane] * dir[i];
   } else {
    coord[i] = candidatePlane[i];
   }
  }
  return (TRUE);
 }

 /* Calculate T distances to candidate planes */
 for (i = 0; i < NUMDIM; i++) {
  if (quadrant[i] != MIDDLE && fabs(dir[i]) > DBL_EPSILON) {
   maxT[i] = (candidatePlane[i] - origin[i]) / dir[i];
  } else {
   maxT[i] = -1.;
  }
 }

 /* Get largest of the maxT's for final choice of intersection */
 whichPlane = 0;
 for (i = 1; i < NUMDIM; i++) {
  if (maxT[whichPlane] < maxT[i]) {
   whichPlane = i;
  }
 }

 /* Check final candidate actually inside box */
 if (maxT[whichPlane] < 0.) {
  return (FALSE);
 }
 
 /* Set the impact coordinates */
 for (i = 0; i < NUMDIM; i++) {
  if (whichPlane != i) {
   coord[i] = origin[i] + maxT[whichPlane] * dir[i];
   if (coord[i] < minB[i] || coord[i] > maxB[i]) {
    return (FALSE);
   }
  } else {
   coord[i] = candidatePlane[i];
  }
 }

 return (TRUE);                 /* ray hits box */
}
