function [P,yfitted]=tit_fit(x,y,order)
if nargin<3, error('Too few arguments passed to cheby_fit'); end

% Function to fit a curve Y vs X using Chebyshev Polynomials to the
% degree, ORDER.
% X and Y are assumed to be row vectors.  This will be changed in
% the future to handle any case.
% This requires the presence of the function CHEBY();
% Note: this routine can be easily modified to used different functions
% just by changing CHEBY() to some other appropriate function.
% Written by Eric Tittley (94 11 10)
F=tittley(x,order);
F=GS_ortho(F);  %orthogonalize the vectors
F=F';

% F is just a series of column vectors to which the amplitudes of
% the linear combinations is to be found.

% Perform the least squares fit.
P=F\y';

% Calculate the result of the fit.
yfitted=F*P;

