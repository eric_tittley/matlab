function err=chisqr_linear_fit(P,x,y,dy)
yfit = P(1)*x +P(2);
err = chisqr(y,yfit,dy);
