function flux = Gruber_bkgrnd_est(Energy,fluc,rad);
% Gruber_bkgrnd_est: Gruber's model of the Cosmic x-ray background.
%
%  flux = Gruber_bkgrnd_est(Energy,fluc,rad);
%
% ARGUMENTS
%  Energy	Beginning and ending energies [keV]
%  fluc		CXB fluctuation level, 8% = 0.08.
%  rad		Radius of the circle of your observation in arcmin.
%
% RETURNS
%  The background flux [ergs/cm^2/s]
%
% NOTES
%  The CXB flucuation level is usually ~8% in the RXTE PCA range.
%  (Valinia et al 1999)).
%  MacDonald's thesis gives fluctuations of 
%  13.0% +/- 2.1% (15. - 20.2) keV
%  15.8% +/- 2.7% (20.2 - 24.9) keV
%   32% +/- 11%   (34 - 41) keV.
%
%  If your observation isn't a circle, just calculate r for a circle of the same
%  area as your observation.  r = sqrt(Area/pi).

% AUTHOR: Danny Hudson (?)
%
% HISTORY
%  020730 Mature version
%
% COMPATIBILITY: Matlab, Octave

% Gruber's Fit is
%
% 7.877E^(-.29) * exp(-E/41.13)		3keV < E < 60keV
% 0.0259(E/60)^(-5.5) + 0.504(E/60)^(-1.59) + 0.0288(E/60)^(-1.05)   60keV<E<6000 keV


% Make sure Energy(1) is the low energy and Energy(2) is the high energy
if Energy(1) > Energy(2)
 Energy(1) = temp;
 Energy(1) = Energy(2);
 Energy(2) = temp;
end

% Check if Energies are in the appropriate range
if Energy(1) < 3 
 fprintf('\nWarning your low energy is below the value for this model!\n')
 fprintf('Assuming model extends down to %g keV\n',Energy(1))
elseif Energy(2) > 6000 
 fprintf('\nWarning your high energy is above the value for this model!\n')
 fprintf('Assuming model extends up to %g keV\n',Energy(2))
end


% Integrate over the energies
if Energy(2) <=60
 bkgrd = quad('7.877.*E.^(-0.29).*exp(-E./41.13)',Energy(1),Energy(2));
elseif Energy(1) < 60 & Energy(2) > 60
 bkgrd = quad('7.877.*E.^(-0.29).*exp(-E./41.13)',Energy(1),60.0);
 bkgrd = bkgrd + quad('0.0259.*(E./60).^(-5.5) + 0.504.*(E./60).^(-1.59) + 0.0288.*(E./60).^(-1.05)',60,Energy(2));
elseif Energy(1) > 60
 bkgrd = quad('0.0259.*(E./60).^(-5.5) + 0.504.*(E./60).^(-1.59) + 0.0288.*(E./60).^(-1.05)',Energy(1),Energy(2));
end


% Convert from keV to ergs
keV2ergs = 1.6021773349e-09; % keV/erg

% A steradian is equal to 3282.8 sq degrees.
%
% Convert from /Sr to /FOV
AREA = pi*(rad/60)^2; %Area of observation in aq degress
bkgrd = bkgrd * keV2ergs * AREA/3282.8; %ergs/cm^2/s/FOV

% flux in ergs/cm^2/s/FOV
flux(1) = bkgrd;
flux(2) = bkgrd*fluc;

fprintf('\nFrom %g to %g keV, you have a background\n',Energy(1),Energy(2))
fprintf('of %g ergs/cm^2/s with a fluctuation of %g ergs/cm^s/s\n\n',flux(1),flux(2))
