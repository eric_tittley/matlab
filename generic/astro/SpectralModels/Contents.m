% Spectral Models
%
% CreateMekalSpec	Create a MEKAL spectrum given the gas conditions.
%
% mekal_fit		Fit a MEKAL spectrum to x-ray telescope data.
%
% redshift_spec		Redshift a spectrum that is in frequency bins.
%
% Gruber_bkgrnd_est	Determines X-ray Background flux using Gruber's Model
