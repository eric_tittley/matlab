function spec_out=redshift_spec(spec_in,frequency_bins,z)
% redshift_spec: Redshift a spectrum, keeping the same frequency bins
%
%  spec_out=redshift_spec(spec_in,frequency_bins,z);
%
% ARGUMENTS
%  spec_in		The amplitude of the un-redshifted spectrum
%  frequency_bins	The bin edges of the frequencies.
%
% RETURNS
%  The redshifted spectrum amplitude in the bins supplied.
%
% NOTES
%  The frequency bins need not be equal size, but must be increasing.

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 11 17 First Version
%  00 11 20 Redefined Nbins to length(frequency_bins)-1 and propagated through
%  00 11 21 Put in check to see if z=0
%
% COMPATIBILITY: Matlab, Octave

if(z==0)
 spec_out=spec_in;
else
 f_redshifted=frequency_bins/(z+1);

 Nbins=length(frequency_bins)-1;

 spec_out=0*spec_in;
 state=zeros(1,4);

 for i=1:Nbins
  % The lower and upper edges of this bin
  lo=frequency_bins(i);
  hi=frequency_bins(i+1); 

  %find the redshifted bin edge that is immediately below the lower bin edge
  dummy=find(f_redshifted<=lo);
  if(~ isempty(dummy))
   lo_rs_indx=dummy(end);
  else
   lo_rs_indx=0;
  end
  %find the redshifted bin edge that is immediately above the upper bin edge
  dummy=find(f_redshifted>=hi);
  if(~ isempty(dummy))
   hi_rs_indx=dummy(1);
  else
   hi_rs_indx=Nbins+1;
  end

  % The contents of this bin will contain some fraction of the contents of the
  % bins spanning lo_rs_indx to hi_rs_indx
  if( (hi_rs_indx-lo_rs_indx) == 0 )
   state(1)=state(1)+1;
   % Generally an impossible situation, except at the edges where there is
   % no overlap
   spec_out(i)=0;
  elseif( (hi_rs_indx-lo_rs_indx) == 1 )
 %  disp([hi_rs_indx lo_rs_indx])
   state(2)=state(2)+1;
   % The bin falls entirely within a red-shifted bin, which can happen if
   % the bins are not all the same size, or the redshift is large.
   if(hi_rs_indx<=Nbins)
    frac=(hi-lo)/(f_redshifted(hi_rs_indx)-f_redshifted(lo_rs_indx));
    spec_out(i)=frac*spec_in(lo_rs_indx);
   else
    spec_out(i)=0;
   end
  elseif( (hi_rs_indx-lo_rs_indx) == 2 )
   state(3)=state(3)+1;
   % The most likely scenario, which corresponds to a simple shift
   % In this case, lo_rs_indx+1 == hi_rs_indx-1, or at least it should
   if( (lo_rs_indx+1) ~= (hi_rs_indx-1) )
    error('not sure what to do, here')
   end
   if(lo_rs_indx==0 | hi_rs_indx==Nbins+1)
    % We are off the edge
    spec_out(i)=0;
   else
    frac_lo=  (f_redshifted(lo_rs_indx+1)-lo) ...
            / (f_redshifted(lo_rs_indx+1)-f_redshifted(lo_rs_indx));
   frac_hi=  (hi-f_redshifted(hi_rs_indx-1)) ...
           / (f_redshifted(hi_rs_indx)-f_redshifted(hi_rs_indx-1));
   spec_out(i)= frac_lo*spec_in(lo_rs_indx) ...
               +frac_hi*spec_in(hi_rs_indx-1);
  end
 elseif( (hi_rs_indx-lo_rs_indx) > 2 )
 %  disp([i lo_rs_indx hi_rs_indx])
 %  disp([ lo f_redshifted(lo_rs_indx)])
   state(4)=state(4)+1;
   % The bin will cover at least one red-shift bin entirely, and some fraction
   % of the neighbouring bins.
   if(lo_rs_indx~=0)
    frac_lo=  (f_redshifted(lo_rs_indx+1)-lo) ...
            / (f_redshifted(lo_rs_indx+1)-f_redshifted(lo_rs_indx));
    spec_out(i)= frac_lo*spec_in(lo_rs_indx);
   end
   if(hi_rs_indx~=Nbins)
    frac_hi=  (hi-f_redshifted(hi_rs_indx-1)) ...
            / (f_redshifted(hi_rs_indx)-f_redshifted(hi_rs_indx-1));
    spec_out(i)= spec_out(i) + frac_hi*spec_in(hi_rs_indx-1);
   end
   % For the middle part, we want everything between the bin edges:
   % lo_rs_indx+1 to hi_rs_indx-1, or everything in bins:
   span_mid=[lo_rs_indx+1:hi_rs_indx-2];
   spec_out(i)= spec_out(i) + sum(spec_in(span_mid));
   if(isempty(span_mid))
    error('You are using the Chewbacca defense!')
   end
  elseif( (hi_rs_indx-lo_rs_indx) < 0 )
   error('hi_rs_indx<lo_rs_indx , the energies must be increasing')
  else
   error('impossible state')
  end

 end % loop over bins

 % Now scale the spectrum by the redshift
 spec_out=spec_out/(z+1);
 %state
end % if z=0 then ... else ...
