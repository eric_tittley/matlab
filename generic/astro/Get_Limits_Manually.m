% Get_Limits_Manually: Manually get the limits of an RA-Dec square on an image.
%
% [Lo, Hi, width] = Get_Limits_Manually()
%
% ARGUMENTS
%  None
%
% RETURNS
%  Lo		Lower left corner (RA,dec)
%  Hi		Upper right corner (RA,dec)
%  width	Width of the square
%
% USAGE
%  Create an image with x-axis RA and y-axis Declination.
%  Call Get_Limits_Manually().
%  Select the lower-left point of the new region.
%  Select the upper-right point.
%
% NOTES
%  The coordinates are assumed to be RA & Dec, which means the lower RA
%  has a larger value than the upper RA.
%  The width is the maximum span in RA or Dec.  The other coordinate is padded
%  so that the region is exactly square.

% AUTHOR: Eric Tittley
%
% HISTORY
%  06 03 17 First version
%
% COMPATIBILITY: Matlab, Octave

function [Lo, Hi, width] = Get_Limits_Manually()

Pos=ginput(2);

Lo=Pos(1,:);
Hi=Pos(2,:);
if(Hi(2) < Lo(2))
 temp=Lo(2);
 Lo(2)=Hi(2);
 Hi(2)=temp;
end
dec0=mean([Lo(2),Hi(2)]);
width = max([(Lo(1)-Hi(1))*cos(dec0*pi/180)*15,Hi(2)-Lo(2)])*60;
Hi = [Lo(1)-(width/60)/(cos(dec0*pi/180)*15), Lo(2)+width/60];
