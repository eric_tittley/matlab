% draw_skewer_orig: Create a skewer of connected cells along input angle phi
%
% SK=draw_skewer(ibound, jbound, x, y, ix, phi, smax)
%
% ARGUMENTS
%  Input
%   ibound    2-vector of lower and upper x-axis index range (int)
%   jbound    2-vector of lower and upper y-axis index range (int)
%   Lx        x-axis grid spacing (float)
%   Ly        y-axis grid spacing (float)
%   x         starting point x-value [0 to ibound(1)*Lx] (float)
%   y         starting point y-value [0 to ibound(2)*Ly] (float)
%   ix        ix=0 if initial point on y-axis, ix=1 if initial point on x-axis
%   phi       direction of skewer (float) (0 to pi/2 only, at the moment)
%   smax      maximum skewer length required (float)
%  Output
%   SK        skewer structure (structure)
%     .i      Cell number within the LOS
%     .j      LOS
%     .s      The distance along the skewer of the centre of the segment
%     .ds     The length of each segment
%
% SEE ALSO
%  Lya_trans_spec_from_slice, fake_Lya_spec

% AUTHOR: Avery Meiksin, converted to Matlab from IDL by Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY:
%  05 24 11 Initial version (IDL) (Avery Meiksin)
%  08 08 25 Matlab version (Eric Tittley)
%  10 01 17 Minor revisions when merging with a second draw_skewer.m conversion.
%  11 08 09 Deal with phi in any quadrant
%
% TODO
%  Deal with the initial x,y not being on the xaxis or yaxis

function SK=draw_skewer_orig(ibound, jbound, Lx, Ly, x, y, ix, phi, smax)

% ARGUMENT CHECKS
if( nargin ~= 9 )
 disp('syntax: SK=draw_skewer_orig(ibound, jbound, Lx, Ly, x, y, ix, phi, smax)')
 error('Insufficient number of arguments')
end

VERBOSE = 0;
xbound = (ibound-1)*Lx;
ybound = (jbound-1)*Ly;
if(VERBOSE)
 disp(['xbound = ', num2str(xbound)])
 disp(['ybound = ', num2str(ybound)])
end

if((x < xbound(1)) || (x > xbound(2)))
 disp(['x = ',num2str(x), '; xbound=',num2str(xbound)])
 error('initial x value out of permitted range')
end
if((y < ybound(1)) || (y > ybound(2)))
 disp(['y = ',num2str(y), '; ybound=',num2str(ybound)])
 error('initial y value out of permitted range')
end

i = floor(x/ Lx)+1;
j = floor(y/ Ly)+1;

if(VERBOSE)
 disp(['initial i = ',int2str(i),' initial j = ',int2str(j)])
end
if((i < ibound(1)) || (i > ibound(2)))
 error('initial i value out of permitted range')
end
if((j < jbound(1)) || (j > jbound(2)))
 error('initial j value out of permitted range')
end

% Check which quadrant we are in, and handle appropriately
phi=mod(phi,2*pi);
if( (phi>=0.) & (phi<=pi/2) )
 quadrant=1;
elseif( (phi>pi/2) & (phi<=pi) )
 quadrant=2;
elseif( (phi>pi) & (phi<=1.5*pi) )
 quadrant=3;
else
 quadrant=4;
end

% Put phi in the correct quadrant
switch quadrant
 case 1,
  % Nothing to do
 case 2,
  phi = pi-phi;
 case 3,
  phi = phi-pi;
 case 4,
  phi = 2*pi-phi;
 otherwise,
  disp('Impossible state')
end

sphi = sin(phi);
cphi = cos(phi);
if((sphi < 0) || (cphi < 0))
 disp(['phi = ', num2str(phi),'; quadrant = ',int2str(quadrant)])
 error('value of phi out of permitted range')
end
tphi = tan(phi);

ssum    = 0.;
ds_last = 0.;
nseg    = 1;
SK.nel  = 0;

while( ssum < smax )
 xmax = i*Lx;
 ymax = j*Ly;
 Lxtmp0 = (ymax-y)/tphi; % Maximum dx given maximum dy
 Lxtmp1 = (xmax-x)*tphi; % Maximum dy given maximum dx
 if(VERBOSE)
  disp(['i = ',int2str(i),' j = ', int2str(j)])
  disp(['x = ',num2str(x),' y = ', num2str(y)])
  disp(['ix = ',int2str(ix)])
  disp(['Lxtmp0 = ', num2str(Lxtmp0)])
  disp(['Lxtmp1 = ', num2str(Lxtmp1)])
 end

 switch ix
  case 0, % segment starts on y-axis
   icond0 = (Lxtmp0 >= Lx); % Test if segment will terminate on right edge.
   switch icond0
    case 0, 
     %terminate on upper x-axis (upper edge)
     ds = (ymax - y)/ sphi;
     xp = x + Lxtmp0;
     yp = ymax;
     ip = i;
     jp = j + 1;
     ix_tmp = 1;
     %enforce wrap-around if necessary
     if(jp > jbound(2))
      yp = ybound(1);
      jp = jbound(1);
     end
     if(VERBOSE)
      disp(['case 0 0: ds = ',num2str(ds)])
     end
     if(VERBOSE)
      disp(['xp = ',num2str(xp),' yp = ',num2str(yp)])
     end
    case 1,
     %terminate on y-axis
     ds = Lx/ cphi;
     xp = xmax;
     yp = y + Lx*tphi;
     ip = i + 1;
     %enforce wrap-around if needed
     if(ip > ibound(2))
      xp = xbound(1);
      ip = ibound(1);
     end
     jp = j;
     ix_tmp = 0;
     if(VERBOSE)
      disp(['case 0 1: ds = ',num2str(ds)])
     end
     if(VERBOSE)
      disp(['xp = ',num2str(xp),' yp = ',num2str(yp)])
     end
    otherwise,
     disp('case ix = 0 has an illegal subcase')
   end
  case 1, %switch ix: segment starts on x-axis
   icond1 = (Lxtmp1 < Ly);
   switch icond1
    case 0,
     %terminate on upper x-axis
     ds = Ly/ sphi;
     xp = x + Ly/ tphi;
     yp = ymax;
     ip = i;
     jp = j + 1;
     ix_tmp = 1;
     %enforce wrap-around if needed
     if(jp > jbound(2))
      yp = ybound(1);
      jp = jbound(1);
     end
     if(VERBOSE)
      disp(['case 1 0: ds = ',num2str(ds)])
     end
     if(VERBOSE)
      disp(['xp = ',num2str(xp),' yp = ',num2str(yp)])
     end
    case 1,
     %terminate on y-axis
     ds = (xmax - x)/ cphi;
     xp = xmax;
     yp = y + Lxtmp1;
     ip = i + 1;
     %enforce wrap-around if needed
     if(ip > ibound(2))
      xp = xbound(1);
      ip = ibound(1);
     end
     jp = j;
     ix_tmp = 0;
     if(VERBOSE)
      disp(['case 1 1: ds = ',num2str(ds)])
     end
     if(VERBOSE)
      disp(['xp = ',num2str(xp),' yp = ',num2str(yp)])
     end
    otherwise,
     disp('case ix = 1 has an illegal subcase')
   end
  otherwise, %switch ix
   disp(['ix = ', num2str(ix), ' is an illegal case'])
 end % switch ix

 if(VERBOSE)
  disp(['ip = ',int2str(ip),' jp = ', int2str(jp)])
 end
 ix = ix_tmp;
 if(VERBOSE)
  disp(['ix = ',int2str(ix),' xp = ', num2str(xp),' yp = ',num2str(yp)])
 end
 ssum = ssum + 0.5*(ds_last + ds);
 if(ssum > smax)
  ssum = smax;
 end
 SK.is(nseg) = i;
 SK.js(nseg) = j;
 SK.ds(nseg) = ds;
 SK.s(nseg) = ssum;
 SK.nel = SK.nel + 1;
 i = ip;
 j = jp;
 x = xp;
 y = yp;
 % TESTING
 SK.x(nseg)=x;
 SK.y(nseg)=y;
 % END
 if(VERBOSE)
  disp(['SK.s[',int2str(nseg),'] = ',num2str(SK.s(nseg))])
 end
 if(ds < 0.)
  disp(['SK.ds ',num2str(ds),' i = ',int2str(i),' j = ',int2str(j)])
 end
 ds_last = ds;
 nseg = nseg + 1;
end % while

% Correct for the quadrant
switch quadrant
 case 1,
  % Nothing to do
 case 2,
  SK.is = ibound(2) - SK.is + 1;
 case 3,
  SK.is = ibound(2) - SK.is + 1;
  SK.js = jbound(2) - SK.js + 1;
 case 4,
  SK.js = jbound(2) - SK.js + 1;
 otherwise,
  disp('Impossible state')
end
