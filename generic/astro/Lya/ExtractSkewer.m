% ExtractSkewer: Extract a skewer from a gridded dataset
%
% Skewer=ExtractSkewer(Data,SkewerParams)
%
% ARGUMENTS
%  Data         The data cube from which to extract the skewer
%                Structure elements:
%                 
%                 v_x   Velocity in x-direction
%                 v_y   Velocity in y-direction
%                 v_z   Velocity in z-direction
%
%  SkewerParams Parameters for the skewer
%                Structure elements:
%                 ibound
%                 jbound
%                 dRx  
%                 dRy  
%                 x    
%                 y    
%                 phi 
%                 smax
%                 
% RETURNS
%  Skewer       A structure containing the skewer data
%                Elements:
%
% SEE ALSO
%  draw_skewer, Lya_trans_spec_from_slice

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  110809 First version, with code taken from Lya_trans_spec_from_slice
%  110816 draw_skewer no longer needs the ix argument

function Skewer=ExtractSkewer(Data,SkewerParams)

%draw skewer SK
SK=draw_skewer(SkewerParams.ibound, ...
               SkewerParams.jbound, ...
               SkewerParams.dRx,    ...
               SkewerParams.dRy,    ...
               SkewerParams.x,      ...
               SkewerParams.y,      ...
               SkewerParams.phi,    ...
               SkewerParams.smax);

%plot(SK.is,SK.js,'.')

% Subtract the starting point (probably zero, but there you go).
R         = (SK.s - SK.s(1));

% Assign the segment positions to the Skewer
Skewer.R  = R(1:SK.nel);

% The segment lengths
dR = R(2:end) - R(1:end-1); % m
dR = [dR, dR(1)]; % Append an interval (m)
Skewer.dR       = dR(1:SK.nel);

% Initialise
Skewer.Vx = zeros(SK.nel,1);
Skewer.Vy = zeros(SK.nel,1);
Skewer.Vz = zeros(SK.nel,1);
Skewer.T  = zeros(SK.nel,1);


% The following could be vectorised, but as yet is not a bottleneck
% To vectorise, turn the j,i list into a vector of indices into the 1-D
% index for each 2-D array.
for nseg=1:SK.nel
 i = SK.is(nseg);
 j = SK.js(nseg);
 Skewer.Vx(nseg)  = Data.Vx(j,i);
 Skewer.Vy(nseg)  = Data.Vy(j,i);
 Skewer.Vz(nseg)  = Data.Vz(j,i);
 Skewer.T(nseg)   = Data.T(j,i); % K
 Skewer.Rho(nseg) = Data.Rho(j,i);
end

% Remember skewer cells
Skewer.i = SK.is;
Skewer.j = SK.js;

% And the angle
Skewer.phi = SkewerParams.phi;
