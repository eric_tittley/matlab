% draw_skewer: Create a skewer of connected cells along input angle phi
%
% SK=draw_skewer(ibound, jbound, Lx, Ly, x, y, phi, smax)
%
% ARGUMENTS
%  Input
%   ibound    2-vector of lower and upper x-axis index range (int)
%   jbound    2-vector of lower and upper y-axis index range (int)
%   Lx        x-axis grid spacing (float)
%   Ly        y-axis grid spacing (float)
%   x         starting point x-value [0 to ibound(1)*Lx] (float)
%   y         starting point y-value [0 to ibound(2)*Ly] (float)
%   phi       direction of skewer (float) (0 to pi/2 only, at the moment)
%   smax      maximum skewer length required (float)
%  Output
%   SK        skewer structure (structure)
%     .i      Cell number within the LOS
%     .j      LOS
%     .s      The distance along the skewer of the centre of the segment
%     .ds     The length of each segment
%
% SEE ALSO
%  Lya_trans_spec_from_slice, fake_Lya_spec, draw_skewer_orig

% AUTHOR: Avery Meiksin, converted to Matlab from IDL by Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY:
%  05 24 11 Initial version (IDL) (Avery Meiksin)
%  08 08 25 Matlab version (Eric Tittley)
%  10 01 17 Minor revisions when merging with a second draw_skewer.m conversion.
%  11 08 09 Deal with phi in any quadrant
%  11 08 16 Major revision.  No longer need "ix" argument, since I've compressed
%       the 4 cases: starting on x/y edge; ending on x/y edge.
%       Now it doesn't matter where in the cell the segment starts, it will
%       find the correct edge.
%       This also permits starting the LOS withing the centre of a cell.
%       Stripped out VERBOSE
%  14 05 27 A rare rounding issue patched. i = floor(x/Lx+eps(x/Lx))+1; etc

function SK=draw_skewer(ibound, jbound, Lx, Ly, x, y, phi, smax)

% ARGUMENT CHECKS
if( nargin ~= 8 )
 disp('syntax: SK=draw_skewer(ibound, jbound, Lx, Ly, x, y, phi, smax)')
 error('Insufficient number of arguments')
end

xbound = (ibound-1)*Lx;
ybound = (jbound-1)*Ly;

if((x < xbound(1)) || (x > xbound(2)))
 disp(['x = ',num2str(x), '; xbound=',num2str(xbound)])
 error('initial x value out of permitted range')
end
if((y < ybound(1)) || (y > ybound(2)))
 disp(['y = ',num2str(y), '; ybound=',num2str(ybound)])
 error('initial y value out of permitted range')
end

i = floor(x/Lx+eps(x/Lx))+1;
j = floor(y/Ly+eps(y/Lx))+1;

if((i < ibound(1)) || (i > ibound(2)))
 error('initial i value out of permitted range')
end
if((j < jbound(1)) || (j > jbound(2)))
 error('initial j value out of permitted range')
end

% Check which quadrant we are in, and handle appropriately
phi=mod(phi,2*pi);
if( (phi>=0.) && (phi<=pi/2) )
 quadrant=1;
elseif( (phi>pi/2) && (phi<=pi) )
 quadrant=2;
elseif( (phi>pi) && (phi<=1.5*pi) )
 quadrant=3;
else
 quadrant=4;
end

% Put phi in the correct quadrant
switch quadrant
 case 1,
  % Nothing to do
 case 2,
  phi = pi-phi;
 case 3,
  phi = phi-pi;
 case 4,
  phi = 2*pi-phi;
 otherwise,
  disp('Impossible state')
end

sphi = sin(phi);
cphi = cos(phi);
if((sphi < 0) || (cphi < 0))
 disp(['phi = ', num2str(phi),'; quadrant = ',int2str(quadrant)])
 error('value of phi out of permitted range')
end
tphi = tan(phi);

ssum    = 0.;
ds_last = 0.;
nseg    = 1;
SK.nel  = 0;

while( ssum < smax )
 xmax = i*Lx;
 ymax = j*Ly;
 Lxtmp0 = (ymax-y)/tphi; % Maximum dx given maximum dy
 Lxtmp1 = (xmax-x)*tphi; % Maximum dy given maximum dx

 if( (x+Lxtmp0) >= xmax )
  %terminate on y-axis (right edge)
  ds = (xmax-x)/ cphi;
  xp = xmax;
  yp = y + Lxtmp1;
  ip = i + 1;
  jp = j;
  %enforce wrap-around if needed
  if(ip > ibound(2))
   xp = xbound(1);
   ip = ibound(1);
  end
 else
  %terminate on upper x-axis (upper edge)
  ds = (ymax-y)/ sphi;
  xp = x + Lxtmp0;
  yp = ymax;
  ip = i;
  jp = j + 1;
  %enforce wrap-around if necessary
  if(jp > jbound(2))
   yp = ybound(1);
   jp = jbound(1);
  end
 end

 ssum = ssum + 0.5*(ds_last + ds);
 if(ssum > smax)
  ssum = smax;
 end
 % The next four lines are the bottleneck
 SK.is(nseg) = i;
 SK.js(nseg) = j;
 SK.ds(nseg) = ds;
 SK.s(nseg) = ssum;
 % end of bottleneck
 SK.nel = SK.nel + 1;
 i = ip;
 j = jp;
 x = xp;
 y = yp;
 if(ds < 0.)
  disp(['SK.ds ',num2str(ds),' i = ',int2str(i),' j = ',int2str(j)])
 end
 ds_last = ds;
 nseg = nseg + 1;

end % while ssum < smax

% Correct for the quadrant
switch quadrant
 case 1,
  % Nothing to do
 case 2,
  SK.is = ibound(2) - SK.is + 1;
 case 3,
  SK.is = ibound(2) - SK.is + 1;
  SK.js = jbound(2) - SK.js + 1;
 case 4,
  SK.js = jbound(2) - SK.js + 1;
 otherwise,
  disp('Impossible state')
end
