% trans_spec_wrapped_Lyb: Generate a wrapped transmission spectrum from RT data
%       for Lybeta, using tau_eff for Lya as a basis.
% 
% ARGUMENTS
%  INPUT, not modified
%   D        The RT data for the slice
%            Required are the elements:
%             R
%             v_z
%             v_x
%             f_H1
%             n_H
%             f_He2
%             n_He
%             T
%   LOS,Cell The ray starts at (LOS,Cell) in the slice.
%   zred     Redshift of the source
%   dz       The span over redshift that we will examine
%   delta_H  The factor by which to multiply f_ * n_* for Hydrogen
%   delta_He The factor by which to multiply f_ * n_* for Helium
%   kms      The range over which to calculate absorption
%   tau_eff  Mean optical depth for Lya
%   Cosmology Structure containing cosmological paramenters:
%            Omega_v
%            Omega_m
%            h100
%   phi      The angle of the LOS (radians)
%  OUTPUT
%   flux_Lya  The transmittance fraction at each velocity in kms
%             due to Ly-alpha absorption
%   flux_Lyb  The transmittance fraction at each velocity in kms
%             due to Ly-beta absorption
%   flux_Lyg  The transmittance fraction at each velocity in kms
%             due to Ly-gamma absorption
%   Skewer    (Optional) the Skewer passed to fake_Lya_spec
%
% REQUIRES
%  draw_skewer

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  110727 First version, from trans_spec_wrapped.m
%  110816 draw_skewer no longer requires the ix argument

function [flux_Lya, flux_Lyb, flux_Lyg, Skewer] = ...
         trans_spec_wrapped(D, LOS, Cell, zred, dz, delta_H, delta_He, kms, ...
                            tau_eff, Cosmology, phi)

% Get some physical constants
hdr.h100=Cosmology.h100;
SI='MKS';
Units

% The dimensions of the data
[NLos,NR]  = size([D.R]);

% Cosmology.
Ho = 100*Cosmology.h100; %km s^-1 Mpc^-1
H = Ho * sqrt(Cosmology.Omega_m*(1. + zred)^3 + Cosmology.Omega_v); % km s^-1 Mpc^-1
disp(['H=',num2str(H),' km s^-1 Mpc^-1'])

% smax is the length of the skewer in Mpc
smax = dz*c/ (1.e3*H*(1. + zred)); % Mpc
disp(['smax = ', num2str(smax), 'Mpc'])
smax = smax*Mpc; % m

% The index bounds of the box.
ibound = [1, NR];
jbound = [1, NLos];

sphi = sin(phi);
cphi = cos(phi);

% Lx and Ly are the x and y grid spacings.  I might have called them
% dRx, dRy.  x is along a LOS, y separates different LOSs
Lx=D(1).R(2)-D(1).R(1);
Rmax = D(1).R(NR)-D(1).R(1)+Lx;
Ly = Rmax/NLos;
disp(['Lx = ',num2str(Lx/Mpc),' Mpc; Ly = ',num2str(Ly/Mpc),' Mpc'])

% The starting point of the ray.  For my purposes this needs
% to be anywhere in the slice (LOS, Cell), but it seems it
% needs to be on the x or y axis (i or j == 0).
% Shift the order of the LOSs such that the LOS we want is at D(1)
D = shift(D,1-LOS,2);

x = (Cell-1)*Lx;
y = 0.;

%draw skewer Skewer
Skewer=draw_skewer(ibound, jbound, Lx, Ly, x, y, phi, smax);

% Along the LOS
Rmax = Skewer.s(Skewer.nel);

% Initialise arrays
R_los = (Skewer.s - Skewer.s(1))/Mpc;
v_los = zeros(1,Skewer.nel);
v     = zeros(1,Skewer.nel);
nH1   = zeros(1,Skewer.nel);
nHe2  = zeros(1,Skewer.nel);
T     = zeros(1,Skewer.nel);

% Generate the skewer data from the RT data for the slice
for nseg=1:Skewer.nel
 i = Skewer.is(nseg); % Cell number within the LOS
 j = Skewer.js(nseg); % Cell LOS
 v_los(nseg) = D(j).v_z(i)*cphi + D(j).v_x(i)*sphi; %km/s
 nH1(nseg)   = D(j).f_H1(i) *D(j).n_H(i) *Skewer.ds(nseg)*delta_H;  % m^-2
 nHe2(nseg)  = D(j).f_He2(i)*D(j).n_He(i)*Skewer.ds(nseg)*delta_He; % m^-2
 T(nseg)     = D(j).T(i); % K
end
disp(['Skewer has ',num2str(Skewer.nel),' segments'])
disp(['Flux will have ',num2str(length(kms)),' bins'])

% Add the peculiar velocities to the Hubble flow, and put into recession.
v = v_los + H*R_los; % km/s
v = -v;

% Compute the observed fluxes at the velocties given in kms
% Effective mean optical depths to normalise to.
tau_eff_Lya  =tau_eff;
% Enumerations.
ProfileDoppler=0;
ProfileVoigt=1;
SpeciesHI_Lya  =0;
SpeciesHeII_Lya=2;
SpeciesHI_Lyb  =3;
SpeciesHI_Lyg  =4;
SpeciesHeII_Lyb=5;
SpeciesHeII_Lyg=6;
% Guess at the normalisation amplitude required to achieve tau_eff.
% Amp will be modified.  This should be input.
Amp=1;
tic
flux_Lya = fake_Lya_spec(v, nHe2,  T, kms, ProfileVoigt, SpeciesHeII_Lya, ...
                         tau_eff_Lya, Amp);
toc
disp(Amp)

% Scale the densities by Amp
nHe2=Amp*nHe2;
tic
flux_Lyb = fake_Lya_spec(v, nHe2, T, kms, ProfileDoppler, SpeciesHeII_Lyb, ...
                         -1, Amp);
toc
tic
flux_Lyg = fake_Lya_spec(v, nHe2, T, kms, ProfileDoppler, SpeciesHeII_Lyg, ...
                         -1, Amp);
toc

if(nargout==4)
 Skewer.v    =v;
 Skewer.nH1  =nH1;
 Skewer.nHe2 =nHe2;
 Skewer.T    =T;
end
