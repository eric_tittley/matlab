function lumin = flux2lumin(flux,z,H);
% flux2lumin: Converts flux to luminosity for a given z.
%
% syntax lumin = flux2lumin(flux,z,H);
%
% ARGUMENTS
%  flux		(ergs/cm^2/s)
%  z		Redshift
%  H		Hubble's constant (km/s/Mpc)
%
% RETURNS
%  lumin	Luminosity (ergs s^-1 h^-2)
%
% NOTES
%  This calculation is good for small z and q = 0.5

% AUTHOR: Danny Hudson
%
% HISTORY
%  030521 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

c = 2.99792458e5 ;% speed of light km/s
pc = 3.0856776e18; % parsec in cm

D = c*z/H; % Distance in Mpc/h.
D = D*1e6*pc; % Distance in cm

% Assume istropic radiation
lumin = flux * 4*pi*D^2;
