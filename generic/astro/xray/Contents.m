% astro/xray: Routines for processing x-ray data
%
% generic/	Generic functions for processing x-ray data.
%
% Chandra/	Functions and scripts peculiar to the Chandra x-ray satellite.
%
% ROSAT/	Functions peculiar to the ROSAT x-ray satellite.
%
% SAX/		Functions peculiar to the SAX x-ray satellite.
