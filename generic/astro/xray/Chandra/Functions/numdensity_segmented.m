function image = numdensity_segmented(x,y,NSearch,Res,L_pix,Nmax)
% numdensity_segmented: Force preload of numdensity object code.
%
% image = numdensity_segmented(x,y,NSearch,Res,L_pix,Nmax)
%
% ARGUMENTS
%   x,y          List of points in the plane used to contribute to the density
%   Nsrch        Number of points to contribute to the density estimate
%   Res          Requested size of the array [Res x Res] to return
%   L_pix        Limits to the node positions, as
%                 L_pix = [x_lo x_hi y_lo y_hi]
%   Nmax         Maximum number of events/particles to grid at a time.
%
% RETURNS
%   image        The gridded data density distribution
%
% NOTES
%  Force preload of numdensity object code.
%  This seems to help prevent the code not loading due to shortage of memory.
%  Error seen when memory is full:
%   ??? Invalid MEX-file '/home/etittley/matlab/toolboxes/mex/numdensity.mexglx':
%   /home/etittley/matlab/toolboxes/mex/numdensity.mexglx: cannot map zero-fill
%   pages: Cannot allocate memory.
%
%  If the number of events/particles (length(x)) is less than Nmax, then
%  numdensity() is called directly.
%
%  Not certain this function is still necessary, given the changes made to
%  numdensity()
%
% SEE ALSO
%  numdensity

N = length(x);
if(N > Nmax)
 Nsegments = ceil(N/Nmax);
 image_cube = zeros(Res,Res,Nsegments);
 N_in_span = ceil(N/Nsegments);
 for i=1:Nsegments-1
  span = [(i-1)*N_in_span+1:i*N_in_span];
  x_seg=x(span);
  y_seg=y(span);
  dummy = numdensity(x_seg,y_seg,NSearch,Res,L_pix);
  image_cube(:,:,i)=dummy;
 end
 span=[(Nsegments-1)*N_in_span+1:N];
 x_seg=x(span);
 y_seg=y(span);
 dummy = numdensity(x_seg,y_seg,NSearch,Res,L_pix);
 image_cube(:,:,Nsegments)=dummy;
 image=sum(image_cube,3);
else
 image = numdensity(x,y,NSearch,Res,L_pix);
end
