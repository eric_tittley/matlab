% astro/xray/Chadra/Functions: Functions particular to analysing Chandra data.
%
% Chandra_Pi_to_Energy	Convert Chandra PI to energy.
%
% FindChipRegion	Find a region which encompasses the events for Chandra.
