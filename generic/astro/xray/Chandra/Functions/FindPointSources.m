% FindPointSources: Find point sources in x-ray data and write a region file.
%
% FindPointSources(data,hdr,Instrument,RegionFile)
%
% ARGUMENTS
%  data		The events data.
%  hdr		The events header from the FITS file.
%  Instrument	The Chandra detector
%  RegionFile	The file to which to write the regions.
%
% RETURNS: Nothing.
%
% SEE ALSO
%  FindChipRegion

% AUTHOR: Eric Tittley
%
% HISTORY
%  050218 First version.
%  050308 Initialise the Region file and check to see if any point sources
%	were found.
%  050404 Use numdensity to calculate the High and Low images.  Lots of
%	tweaking.

function FindPointSources(data,hdr,Instrument,RegionFile)

%PSF = 0.5; % arcsec
Sigma = 4;

%dy=fitsfindkeyvalue(hdr,'TCDLT12'); % deg/pixel
%dy = dy*3600; % seconds/pixel

switch Instrument
 case 'I'
  good=find(data.ccd_id<=3);
 case 'S'
  good=find(data.ccd_id==7);
 otherwise
  disp('Unknown instrument')
end

x = data.x(good);
y = data.y(good);
clear data

min_x = floor(min(x));
min_y = floor(min(y));
Range(1) = ceil(max(x)-min_x);
Range(2) = ceil(max(y)-min_y);
Range = 2*ceil(max(Range)/2); % Make it a multiple of 2.
Limits = [min_x,min_x + Range,min_y,min_y + Range];

Res = Range/2;

%ImgBase = imagebin(x,y,Res,Limits);
%ImgHigh = smooth(ImgBase,2,'gauss');
%ImgHigh(ImgHigh<0) = 0*ImgHigh(ImgHigh<0);
%ImgLow  = smooth(ImgBase,5,'gauss');
%ImgLow(ImgLow<0) = 0*ImgLow(ImgLow<0);

%size(x),size(y),Res,Limits

ImgHigh = numdensity(x,y, 16,Res,Limits)'/Res^2;	% #/pixel
ImgLow  = numdensity(x,y,100,Res,Limits)'/Res^2;	% #/pixel
ImgLow = smooth(ImgLow,10,'gauss'); % Minimum smoothing.

%figure(1)
%imagesc(log(ImgBase-min(min(ImgBase))+1e-1)), colorbar
%figure(2)
%imagesc((log(ImgHigh-min(min(ImgHigh))+1e-1))), colorbar
%return

%Base=mean(mean(ImgBase));
Base = median(ImgLow(find(ImgLow>0)));
Diff=ImgHigh-ImgLow;
Diff(ImgLow>Base)=Diff(ImgLow>Base)./(ImgLow(ImgLow>Base));

%figure(1)
%imagesc(Diff>Sigma), colorbar
%return

% Pull out all regions with Diff > Sigma (i.e. sources)
[j,i]=ind2sub(size(Diff),find(Diff>Sigma));

%ph=plot(i,j,'.'); set(ph,'markersize',1)
%return

% Initialise a non-empty Region structure.
Region(1).Sign=-1;
Region(1).Shape='circle';
Region(1).P=[0,0,0];

r=[i';j';zeros(1,length(i))];
if(size(r,2) > 10 )
 [GroupNum,NGroups]=fof(r,2,1,99999,0);

 ps_x = zeros(1,NGroups);
 ps_y = zeros(1,NGroups);
 for grp = 1:NGroups
  indx = find(GroupNum==grp);
  ps_x(grp) = mean(i(indx));
  ps_y(grp) = mean(j(indx));
 end

 % ps_[xy] are in binned-image coords
 % Need to translate into grid coords
 Grid_x = ps_x*2+min_x-1.5;
 Grid_y = ps_y*2+min_y-1.5;

 %ph=plot(ps_x,ps_y,'.'); set(ph,'markersize',1)
 %ph=plot(Grid_x,Grid_y,'.'); set(ph,'markersize',1)
 %return

 %Diff = ImgHigh - ImgLow;
 Diff = ImgHigh;
 n_regions = 0;
 for i=1:NGroups
  p_x = round(ps_y(i));
  p_y = round(ps_x(i));
  SubImage_x = [p_x-10:p_x+10];
  SubImage_x = SubImage_x( find(SubImage_x>0 & SubImage_x<=Res) );
  SubImage_y = [p_y-10:p_y+10];
  SubImage_y = SubImage_y( find(SubImage_y>0 & SubImage_y<=Res) );
  SubImage=Diff(SubImage_x,SubImage_y);
  %imagesc(SubImage), colorbar
  disp(i)
  [bias,sigma,ampl,x0,y0]=gaussian_fit_2d(SubImage);
  if( (sigma<3) & (ampl>2) & x0>9 & x0<13 & y0>9 & y0<13 )
%   SubImage(SubImage<0) = 0*SubImage(SubImage<0);
%   TotalSignal = sum(sum(SubImage));
%   % Twice the value, since the BG will also contain signal
%   count = 2*floor(TotalSignal);
%   if(count>0)
%    n_regions=n_regions+1;
%    x_tmp = x - Grid_x(i);
%    y_tmp = y - Grid_y(i);
%    r2 = (x_tmp.^2 + y_tmp.^2);
%    r2_sort = sort(r2);
%    radius = min([sqrt(r2_sort(count))+0.1,3*sigma]);
%    Region(n_regions).Sign=-1;
%    Region(n_regions).Shape='circle';
%    Region(n_regions).P=[Grid_x(i),Grid_y(i),radius];
%   else
%    imagesc(SubImage), colorbar
%    sum(sum(SubImage))
%    disp(['ERROR: count=',int2str(count)])
%    pause
%   end
   n_regions=n_regions+1;
   Region(n_regions).Sign=-1;
   Region(n_regions).Shape='circle';
   Region(n_regions).P=[Grid_x(i),Grid_y(i),6*sigma];
  end % if Gaussian represents a point source
 end % loop over groups
end % if enough points to group, else

if(Region(1).P==[0,0,0])
 disp(['WARNING: no point sources found'])
end

WriteRegionFile(Region,RegionFile)
