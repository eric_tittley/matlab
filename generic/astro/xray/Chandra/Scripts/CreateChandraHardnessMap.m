clear
clf
more off

% User Servicable parts.

fid=fopen('ObjName','r');
Cluster=fscanf(fid,'%s');
fclose(fid);
CPS_min=0;

% Also note that I need the following Region files:
%  Regions/PointSources.reg ( a list of sources to remove)
%  FindPointSources() will produce this.

%%%% End of User Servicable parts
Res=200;

% The list of observation numbers
load ObsID

% The instrument for each observation
load Inst.mat

ContourLevels=2.^[-16.5:0.5:0];

% The smoothing parameters
NSearch=200;
BeamWidth_arcmins=2/60; % arcmins

FontSize=10;

%%% Read in Exposure maps and combine (need Res before next step).
for i=1:length(ObsID)
 [hdr_EM,EM_tmp]=fitsload(['product/expmap_',int2str(ObsID(i)),'.fits']);
 if(i==1)
  EM=EM_tmp;
 else
  EM=EM+EM_tmp;
 end
end
Res=size(EM,2);

%%% Read in the Events data.
%%% Filter
%%% Put into a common coordinate system (Physical, with Lower Left = 0,0)
%%% Combine

%%%%% Get the limits of the image %%%%%
% Get the Limits in RA and DEC
load Limits
L=zeros(1,4);
L(1)=Lo(1);
L(2)=Hi(1);
L(3)=Lo(2);
L(4)=Hi(2);
BeamHalfWidth= BeamWidth_arcmins/2 /60*Res/(L(4)-L(3));
% Get the size of the image in pixels
ObsStr=int2str(ObsID(1));
Lphysical=load(['Limits_',ObsStr,'.dat']); % Physical Units
L_pix=[0 Lphysical(2)-Lphysical(1) 0 Lphysical(4)-Lphysical(3)];
% Tick labels
possible_tic_seps = [1/60 1/30 1/12 1/6 1/4 1/3 1/2 1 2 5 10 15 20 30 60 ...
                     120 300 600 900 1200 1800 3600 7200 18000];
dec_range = (Hi(2)-Lo(2))*60; % Minutes
ideal_tic_sep = dec_range/10; % Minutes
tic_sep=possible_tic_seps(interp1(possible_tic_seps,[1:length(possible_tic_seps)],ideal_tic_sep,'nearest'));
DecTicks=[floor(Lo(2)):tic_sep/60:ceil(Hi(2))]; %Degrees
%if(strcmp(Inst(1),'S'))
% RA_step=20; % seconds
%else
% RA_step=30;
%end
RA_range = (Lo(1)-Hi(1))*3600; % Seconds
ideal_tic_sep = RA_range/4; % Seconds
tic_sep=possible_tic_seps(interp1(possible_tic_seps,[1:length(possible_tic_seps)],ideal_tic_sep,'nearest'));
RATicks=[floor(Hi(1)*60)*60:tic_sep:ceil(Lo(1)*60)*60]/3600; % Hours


%%%%% Get the base image %%%%%
disp('Getting the base image...')
% Image has units cts/pix
mat_filename = 'Hardness_base.mat';
if(~ exist(mat_filename) )
 disp(' Accumulating events...')
 x_lo=[];
 y_lo=[];
 x_hi=[];
 y_hi=[];
 for i=1:length(ObsID)
  ObsStr=int2str(ObsID(i));
  disp(['  ',ObsStr])
  %% Read in the events data
  eval(['load evt2_',ObsStr,'.mat'])
  
  %% Filter

  % GTI
  [hdr_GTI,GTI]=fitsload(['product/evt2_',ObsStr,'.gti'],2);
  index=[];
  t=data.time;
  for interval=1:length(GTI.START)
   index=[index;find(t>=GTI.START(interval) & t<=GTI.STOP(interval))];
  end 
  data.x=data.x(index);
  data.y=data.y(index);
  data.energy=data.energy(index);
  data.ccd_id=data.ccd_id(index);

  %Filter out the point sources
  PointSources=ParseRegionFile(['Regions/PointSources_',ObsStr,'.reg']);
  index=FilterByRegion(data.x,data.y,PointSources);
  data.x=data.x(index);
  data.y=data.y(index);
  data.energy=data.energy(index);
  data.ccd_id=data.ccd_id(index);

  %Filter out the high-energy events, which are almost exclusively
  % background (restrict to 0.6 - 7 keV)
  % index=find(data.energy>600 & data.energy<7000);
  % Actually, the lower boundary is not necessary
  index=find(data.energy>100 & data.energy<7000);
  data.x=data.x(index);
  data.y=data.y(index);
  data.ccd_id=data.ccd_id(index);
  data.energy=data.energy(index);

  % Filter the chips
  if(Inst(i)=='S')
   % Filter out the chips other than 7
   index=find(data.ccd_id==7);
  elseif(Inst(i)=='I')
   index=find(data.ccd_id<=3);
  else
   error('Unknown Instrument')
  end
  data.x=data.x(index);
  data.y=data.y(index);
  data.energy=data.energy(index);

  % Put into common coordinate system, with Lower Left = 0,0 
  %x0=fitsfindkeyvalue(hdr,'TCRPX11');
  %y0=fitsfindkeyvalue(hdr,'TCRPX12');
  %dx=fitsfindkeyvalue(hdr,'TCDLT11');
  %dy=fitsfindkeyvalue(hdr,'TCDLT12');
  %alpha0=fitsfindkeyvalue(hdr,'TCRVL11');
  %dec0=fitsfindkeyvalue(hdr,'TCRVL12');

  % Split into soft and hard energies
  MidEnergy=median(data.energy);
  index_lo=find(data.energy <MidEnergy);
  index_hi=find(data.energy>=MidEnergy);

  % Put the data on common grid
  Lphysical=load(['Limits_',ObsStr,'.dat']); % Physical Units
  x_lo=[x_lo;data.x(index_lo)-Lphysical(1)];
  y_lo=[y_lo;data.y(index_lo)-Lphysical(3)];
  x_hi=[x_hi;data.x(index_hi)-Lphysical(1)];
  y_hi=[y_hi;data.y(index_hi)-Lphysical(3)];

 end % loop over ObsID

 clear t index

 disp('Creating the image...')
 % ensure double
 x_lo=double(x_lo);
 y_lo=double(y_lo);
 x_hi=double(x_hi);
 y_hi=double(y_hi);
 L_pix=double(L_pix);
 Nmax=200^3;
 disp([num2str(length(x_lo)),' events to map'])
 Image_base_lo=numdensity_segmented(x_lo,y_lo,NSearch,Res,L_pix,Nmax)'; % Units of cts/frame
 Image_base_lo=Image_base_lo/Res^2; % Units of cts/pixel
 disp([num2str(length(x_hi)),' events to map'])
 Image_base_hi=numdensity_segmented(x_hi,y_hi,NSearch,Res,L_pix,Nmax)'; % Units of cts/frame
 Image_base_hi=Image_base_hi/Res^2; % Units of cts/pixel
 eval(['save ',mat_filename,' Image_base_lo Image_base_hi'])
else
 disp('Base image already created, loading...')
 eval(['load ',mat_filename])
end

%%%%% Correct by the exposure map
disp('Correcting by the exposure map...')
% EM has units ( cm^2 s )
if(~ exist('EM.mat'))
 disp(' Creating exposure map...')
 Image_base=Image_base_lo+Image_base_hi;
 radius=0*Image_base;
 good=find(Image_base>0.01);
 radius(good)=sqrt(NSearch./(pi*Image_base(good)));
 % radius=2*radius;
 % radius=radius/2; % Need this line when there is kernel smoothing
 disp('  Smoothing the exposure map...')
 EM_smooth=SmoothOverRadius(EM,radius);
 save EM EM_smooth EM
else
 disp('  Loading exposure map...')
 load EM
end
Image_scaled_lo=0*Image_base_lo;
Image_scaled_hi=0*Image_base_hi;
good=find(EM_smooth>0);
Image_scaled_lo(good)=Image_base_lo(good)./EM_smooth(good);
Image_scaled_hi(good)=Image_base_hi(good)./EM_smooth(good);

% Now the hardness map
Image_scaled = Image_scaled_hi ./ Image_scaled_lo;

% Cull out any NaNs. Sometimes they happen
Mask=find(isnan(Image_scaled));
if( ~isempty(Mask) )
 Image_scaled(Mask)=1;
 disp(['Warning: produced ',int2str(length(Mask)),' NaNs'])
end

% Smooth the image by a Gaussian to the beam width
disp('Smoothing the image...')
Image=Smooth(Image_scaled,BeamHalfWidth,'gauss');

% Clip the image where the exposure map is zero
Image=Image.*(1&EM);

% Set the aspect ratio for the axes
dec0=mean(L(3:4));
deg_per_hr=cos(dec0*pi/180)*360/24;

figure(1)
colormap(hot(256));

Xaxis=L(1):(L(2)-L(1))/(Res-1):L(2);
Yaxis=L(3):(L(4)-L(3))/(Res-1):L(4);

Hardness_min=min(Image(:));
Hardness_max=max(Image(:));
Hardness_min=0.1;
Hardness_max=1.3;

save Hardness.mat Image Xaxis Yaxis Hardness_min Hardness_max deg_per_hr

figure(1)
imagesc(Xaxis,Yaxis,Image,[Hardness_min Hardness_max])
set(gca,'ydir','nor','xdir','rev')

set(gca,'dataaspectratio',[1 deg_per_hr 1])

set(gca,'FontSize',FontSize)
set(gca,'Ytick',DecTicks);
if((DecTicks(2)-DecTicks(1))*60 < 0.99)
 set(gca,'yTickLabel',DEC2str(DecTicks,3))
else
 set(gca,'yTickLabel',DEC2str(DecTicks,2))
end
set(gca,'Xtick',RATicks);
set(gca,'xTickLabel',RA2str(RATicks,3))

cbh=colorbar;
set(cbh,'fontsize',FontSize)
set(get(cbh,'Ylabel'),'string','Hardness ratio');
set(get(cbh,'Ylabel'),'fontsize',FontSize);

%hold on
%contour(Xaxis,Yaxis,Image,ContourLevels,'k');
%hold off

Title=[Cluster,' Hardness'];
title(Title)

figure(1)
eval(['print -depsc ',Cluster,'_Hardness.eps'])
