clear
clf

% User Servicable parts.

fid=fopen('ObjName','r');
 Cluster=fscanf(fid,'%s');
fclose(fid);
CPS_min=0;
CPS_max=64e-4;

% Also note that I need the following Region files:
%  Regions/PointSources.reg ( a list of sources to remove)
%  FindPointSources() will produce this.

%%%% End of User Servicable parts
Res=200;

% The list of observation numbers
load ObsID

% The instrument for each observation
load Inst.mat

ContourLevels=2.^[-16.5:0.5:0];
DecTicks=[-90:2/60:90];
if(strcmp(Inst(1),'S'))
 RA_step=20; % seconds
else
 RA_step=30;
end
RATicks=[0:RA_step:24*3660]/3600;

% The smoothing parameters
NSearch=200;
BeamWidth_arcmins=10/60; % arcmins

FontSize=16;

%%% Read in Exposure maps and combine
for i=1:length(ObsID)
 [hdr_EM,EM_tmp]=fitsload(['product/expmap_',int2str(ObsID(i)),'.fits']);
 if(i==1)
  EM=EM_tmp;
 else
  EM=EM+EM_tmp;
 end
end
Res=size(EM,2);

%%% Read in the Events data.
%%% Filter
%%% Put into a common coordinate system (Physical, with Lower Left = 0,0)
%%% Combine

x=[];
y=[];
for i=1:length(ObsID)
 ObsStr=int2str(ObsID(i));
 %% Read in the events data
 eval(['load evt2_',ObsStr,'.mat'])
 
 %% Filter

 % GTI
 [hdr_GTI,GTI]=fitsload(['product/evt2_',ObsStr,'.gti'],2);
 index=[];
 t=data.time;
 for interval=1:length(GTI.START)
  index=[index;find(t>=GTI.START(interval) & t<=GTI.STOP(interval))];
 end 
 data.time=data.time(index);
 data.x=data.x(index);
 data.y=data.y(index);
 data.energy=data.energy(index);
 data.ccd_id=data.ccd_id(index);

 %Filter out the point sources
 PointSources=ParseRegionFile(['Regions/PointSources_',ObsStr,'.reg']);
 index=FilterByRegion(data.x,data.y,PointSources);
 data.x=data.x(index);
 data.y=data.y(index);
 data.energy=data.energy(index);
 data.ccd_id=data.ccd_id(index);

 %Filter out the high-energy events, which are almost exclusively
 % background (restrict to 0.6 - 7 keV)
 index=find(data.energy>600 & data.energy<7000);
 data.x=data.x(index);
 data.y=data.y(index);
 data.energy=data.energy(index);
 data.ccd_id=data.ccd_id(index);

 % Filter the chips
 if(Inst(i)=='S')
  % Filter out the chips other than 7
  index=find(data.ccd_id==7);
 elseif(Inst(i)=='I')
  index=find(data.ccd_id<=3);
 else
  error('Unknown Instrument')
 end
 data.x=data.x(index);
 data.y=data.y(index);
 data.energy=data.energy(index);
 data.ccd_id=data.ccd_id(index);

 % Put into common coordinate system, with Lower Left = 0,0 
 x0=fitsfindkeyvalue(hdr,'TCRPX11');
 y0=fitsfindkeyvalue(hdr,'TCRPX12');
 dx=fitsfindkeyvalue(hdr,'TCDLT11');
 dy=fitsfindkeyvalue(hdr,'TCDLT12');
 alpha0=fitsfindkeyvalue(hdr,'TCRVL11');
 dec0=fitsfindkeyvalue(hdr,'TCRVL12');

 % Put the data on common grid
 Lphysical=load(['Limits_',ObsStr,'.dat']); % Physical Units
 x=[x;data.x-Lphysical(1)];
 y=[y;data.y-Lphysical(3)];
 % x & y are in physical units, with the lower right at (0,0)
 L_pix=[0 Lphysical(2)-Lphysical(1) 0 Lphysical(4)-Lphysical(3)];

 % Get the Limits in RA and DEC
 L=zeros(1,4);
 L(1:2)=((Lphysical(1:2)-x0)*dx/cos(dec0*pi/180)+alpha0)/15; % RA
 L(3:4)=(Lphysical(3:4)-y0)*dy+dec0; % DEC
 
 % deg_per_hr=cos( mean(L(3:4))*pi/180) *360/24;
 BeamHalfWidth= BeamWidth_arcmins/2 /60*Res/(L(4)-L(3));

end % loop over ObsID


%%%%% Get the base image %%%%%
% Image has units cts/pix
mat_filename = 'Image_base.mat';
if(~ exist(mat_filename) )
 disp('Creating the image')
 Image_base=numdensity(x,y,NSearch,Res,L_pix)'; % Units of cts/frame
 Image_base=Image_base/Res^2; % Units of cts/pixel
 eval(['save ',mat_filename,' Image_base'])
else
 eval(['load ',mat_filename])
end

%%%%% Correct by the exposure map
% EM has units ( cm^2 s )
if(~ exist('EM.mat'))
 radius=0*Image_base;
 good=find(Image_base>0.01);
 radius(good)=sqrt(NSearch./(pi*Image_base(good)));
 % radius=2*radius;
 radius=radius/2; % Need this line when there is kernel smoothing
 disp('smoothing the exposure map')
 EM_smooth=SmoothOverRadius(EM,radius);
 save EM EM_smooth EM
else
 load EM
end
Image_scaled=0*Image_base;
good=find(EM_smooth>0);
Image_scaled(good)=Image_base(good)./EM_smooth(good);

% Smooth the image by a Gaussian to the beam width
disp('smoothing the image')
Image=smooth(Image_scaled,BeamHalfWidth,'gauss');

% Clip the image where the exposure map is zero
Image=Image.*(1&EM);

% Current units for Image are cts cm^-2 s^-1 pix^-2
% Such that sum(sum(Image))*(exposure time)*(mean eff. area) = # of events
% Let's convert to cts cm^-2 s^-1 arcmin^-2
pix=(L(4)-L(3))*60/(Res-1); % one pixel = 'x' arcmin
Image=Image/pix^2; % Image is now in units cts cm^-2 s^-1 arcmin^-2

% Set the aspect ratio for the axes
dec0=mean(L(3:4));
deg_per_hr=cos(dec0*pi/180)*360/24;

figure(1)
colormap(flipud(colourmap(256)));

Xaxis=L(1):(L(2)-L(1))/(Res-1):L(2);
Yaxis=L(3):(L(4)-L(3))/(Res-1):L(4);

save Image.mat Image Xaxis Yaxis CPS_min CPS_max deg_per_hr

%imagesc(Xaxis,Yaxis,Image,[CPS_min CPS_max])
imagesc(Xaxis,Yaxis,Image)
set(gca,'ydir','nor','xdir','rev')

set(gca,'dataaspectratio',[1 deg_per_hr 1])

set(gca,'FontSize',FontSize)
set(gca,'Ytick',DecTicks);
set(gca,'yTickLabel',DEC2str(DecTicks,2))
set(gca,'Xtick',RATicks);
set(gca,'xTickLabel',RA2str(RATicks,3))

cbh=colorbar;
set(cbh,'fontsize',FontSize)
set(get(cbh,'Ylabel'),'string','cts cm^{-2} s^{-1} arcmin^{-2}');
set(get(cbh,'Ylabel'),'fontsize',FontSize);

%hold on
%contour(Xaxis,Yaxis,Image,ContourLevels,'k');
%hold off

Title=[Cluster,' Flux: 0.6 - 7 keV'];
title(Title)

if(0)
% Contour plot
figure(2)
colormap([0 0 0])
contour(Xaxis,Yaxis,Image,ContourLevels);
set(gca,'ydir','nor','xdir','rev')

set(gca,'dataaspectratio',[1 deg_per_hr 1])

set(gca,'FontSize',FontSize)
set(gca,'Ytick',DecTicks);
set(gca,'yTickLabel',DEC2str(DecTicks,2))
set(gca,'Xtick',RATicks);
set(gca,'xTickLabel',RA2str(RATicks,3))

title(Title)
end

if(0)

% Write the image to FITS format
hdr_out=[];
% Mandatory keywords
hdr_out=fitsaddkey(hdr_out,'XTENSION','IMAGE',0);
hdr_out=fitsaddkey(hdr_out,'BITPIX',-32,0);
hdr_out=fitsaddkey(hdr_out,'NAXIS',2,0);
hdr_out=fitsaddkey(hdr_out,'NAXIS1',Res,0);
hdr_out=fitsaddkey(hdr_out,'NAXIS2',Res,0);
hdr_out=fitsaddkey(hdr_out,'PCOUNT',0,0);
hdr_out=fitsaddkey(hdr_out,'GCOUNT',1,0);

% Some useful info pertaining to this writing of the file
D=datevec(now);
DATE=[int2str(D(1)),'-',int2str(D(2)),'-',int2str(D(3)),...
      'T',int2str(D(4)),':',int2str(D(5)),':',int2str(D(6))];
hdr_out=fitsaddkey(hdr_out,'CREATOR','Matlab script PlotImage.m, using fitswrite',0);

% Useful info from the original hdr (everything between LONGSTRN
% and HISTNUM, not including HISTNUM)
%LineStart=fitsfindkey(hdr,'LONGSTRN');
%LineEnd=fitsfindkey(hdr,'HISTNUM')-1;
%hdr_out=[hdr_out;hdr(LineStart:LineEnd,1:80)];

% Position
hdr_out=fitsaddkey(hdr_out,'CTYPE1','RA---TAN',0);
hdr_out=fitsaddkey(hdr_out,'CTYPE2','DEC--TAN',0);
hdr_out=fitsaddkey(hdr_out,'CRPIX1',Res,0);
hdr_out=fitsaddkey(hdr_out,'CRPIX2',1.0,0);
hdr_out=fitsaddkey(hdr_out,'CRVAL1',L(1)*15,0);
hdr_out=fitsaddkey(hdr_out,'CRVAL2',L(3),0);
hdr_out=fitsaddkey(hdr_out,'CDELT1',-1*(L(2)-L(1))*15/Res,0);
hdr_out=fitsaddkey(hdr_out,'CDELT2',   (L(4)-L(3))/Res,0);

% This will not be an XTENSION
hdr_out=fitsremovekey(hdr_out,'XTENSION');

% Write the FITS file
if(exist(filename))
 eval(['!rm ',filename])
end
status=fitswrite(hdr_out,fliplr(Image),filename);

end % if(0)

figure(1)
eval(['print -depsc -cmyk ',Cluster,'_CMYK.eps'])
eval(['print -depsc ',Cluster,'_RGB.eps'])

if(0)
figure(2)
eval(['print -deps ',Cluster,'_Contour.eps'])
end
