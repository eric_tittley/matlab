function [n_H,N_H,rho,rho_e] = est_Density_cosmo(rad,z,H,norm,Z,OMEGA,OMEGA_V)
% est_Density_cosmo: Galaxy cluster density estimate given a normalization from a
%	       Mekal or Raymond-Smith model
%
% [n_H,N_H,rho,rho_e] = est_Density_cosmo(rad,z,H,norm,Z,OMEGA,OMEGA_V)
%
% ARGUMENTS
%  rad		Angular radius on the extraction region in arcmin
%  z		Redshift of the cluster
%  H		Hubble constant (km Mpc^-1 s^-1)
%  norm 	Normalization of the mekal or raymond-smith model
%  Z		Metallicity
%  OMEGA	Matter density of the universe (OMEGA_M + OMEGA_R)
%  OMEGA_V	Vacuum density of the universe
%
% RETURNS
%  n_H		Average hydrogen number density [cm^-3
%  N_H		Number of hydrogen atoms in the region
%  rho		Density [g cm^-3]
%  rho_e	Density of free electrons [g cm^-3]
%
% NOTES
%  OMEGA_R = 4.1844e-5*(100/H)^2 (T_2.73)^4, where T is assumed to be 2.73 K.
%
% REQUIRES
%  cosmo_z2D RhoToNeNi
%
% SEE ALSO
%  est_Density

% AUTHOR: Danny Hudson, Eric Tittley
%
% HISTORY
%  030723 4th Anniversary of Chandra
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

%__________________U_N_I_V_E_R_S_A_L__C_O_N_S_T_A_N_T_S_________________________%
% From Allen's Astrophysical quantities 4th Ed.					%
c = 2.99792458e5; 		% speed of light in km s^-1			%
%mass_H = 1.6735344e-24;	% mass of hydrogen atom in grams		%
mass_H = 1.672623110e-24; 	% mass of a stripped hydrogen atom in grams	%
mass_e = 9.109389754e-28; 	% mass of electron in grams			%
Mpc2cm = 3.0857e24;		% cm/Mpc					%
%_______________________________________________________________________________%

%_______________________________________________________________%
% If we assume uniform density, we can used the normalization	%
% of our mekal model to determine the density			%
%								%
% According to XSPEC						%
% Norm = 1e-14/(4*pi(D_A(1+z))^2)*int(n_e n_h dV)		%
%								%
% where D_A is the angular size distance			%
% z is the redshift						%
% and n_e and n_h are the electron and hydrogen densities 	%
% respectively							%
%_______________________________________________________________%

%_______________________________________________________________________%
% We need n_h, and from the metallicity, we should be able		%
% to calculate the density						%
% rho = n_h*mass_h + Y*n_h*mass_He + Z*n_h*avg(mass_others)		%
%									%
% If we assume uniform density						%
% int(n_e n_h dV) = V*ratio*(N_h)^2,					%
% where N_h is the number of hydrogen ions and ratio is the ratio of	%
% electrons to hydrogen ions (n_e/n_h)					%
% so, that								%
%  N_H =sqrt(Norm*4*pi*(D_A(1+z))^2/(V*ratio)				%			
%_______________________________________________________________________%

%______________C_A_L_C_U_L_A_T_I_N_G__D_A_(A_N_G_U_L_A_R__S_I_Z_E__D_I_S_T_A_N_C_E______________%
%												%
 				D_A = cosmo_z2D(z,H,OMEGA,OMEGA_V);  % D_A in Mpc		%
%												%
				D_A = D_A*Mpc2cm; % D_A in cm					%
%_______________________________________________________________________________________________%


% We assume the the mekal model uses n_H and not n_i, (I'll have to check on this)

[ne,ni] = RhoToNeNi(1,Z);	% density doesn't matter because we just want the ratio.

%%%%%%%% From Allen's, 4th edition %%%%%%%%%%%%
%%                                Stripped   %%
%% Element group Number   Mass    electrons  %%
%%                                           %%
%%  H            100      100     100        %%
%%  He             9.8     39      20        %%
%%  C,N,O,Ne       0.145    2.19    1.1      %%
%%  Other          0.013    0.44    0.21     %%
%%                                           %%
%%  Total        109.96   141.63  121.3      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%so 
nh = ni*100/(100+9.8 + Z*.145 + Z*.013);	% ni is the number of ions Z is the solar metallicity
ratio = ne/nh;

N_H = sqrt(norm*4*pi*(D_A*(1+z))^2/1e-14/ratio); 			             % h^(-1) hydrogen atoms in region
N_e = N_H*ratio; 			             				     % h^(-1) electrons in region

n_H = N_H/(4/3 * pi * (D_A*rad/60*pi/180)^3)^(1/2);	  				     % hydrogen density (h^(1/2) cm^-3)
n_e = n_H*ratio;	  				     			     % electron density (h^(1/2) cm^-3)

rho = n_H*mass_H;	  				     % hydrogen density (h^(1/2) g cm^-3)
rho_e= n_e*mass_e;	  				     % electron density (h^(1/2) g cm^-3)

rho_i = n_H*mass_H + 9.8/100*n_H*mass_H*4 + ...
		 n_H*mass_H*Z*2.19/100 + n_H*Z*mass_H*.44/100 + ratio*n_H*mass_e; % density in (h^(1/2) g cm^-3)
% output results
fprintf('\n\nThe results based on the input are: \n \n');
fprintf('You have a plasma with %g h_%g^(-1) hydrogen atoms. \n',N_H,H);
fprintf('For a region with a radius of %g h_%g^(-1) kpc, \n',D_A/Mpc2cm*1000*rad/60*pi/180,H);
fprintf('this corresponds to a density of %e h_%g^(1/2) cm^-3 \n',n_H,H);
fprintf('or %g h_%g^(1/2) g cm^-3. \n',rho,H);
fprintf('You have  %g h_%g^(-1) electrons. \n',N_e,H);
fprintf('or a density of %e h_%g^(1/2) cm^-3 \n',n_e,H);
fprintf('or %g h_%g^(1/2) g cm^-3. \n',rho_e,H);
fprintf('The other elements contribute to this density,\n');
fprintf('so that the ion density is %e h_%g^(1/2) cm^-3, or \n',n_H*(100+Z*9.96)/100,H);
fprintf('%g h_%g^(1/2) g cm^-3 \n \n',rho_i,H);


%______________________________________SUBROUTINE COSMO_z2D_____________________________________________%
%													%
%		Calculates D_A for this program for cosmological distances				%
	function DA = cosmo_z2D(z,H,OMEGA,OMEGA_V);			  				%
%													%
% 	z is the redshift										%
% 	H is the hubble constant									%
% 	OMEGA is the Matter density of the universe (OMEGA_M + OMEGA_R)					%
% 	OMEGA_R = 4.1844e-5*(100/H)^2 (T_2.73)^4, where T is assumed to be 2.73 K.			%
% 	OMEGA_V is the vacuum density of the universe							%
% 	This program calculates the Angular size distance DA						%
%													%
% 	These calculations are taken from Ned Wright's very helpful website...				%
% 	see http://www.astro.ucla.edu/~wright/Distances_details.gif					%
% 	http://www.astro.ucla.edu/~wright/cosmo_02.htm							%
%													%
%													%
%													%
%	%%%%%%%%%%%%% C O N S T A N T S %%%%%%%%%							%
%	% Speed of light from X-ray Data Booklet%							%		
%	%					%							%
       		c = 2.99792458e5; % km/s	%							%
%	%					%							%
%	%					%							%
%	%  OMEGA_R from "Allen's Astrophysical	%							%
%	%	         Quantities 4th ed."	%							%
%	%					%							%
        	OMEGA_R = 4.1844e-5*(100/H)^2;  %							%
%	%          	(T_2.73)^4 (units of	%							%
%	%			      2.73 K) 	%							%
%	%		       			%							%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%							%
%													%
%													%
%													%
%	%%%%%%%%%%%%%%%%%% V A R I A B L E   D E F I N I T I O N S %%%%%%%%%%%%%%			%
%	%									%			%
%	% 		  Omega_m is the matter density				%			%
%	%									%			%
		  	OMEGA_M = OMEGA - OMEGA_R;				%			%
%	%									%			%
%	%									%			%
%	% 	         Omega_v = LAMBDA*c^2/(3H^2) 	 			%			%
%	%									%			%
%	%									%			%
%	% 		Omega_r0 is the radiation density 			%			%
%	%									%			%
%	%									%			%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%			%
%													%
%													%
%													%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%% F O R M U L A E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%	%										%		%
%	% 			      D = (cZ(z)/H)					%		%
%	%										%		%
%	% 		DA = cZ(z)J([1-OMEGA_TOT]Z^2)/(H (1+z)), where			%		%
%	%										%		%
%	% 		Z(z) = int(da/a/sqrt(X),a=1/(1+z)...1) and			%		%
%	%										%		%
%	% 	X(a) = Omega_m0/a + Omega_r0/a^2 + Omega_v0*a^2 + (1-OMEGA_TOT)		%		%
%	%										%		%
%	% 	J(x) = sin(sqrt(-x)/sqrt(-x)		     for x<0			%		%
%	%            = 1 + x/6 + x^2/120 + ...x^n/(2n+1)!    for x~0			%		%
%	%            = sinh(sqrt(x)/sqrt(x)		     for x>0			%		%
%	%										%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%													%
%	%------------------------------- Calculate Z.-----------------------------------%		%
%	%                   The integral is the only really tricky part			%		%
%	%										%		%
%	%										%		%
%	%------------------- Calculate the integral using Maple-------------------------%		%
%	%										%		%
%	%										%		%
%	%	CREATE A COMMAND LINE FOR THE INTEGRAL + AN QUIT COMMAND		%		%
%	%										%		%
%	%										%		%
%	%			        FIND MAPLE					%		%
%	%										%		%
		[DMY,MPL] = unix('locate maple | grep /bin/maple');			%		%
%	%										%		%
%	% 		      CHECK TO SEE IF MAPLE WAS FOUND				%		%
%	%										%		%
%	%										%		%
%	%_______________________________________________________________________________%		%
%	%										%		%
%	%		   IF "NO" DO THE INTEGRATION WITH MATALB			%		%
%	%										%		%
%	%										%		%
		if DMY == 1								%		%
	 	 fprintf('\nCouldn''t find Maple, doing the integral ');		%		%
        	 fprintf('numerically with matlab.\n\n\n');				%		%
%	%										%		%
%	%                create an inline function for the integrand			%		%
%	%										%		%
		 INT = inline(['1./a./sqrt(',num2str(OMEGA_M),'./a + '...		%		%
                                            ,num2str(OMEGA_R),'./(a.^2) + '...		%		%
				            ,num2str(OMEGA_V),'.*a.^2 + '...		%		%
				            ,num2str(1-(OMEGA+OMEGA_V)),')']);		%		%
%	%										% 		%   
%	% 		Make a vector that defines the step size for			%		%
%	%		      the numerical integration					%		%
%	%		(2e-5 precision is good enough for now)				%		%
%	%										%		%
		  A = [1/(1+z):(1-1/(z+1))/(5e4-1):1];					%		%
		  N=length(A);								%		%
%	%										%		%
%	%										%		%
%	% 		     Integrate using the trapezoidal rule			%		%
	           Z=sum((1/2*(INT(A(1:N-1)) + INT(A(2:N)))).*(A(2:N)-A(1:N-1)));	%		%
%	%										%		%
%	%										%		%
%	%										%		%
		 else									%		%
%	%_______________________________________________________________________________%		%
%	%										%		%
%	%		  IF "YES" USE MAPLE TO DO THE INTEGRAL				%		%
%	%										%		%
%	%										%		%
		  PST = findstr(MPL,'bin/maple');					%		%
		  DRCTRY = MPL(1:PST(1)+3);						%		%
%	%										%		%
%	%										%		%
%	%			   EXECUTE THE COMMAND					%		%
%	%										%		%
	          cmds = ['evalf(int(1/a/sqrt(',num2str(OMEGA_M),'/a + '...		%		%
		                               ,num2str(OMEGA_R),'/(a^2) + '...		%		%
					       ,num2str(OMEGA_V),'*a^2 + '...		%		%
					       ,num2str(1-(OMEGA+OMEGA_V)),'),a='...	%		%
					       ,num2str(1/(1+z)),'..1),20); quit;'];	%		%
	          [DMY,TEMP_Z] =  unix(['echo "',cmds,'" | ',DRCTRY,'maple -q']);	%		%
%	%										%		%
%	%										%		%
%	%		   PARSE THE OUTPUT FOR THE NUMERIC PART			%		%
%	%		   it should end 2 positions before the				%		%
%	%			    the first CR		 			%		%
%	%										%		%
		  PRSZ = findstr(TEMP_Z,'[');						%		%
		  Z = str2num(TEMP_Z(1,1:PRSZ(1)-2));					%		%
	         end									%		%
%	%										%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%													%
%	% ----------------------Calculate D  D_A  D_L-----------------------------------%		%
%													%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%	%										%		%
	                               D = c*Z/H;					%		%
%	%										%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%													%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D_A   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%	%										%		%
%	%		Calculate J based on the value of 1-(OMEGA+OMEGA_V)		%		%
%	%										%		%
%	% 				Flat universe					%		%
%	% 				  J = 1						% 		%
	         if 1-(OMEGA+OMEGA_V) == 0						%		%
	                           DA = c*Z/H/(1+z);					%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%				Almost Flat Universe				%		%
%	%										%		%
%	% if |(1-(OMEGA+LAMBDA))|Z^2 							%		%
%	% is less than 0.1 								%		%
%	% (ie close to a flat universe)							%		%
%	% then calculate a series (20 terms is more than enough)			%		%
	        elseif abs(1-(OMEGA+OMEGA_V)).*Z.^2 < .1				%		%
%	%										%		%
	            		   DA = c.*Z./H./(1+z).*...				%		%
				        sum(((1-(OMEGA+OMEGA_V)).*Z.^2).^[0:20]./...	%		%
					      FACTORIAL(2.*[0:20]+1));			%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 				 Closed Universe				%		%
%	%										%		%
		elseif 1-(OMEGA+LAMBDA) < 0						%		%
	 			   DA = c*Z/H/(1+z)*...					%		%
				        sin(sqrt(-1*(1-(OMEGA+OMEGA_V))*Z^2))/...	%		%
					sqrt(-1*(1-(OMEGA+OMEGA_V))*Z^2);		%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 				 Open Universe					%		%
%	%										%		%
		else									%		%
				    DA = c*Z/H/(1+z)*...				%		%
				        sinh(sqrt((1-(OMEGA+OMEGA_V))*Z^2))/...		%		%
					sqrt((1-(OMEGA+OMEGA_V))*Z^2);			%		%
		end									%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%													%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D_L   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%	%										%		%
					DL = (1+z)^2*DA;				%		%
%	%										%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%													%
%	%-------------------F A C T O R I A L    S U B R O U T I N E--------------------%		%
%	% 	     This a little function I wrote to calculate a factorial for	%		%
%	%	        a vector of values that does not involve a "for loop"		%		%
%	%										%		%
%	%										%		%
				   function F = FACTORIAL(N);				%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%			 Determine the maximum factorial			%		%
%	% 			  that needs to be computed.				%		%
%	%										%		%
		MN = max(N);								%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			Create a lower triangular matrix 			%		%
%	%			with vlues of 1	and size MN x MN			%		%
%	% 			There is also a built-in MATLAB routine 		%		%
%	%				"qr" to do this					%		%
%	%										%		%
%	%										%		%
		A = fliplr([1:MN]'*[1:MN])...						%		%
		  - flipud([1:MN]'*[1:MN]);						%		%
		A(A>=0) = 1;								%		%
		A(A<0) = 0;								%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			Make a matrix of columns with values 			%		%
%	%			   going from MN down to 1				%		%
%	%										%		%
		B = [MN:-1:1]'*ones(1,MN);						%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			      Make a Matrix of columns 				%		%
%	%		      [[MN:-1:1]',0;[MN-1:-1:1]',0;0;[MN-2:-1:1]',...		%		%
%	%										%		%
		C=A.*B;									%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			  Change all the zeros in C to ones			%		%
		C(C==0)=1;								%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			Flip it left to right and take 				%		%
%	%			the product of all the columns				%		%
		TF = fliplr(prod(C));							%		%
%	%										%		%
%	%										%		%
%	%										%		%
%	% 			Extract the values of the vector N			%		%
		F = TF(N);								%		%
%	%										%		%
%	%										%		%
%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		%
%													%
%_______________________________________________________________________________________________________%
