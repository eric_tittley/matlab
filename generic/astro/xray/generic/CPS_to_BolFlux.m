function flux=CPS_to_BolFlux(CPS,Channels,T,Z,z,rsp,E_range)
%
% flux=CPS_to_BolFlux(CPS,Channels,T,Z,z,rsp,[E_range]);

if(nargin==6)
 E_range=[0 100];
end

hdr.h100=0.6;
Units

Dist_nom=260;
Rho_nom =1e-28;
Vol_nom =1e10;

spec_new=CreateMekalSpec(1,T,Rho_nom,rsp,Vol_nom,Dist_nom,[1,1,Z*ones(1,13)],z);

CPS_initial=sum(spec_new(Channels));

Norm=CPS/CPS_initial
dbin=0.1;
bins=[E_range(1):dbin:E_range(2)];
[BolSpec,ed]=mekal(bins,Norm*Vol_nom*kpc^3/1e50/(Dist_nom*1e6)^2,(3/4)*Rho_nom/amu,T/keV,[1,1,Z*ones(1,13)]);
BolSpec=redshift_spec(BolSpec,bins,z);
E=(bins(1:end-1)+bins(2:end))/2;
flux=sum(BolSpec.*E'*dbin)*1.60184e-9; % erg/s/cm^2
