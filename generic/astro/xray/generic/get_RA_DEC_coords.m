function [RA,Dec]=get_RA_DEC_coords(data,header,Limits)
% get_RA_DEC_coords: RA and Dec of the events
%
% [RA,Dec]=get_RA_DEC_coords(data,header,[Limits]);
%
%
% ARGUMENTS
%  data		The events in detector coordinates
%		 data.X & data.Y must be defined
%  header	The header for the data. The following must be set:
%		 TCRPX1
%		 TCRPX2
%		 TCDLT1
%		 TCDLT2
%		 TCRVL1
%		 TCRVL2
%  limits	(optional) Trim the data but the limits:
%		 [RA_min RA_MAX Dec_min Dec_max]
%		
% RETURNS
%  RA and Dec of the events
%
% REQUIRES
%  fitsfindkeyvalue
%
% SEE ALSO
%  fitsload

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 28 First Version
%
% COMPATIBILITY: Matlab, Octave

x0=fitsfindkeyvalue(header,'TCRPX1');
y0=fitsfindkeyvalue(header,'TCRPX2');
dx=fitsfindkeyvalue(header,'TCDLT1');
dy=fitsfindkeyvalue(header,'TCDLT2');
alpha0=fitsfindkeyvalue(header,'TCRVL1');
dec0=fitsfindkeyvalue(header,'TCRVL2');
RA=(data.X-x0)*dx/cos(dec0*pi/180)+alpha0;
Dec=(data.Y-y0)*dy+dec0;

% Convert to HMS from DMS
RA=RA/180*12;

if(nargin==3)
 good=find(RA>Limits(1) & RA<Limits(2) & Dec>Limits(3) & Dec<Limits(4));
 RA=RA(good);
 Dec=Dec(good);
end
