function [rmf,Energies,Channels]=ExtractRMF(rmf_struct)
% ExtractRMF: Extract an RMF from a OGIP RMF structure.
%
% [rmf,Energies,Channels]=ExtractRMF(rmf_struct);
%
% Given a structure from an RMF (or Response) fits file,
% extract and return the RMF (or Response) matrix.
%
% rmf will have dimensions [N_energies x N_Channels];
%
% See also:
%  fitsload CreateRMFStruct

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 12 06 Modify comments
%  01 04 26 Incorporate support for multi-group RMF/RSP's.
%
% COMPATIBILITY: Matlab, Octave
%
% BUGS
% Since Energies is just the average of ENERG_LO and ENERG_HI,
% some potentially useful information is lost.  But since that
% information is already provided by the user, the user does not
% lose it.

N_Chan=max(max(rmf_struct.F_CHAN+rmf_struct.N_CHAN))-1;

N_E=length(rmf_struct.F_CHAN(:,1));
rmf=zeros(N_E,N_Chan);
for i=1:N_E
 for j=1:rmf_struct.N_GRP(i)
  rmf_span= rmf_struct.F_CHAN(i,j)+[0:rmf_struct.N_CHAN(i,j)-1];
  if(j==1)
   mat_lo=0;
  else
   mat_lo=sum(rmf_struct.N_CHAN(i,1:j-1));
  end
  rmf(i,rmf_span) =  rmf_struct.MATRIX(i,mat_lo+[1:rmf_struct.N_CHAN(i,j)]);
 end % loop over subsets
end % loop over energies

Energies=(rmf_struct.ENERG_LO+rmf_struct.ENERG_HI)/2;
Channels=[1:N_Chan];
