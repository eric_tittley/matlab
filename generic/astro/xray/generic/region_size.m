function rad = region_size(region,shape);
% region_size: The effective radius of a region.
%
%  rad = region_size(region,shape);
%
% ARGUMENTS
%  region	region file
%  shape	(optional) The assumed 3-D shape of the region. Can be one of:
%		 'sphere' (default), 'cylinder',or 'cube'.
%
% RETURNS
%  The radius of a sphere (in arcmin) of the equivalent volume of the region.
%
% NOTES
%  For a sphere, the radius is calculated for a circle of the same area of the
%  region.
%
%  For a cylinder, the radius is calculated for a circle of the same area of the
%  region. The hieght of the cylinder is assumed to be the diameter of the
%  circle. Finally the radius of a sphere with the same volume as the cylinder
%  is calculated.
%
%  For a cube, the side of a square is calculated for the area of the region.
%  The radius of a sphere, with the same volume as the cube is calculated.
%
%  rad may be input to est_Density to get an density estimate for a given region
%  (assuming constant density in that region)
%
%  ONLY SUPPORT 'circle', 'rotbox', and 'pie' regions
%
%  THIS PROGRAM ASSUMES A LINEAR SUMMATION OF REGIONS!  THAT IS FOR 
%  +circle(345,234,234)
%  -box(456,234,234,123,65)
%  -pie(234,123,132,154,123,154)
%  +circle(363,432,345)
%  it will be reg1 - reg2 - reg3 + reg4 => net region
%
%  This function was written for Chandra, but I can't see why this is necessary.
%
% REQUIRES
%  ParseRegionFile
%
% SEE ALSO
%  est_Density Regions


% AUTHOR: Danny Hudson
%
% HISTORY
%  2003 - July - 23 (4th anniversary of Chandra)
%
% COMPATIBILITY: Matlab, Octave

%-----Check the shape the user would like-------%
% s = 1 => sphere			  	%
% s = 2 => cylinder				%
% s = 3 => cube					%
% 					  	%
if ~(exist('shape','var'))			%
 shape = 'sphere';				%
end						%
if length(shape) == 8 & shape == 'cylinder'	%
 s = 2;						%
elseif length(shape) == 4 & shape == 'cube'	%
 s = 3;						%
else						%
 shape = 'sphere';				%
 s = 1;						%
end						%
%						%
%-----------------------------------------------%





%--------Set Chandra's Resolution---------------%
%						%
% 1.367e-4 degrees/pix				%
%						%
	min2pix = 1.3666666666667E-04*60;	% 
%				min/pix		%
%						%
%-----------------------------------------------%





%---------------------------------------  O F F S E T S  -----------------------------------------------%
%													%
% 	This section of the program determines the offsets of the various regions		 	%
% 	and the smallest offset (that is where we define zero).  Since we are only			%
% 	interested in the net "on" region, the total offset is only calculated for			%
% 	"on" regions.  This is especially important since there are sometimes very large		%
% 	"off" regions.											%
%													%
%													%
%					SET THE VARIABLES						%
%													%
		Reg = ParseRegionFile(region);								%
%													%
%			    Reg(:).Shape => shape of each region in the file				%
%		            Reg(:).P     => Parameters (center,size,etc...) 				%
%                                               of each region in the file				%
%		            Reg(:).Sign  => +1="on" -1="off"						%
%													%
%													%
		bx = 0;											%
%													%
%			bx is the counter for the corners of the boxes (we will need			%
%									their positions			%	
%									later in the program)		%
%													%
%													%
		CNTR = 0;										%
%													%
%			CNTR is the COUNTER for the on-regions to					%
%			     determine where zero will be						%
%_______________________________________________________________________________________________________%
%													%
%													%
%													%
% 				       Loop through the regions						%
%													%
for k = 1:length(Reg)											%
%													%
%_______________________________________________________________________________________________________%
%					For a CIRCLE 							%
%													%
 if (length(Reg(k).Shape) >=6 & Reg(k).Shape(1:6) == 'circle')						%
%													%
% 		This is trivial 									%
%		the lowest    region is  y_center-radius						%
%		the left most region is  x_center-radius						%
%													%
  offset{k}(1)  = Reg(k).P(1)-Reg(k).P(3);								%
  offset{k}(2)  = Reg(k).P(2)-Reg(k).P(3);
  toffset{k}(1) = Reg(k).P(1)-Reg(k).P(3);								%
  toffset{k}(2) = Reg(k).P(2)+Reg(k).P(3);								%
%													%
%_______________________________________________________________________________________________________%
%					For a PIE 							%
%													%
 elseif (length(Reg(k).Shape) >=3 & Reg(k).Shape(1:3) == 'pie')						%
%													%
% 		Keep this is trivial (consider the entire circle it comes from)				%
%		the lowest    region is  y_center-radius						%
%		the left most region is  x_center-radius						%
%													%
  offset{k}(1)  = Reg(k).P(1)-Reg(k).P(4);								%
  offset{k}(2)  = Reg(k).P(2)-Reg(k).P(4);								%
  toffset{k}(1) = Reg(k).P(1)-Reg(k).P(4);								%
  toffset{k}(2) = Reg(k).P(2)+Reg(k).P(4);								%
%													%
%_______________________________________________________________________________________________________%
%					For a BOX 							%
%													%
 elseif (length(Reg(k).Shape) >= 6 & Reg(k).Shape(1:6) == 'rotbox')					%
%													%
% 		First add on to the bx counter 								%
%		(later we will need to acces info							%
%		 about the corner of each bx)								%
  bx = bx+1;												%
%													%
% 		reset "l" to zero 									%
%		("l" is a little trick to get 								%
%		 an alternating odd, odd, then 								%
%				even, even)								%
  l = 0;												%
%													%
% 		Determine the rotation of the 								%
%		box and create a rotation matrix							%
  theta = -Reg(k).P(5)*pi/180;										%
  rotmatrix = [cos(theta) sin(theta);-sin(theta) cos(theta)];						%
%													%
%													%
% 		Clear variable t_corner									%
%		 	      (temporary_corner)							%				
  clear t_corner											%
%													%
%++++++++++++++++++++++++++++ LOOP through the four corners of the box +++++++++++++++++++++++++++++++++%
  for j = 1:4												%
%													%
%		l = 1,3,6,10 (odd,odd,even,even)							%
   l=l+j;												%
%													%
%													%
%		Locate the 4 corners in the 								%
%		unrotated box										%
%													%
   t_corner(j,1) = Reg(k).P(1)+(-1)^j*Reg(k).P(3)/2;	% order (left  right  left  right)		%
   t_corner(j,2) = Reg(k).P(2)+(-1)^l*Reg(k).P(4)/2;	% order (down  down   up    up   )		%
%													%  
%													%
%		Rotate the corners with the								%
%		rotation matrix										%
   corner{bx}(j,1:2) = (rotmatrix*...									%
                        [t_corner(j,1)-Reg(k).P(1);...							%
			 t_corner(j,2)-Reg(k).P(2)]...							%
		      + [Reg(k).P(1);Reg(k).P(2)])';							%
%													%
%													%
  end													%
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++%
%													%
%		Find the offset (the left most point							%
%				 the lowest point, the							%
%				 right most and the highes)						%
%													%
  offset{k}(1) = min(corner{bx}(:,1));									%
  offset{k}(2) = min(corner{bx}(:,2));									%
  toffset{k}(1) = max(corner{bx}(:,1));									%
  toffset{k}(2) = max(corner{bx}(:,2));									%
%													%
%													%
 end													%
%_______________________________________________________________________________________________________%
%													%
% 		If we are conisdering an "on"								%
%		region then record the values								%
%		in a vextor, which we will check							%
%		to find the left most and lowest							%
%		points considered (0,0) of our image							%
%		We also need the right most and								%
%		highest point to get the size of the							%
%		image.											%
%													%
%													%
 if Reg(k).Sign > 0											%
  CNTR = CNTR + 1;											%
  xoffset(CNTR) = offset{k}(1);										%
  yoffset(CNTR) = offset{k}(2);										%
  xtoffset(CNTR) = toffset{k}(1);									%
  ytoffset(CNTR) = toffset{k}(2);									%
 end													%
%													%
%													%
%													%
end													%
%													%
%-------------------------------------------------------------------------------------------------------%


%------------------------------Find the value of "0,0", the net off-set---------------------------------%
%													%
%													%
				total_offset(1) = min(xoffset);						%
				total_offset(2) = min(yoffset);						%
%													%
%													%
%													%
% 		Chandra's physical coordinates are 	    						%
% 		very large.  We only need a reasonable  	   					%
% 		region.  Otherwise the matrices are 	    						%
%		too large			    							%
%					   							 	%
				total_toffset(1) = max(xtoffset);					%
				total_toffset(2) = max(ytoffset);					%
				SIZE = max(total_toffset-total_offset);					%
%													%
%-------------------------------------------------------------------------------------------------------%

%----------------------------------------- The strategy ------------------------------------------------%
%													%
%	The net area of a region is the number of pixels in it times the area per pixel.		%
%	So, if we use an "off" region is zeros and an "on" region as ones.  If we add an		%
%	"on" region and dot and "off region", we will have the net region.  That is			%
%	an "on" region is the inverse of an "off" region.						%
%													%
%-------------------------------------------------------------------------------------------------------%

%------------------------------------ M A K E   T H E   I M A G E --------------------------------------%
%													%
%													%
% 		MAKE AN EMPTY IMAGE									%
IMAGE = zeros(SIZE,SIZE);										%
%													%
%		RESET THE "bx" COUNTER									%
bx = 0;													%
%													%
%_______________________________________________________________________________________________________%
%													%
%			        L O O P    T H R O U G H  T H E   R E G I O N S				%
%													%
%													%
for k =1:length(Reg);
%													%
%													%
%+++++++++++++++++++++++++++++++++++++++++   CIRCLE   ++++++++++++++++++++++++++++++++++++++++++++++++++%
%													%
%		Circle is the easiest region								%
%		Make a grid with (0,0) as the								%
%		center of the circle in the								%
%		"offset" coordinates									%
%													%
 if length(Reg(k).Shape) >=6 & Reg(k).Shape(1:6) == 'circle'						%
  a = ones(1,SIZE);											%
  b = [1:SIZE]-Reg(k).P(1)+total_offset(1);								%
  x = a'*b;												%
  b = [1:SIZE]-Reg(k).P(2)+total_offset(2);								%
  y = b'*a;												%
  r = sqrt(x.^2+y.^2);											%
  temp_image=zeros(SIZE,SIZE);										%
  if Reg(k).Sign > 0											%
   temp_image(r<Reg(k).P(3)) = 1;									%
   IMAGE(IMAGE==0) = IMAGE(IMAGE==0)+temp_image(IMAGE==0);						%
  else													%
   temp_image(r>Reg(k).P(3)) = 1;									%
   IMAGE = temp_image.*IMAGE; 										%
  end 													%
%													%
%													%
%+++++++++++++++++++++++++++++++++++++++++     PIE     +++++++++++++++++++++++++++++++++++++++++++++++++%
%													%
%		This is a little trickier								%
%		than the circle.  Not only								%
%		do we need a grid of radii								%
%		but also angles.  We also								%
% 		have to watch out for									%
%		situations when PHI(1) > PHI(2)								%
%													%
%													%
 elseif length(Reg(k).Shape) >=3 & Reg(k).Shape(1:3) == 'pie'						%
  a = ones(1,SIZE);											%
  b = [1:SIZE]-Reg(k).P(1)+total_offset(1);								%
  x = a'*b;												%
  b = [1:SIZE]-Reg(k).P(2)+total_offset(2);								%
  y = b'*a;												%
  r = sqrt(x.^2+y.^2);											%
  PHI = atan2(y,x)*180/pi;										%
  PHI(PHI<=0) = PHI(PHI<=0)+360;									%
  temp_image=zeros(SIZE,SIZE);										%
  if Reg(k).P(5) >  Reg(k).P(6)										%
   S_PHI =  Reg(k).P(5) - 360;										%
   PHI(PHI>=Reg(k).P(5)) = PHI(PHI>=Reg(k).P(5))-360;							%
  else													%
   S_PHI = Reg(k).P(5);											%
  end													%
  F_PHI = Reg(k).P(6);											%
  													%
  if Reg(k).Sign > 0											%
   temp_image(r>Reg(k).P(3) & r<Reg(k).P(4) & PHI>=S_PHI & PHI<=F_PHI) = 1;				%
   IMAGE(IMAGE==0) = IMAGE(IMAGE==0)+temp_image(IMAGE==0);						%
  else													%
   temp_image(r<Reg(k).P(3) & r>Reg(k).P(4) & PHI<S_PHI & PHI>F_PHI) = 1;				%
   IMAGE = temp_image.*IMAGE; 										%
  end													%
%													%
%													%
%+++++++++++++++++++++++++++++++++++++++      ROTBOX     +++++++++++++++++++++++++++++++++++++++++++++++%
%													%
%		The rotated box is by far								%
%		the most difficult.  Luckily								%
%		we have already found the								%
%		four corners and we know where								%
%		they are (lower left, etc...).								%
%	 	We use two points to find the 								%
%		slope, and then use the slope								%
%		intercept form to find the								%
%		Once we have the four lines,								%
%		we just use inequalities to								%
%		isolate the region inside and								%
%		outside the box.									%
%													%
%													%
 elseif (length(Reg(k).Shape) >= 6 & Reg(k).Shape(1:6) == 'rotbox')					%
  bx = bx+1;												%
  corner{bx}(:,1) = corner{bx}(:,1)-total_offset(1);							%
  corner{bx}(:,2) = corner{bx}(:,2)-total_offset(2);							%
  a = ones(1,SIZE);											%
  b = [1:SIZE];												%
  x = a'*b;												%
  y = b'*a;												%
  l = [1 2 4 3 1];											%
  temp_image = zeros(SIZE,SIZE);									%
%													%
%													%
%				LOOP THROUGH THE CORNERS (1 2 3 4)					%
%													%
%													%
%		NOTE the definition of "l" allows							%
%		us to call j=4+1 => l(5) = 1								%
%													%
  for j = 1:4												%
%													%
% 		Determine the slope									%
%													%
   slope = (corner{bx}(l(j+1),2) ...									%
          - corner{bx}(l(j),2))/(corner{bx}(l(j+1),1) ...						%
	  - corner{bx}(l(j),1));									%
   b = corner{bx}(l(j+1),2) - slope*corner{bx}(l(j+1),1);						%
%													%
%													%
% 		Now Determine the pointing								%
%													%
   if (corner{bx}(l(j+1),2) == min(corner{bx}(:,2)) | corner{bx}(l(j),2) == min(corner{bx}(:,2)))	%
    temp_image(y > slope.*x + b) = temp_image(y > slope.*x + b) + 1;					%
   else													%
    temp_image(y < slope.*x + b) = temp_image(y < slope.*x + b) + 1;					%
   end													%
  end													%
%													%
%													%
  if Reg(k).Sign > 0											%
   temp_image(temp_image~=4) = 0;									%
   temp_image(temp_image==4) = 1;									%
   IMAGE(IMAGE==0) = IMAGE(IMAGE==0)+temp_image(IMAGE==0);						%
  else													%
   temp_image(temp_image~=4) = 1;									%
   temp_image(temp_image==4) = 0;									%
   IMAGE = temp_image.*IMAGE; 										%
  end													%
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++%
 end													%
%													%
%													%
end													%
%-------------------------------------------------------------------------------------------------------%



%---------------------------------- M A K E   T H E   I M A G E  ---------------------------------------%
%													%
hold off												%
imagesc(IMAGE);												%
XO =total_offset(1) - fix(total_offset(1)/100)*100;							%
YO =total_offset(2) - fix(total_offset(2)/100)*100;							%
set(gca,'Xtick',[100:100:round(SIZE/1000)*1000]'-XO);							%
set(gca,'XtickLabel',num2str([100:round(SIZE/1000)*100:round(SIZE/1000)*1000]'-XO+total_offset(1)));	%
set(gca,'ydir','norm');											%
set(gca,'Ytick',[100:round(SIZE/1000)*100:round(SIZE/1000)*1000]'-YO);					%
set(gca,'YtickLabel',num2str([100:100:round(SIZE/1000)*1000]'-YO+total_offset(2)));			%
%													%
%-------------------------------------------------------------------------------------------------------%


%-------------------- C A L C U L A T E   T H E   E Q U I V A L E N T   R A D I U S  -------------------%
if     s == 1												%
 rad = sqrt(sum(sum(IMAGE))*min2pix^2/pi);								%
elseif s == 2												%
 crad = sqrt(sum(sum(IMAGE))*min2pix^2/pi);								%
 Vol = pi*crad^2*2*crad;										%
 rad = (3*Vol/4/pi)^(1/3);										%
elseif s == 3												%
 side = sqrt(sum(sum(IMAGE))*min2pix^2);								%
 Vol = side^3;												%
 rad = (3*Vol/4/pi)^(1/3);										%
end													%
%-------------------------------------------------------------------------------------------------------%
