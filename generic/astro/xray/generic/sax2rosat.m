% sax2rosat: ROSAT coordinates and radius for a BeppoSAX (SKY) extract region
%
% [rosat_x,rosat_y,rosat_r] = sax2rosat(sax_h,rosat_h,sax_x,sax_y,sax_r)
%
% ARGUMENTS
%  sax_h
%  rosat_h
%  sax_x
%  sax_y
%  sax_r
%
% RETURNS
%  rosat_x
%  rosat_y
%  rosat_r
%
% NOTES
%  The header is the zero entry header from the image file
%
% REQUIRES
%  sax2asca
%
% SEE ALSO
%  sax2asca, asca2rosat

% AUTHOR: Danny Hudson
%
% HISTORY
%  02-07-30 Mature version
%  07 10 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function [ro_x,ro_y,ro_r] = sax2rosat(bsh,rh,bs_x,bs_y,bs_r)

[ro_x,ro_y,ro_r] = sax2asca(bsh,rh,bs_x,bs_y,bs_r);
