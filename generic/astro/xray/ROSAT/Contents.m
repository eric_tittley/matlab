% astro/xray/ROSAT: Tools for manipulating ROSAT data.
%
% EXP_MAP_COR			Calculates exposuremap correction factor
%
% ROSAT_exp_map_correction	Calculates exposuremap correction factor
%
% TODO:
%  What is the difference between EXP_MAP_COR and ROSAT_exp_map_correction?
