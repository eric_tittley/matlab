% ROSAT_exp_map_corrections:  Determine the exposuremap correction factor.
%
% factor = ROSAT_exp_map_correction(src.reg,back.reg,header,exp_map);
%
% ARGUMENTS
%  src.reg	The file with the region being extracted (string)
%		assumed to be circle(#,#,#)
%		and/or -circle(#,#,#) etc...
%
% back.reg	The file with the regions being used for background (string)
%		assumed to be circle(#,#,#)
%		and/or -circle(#,#,#) etc...
%
% header	The header of the fits file being used (assumed to be an image
%		or pha file main header(for now))
%
% exp_map	The exposure map file to be used (mex file)
%
% RETURNS
%  factor	The factor by which the spectral file backscal should be
%		multiplied (OR the background file divided)
%
% REQUIRES
% fits tools
%
% SEE ALSO
%  ROSAT_tools, EXP_MAP_COR.m

% AUTHOR: Danny Hudson
%
% HISTORY
%  02 07 30 Mature version
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function factor = ROSAT_exp_map_correction(infile1,infile2,h,exp_map);

[exp_h,exp_d] = fitsload(exp_map);
exp_sz_x = fitsfindkeyvalue(exp_h,'NAXIS1');
exp_sz_y = fitsfindkeyvalue(exp_h,'NAXIS2');


sz_x = fitsfindkeyvalue(h,'NAXIS1');
sz_y = fitsfindkeyvalue(h,'NAXIS2');

if sz_x ~= sz_y
 fprintf('This is not a square image, and it may cause problems!')
end

ratio = exp_sz_x/sz_x;


fid = fopen(infile1,'r');
ch = 0;
t = 1;
u = 1;
while ch ~= -1
 line = fgetl(fid);
 ch = line;
 if line(1) == '-' & ch ~= -1
  on_region_subtract(u,1:3) = str2num(line(9:length(line)-1));
  u = u + 1;
 elseif line(1) == 'c'
  on_region(t,1:3) = str2num(line(8:length(line)-1));
  t = t + 1;
 end
end
fclose(fid);
if exist('on_region_subtract') == 0
 on_region_subtract = zeros(1,3);
end


fid = fopen(infile2,'r');
ch = 0;
t = 1;
u = 1;
while ch ~= -1
 line = fgetl(fid);
 ch = line;
 if line(1) == '-' & ch ~= -1
  back_region_subtract(u,1:3) = str2num(line(9:length(line)-1));
  u = u + 1;
 elseif line(1) == 'c'
  back_region_add(t,1:3) = str2num(line(8:length(line)-1));
  t = t + 1;
 end
end

if exist('back_region_subtract') == 0
 back_region_subtract = zeros(1,3);
end
on_region = on_region*ratio;
on_region_subtract = on_region_subtract*ratio;
back_region_add = back_region_add*ratio;
back_region_subtract = back_region_subtract*ratio;


on_area = pi*on_region(3)^2 - sum(pi*on_region_subtract(:,3).^2);
back_area = sum(pi*back_region_add(:,3).^2) - sum(pi*back_region_subtract(:,3).^2);

On_mask = zeros(exp_sz_x,exp_sz_y);
On_mask_subtract = ones(exp_sz_x,exp_sz_y);
Back_mask_add = On_mask;
Back_mask_subtract = ones(exp_sz_x,exp_sz_y);

a = [1-on_region(1):exp_sz_x-on_region(1)];
b = ones(exp_sz_y,1);
x = b*a;
a = [1-on_region(2):exp_sz_y-on_region(2)];
b = ones(exp_sz_x,1);
y = a'*b';
r = sqrt(x.^2 + y.^2);
On_mask(r <= on_region(3)) = 1;

for t = 1:length(on_region_subtract(:,1))
 a = [1-on_region_subtract(t,1):exp_sz_x-on_region_subtract(t,1)];
 b = ones(exp_sz_y,1);
 x = b*a;
 a = [1-on_region_subtract(t,2):exp_sz_y-on_region_subtract(t,2)];
 b = ones(exp_sz_x,1);
 y = a'*b';
 r = sqrt(x.^2 + y.^2);
 On_mask_subtract(r <= on_region_subtract(t,3)) = 0;
end

On_mask = On_mask.*On_mask_subtract.*exp_d;
Total_on = sum(sum(On_mask));


for t = 1:length(back_region_add(:,1))
 a = [1-back_region_add(t,1):exp_sz_x-back_region_add(t,1)];
 b = ones(exp_sz_y,1);
 x = b*a;
 a = [1-back_region_add(t,2):exp_sz_y-back_region_add(t,2)];
 b = ones(exp_sz_x,1);
 y = a'*b';
 r = sqrt(x.^2 + y.^2);
 Back_mask_add(r <= back_region_add(t,3)) = 1;
end

for t = 1:length(back_region_subtract(:,1))
 a = [1-back_region_subtract(t,1):exp_sz_x-back_region_subtract(t,1)];
 b = ones(exp_sz_y,1);
 x = b*a;
 a = [1-back_region_subtract(t,2):exp_sz_y-back_region_subtract(t,2)];
 b = ones(exp_sz_x,1);
 y = a'*b';
 r = sqrt(x.^2 + y.^2);
 Back_mask_subtract(r <= back_region_subtract(t,3)) = 0;
end
Back_mask = Back_mask_add.*Back_mask_subtract;
Back_mask = Back_mask.*exp_d;
imagesc(Back_mask + On_mask)
Total_back = sum(sum(Back_mask));

factor = Total_on/on_area * back_area/Total_back;
