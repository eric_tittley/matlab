% back_sky (astro/xray/SAX): Transform a back file to sky coordinates.
%
% Transform a back file to sky coordinates of an event file from which the
% matrix was created.  ONLY STABLE FOR MECS2.  ALWAYS CHECK YOUR RESULTS
%
% data = back_sky(file,outfile,matrix)
%
% ARGUMENTS
%  file 	The file to be read
%  outfile	The name of the outputfile
%  matrix	The matrix given by transform_matrix
%
% RETURNS
%  data		The transformed data
%
%
% REQUIRES
%  fits tools
% 
% SEE ALSO
%  SAX_tools, transform_matrix

% AUTHOR: Danny Hudson
%
% HISTORY
%  020730 Mature version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave (?)

function data = back_sky(file,outfile,m);

% Read in the data
[a,b] = fitsload(file,0);
[h,d] = fitsload(file,1);
[c,e] = fitsload(file,2);

% Get rid of bad events
d_e.DETX = d.DETX(d.X~=-1);
d_e.DETY = d.DETY(d.X~=-1);
d_e.PHA = d.PHA(d.X~=-1);

% Determine the detector
detector = fitsfindkeystring(h,'INSTRUME');

% Parse header information
image_sizex = fitsfindkeyvalue(h,'TLMAX9');
image_sizey = fitsfindkeyvalue(h,'TLMAX10');
NAME = fitsfindkeystring(h,'OBJECT');
if detector(1:4) == 'MECS'
	X_COOR = fitsfindkeyvalue(h,'TCRPX9');
	RA = fitsfindkeyvalue(h,'TCRVL9');
	Y_COOR = fitsfindkeyvalue(h,'TCRPX10');
	DEC = fitsfindkeyvalue(h,'TCRVL10');
	where = fitsfindkey(h,'NAXIS2');
	th = fitsremovekey(h,'NAXIS2');
	clear h;
	h = fitsaddkey(th,'NAXIS2',length(d_e.DETX),where,'number of rows in table');
elseif detector(1:4) == 'LECS'
	X_COOR = fitsfindkeyvalue(h,'TCRPX11');
	RA = fitsfindkeyvalue(h,'TCRVL11');
	Y_COOR = fitsfindkeyvalue(h,'TCRPX12');
	DEC = fitsfindkeyvalue(h,'TCRVL12');
	where = fitsfindkey(h,'NAXIS2');
	th = fitsremovekey(h,'NAXIS2');
	clear h;
	h = fitsaddkey(th,'NAXIS2',length(d_e.DETX),where,'number of rows in table');
else
	X_COOR = image_sizex/2;
	RA = fitsfindkeyvalue(h,'RA_PNT');
	Y_COOR = image_sizey/2;
	DEC = fitsfindkeyvalue(h,'RA_PNT');
	where = fitsfindkey(h,'NAXIS2');
	th = fitsremovekey(h,'NAXIS2');
	clear h;
	h = fitsaddkey(th,'NAXIS2',length(d_e.DETX),where,'number of rows in table');
	fprintf('WARNING!!!! DETECTOR UNRECOGNIZED!  COORDINATES ARE PROBABLY WRONG!!!!')
end

% Get RID of all information pertaining to bad pixels
% and rotate the detector to the events file orientation
if detector(1:4) == 'MECS'
	data.TIME = d.TIME(d.X~=-1);
	data.PHA = d_e.PHA;
	data.RAWX = d.RAWX(d.X~=-1);
	data.RAWY = d.RAWY(d.X~=-1);
	data.BL = d.BL(d.X~=-1);
	data.PI = d.PI(d.X~=-1);
	data.DETX = d_e.DETX;
	data.DETY = d_e.DETY;
	data.X = m(1)*(d_e.DETX) + m(2)*(d_e.DETY) + m(5);
	data.Y = m(3)*(d_e.DETX) + m(4)*(d_e.DETY) + m(6);
elseif detector(1:4) == 'LECS'
	data.TIME = d.TIME(d.X~=-1);
	data.PHA = d_e.PHA;
	data.RAWX = d.RAWX(d.X~=-1);
	data.RAWY = d.RAWY(d.X~=-1);
	data.BL = d.BL(d.X~=-1);
	data.VETO = d.VETO(d.X~=-1);
	data.PI = d.PI(d.X~=-1);
	data.PIC = d.PIC(d.X~=-1);
	data.DETX = d_e.DETX;
	data.DETY = d_e.DETY;
	data.X = m(1)*(d_e.DETX) + m(2)*(d_e.DETY) + m(5);
	data.Y = m(3)*(d_e.DETX) + m(4)*(d_e.DETY) + m(6);
else
	data.PHA = d_e.PHA;
	data.X = m(1)*(d_e.DETX) + m(2)*(d_e.DETY) + m(5);
	data.Y = m(3)*(d_e.DETX) + m(4)*(d_e.DETY) + m(6);
end

%Create the output fits file 
fitswrite_2(a,b,outfile);
fitswrite(h,data,outfile);
fitswrite(c,e,outputfile);
