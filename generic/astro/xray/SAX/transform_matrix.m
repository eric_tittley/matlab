% transform_matrix: (astro/xray/SAX): Transform MECS2 and LECS data.
%
% matrix = transform_matrix(header,data,iterations)
%
% ARGUMENTS
%  header	The header for the file
%  data		The data for the file
%  iterations	The number of times the matrix should be
%		calculated and then averaged.
% 		(default 100)
%
% RETURNS
%  matrix 	The transformed data given as a vector such that element m(i)
%		should be multiplied by the detector coordinates
%		as shown below
%
%		d.X = m(1)*d.DETY + m(2)*d.DETX + m(5)
%		d.Y = m(3)*d.DETY + m(4)*d.DETX + m(6)
%
% NOTES
%  Only stable for LECS and MECS2
%
% REQUIRES
%  fitsfindkeystring, fitsfindkeyvalue
%
% SEE ALSO
%  fits, back_sky

% AUTHOR: Danny Hudson
%
% HISTORY
%  02 07 30 Mature version
%  07 10 29 Mofified headers
%
% COMPATIBILITY: Matlab, Octave

function [matrix] = transform_matrix(h,d,iterations);

bool = exist('iterations');

if bool == 0
	iterations = 100;
end


d.DETX = d.DETX(d.X~=-1);
d.DETY = d.DETY(d.X~=-1);
d.Y = d.Y(d.X~=-1);
d.X = d.X(d.X~=-1);


detector = fitsfindkeystring(h,'INSTRUME');

if detector(1:4) == 'MECS'
	X_COOR = fitsfindkeyvalue(h,'TCRPX9');
	RA = fitsfindkeyvalue(h,'TCRVL9');
	Y_COOR = fitsfindkeyvalue(h,'TCRPX10');
	DEC = fitsfindkeyvalue(h,'TCRVL10');
elseif detector(1:4) == 'LECS'
	X_COOR = fitsfindkeyvalue(h,'TCRPX11');
	RA = fitsfindkeyvalue(h,'TCRVL11');
	Y_COOR = fitsfindkeyvalue(h,'TCRPX12');
	DEC = fitsfindkeyvalue(h,'TCRVL12');
else
	X_COOR = image_sizex/2;
	RA = fitsfindkeyvalue(h,'RA_PNT');
	Y_COOR = image_sizey/2;
	DEC = fitsfindkeyvalue(h,'RA_PNT');
	fprintf('WARNING!!!! DETECTOR UNRECOGNIZED!  COORDINATES ARE PROBABLY WRONG!!!!')
end

if iterations+2 > length(d.DETX)
	iterations = length(d.DETX)-2;
end

%Use CRAMER'S RULE TO DETERMINE TRANSFORMATION MATRIX
for n = 1:iterations
	x = d.DETY(n:n+2);
	y = d.DETX(n:n+2);
	xp = d.X(n:n+2);
	yp = d.Y(n:n+2);

	Mat = [x(1) y(1) 0 0 1 0 ;0 0 x(1) y(1) 0 1;x(2) y(2) 0 0 1 0 ;0 0 x(2) y(2) 0 1; x(3) y(3) 0 0 1 0 ;0 0 x(3) y(3) 0 1];

	V = [xp(1); yp(1);xp(2);yp(2);xp(3);yp(3)];                                

	for k = 1:6
		M(:,:,k) = Mat;
		M(:,k,k) = V;
		m(k,n) = det(M(:,:,k))/det(Mat);
	end
end

for k = 1:6
	matrix(k) = mean(m(k,:));
end
