function L = LT_relation(T)
% LT_relation: The Luminosity-Temperature relation
%
% lumin = LT_relation(T)
%
% Using the relationship of Horner et al, determine the luminosity for
% a given cluster temperature
%
% ARGUMENTS
%  T	Temperature (kT (erg))
%
% RETURNS
%  lumin	The x-ray luminosity  (erg/s)
%		[expected, upperlimit, lowerlimit]
%
% NOTES
%  VERY, VERY ROUGH!!!
%
%  H = 50 h_50 km/s/Mpc
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Danny Hudson
%
% HISTORY
%  020801 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

% C is found by polyfitting log10(L) - 3.3*log10(T)
% to a zero order polynomial
C = 42.74545316461461; 

L(1) = 10^(3.3*log10(T)+C);
L(2) = 10^(3.2*log10(T)+C);
L(3) = 10^(3.4*log10(T)+C);

% Appendix
%
% After examining the data
% 	kT		L (ergs/s)
%	 0.4		2.9 +/- 0.1 x 10^41
%	 0.5		6.0 +/- 0.1 x 10^41
%	 0.6		1.0 +/- 0.1 x 10^42
%	 0.7		1.9 +/- 0.1 x 10^42
%	 0.8		2.8 +/- 0.1 x 10^42
%	 0.9		4.0 +/- 0.1 x 10^42
%	 1.0		5.5 +/- 0.2 x 10^42
%	 2.0		5.2 +/- 0.2 x 10^43
%	 3.0		1.9 +/- 0.1 x 10^44
%	 4.0		5.5 +/- 0.2 x 10^44
%	 5.0		1.1 +/- 0.1 x 10^45
%	 6.0		1.8 +/- 0.2 x 10^45
%	 7.0		3.0 +/- 0.1 x 10^45
%	 8.0		5.5 +/- 0.2 x 10^45
%	 9.0		9.0 +/- 1.0 x 10^45
%	10.0		1.1 +/- 0.2 x 10^46
%
% gives best fit
%
% L = 10^(42.75289789770726) * 10^(3.25901572777549*log10(T)) weighted
%
% or
% L = 10^(42.75276992704378) * 10^(3.27808275904367*log10(T)) unweighted
