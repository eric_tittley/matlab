function [wave,spec,header]=elginread(filename,nostrip_flag);
 if nargin~=1 & nargin~=2 error('Incorrect number of parameters passed to ELGINREAD'), end
 if nargin==2 nostrip_flag=1;
 else nostrip_flag=0;
 end

 line=1;
 header='';
 command=['!tail -236 ',filename,' > datafile54552.tmp'];
 eval(command);
 infile=fopen('datafile54552.tmp','r');
 pixel=1;
 spec_in=0*[1:1872];
 spec_in=fscanf(infile,'%i',1872);
 fclose(infile);
 !del datafile54552.tmp
 if nostrip_flag spec=spec_in;
 else spec=spec_in(find(spec_in));
 end
 wave=[1:length(spec)];

 
