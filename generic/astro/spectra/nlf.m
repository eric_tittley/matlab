function [yfit,aout]=nlf(xi,yi,a,sigma)
n=length(xi);
m=length(a);
xi=xi(:);
yi=yi(:);
sigma=sigma(:);
sigma2=sigma.^2;
lambda=0;

y=f1(xi,a);
chi_o=sum((yi-y).^2./sigma2);
chi_old=chi_o+1;

while chi_o<chi_old;
% Calculate Beta
 for i=1:m
  ap=a;
  ap(i)=ap(i)*1.01;
  if(abs(ap(i))<1e-10) ap(i)=1e-10; end
  yp=f1(xi,ap);
  beta(i)=-1/2*(chi_o-sum((yi-yp).^2./sigma2))/(ap(i)/100);
  dydj(:,i)=(yp-y)./sigma/(ap(i)/100);
 end
 
% Calculate Alpha
 for j=1:m
  for k=j+1:m
   alpha(j,k)=sum(dydj(:,j).*dydj(:,k));
   alpha(k,j)=alpha(j,k);
  end
 end
 for j=1:m
  alpha(j,j)=sum(dydj(:,j).^2)*(lambda+1);
 end

% Calculate da
 da=beta'\alpha;

% Increment
 aout=a;
 a=a-da;

 chi_old=chi_o;
 yfit=y;
 y=f1(xi,a);
 chi_o=sum((yi-y).^2./sigma2);
end


