function [levels,bisector,centre,errors]=bisect(wave,spec,line,interact_flag,di,clean);
if nargin~=5 & nargin~=6 error('Incorrect number of arguments passed to BISECT'), end
if nargin<6
 clean=spec;
 clean=clean(:);
else clean=clean(:);
end

wave=wave(:);
spec=spec(:);

line=find(wave==line);

%Find the region over which to perform the bisection
 width=1;
 flag=1;
 while(flag)
  if(sum((clean(line-width:line-1)-clean(line-width+1:line))<=0))==0 width=width+1;
  else widthleft=width-1; flag=0;
  end
 end
 width=1;
 flag=1;
 while(flag)
  if(sum((clean(line+1:line+width)-clean(line:line+width-1))<=0))==0 width=width+1;
  else widthright=width-1; flag=0;
  end
 end
 range=[line-widthleft:line+widthright];
 bisect_to=min([clean(range(1)),clean(range(length(range)))])-.02;

%Spline interpolate the spectrum to 10 times the pixel density
 spline_x=[wave(line-widthleft):.1:wave(line+widthright)];
 spline_y=spline(wave(range),spec(range),spline_x);

size(spline_x), size(spline_y)

 bisect_flag=1;
 if(interact_flag)
  plot(spline_x,[spline_y;bisect_to*(1&spline_y)])
  use_flag='';
  use_flag=input('Is this line clean enough for a bisector analysis? (y/n)','s');
  if(strcmp(use_flag,'n')|strcmp(use_flag,'N'))
   levels=0; bisector=0; errors=0; centre=0; bisect_flag=0;
  end
 end

if(bisect_flag)
  
%Find the centre of the line
 temp=find(min(spline_y)==spline_y);
 line=temp(1);
 [centre,min]=line_min(spline_x,spline_y,line,0);
 level=floor(min*100)/100;

%Perform the bisection
 bisector(1)=0;
 levels(1)=min;
 errors(1)=0;
 k=2;
 while (level<bisect_to)
  pts=find(level>spline_y);
  if(length(pts)>0)
   if(length(pts)<=2)
    bisector(k)=0;
    errors(k)=0;
   else
    left=pts(1);
    right=pts(length(pts));
    pout=polyfit(spline_y(left-1:left+1),spline_x(left-1:left+1),2);
    left_bi=polyval(pout,level);
    pout=polyfit(spline_y(right-1:right+1),spline_x(right-1:right+1),2);
    right_bi=polyval(pout,level);
    bisector(k)=(left_bi+right_bi)/2-centre;
    errors(k)=reticon_error(di,level)*.2/2 *sqrt( 1/(spline_y(left+1)-spline_y(left-1))^2+1/(spline_y(right+1)-spline_y(right-1))^2);
   end
   levels(k)=level;
   k=k+1;
  end
  level=level+.01;
 end


end

% End of Programme bisect.m
