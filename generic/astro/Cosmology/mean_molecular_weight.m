% mean_molecular_weight: The mean molecular weight (1/amu) for cosmological gas.
%
% mu = mean_molecular_weight(Y,f_HII,f_HeII,f_HeIII)
%
% ARGUMENTS
%  Y  Helium mass fraction
%  f_HII   HII fraction = n_HII/n_H
%  f_HeII  HeII fraction
%  f_HeIII HeIII fraction
%
% RETURNS
%  mu	Mean molecular weight = mu*amu
%
% REQUIRES
%  nothing
%
% SEE ALSO
%  Units

function mu = mean_molecular_weight(Y,f_HII,f_HeII,f_HeIII)

mu = 4./( 4*(1-Y)*(1+f_HII) + Y*(1+f_HeII+2*f_HeIII) );
