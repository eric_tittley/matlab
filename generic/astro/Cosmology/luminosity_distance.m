function D_L = luminosity_distance(z, Omega_m, Ho)
% luminosity_distance: luminosity distance.
%
% D_L = luminosity_distance(z, Omega_m, Ho)
%
% ARGUMENTS
%  z		Redshift
%  Omega_m	Pressureless matter content (current)
%  Ho		Hubble parameter (current) [1/s]
%
% RETURNS
%  The luminosity distance [m]
%
% NEEDS
%  luminosity_distance_eta
%
% NOTES
%  An approximation taken from Pen 1999 ApJS 120, 49
%  Exact for Omega_m -> 1 or 0
%  For 0.2 < Omega_m < 1, the error is < 0.4%
%  For any choice of parameters, the error is < 4%
%
% SEE ALSO
%  luminosity_distance_eta

% AUTHOR: Eric Tittley
%
% HISTORY
%  070518 First version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

c=299792458;          % m/s (exact)

D_L = c/Ho * (1+z) * (  luminosity_distance_eta(1,Omega_m) ...
                      - luminosity_distance_eta(1/(1+z),Omega_m) ) % m
