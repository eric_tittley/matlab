% time2redshift: The redshift of a the universe when at a given age
%
% z=time2redshift(t,omega_m,h)
%
% ARGUMENTS
%  t            Time [vector]
%  omega_m      The current contribution to the critical density of all matter.
%  h            The current dimensionless Hubble parameter (H = h*100 km/s/Mpc)
%
% RETURNS
%  z            Redshift at each time in t [vector]
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave and Matlab
%
% HISTORY
%  100402 (From expansion_factor_from_time.c [Eric Tittley])

function z=time2redshift(t,omega_m,h)
 SI='MKS';
 hdr.h100=h;
 Units
 H_0 = h*1e5/Mpc; % s^{-1}
 OneMinusEps = 0.999999;
 OnePlusEps = 1.000001;
 if(  (omega_m - OneMinusEps >  eps) ...
    & (omega_m - OnePlusEps  < -eps) )
  a = (1.5*t*H_0).^(2/3);
 else
  a_1 = (omega_m/(1-omega_m)).^(1/3);
  a_2 = sinh(3/2*H_0*sqrt(1.-omega_m)*t).^(2/3);
  a = a_1 * a_2;
 end
 % Convert expansion factor to redshift
 z= 1./a - 1;
