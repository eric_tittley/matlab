function eta=luminosity_distance_eta(a, Omega_m)
% luminosity_distance_eta: caclulates eta for the luminosity distance calculation.
%
% eta=luminosity_distance_eta(a, Omega_m)
%
% ARGUMENTS
%  a		The expansion factor
%  Omega_m	The pressureless matter content
%
% RETURNS
%  eta
%
% NOTES
%  Don't call this function.  It is used by luminosity_distance.
%
% SEE ALSO
%  luminosity_distance

% AUTHOR: Eric Tittley
%
% HISTORY
%  070518 First version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

s=((1-Omega_m)/Omega_m)^(1/3);

eta = 2*(s^3+1)^(1/2)*(  a^-4 - 0.1540*s*a^-3 + 0.4304*s^2*a^-2 ...
                 + 0.19097*s^3/a + 0.066941*s^4 )^(-1/8);
