function imf=IMF(M)
% IMF: Initial Mass Function for stars
%
% imf=IMF(M)
%
% ARGUMENTS
%  M	The mass at which to calculate dN/dM [Mo]
%
% RETURNS
%  The initial mass function dN/dM at M.
%
% NOTES
%  IMF is normalized such that int(m*IMF(m),0,inf)=1 Mo
%  Uses the Scalo IMF (J.M. Scalo, Fundamental Cosmic Physics 11, 1 1986)

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

% The Norm == 1/0.17 + (1-10^-1.27)/1.27 + 10^-1.27 / 0.45
Norm=6.746808947;

imf=0*M;

imf(M>=10)       = M(M>=10).^-2.45 * 10^2.45/10^3.27;
imf(M<10 & M>=1) = M(M<10 & M>=1).^-3.27;
imf(M<1)         = M(M<1).^-1.83;

imf=imf/Norm;
