function mask=MaskStars(Z,sigmas)
% MaskStars: Create a mask to remove stars from an image via sigma clipping.
%
% mask=MaskStars(Z,sigmas)
%
% ARGUMENTS
%  Z		Image
%  sigmas	Sigma to use for sigma clipping.
%
% RETURNS
%  mask		The mask ( 1=> star-free )
%
% SEE ALSO
%  clip

% AUTHOR: Eric Tittley
%
% HISTORY
%  021125 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

mask=ones(size(Z));
Nelems_o=prod(size(Z))+1;
while(length(find(mask))<Nelems_o)
 Nelems_o=length(find(mask));
 M=mean(Z(find(mask)));
 S=std(Z(find(mask)));
 mask= ( Z>(M-sigmas*S) & Z<(M+sigmas*S) );
end
