function img_aligned=align_star(img_stack,L)
% align_star:  Register a series of images.
%
% img_aligned=align_star(img_stack,L)
%
% ARGUMENTS
%  img_stack	A [NxMxL] array of L MxN images.
%  L		The limits (xlo xhi ylo yhi) of a volume isolating a single
%		star.
%
% RESULTS
%  A combined image, aligned to the chosen star.
%
% NOTES
%  Given a set of images, img_stack, and the limits, L, that
%  enclose a star that is in each of them, determine the centre
%  of the star in each image and align the images so that the
%  selected star is aligned.
%
%  Uses only one star, so assumes no rotation.
%
% SEE ALSO
%  photometry

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  011017 Mature version

img_aligned=0*img_stack;

[N,M,Nframes]=size(img_stack);

% Preliminary run to centre the peaks approxiamately
for i=1:Nframes
 star=img_stack(L(1):L(2),L(3):L(4),i);
 star_smooth=smooth(star,5,'gauss');
 Peaks=find(star_smooth==max(max(star_smooth)));
 [y,x]=ind2sub(size(star_smooth),Peaks(1));
 %disp([x,y])
 shiftx=(L(4)-L(3)+1)/2-x;
 shifty=(L(2)-L(1)+1)/2-y;
 img_stack(:,:,i)=shift(  img_stack(:,:,i),shiftx,2); % 1=>row=>y, 2=>column=>x
 img_stack(:,:,i)=shift(img_stack(:,:,i),shifty,1);
end

img_aligned=img_stack;

if(1)
% Refine the centring.
for i=1:Nframes
 star=double(img_stack(L(1):L(2),L(3):L(4),i));
 imagesc(star)
 %star_smooth=smooth(star,5,'gauss');
 
 [bias,sigma,ampl,x,y]=fit_gaussian(star);
 disp([x,y])
 img_aligned(:,:,i)=shift(  img_stack(:,:,i),(L(4)-L(3)+1)/2-x,2); % 1=>row=>y, 2=>column=>x
 img_aligned(:,:,i)=shift(img_aligned(:,:,i),(L(2)-L(1)+1)/2-y,1);
end
endif
