function [x,y,ampl,sigma,bias,frame]=photometry(frame,min_counts,HWHM_max,N_sigma_max)
% photometry: quick-and-dirty photometry/astrometry.
%
% [x,y,ampl,sigma,bias]=photometry(frame,min_counts,HWHM_max,[N_sigma_max]);
%
% ARGUMENTS
%  frame	The image on which to perform photometry.
%  min_counts	Lower tolerance level to ampl.  A larger values does 
% 		photometry on the brighter objects only.
%  HWHM_max	Used to separate mis-fits from true fits.  It should be set to
%		about twice the mean Half-Width-Half_Max for the stars
%  N_sigma_max	(optional) Maximum number of standard deviations above 
%	the background a peak should be before it is examined to see if it
%	is a star. The default is 5.
%
% RETURNS
%  x,y		Position of each star
%  ampl		Amplitude of the Gaussian fit to each star
%  sigma	Sigma of the Guassiean fit to each star
%  bias		The bias fit to each star
%  frame	The initial frame after all the stars have been catalogued.
%
% REQUIRES
%  smooth fit_gaussian
%
% SEE ALSO
%  align_star CosmicRayClean fake_star PhotometryGUI

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 01 24 First Version
%
%  01 01 30 Find stars using a smoothed image.  Cuts down on the
%	'false' stars.
%	Mask out the edges.
%	Improved method for determining cut-off level: limit search to 
%	peaks more than N_sigma_max above the background.
%	Change 'sigma_max' to 'HWHM_max' since it makes more sense.
%
% COMPATIBILITY: Matlab, Octave

if(nargin==3)
 N_sigma_max=5;
end

% The size of the input frame
[max_y,max_x]=size(frame);

% The width of the extracted mini-frame
width=64;
span=[-width/2:width/2];

% Determine the background level and the standard deviation of the background
[N,X]=hist(reshape(frame,max_x*max_y,1),round(max_x*max_y/1000));
N_max_pos=find(N==max(N));
span_n=[N_max_pos-10:N_max_pos+10];
P=gauss_fit(X(span_n),N(span_n),[integrate(N)*(X(2)-X(1))/2,std(X(span_n))/2,X(N_max_pos)]);
BG_level=P(3);
BG_std=P(2);

% damp the edges to eliminate interference from the edges.
pad=width/4;
y_cord=[1:max_y]'*ones(1,max_x);
x_cord=ones(max_y,1)*[1:max_x];
dist(:,:,1)=x_cord-1;
dist(:,:,2)=max_x-x_cord;
dist(:,:,3)=y_cord-1;
dist(:,:,4)=max_y-y_cord;
dist(dist>pad)=dist(dist>pad)*0+pad;
dist=min(dist,[],3);
mask=1-(dist./pad-1).^2;
frame=frame.*mask+BG_level*(1-mask);

% We will use a smoothed frame to find the stars
frame_smooth=smooth(frame,3,'gauss');

star=0;
last_ampl=min_counts+10;
frame_max=(N_sigma_max+1)*BG_std+BG_level;
% Loop through, looking for stars while the amplitudes of the found stars
% are greater than the the user-supplied value and the maximum level is
% greater than N_sigma_max above the background
while( last_ampl>min_counts & frame_max>(N_sigma_max*BG_std+BG_level) )
 frame_max=max(max(frame_smooth))
 [y0,x0]=ind2sub(size(frame),find(frame_smooth==frame_max));
 x0=x0(1)
 y0=y0(1)
 % Test to see if we are at the edge
 if(  (x0-width/2)>=1 & (x0+width/2)<=max_x ...
    & (y0-width/2)>=1 & (y0+width/2)<=max_y ) 
  % If not at the edge, then fit the star and then subtract the fitted image
  % First fit the star.  A stellar image hasn't been fit properly if it
  % has a large sigma
  width_d=2*width;
  sigma_d=HWHM_max+1;
  while(sigma_d>HWHM_max & width_d>2)
   width_d=width_d/2;
   span=[-width_d/2:width_d/2];
   % We will use a try, catch since fit_gaussian can error out
   % if it cand find a max, for example
   try
    [bias_d,sigma_d,ampl_d,x_d,y_d]=fit_gaussian(frame(y0+span,x0+span));
   catch
%    width_d=1;
    disp('Caught an error in fit_gaussian')
    disp([x0,y0])
    disp(['bias=',num2str(bias_d),' sigma=',num2str(sigma_d), ...
          ' ampl=',num2str(ampl_d),' x,y=',num2str([x_d,y_d])])
%    clf
%    imagesc(frame(y0+span,x0+span)), colorbar
%    pause
   end
  end
  if(ampl_d<0)
   disp('Ampl < 0')
   disp([x0,y0])
%   imagesc(frame(y0+span,x0+span)), colorbar
%   pause
   frame(y0,x0)=mean(mean(frame(y0+span,x0+span)));
   ampl_d=0; % this will effectively prevent the subtraction of the image, below
  end
  % Subtract the star from the main image
  fake=fake_star((width_d+1)*[1,1],0,sigma_d,ampl_d,x_d,y_d);
  frame(y0+span,x0+span)=frame(y0+span,x0+span)-fake;
  frame_smooth(y0+span,x0+span)=frame_smooth(y0+span,x0+span)-smooth(fake,3,'gauss');
  if(width_d>2)
   % Update parameters for this star
   star=star+1;
   bias(star)=bias_d;
   sigma(star)=sigma_d;
   ampl(star)=ampl_d;
   x(star)=x0-width/2+x_d-1;
   y(star)=y0-width/2+y_d-1;
   last_ampl=ampl(star)
  end
 else
  % If at the edge, simply fit the star and remove it, but ignore the
  % photometry
  span=[-width/2:width/2];
  if(x0==1 | x0==max_x | y0==1 | y0==max_y)
   frame(y0,x0)=0;
  else % Peak is not exactly on the edge
   if( (x0-width/2)>=1 & (x0+width/2)<=max_x ) % x is not the problem
    spanx=x0+span;
   elseif( (x0-width/2)<1 ) % x is near the lower edge
    spanx=[1:2*x0-1];
   else % x is near the upper edge
    spanx=[2*x0-max_x:max_x];
   end % end calculating span for x
   if( (y0-width/2)>=1 & (y0+width/2)<=max_y ) % y is not the problem
    spany=y0+span;
   elseif( (y0-width/2)<1 ) % y is near the lower edge
    spany=[1:2*y0-1];
   else % y is near the upper edge
    spany=[2*y0-max_y:max_y];
   end
   try
    [bias_d,sigma_d,ampl_d,x_d,y_d]=fit_gaussian(frame(spany,spanx));
   catch
    disp('Caught an error in fit_gaussian')
    disp([x0,y0])
    disp(['bias=',num2str(bias_d), 'sigma=',num2str(sigma_d), ...
          ' ampl=',num2str(ampl_d),' x,y=',num2str([x_d,y_d])])
%    clf
%    spany,spanx
%    imagesc(frame(spany,spanx)), colorbar
%    pause
   end
%     disp(['bias=',num2str(bias_d),' sigma=',num2str(sigma_d), ...
%           ' ampl=',num2str(ampl_d),' x,y=',num2str([x_d,y_d])])
   if(ampl_d<0), ampl_d=1; end
   fake=fake_star([length(spany),length(spanx)],0,sigma_d,ampl_d,x_d,y_d);
   frame(spany,spanx)=frame(spany,spanx)-fake;
   frame_smooth(spany,spanx)=frame_smooth(spany,spanx)-smooth(fake,3,'gauss');
%   figure(1), imagesc(fake), colorbar
%   figure(2), imagesc(frame(spany,spanx)), colorbar, pause
  end % if the peak is right on the edge, else, end
 end % if the peak is not in the border region, else, end
end % while the last ampl > min_ampl
