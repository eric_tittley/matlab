% astro/Photometry: Photometry tools
%
%  align_star		Register a series of images.
%
%  CosmicRayClean	Cleans cosmic rays from an image.
%
%  fake_star		Returns a fake stellar image (Gaussian).
%
%  MaskStars		Create a mask to remove stars from an image.
%
%  photometry		Quick-and-dirty photometry/astrometry.
