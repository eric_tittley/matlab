function img_clean=CosmicRayClean(img,Nsigmas)
% CosmicRayClean: Cleans cosmic rays from an image.
%
% img_clean=CosmicRayClean(img,[Nsigmas]);
%
% ARGUMENTS
%  img		The frame to clean
%  Nsigmas	(optional) The number of sigmas above the background that is
%		required to flag pixels as a Cosmic ray event.

% AUTHOR: Eric Tittley, Chris Frye
%
% HISTORY
%  01 01 26 First edition
%  01 01 29 Fixed bug in x-y coordinates.
%
% COMPATIBILITY: Matlab, Octave

% set the default value of Nsigmas
if(nargin==1)
 Nsigmas=100;
end

% Smooth the image
img_smooth=smooth(img,2,'gauss');

% Get the residuals
del=img-img_smooth;

% Find Sigmas 
sigma=std(reshape(del,510*765,1));

% Find the Cosmic rays
[y,x]=ind2sub(size(img),find(del>Nsigmas*sigma));

% Remove the Cosmic rays (set to the background value)
img(y,x)=img(y,x)*0+mean(mean(img));       

% Report the number of CR's found
disp(['Found ',int2str(length(x)),' cosmic ray(s)'])

% Store the image for output (a bit unecessary)
img_clean=img;
