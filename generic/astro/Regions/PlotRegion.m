function handle=PlotRegion(Regions,mx,bx,my,by)
% PlotRegion: Plot regions to a current figure
%
% handle=PlotRegion(Regions,[mx,bx,my,by])
%
% ARGUMENTS
%  Regions	A structure containing the regions
%  mx,my	Scaling factors in the x & y direction [optional, default 1]
%  bx,by	Offsets in the x & y direction [optional, default 0]
%
% RETURNS
%  A handle to the patches plotted.
%
% NOTES
%  Given a set of regions, Regions, parsed from a SAO region file
%  by ParseRegions, plot the regions on the current figure.
%
%  The optional arguments (x, bx, my, by) can be used to scale the 
%  regions.  This is usefull for plotting regions given in detector
%  coordinates to figures with axes in RA and Dec.
%
% SEE ALSO
%  FilterByRegion MaskFromRegion ParseRegionFile WriteRegionFile WriteRegionFile_SAS

% AUTHORS
%  Eric Tittley, Chris Frye
%
% HISTORY
%  01 01 11 First version, only does rotbox
%  01 01 12 Added circle and looping through all regions in a list of
%	regions.
%  01 01 22 Added support for line
%  01 01 23 Added support for box
%  01 08 21 Added support for ellipse. (Chris Frye)
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% COPYRIGHT:
%	Copyright Eric Tittley (2001).
%	Free for use, so long as you don't receive money or services
%	by redistributing it in whole or in part, even within a package.
%	See the GPL for more details.
%
% GUARANTEE:
% Yeah, right.  NONE!

if(nargin==1)
 mx=1;
 my=1;
 bx=0;
 by=0;
end

for i=1:length(Regions)
 Region=Regions(i);
 switch lower(Region.Shape)
  case 'rotbox'
   if(length(Region.P)~=5)
    error('rotbox must have 5 parameters')
   end
   x=Region.P(1);
   y=Region.P(2);
   width=Region.P(3);
   height=Region.P(4);
   theta=Region.P(5)/180*pi;
   %Starting from Top right corner, clockwise.
   Corner(1,1)=x-height*sin(theta)/2-width*cos(theta)/2;
   Corner(2,1)=y+height*cos(theta)/2-width*sin(theta)/2;
   Corner(1,2)=x-height*sin(theta)/2+width*cos(theta)/2;
   Corner(2,2)=y+height*cos(theta)/2+width*sin(theta)/2;
   Corner(1,3)=x+height*sin(theta)/2+width*cos(theta)/2;
   Corner(2,3)=y-height*cos(theta)/2+width*sin(theta)/2;
   Corner(1,4)=x+height*sin(theta)/2-width*cos(theta)/2;
   Corner(2,4)=y-height*cos(theta)/2-width*sin(theta)/2;
   Corner(1,:)=mx*Corner(1,:)+bx;
   Corner(2,:)=my*Corner(2,:)+by;
   handle(i)=line([Corner(1,:) Corner(1,1)],[Corner(2,:) Corner(2,1)]);

  case 'circle'
   if(length(Region.P)~=3)
    error('circle must have 3 parameters')
   end
   x=Region.P(1);
   y=Region.P(2);
   radius=Region.P(3);
   x=mx*x+bx;
   y=my*y+by;
   handle(i)=ellipse(radius*mx,radius*my,x,y,0);

  case 'line'
   if(length(Region.P)~=4)
    error('line must have 4 parameters')
   end
   x=Region.P([1,3]);
   y=Region.P([2,4]);
   x=mx*x+bx;
   y=my*y+by;
   handle(i)=line(x,y);
   
  case 'box'
   if(length(Region.P)~=4)
    error('box must have 4 parameters')
   end
   x=Region.P(1);
   y=Region.P(2);
   width=Region.P(3);
   height=Region.P(4);
   %Starting from Top right corner, clockwise.
   Corner(1,1)=x-width/2;
   Corner(2,1)=y+height/2;
   Corner(1,2)=x+width/2;
   Corner(2,2)=y+height/2;
   Corner(1,3)=x+width/2;
   Corner(2,3)=y-height/2;
   Corner(1,4)=x-width/2;
   Corner(2,4)=y-height/2;
   Corner(1,:)=mx*Corner(1,:)+bx;
   Corner(2,:)=my*Corner(2,:)+by;
   handle(i)=line([Corner(1,:) Corner(1,1)],[Corner(2,:) Corner(2,1)]);

  case 'ellipse' % Written by Chris Frye 01-08-23
   if(length(Region.P)~=5)
    error('ellipse must have 5 parameters')
   end
   x=Region.P(1);
   y=Region.P(2);
   A=Region.P(3);
   B=Region.P(4);
   x=mx*x+bx;
   y=my*y+by;
   theta=Region.P(5);
   handle(i)=ellipse(A*mx,B*my,x,y,theta);

  otherwise
   error('Unknown region')
 end % Switch
end % Loop through Regions
