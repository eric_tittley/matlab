% MaskFromRegion: Create a mask from a region structure.
%
% mask=MaskFromRegion(region,template)
%
% ARGUMENTS
%  region	A structure containing one or more regions.
%  template	An array on which to base the mask
%
% RETURNS
%  A mask based on the template
%
% NOTES
%  Creates a mask from a .reg file parsed with ParseRegionFile and a template.  
%  It works best (and possibly only) on region files in the ciao format.  The 
%  regions which are currently supported are:
%	circle
%	rotbox
%	ellipse
%	annulus
%	box
%
%  An unknown region will cause the function to abort.
%
% USAGE
%  An example of a parsed region file is:
%
% >>region
%ans = 
%     Sign: 1
%    Shape: 'rotbox'
%        P: [392 408.5000 96 51 322.3789]
%
% SEE ALSO
%  FilterByRegion ParseRegionFile PlotRegion WriteRegionFile WriteRegionFile_SAS

% AUTHOR: Eric Tittley (?)
%
% HISTORY
%  01 05 02  Created from pieces of rotting corpses.
%  01 08 24  Added rotated ellipse, annulus, and box.  Fixed a problem 
%	with rotbox.
%  07 12 22 Regularised comments.
%
% COMPATIBILITY: Matlab, Octave

function mask=create_mask(region,template)

%use the imput image as a template for the mask
mask=zeros(size(template));

%create an x position map and a y position map for the mask
x_pos=zeros(size(template)); 
for n=1:(size(x_pos,1));
x_pos(n,:)=(1:(size(template,2)));
end
y_pos=zeros(size(template));
for n=1:(size(y_pos,2));
y_pos(:,n)=(1:(size(template,1)))';
end

%loop to create the masked regions
for n=1:size(region,2);
if isequal(region(n).Shape,'circle')==1
reg_pixels=find(sqrt(((y_pos-region(n).P(:,2)).^2)+((x_pos-region(n).P(:,1)).^2))<=(region(n).P(:,3)));

elseif isequal(region(n).Shape,'rotbox')==1 & region(n).P(:,5)==0
reg_pixels=find(y_pos>=(region(n).P(:,2)-(0.5*region(n).P(:,4)))...  
            &  y_pos<=(region(n).P(:,2)+(0.5*region(n).P(:,4)))... 
            & x_pos>=(region(n).P(:,1)-(0.5*region(n).P(:,3))) ...
            & x_pos<=(region(n).P(:,1)+(0.5*region(n).P(:,3))));

elseif isequal(region(n).Shape,'rotbox')==1 & region(n).P(:,5)~=0
xc=region(n).P(:,1);
yc=region(n).P(:,2);
width=region(n).P(:,3);
height=region(n).P(:,4);
ang=region(n).P(:,5);
%check the angles
if ang>180,ang=ang-180;end
radang1=ang*(pi/180);
radang2=radang1-(pi/2);
b1=yc-(tan(radang1)*xc);
b2=yc-(tan(radang1-(pi/2))*xc);
l1=(0.5*height)/sin((pi/2)-radang1);
if l1<0,l1=abs(l1);else end
l2=(0.5*width)/sin(radang1);
reg_pixels=find( (y_pos-(tan(radang1).*x_pos))<=(b1+l1)...
              &  (y_pos-(tan(radang1).*x_pos))>=(b1-l1)...
	      &  (y_pos-(tan(radang2).*x_pos))<=(b2+l2)...
	      &  (y_pos-(tan(radang2).*x_pos))>=(b2-l2));

elseif isequal(region(n).Shape,'box')==1 
reg_pixels=find(y_pos>=(region(n).P(:,2)-(0.5*region(n).P(:,4)))...  
            &  y_pos<=(region(n).P(:,2)+(0.5*region(n).P(:,4)))... 
            & x_pos>=(region(n).P(:,1)-(0.5*region(n).P(:,3))) ...
            & x_pos<=(region(n).P(:,1)+(0.5*region(n).P(:,3))));

elseif isequal(region(n).Shape,'ellipse')==1 & region(n).P(:,5)==0
reg_pixels=find((((x_pos-region(n).P(:,1)).^2)./(region(n).P(:,3)^2))+(((y_pos-region(n).P(:,2)).^2)./(region(n).P(:,4)^2))<=1);

elseif isequal(region(n).Shape,'ellipse')==1 & region(n).P(:,5)~=0
xy=[reshape(x_pos,prod(size(x_pos)),1) reshape(y_pos,prod(size(y_pos)),1)];
rotxy=rot3d(xy,(region(n).P(:,5)*pi/180),3,[region(n).P(:,1) region(n).P(:,2)]);
nx_pos=reshape(rotxy(:,1),size(x_pos));
ny_pos=reshape(rotxy(:,2),size(y_pos));
reg_pixels=find((((nx_pos-region(n).P(:,1)).^2)./(region(n).P(:,3)^2))+(((ny_pos-region(n).P(:,2)).^2)./(region(n).P(:,4)^2))<=1);

elseif isequal(region(n).Shape,'annulus')==1
reg_pixels=find(sqrt(((y_pos-region(n).P(:,2)).^2)+((x_pos-region(n).P(:,1)).^2))<(region(n).P(:,4))...
        & sqrt(((y_pos-region(n).P(:,2)).^2)+((x_pos-region(n).P(:,1)).^2))>(region(n).P(:,3)));

else error(strcat(['unknown region "',region(n).Shape,'"'])),end
mask(reg_pixels)=1;
end
