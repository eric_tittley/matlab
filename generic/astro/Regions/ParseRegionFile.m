function Regions=ParseRegionFile(regionfilename)
% ParseRegionFile: Read in an SAOimage-compatible region file
%
% Regions=ParseRegionFile(regionfilename)
%
% ARGUMENTS
%  regionfilename	SAOimage-compatible region file
%
% RETURNS
%  An array of structures giving the details of the contents of the region file.
%
% SEE ALSO
%  FilterByRegion MaskFromRegion PlotRegion WriteRegionFile WriteRegionFile_SAS

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

fid=fopen(regionfilename,'r');
if(fid<0)
 error('Unable to find/open region file')
end

region=0;

line=[];
ch=0;
while( ~ isempty(ch) )
 % character 10 is a carriage return
 ch=0;
 while(ch~=10)
  ch=fread(fid,1,'uchar');
  line=[line,char(ch)];
 end
 line=line(1:end-1); % Strip the trailing CR

 CommentPos=find(line=='#');
 if( ~ isempty(CommentPos))
  line=line(1:CommentPos(1)-1);
 end

 if( ~ isempty(line) )

  region=region+1;
  % Get the sign
  Regions(region).Sign=+1;
  if(line(1)=='+' | line(1)==' ')
   Regions(region).Sign=+1;
   line=line(2:end);
  elseif(line(1)=='-')
   Regions(region).Sign=-1;
   line=line(2:end);
  end
  
  % Get the shape
  StartBracketPos=find(line=='(');
  Regions(region).Shape=line(1:StartBracketPos(1)-1);
  line=line(StartBracketPos(1)+1:end); 

  % Get the various parameters
  CommaPos=[0,find(line==',')];
  NCommasPlus1=length(CommaPos);
  for i=2:NCommasPlus1
   Regions(region).P(i-1)=sscanf(line(CommaPos(i-1)+1:CommaPos(i)-1),'%f');
  end
  EndBracketPos=find(line==')');
  Regions(region).P(NCommasPlus1)= ...
   sscanf(line(CommaPos(NCommasPlus1)+1:EndBracketPos-1),'%f');

 end % if the line is not empty
 line=[];
end % while loop
fclose(fid);
