% write_cell_array_to_ascii: Write a cell array to and ASCII file
%
%  write_cell_array_to_ascii(array,file)
%
% ARGUMENTS
%  array  Cell array to write
%  file   File name to create
%
% RETURNS
%  Nothing
%
% REQUIRES
%  Nothing
%
% NOTES
%  Cell arrays should usually be written using a customn scritp owing to the
%  variety of possible datatypes and layouts possible.
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  080822 First version.  Written to fulfill a request by Rachel M

function write_cell_array_to_ascii(array,file)

fid=fopen(file,'wt');

N=numel(array);

% The following would be more robust if written using the isa() function.
for i=1:N
 elem=array{i}
 if(isnumeric(elem))
  if(isinteger(elem))
   disp('int')
   fprintf(fid,'%i\n',elem);
  end
  if(isfloat(elem))
   disp('float')
   fprintf(fid,'%e\n',elem);
  end
 else
  if(isstr(elem))
   fprintf(fid,'%s\n',elem);
  end
  %if(ischar(elem))
  % fprintf(fid,'%c\n',elem);
  %end
 end
end
