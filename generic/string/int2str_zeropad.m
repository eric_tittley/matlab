% int2str_zeropad: Convert an integer into a zero-paddeed string of fixed length.
%
% string=int2str_zeropad(int_in,digits)
%
% ARGUMENTS
%  int_in	The input integer.
%  digits	The number of digits of the output string.
%
% OUTPUT
%  string	A string containing the character representation of int_in.
%
% USAGE
%  >> int2str_zeropad(2,3)
%  ans = 002
%  >> int2str_zeropad(12,3)
%  ans = 012
%  >> int2str_zeropad(122,3)
%  ans = 122
%  >> int2str_zeropad(1224,3)  % NOTE POTENTIALLY UNWANTED BEHAVIOUR
%  ans = 122
%  >> int2str_zeropad(1.224,3)
%  ans = 001
%  >> int2str_zeropad(-2,3)
%  ans = -02
%  >> int2str_zeropad(-2,2)
%  ans = -2
%  >> int2str_zeropad(-2,1)  % NOTE POTENTIALLY UNWANTED BEHAVIOUR
%  ans = -
%
% SEE ALSO
%  int2str num2str

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 10 28 Mature version
% 	Changed line from is=num2str(int_in); to is=int2str(int_in);
%	Wrote comments.
%  02 10 29 Deals with negative integers, now.
%  07 10 29 Modified comments.
%
% COMPATIBILITY: Matlab, Octave

function string=int2str_zeropad(int_in,digits)

is=int2str(int_in);

if(int_in>=0) % if positive
 if(length(is)>=digits)
  string=is(1:digits);
 else
  string=is;
  for i=1:digits-length(is)
   string=['0',string];
  end
 end
else % negative
 is=is(2:end); % strip off leading '-'
 digits=digits-1;
 if(length(is)>=digits)
  string=is(1:digits);
 else
  string=is;
  for i=1:digits-length(is)
   string=['0',string];
  end
 end
 string=['-',string];
end % if positive, else

% END
