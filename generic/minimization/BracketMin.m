function x_min=BracketMin(f,a,b,c,X)
% Find the minimum of a function using bracketing.
%
% x_min=BracketMin(f,a,b,c,X);
%
% f	Inline function object.
% a,b,c	Lower, middle, and upper initial bracket point.
%	a < b < c
% X     A structure containing anything else f might need.

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 02 20 First Version.

if(nargin~=5)
 error('x_min=BracketMin(f,a,b,c,X)')
end
if(a>b | a>c | b>c )
 error('a < b < c must be true')
end

fa=f(a,X);
fc=f(c,X);
%while( (c-a)>4*sqrt(eps*abs(b)) )
while( (c-a) > ( 1e-4*abs(b) + 1e-4 )  )
 fb=f(b,X);
 if( fb<fa & fb<fc ) % b is a minimum of the 3 points
  if( fa>fc )	% keep c, recalculate a
   a=(a+b)/2;
   fa=f(a,X);
  else		% keep a, recalculate c
   c=(b+c)/2;
   fc=f(c,X);
  end
 else		% b is not a minimum
  if( fa<fb )	% a is a minimum
   dx=b-a;
   c=b;
   b=a;
   a=b-dx;
   fc=fb;
   fb=fa;
   fa=f(a,X);
  else		% c is a minimum
   dx=c-b;
   a=b;
   b=c;
   c=b+dx;
   fa=fb;
   fb=fc;
   fc=f(c,X);
  end
 end
end % while

x_min=b;
