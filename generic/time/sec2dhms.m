% sec2dhms: Convert seconds to Days, Hours, Minutes, Seconds
%
% [d,h,m,s]=sec2dhms(time)
%
% ARGUMENTS
%  time		The number of seconds.  Can be an array.
%
% RETURNS
%  d		The number of days.
%  h		The number of hours.
%  m		The number of minutes.
%  s		The number of seconds.
%
% REQUIRES
%  nothing
%
% SEE ALSO
%  deg2dms, dms2deg, RA2str, DEC2str, Greg2Jul

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  071017 First version.

function [d,h,m,s]=sec2dhms(time)

d=floor(time/(24*3600));
time=time-d*24*3600;
h=floor(time/3600);
time=time-h*3600;
m=floor(time/60);
s=time-m*60;
