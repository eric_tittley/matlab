% DEC2str : Prepare a DD:MM:SS string for a declination (or any angle).
%
% string=DEC2str(dec,[depth])
%
% ARGUMENTS
%  dec	  The angle (degrees) in decimal notation.
%
%  depth  The precision. (optional)
%	1 => just the degrees
%	2 => Degrees, and minutes
%	3 => Degrees, minutes, seconds (default)
%
% RETURNS
%  string	A string representation of the angle
%
% SEE ALSO
%  RA2str dms2deg deg2dms

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 03 01 Mature code
%  01 03 01 Changed 'sec' to 'second' since 'sec' is a
% 	Matlab intrinsic.
%  05 03 08 Bug: When -1 < dec < 0, the string is missing the '-'.
%  07 11 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function string=DEC2str(dec,depth)

if(nargin==1), depth=3; end
tol=1e-6;
string=char(zeros(length(dec),12));
decsign=sign(dec);
decabs=abs(dec);
decfrac=rem(decabs,1);
decint =floor(decabs);
for i=1:length(dec)
 deg=decint(i);
 if(deg<0.01 & deg>-0.01), deg=0; end
 minute=floor(decfrac(i)*60);
 if(minute<0.01), minute=0; end
 if(abs(minute-60)<tol), deg=deg+decsign(i); minute=0; end
 second=round(rem(decfrac(i)*60,1)*60);
 if(second<0.01), second=0; end
 if(abs(second-60)<tol), minute=minute+1; second=0; end
 if(decsign(i)>=0)
  dummy=[];
 else
  dummy='-';
 end
 dummy=[dummy,int2str(deg)];
 if(round(depth)>1), dummy=[dummy,':',int2str_zeropad(minute,2)]; end
 if(round(depth)>2), dummy=[dummy,':',int2str_zeropad(second,2)]; end
 string(i,1:length(dummy))=dummy;
end

