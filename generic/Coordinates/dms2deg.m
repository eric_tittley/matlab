% dmstodeg: Converts the 3 components, deg,min,sec, into the scalar, degrees.
%           Also works for H,M,S
%
% degrees=dmstodeg(deg,minutes,seconds)
%
% ARGUMENTS
%  deg		Degrees
%  minutes	Minutes (positive)
%  seconds	Seconds (positive)
%
%  Only the deg component should have a sign
%
% RETURNS
%  degrees
%
% NOTES
%  Undefined for negative min and sec values.
%
% REQUIRES
%
% SEE ALSO
%  dms2deg

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 02 06
%   Added support for [Mx3] input.
%   changed 'min' to 'minutes' soas to not conflict with function.
%  01 05 14
%   Made the routine robust to -'ive values with the deg == 0 .
%   Changed [M,N]=size(deg); to N=size(deg,2); .
%  07 11 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function degrees=dms2deg(deg,minutes,seconds)

if(nargin==1)
 N=size(deg,2);
 if(N~=3)
  disp('Syntax: degrees=dms2deg(deg,minutes,seconds)')
  disp('     or degrees=dms2deg(deg) where deg is an [Mx3] vector')
  return
 end
 seconds=deg(:,3);
 minutes=deg(:,2);
 deg=deg(:,1);
end

s= (deg~=0).*sign(deg) + ...
   (deg==0 & minutes~=0).*sign(minutes) + ...
   (deg==0 & minutes==0).*sign(seconds) ;
degrees = deg + s.*abs(minutes)/60 + s.*abs(seconds)./3600;
