% DistanceBetweenPoints: Find the angular distance between two points on a sphere
%
%  d=DistanceBetweenPoints(RA1,Dec1,RA2,Dec2)
%
% ARGUMENTS
%  RA1	RA  of point 1 [degrees]
%  Dec1 Dec of point 1 [degrees]
%  RA2	RA  of point 2 [degrees]
%  Dec2 Dec of point 2 [degrees]
%
% RETURNS
%  d	Angular distance [degrees]
%
% NOTES
%  WARNING: Not likely robust. Good for nearby points.
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  03-10-14 First version.
%  07 11 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function d=DistanceBetweenPoints(RA1,Dec1,RA2,Dec2)

d_E=Dec1/180*pi; d_L=Dec2/180*pi; RA=(RA2-RA1)/180*pi;
alpha=acos(cos(d_L)*cos(RA));
theta=pi/2-asin(sin(d_L)/sin(alpha));
d=acos( cos(d_L)*cos(RA)*cos(d_E)+sin(alpha)*sin(d_E)*cos(theta));
d=d*180/pi;
