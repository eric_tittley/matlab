% deg2dms: Converts degrees (or hours) into the vector [deg, mn, sec] (or [H,M,S])
%
% dms=deg2dms(deg)
%
% ARGUMENTS
%  deg	Degrees (or hours) [degrees/hours]
%
% RETURNS
%  dms  [deg/hours, minutes, seconds]
%
% NOTES
%  Only the deg scalar element maintains the sign of the input.
%  deg may be a column vector
%
% REQUIRES
%
% SEE ALSO
%  dms2deg

% AUTHOR: Eric Tittley
%
% HISTORY:
%  00 11 28
%   Mature code.
%  00 11 28
%   Changed 'min' to 'mn' since 'min' is a built-in function.
%  01 05 14
%   Made the routine robust to deg==0 .
%   Properly treat the sign when deg is almost zero.
%  07 11 05 Modified comments.
%
% COMPATIBILITY: Matlab, Octave

function dms=deg2dms(deg)

Tol=-1e-4;

if(deg~=0)
 s=abs(deg)./deg;
else
 s=1;
end
deg=abs(deg);

hr=floor(deg);
mn=floor((deg-hr)*60);
Cor=find(mn-60>Tol);
hr(Cor)=hr(Cor)+1;
mn(Cor)=mn(Cor)-60;
% The previous three lines to the following 4, in vector notation
%if(mn-60>Tol)
% hr=hr+1;
% mn=mn-60;
%end
second=(deg-hr-mn/60)*3600;

Cor=find(second-60>Tol);
mn(Cor)=mn(Cor)+1;
second(Cor)=second(Cor)-60;
second(second<0)=second(second<0)*0;
Cor=find(mn-60>Tol);
hr(Cor)=hr(Cor)+1;
mn(Cor)=mn(Cor)-60;
% The previous 7 lines do the same as the next 9, but vectorized
%if(second-60>Tol)
% mn=mn+1;
% second=second-60;
% if(second<0), second=0; end
% if(mn-60>Tol)
%  hr=hr+1;
%  mn=mn-60;
% end
%end

h_neg=find(hr~=0);
m_neg=find(hr==0 & mn~=0);
s_neg=find(hr==0 & mn==0);
dms=zeros(length(hr),3);
if(~ isempty(h_neg)) 
 dms(h_neg,:)=[s(h_neg).*hr(h_neg),mn(h_neg),second(h_neg)];
end
if(~ isempty(m_neg))
 dms(m_neg,:)=[hr(m_neg),s(m_neg).*mn(m_neg),second(m_neg)];
end
if(~ isempty(s_neg))
 dms(s_neg,:)=[hr(s_neg),mn(s_neg),s(s_neg).*second(s_neg)];
end
