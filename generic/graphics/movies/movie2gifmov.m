function movie2gifmov(M,filename,loopflag,delay)
% movie2gifmov: Generate an animated GIF from a matlab movie structure.
%
% movie2gifmov(A,filename,loopflag,delay);
%
% ARGUMENTS
%  M		Matlab movie structure generated using getframe
%  filename	The animated GIF file to generate
%  loopflag	1, to make a looping gif movie, 0 otherwise
%  delay	The delay, in seconds, between frames (1/100th minimum fraction)
%
% RETURNS
%  nothing
%
% REQUIRES
%  System commands: tifftogif gifmerge
%
% SEE ALSO
%  getframe

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  050923 Mature version
%  080612 Regularised comments.  Changed paths to tifftogif

[dummy,Nframes]=size(M);
if(Nframes>9999), error('Ooops, I can not deal with more than 9999 frames'); end

for i=1:Nframes
 file=['/tmp/temp_',int2str_zeropad(i,4),'.tif'];
 file_gif=['/tmp/temp_',int2str_zeropad(i,4),'.gif'];
 disp(file)
 imwrite(M(i).cdata,file,'tif')
 %file=['/tmp/temp_',int2str_zeropad(i,4)];
 cmd=['!tifftogif ',file,' ',file_gif];
 eval(cmd)
end

delay=round(delay*100);
if(loopflag==1)
 cmd=['!gifmerge -l0 -',int2str(delay),' /tmp/temp_*.gif > ',filename];
else, cmd=['!gifmerge -',int2str(delay),' /tmp/temp_*.gif > ',filename];
end
 
eval(cmd)
eval('!rm /tmp/temp_*.gif /tmp/temp_*.tif')
