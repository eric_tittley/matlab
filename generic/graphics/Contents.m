% graphics: Image and plotting tools
%
%  img/		Tools to manipulate image arrays
%
%  movies/      Tool to generate and manipulate movies
%
%  plot/	Tools for making special plots and manipulating plots
%
%  plot2svg/    Tools to generate SVG (Scalable Vector Graphics) from plots.
%
%  contour_legend_handles Handles to unique levels in a contour plot
%
%  getbox	  Interactively get the coordinates of a box on a figure.
%
%  set_printer    Configure the figure's postscript position and paper size.
