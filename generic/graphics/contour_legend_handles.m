% contour_legend_handles: Handles to unique levels in a contour plot
%
% lh=contour_legend_handles(ch);
%
% ARGUMENTS
%  ch   Handle returned by contour()
%
% RETURNS
%  Set of handles to unique levels
%
% USAGE
%  [dummy,ch]=contour(rand(10,10),[0.25 0.5 0.75 ]);
%  lh=contour_legend_handles(ch);
%  legend(lh,'0.25','0.50','0.75')

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function lh=contour_legend_handles(ch)

UD(1)=get(ch(1),'UserData');
lh(1)=ch(1);
j=1;

for i=2:length(ch)
 UD_tmp=get(ch(i),'UserData');
 if(isempty(find(UD==UD_tmp))) % if it is not yet found
  j=j+1;
  UD(j)=UD_tmp;
  lh(j)=ch(i);  
 end
end
