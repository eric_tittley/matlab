% generic/graphics/img: Image processing
%
%  A_scale	    Convert an image into colourmap-index image.
%
%  addimage	    Add images together, incorporating shifts and keeping track
%		    of the number of images contributing to each pixel
% 
%  bestblk	    Returns the 1-by-2 block size BLK closest to but smaller
%		    than K-by-K for block processing. Used by imresize.
%
%  colourbar_label  Labels a colourbar.
%
%  colourbar_log    Colourbar in log units
%
%  colourmap	    My personal colourmap.
%
%  CreateRGBI       Combine two images to create a single RGBI (aka RGBL) image
%
%  gray2ind         Convert a grayscale image (intensity) into an indexed image
%
%  imagebin	    Bin (x,y) positions to create an image.
%
%  imagesc_polar    Display an image in polar projection
%
%  imresize	    Returns an image matrix that is M times larger (or smaller)
% 
%  isgray	    Determine if image is an intensity image
% 
%  jetSM            A jet-like colourmap similar to what SM provides
%
%  rebin_img	    Rebin an image (downward).
%
%  SmoothImgFromTemplate	Smooth and image given the smoothing radius
%				for each pixel.
