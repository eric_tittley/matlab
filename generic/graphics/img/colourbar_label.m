% colourbar_label:  Add a label to a colour bar
%
% colourbar_label(cbh,label)
%
% ARGUMENTS
%  cbh	    Handle to the current colourbar
%
%  label    The text to label the colourbar
%
% RETURNS
%  nothing

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab at least

function colourbar_label(cbh,label)

cbh.Label.String=label;
