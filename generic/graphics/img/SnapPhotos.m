function [MergedImage,theta,dx,dy]=SnapPhotos(I1,I2,ThetaBracket);
% SnapPhotos: Merge similarly-aligned photos
%
% [MergedImage,theta,dx,dy]=SnapPhotos(I1,I2,ThetaBracket);
%
% ARGUMENTS
%  I1	First image
%  I2	Second image
%  ThetaBracket	The range of possible rotation
%
% RETURNS
%  MergedImage	Combined image
%  theta	Rotation between images
%  dx,dy	Displacement
%
% REQUIRES
%  registerimage
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab and Octave
%
% HISTORY
%  091122 First version
%  091207 Comments added.

% Take the green image
R1=I1(:,:,2);
R2=I2(:,:,2);

[N,M]=size(R1);

% Extract a square in the centre
width=800;
spanx=N/2+[-width/2-1:width/2];
spany=M/2+[-width/2-1:width/2];
A=R1(spanx,spany);
B=R2(spanx,spany);

[dx,dy,theta]=registerimage(A,B,ThetaBracket)

B=imrotate(B,theta,'nearest','crop');
B=shift(shift(B,-dx,1),-dy,2);

MergedImage=A+B;
