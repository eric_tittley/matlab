function ph=imagesc_polar(A,Alim)
% imagesc_polar: Display an image in polar projection
%
% ph=imagesc_polar(A,[Alim])
%
% ARGUMENTS
%  A	An MxN unscaled image array.  M radial bins. N azimuthal bins.
%  Alim An optional range for Alim
%
% RETURNS
%  ph	A handle to the Patch object.
%
% REQUIRES
%  A_scale
%
% SEE ALSO
%  patch

% AUTHOR: Eric Tittley
%
% HISTORY
%  090506 First version
%  090507 Added comments
%  090824 Pass Alim in as an option
%
% COMPATIBILITY: Matlab & Octave

% 'r' => Radial distance
% 't' => Azimuthial distance (theta)

[Nr,Nt]=size(A);

% Scale the image values to 1:Ncolormap_entries
if(nargin==1)
 Alim=[min(min(A)),max(max(A))];
end
Nc = size(colormap,1);
A=A_scale(A,Alim(1),Alim(2),Nc);

% The edges
Edges_r=[0:1/Nr:1];
Edges_t=[0:1/Nt:1]*2*pi;

% The number of patches
Npatches = Nr*Nt;

% The patch coordinates
X=zeros(4,Npatches);
Y=zeros(4,Npatches);

% Calculate the patch coordinates
k=0;
for i=1:Nt
 lo_theta = Edges_t(i);
 hi_theta = Edges_t(i+1);
 for j=1:Nr
  lo_r = Edges_r(j);
  hi_r = Edges_r(j+1);
  k=k+1; % patch number
  [X(1,k),Y(1,k)]=pol2cart(lo_theta,lo_r);
  [X(2,k),Y(2,k)]=pol2cart(hi_theta,lo_r);
  [X(3,k),Y(3,k)]=pol2cart(hi_theta,hi_r);
  [X(4,k),Y(4,k)]=pol2cart(lo_theta,hi_r);
 end % loop over radii
end % loop over theta

% Convert A into a vector
A=reshape(A,1,Npatches);

% Set the colour values for the patches
map=colormap;
C=reshape(map(A,:),1,Npatches,3);

% Generate the figure
ph=patch(X,Y,C);
set(ph,'EdgeColor','none');
set(gca,'visible','off')
axis square

circle(1,0,0,'k');
