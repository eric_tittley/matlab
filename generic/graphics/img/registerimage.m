function [dx,dy,theta]=registerimage(A,B,ThetaBracket)
% Calculate the shift and rotation between two images.
%
% [dx,dy,theta]=registerimage(A,B,ThetaBracket)
%
% ARGUMENTS
%  A
%  B
%
% RETURNS
%  dx
%  dy
%  theta
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBLE: Octave, Matlab
%
% HISTORY
%  091123 Started

[nA,mA]=size(A);
[nB,mB]=size(B);

if(nA~=mA | nB~=mB)
 error('Matrices A & B must be square')
end
if(nA~=nB | mA~=mB)
 error('Matrices A & B must be the same size')
end

X.A=A;
X.B=B;

if(0)
theta=[-5:0.1:5];
sigmas=0*theta;
for j=1:length(theta)
 sigmas(j)=FuncToMinimise(theta(j),X);
end
plot(theta,sigmas)
pause
end

theta_min=BracketMin(@FuncToMinimise, ...
                     ThetaBracket(1),sum(ThetaBracket)/2,ThetaBracket(2),X);

B=imrotate(B,theta_min,'nearest','crop');
[sigma, ampl, dx, dy] = CrossCorr(A,B);
theta=theta_min;

end
% END OF MAIN FUNCTION

 % Nested Function to minimize
 function err=FuncToMinimise(theta,X);
  disp(theta)
  Brot=imrotate(X.B,theta,'nearest','crop');
  [sigma,ampl,x,y]=CrossCorr(X.A,Brot);
  Bshift=shift(Brot,-x,1);
  Bshift=shift(Bshift,-y,2);
  % Want to minimise sigma and maximise ampl;
  %err=(sum(sum(X.B))-ampl)*sigma;
  err=sum((X.A(:)-Bshift(:)).^2);
  disp(x)
  disp(y)
  disp(err)
 end

% ============ Local functions =============



function [sigma, ampl, x, y] = CrossCorr(A,B)

[N,M]=size(A);

% FFT's of the snips
Af=fft2(A);
Bf=fft2(B);

% FFT of the CrossCorr
Crossf=conj(Af).*Bf;

% The Cross Correlation
Cross=ifft2(Crossf);

% Just take the real part. The imaginary is negligible
rCross=real(Cross);

% Centre the 0,0
Spec=shift(shift(rCross,N/2,1),N/2,2);

% Scale to 0:65536
Spec=(Spec-min(min(Spec)))/(max(max(Spec)) - min(min(Spec)))*65536;

% Find the peak
max_index = find(Spec==max(max(Spec)));
[xbase,ybase]=ind2sub(size(Spec),max_index(1));

% Now for a very arbitrary step: Put a box of width 51 around the peak before
% fitting the gaussian
subSpec=Spec(xbase-25:xbase+25,ybase-25:ybase+25);

% Find the peak in the cross-correlation
[bias,sigma,ampl,xfine,yfine]=fit_gaussian(subSpec);

x = xbase-N/2 + xfine-25;
y = ybase-N/2 + yfine-25;

end
% End of CrossCorr()
