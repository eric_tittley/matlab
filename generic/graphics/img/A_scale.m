function Asc=A_scale(A,minA,maxA,ncolours)
% Convert an image into colourmap-index image.
%
% Asc=A_scale(A,minA,maxA,ncolours);
%
% INPUT
%  A		The original image.
%  minA		The value in A which will be indexed to 1.
%  maxA 	The value in A which will be indexed to ncolours.
%  ncolours	The number of colours in the colourmap.
%
% OUTPUT
%  Asc		The scaled image.  Elements are type uint8.

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 04 02 Mature version.  Comments added.

Asc=(A-minA)/(maxA-minA)*(ncolours-1)+1;
Asc(Asc<1)=1+0*Asc(Asc<1);
Asc(Asc>ncolours)=ncolours+0*Asc(Asc>ncolours);
Asc=uint8(round(Asc));

%END
