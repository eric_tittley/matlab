function [imagebase,imagenum]=addimage(imagebase,image,imagenum,shift)

image=image+10;
image=image.*(image>0);

imagebase(512-shift(1):676-shift(1),512-shift(2):703-shift(2))= imagebase(512-shift(1):676-shift(1),512-shift(2):703-shift(2))+image.*(image>0);

imagenum(512-shift(1):676-shift(1),512-shift(2):703-shift(2))= imagenum(512-shift(1):676-shift(1),512-shift(2):703-shift(2))+(image>0);
