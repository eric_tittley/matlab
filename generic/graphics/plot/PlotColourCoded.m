% PlotColourCoded plot x,y,z as a two dimensional plot with colour
%                 determined by z
%
% PlotColourCoded(x,y,z,NColours,Psym,Psize,ColourMapName)
%
% ARGUMENTS
%  x            X-axis positions [N-Vector]
%  y            Y-axis positions [N-Vector]
%  z            Values at (x,y) [N-Vector]
%  NColours     Number of divisions in the colours
%  Psym		The symbol to use [Optional. Default '.']
%  Psize        Size of the symbol [Optional. Default 1]
%  ColourMapName Name of the colour map to use [Optional. Default 'jet'] 
%
% RETURNS
%  nothing
%
% USAGE
%  X=2*pi*rand(1,10000);
%  Y=2*pi*rand(1,10000);
%  Z=10*sin(X).*sin(Y);
%  PlotColourCoded(X,Y,Z,10)
%  colorbar
% 
% SEE ALSO
%

% AUTHOR: Fraser Thompson & Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  110302 First version

function PlotColourCoded(x,y,z,NColours,Psym,Psize,ColourMapName)
if(nargin==4)
 Psym='.';
 Psize=1;
 ColourMapName='jet';
end

range=max(z) - min(z);
bin_length=range/NColours;

bin_edges=[min(z):bin_length:max(z)]; % Length = NColours+1

% Boost the last bin edge so we get all the points
bin_edges(end)=bin_edges(end)*1.00001;

% Set a colormap. Default jet.
eval(['A=',ColourMapName,'(NColours);'])

for i=1:NColours
 good=find(z>=bin_edges(i) & z<bin_edges(i+1));
 plot(x(good),y(good),Psym, 'MarkerSize',Psize, 'color', A(i,:));
 if(i==1)
  hold on
 end
end
hold off

% Set the limits for the colour map, if colorbar is asked for
set(gca, 'CLim', [min(z) max(z)]); 
