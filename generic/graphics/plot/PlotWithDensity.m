% PlotWithDensity: plot() which changes the point colour where points overlap
%
% Handle = PlotWithDensity(X,Y,varargin(:))
%
% ARGUMENTS
%  X    Event positions (X)
%  Y    Event positions (Y)
%
% RETURNS
%  Handle to the figure
%
% The colormap has NLevels = maximum number of particles in pixel.
%
% USAGE
%  To put on a colorbar, you will need to shift the range by 1. Otherwise the
%  background is assumed to represent 1, when it should be zero.
%  The following does this:
%   Handle=PlotWithDensity(X,Y,'.','markersize',1);
%   H=colorbar;
%   CLim=get(gca,'CLim')
%   set(H,'YLim',-0.5+CLim) ; % Adjusts the scale (number positions)
%   H_im=get(H,'child');
%   set(H_im,'YData',CLim-1); % Adjusts the colour bar colours

% TODO
%  Still a work in progress.
%  The information provided to colorbar is incorrect.
%  If NLevels < 14, the colorbar info is still incorrect.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function Handle = PlotWithDensity(X,Y,varargin);

% Parse varargin
p=inputParser;
addRequired(p,'X',@isnumeric);
addRequired(p,'Y',@isnumeric);
addParameter(p,'XLim',[],@isnumeric);
addParameter(p,'YLim',[],@isnumeric);
% Keep unmatched arguments
p.KeepUnmatched = true;
parse(p,X,Y,varargin{:});

% Pack unmatched arguments to pass to plot()
tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
plotArgs = reshape(tmp',[],1)';

% Force X * Y to be column vectors
X=X(:);
Y=Y(:);

if(isempty(p.Results.XLim) || isempty(p.Results.YLim))

 %Handle = plot(X,Y,plotArgs);
 Handle = plot(X,Y);
 % Get the axes limits
 XLim=get(gca,'XLim');
 YLim=get(gca,'YLim');
else
 XLim=p.Results.XLim;
 YLim=p.Results.YLim;
 Handle = plot(X,Y,plotArgs{:});
 set(gca,'XLim',XLim);
 set(gca,'YLim',YLim);
end
Limits=[XLim YLim];

% Determine the number of pixels in the figure window
FigurePosition = get(gcf,'Position');
FigureWidth    = FigurePosition(3);
FigureHeight   = FigurePosition(4);
AxesPosition   = get(gca,'Position');
AxesWidth      = floor(FigureWidth*AxesPosition(3));
AxesHeight     = floor(FigureHeight*AxesPosition(4));

% The position of each pixel
dX = (XLim(2)-XLim(1))/AxesWidth/2;
XCentres=[XLim(1)+dX XLim(2)-dX];
dY = (YLim(2)-YLim(1))/AxesHeight/2;
YCentres=[YLim(1)+dY YLim(2)-dY];

% Generate the binned image
A=imagebin(X,Y,[AxesHeight AxesWidth],Limits);
Nlevels=max(A(:))
% colourmap() can't handle fewer than 14 levels. This is my function.
if(Nlevels<14)
 Nlevels=14;
end
colormap([[1 1 1];flipud(colourmap(Nlevels))]);
Handle=image(XCentres,YCentres,A+1);
set(gca,'XLim',XLim,'YLim',YLim,'YDir','normal');
