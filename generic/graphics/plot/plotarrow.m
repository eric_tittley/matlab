function handle=plotarrow(r_o,r_f)
% Draw arrows on a figure.
%
% handle=plotarrow(r_o,r_f)
%
% Plot an arrow on the current figure from r_o=[x_o,y_o] to r_f=[x_f,y_f]
% r_o and r_f can be [N,2] arrays, leading to N arrows being plotted.
%
% handle is a list of line handles which may be used after calling
% to set the colour, linetype, etc.

% TODO:
% Change the size of the arrow head.  Barbs are 5 mm at the moment.
%
% AUTHOR: Eric Tittley
%
% HISTORY
%  01-05-17 First Version
%  01-05-18 Added comments.
%  01-10-22 For some reason, the code wasn't working.  And I couldn't tell
%	how it could ever have worked in the first place.  The calculations
%	for phi_u and phi_l were wrong, as were the method for correcting the
%	scaling.  And some of the vectorized parts were done incorrectly.
%	Anyway, got it working again.
%  03-01-26 Fixed so that the routine works for multiple r_o/r_f pairs.

if( size(r_o,1) > 1 ) % If more than one arrow, call recursively
 lh=zeros(3*size(r_o,1),1);
 for i=1:size(r_o,1)
  lh([(i-1)*3+1:i*3])=plotarrow(r_o(i,:),r_f(i,:));
 end
 handle=lh;
else % Just one arrow
 % Plot the main line
 lh=line([r_o(:,1),r_f(:,1)],[r_o(:,2),r_f(:,2)]);

 % The arrow head
 % The length of the barbs will be 5 mm and at an angle of 30 degrees.
 BarbLength=0.5; %cm
 theta=30/180*pi; % Convert angle to radians.

 Position=get(gca,'Position'); % In units of figure
 PaperPosition=get(gcf,'PaperPosition'); % In units of PaperUnits, usually inches

 XLength_cm=Position(3)*PaperPosition(3)*2.54; % cm
 XLim=get(gca,'XLim');
 XLength=XLim(2)-XLim(1);
 XScale=XLength_cm/XLength; %cm/x-axis unit

 YLength_cm=Position(4)*PaperPosition(4)*2.54; % cm
 YLim=get(gca,'YLim');
 YLength=YLim(2)-YLim(1);
 YScale=YLength_cm/YLength; %cm/y-axis unit

 % The angle the arrow will make
 phi=pi/2+0*r_f(:,1);
 m=find((r_f(:,1)-r_o(:,1))~=0);
 phi(m)=atan( (r_f(m,2)-r_o(m,2))./(r_f(m,1)-r_o(m,1))*(YScale/XScale) );
 % Correct for the case where phi should be between pi/2 and 3pi/2.
 m=find(r_f(:,1)<r_o(:,1));
 phi(m)=phi(m)+pi;

 phi_u=phi+pi-theta; % The angle the upper barb will make
 phi_l=phi+pi+theta; % The angle the lower barb will make

 dx_u=BarbLength*cos(phi_u)/XScale;
 dy_u=BarbLength*sin(phi_u)/YScale;
 dx_l=BarbLength*cos(phi_l)/XScale;
 dy_l=BarbLength*sin(phi_l)/YScale;

 %Plot the upper barb
 for i=1:size(r_o,1)
  lh_tmp=line(r_f(i,1)+[0,dx_u],r_f(i,2)+[0,dy_u]);
  lh=[lh,lh_tmp];
 end
 %Plot the lower barb
 for i=1:size(r_o,1)
  lh_tmp=line(r_f(i,1)+[0,dx_l],r_f(i,2)+[0,dy_l]);
  lh=[lh,lh_tmp];
 end
 handle=lh;

end % if more than one arrow, else, end

% DONE
