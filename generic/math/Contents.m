% math: Mathematical functions and tools
%
% funcs	Special Functions
%
% tools	Tools
%
% Roots	Finding roots (zeros) of functions in 1 or more dimensions.
