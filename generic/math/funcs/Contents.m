%  math/funcs Special Functions
% 
%  gauss	Gaussian profile.
%
%  lorentz	Lorentzian profile.
% 
%  voigt	A Voigt profile.
%
%  Poisson	The probability distribution of for Poisson statistics.
%
%  ProbOfMean	The probability of the given mean value presuming Poisson stats.
