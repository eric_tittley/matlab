function out=gauss(ampl,sigma,wave,wave0)
% gauss: Gaussian function y=gauss(x)
%
% Syntax: out=gauss(ampl,sigma,wave,wave0);
%     or: out=gauss(wave,P);
%
% INPUT
%  wave		x's at which to compute y	Scalar or Vector
%  ampl		'amplitude' of the Gaussian	Scalar
%  sigma	FWHM of the Gaussian		Scalar
%  wave0	centre position of the Gaussian Scalar
% OR
%  P		[ampl sigma wave0]
%
% OUTPUT
%  out		y(x)
%
% SEE ALSO
%  gauss_fit fit_gaussian, lorentz, voigt

% AUTHOR: Eric Tittley
%
% HISTORY
%  01-12-04 Mature code.
%	Added comments.
%	Added ability to take P as an argument.
%  02-02-09 Comments said routine was named 'Gaussian'.  Changed to 'gauss'.
%
% COMPATIBILITY: Matlab, Octave

if(nargin==2)
 wave=ampl;
 P=sigma;
 if(length(P)==3)
  ampl=P(1);
  sigma=P(2);
  wave0=P(3);
 else
  error('P must have [ampl sigma wave0]')
 end
elseif(nargin~=4)
 error('Syntax: out=gauss(ampl,sigma,wave,wave0); or out=gauss(wave,P);')
end

out=ampl/sigma * exp(-((wave-wave0)/sigma).^2/2);
