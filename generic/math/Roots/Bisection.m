function [x_r,x]=Bisection(f,a,b)
% Bisection method for finding roots of non-linear equations.
%
% x_r=Bisection(f,a,b)		Returns the root satisfying f(x_r)=0
% [x_r,x]=Bisection(f,a,b)	Returns the root, plus intermediate steps, x.
%
% f	The function
% a	The lower end of the span containing the root.
% b	The upper end of the span containing the root.
%
% SEE ALSO
%  Muller, Secant, Newton

% AUTHOR: Eric Tittley
%
% HISTORY
%  03-01-21 First Version
%
% COMPATIBILITY: Matlab, Octave

% The tolerance
Tol=eps*(abs(a)+abs(b))/2;

% Pre-calculate the values at the ends.
f1=f(a);
f3=f(b);

while( (b-a)>Tol )
 % Determine the centre point
 c=(a+b)/2;
 % Calculate the function at the centre
 f2=f(c);
 % Determine which span contains the root.
 % Assumes one span does.
 if( sign(f1*f2)<0)
  b=c;
  f3=f2;
 else
  a=c;
  f1=f2;
 end
 % Update the tolerance
 Tol=eps*(abs(a)+abs(b));
end

x_r=(a+b)/2; % Take the average of the span
