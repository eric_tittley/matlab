function [x_r,x]=Newton(f,x_g,P)
% Newton's Method for finding roots of non-linear equations.
%
% x_r=Newton(f,x_g)	Returns the root satisfying f(x_r)=0
% [x_r,x]=Newton(f,x_g)	Returns the root, plus intermediate steps, x.
%
% f	The function (inline function object)
% x_g	An initial guess point.
% P	A set of parameters to pass to f such
%
% The function is called as f(x,P)
%
% SEE ALSO
%  Muller, Bisection, Secant

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 01 21 First Version
%  03 05 14 Added comment.
%	Set minimum dx to x_g*eps.
%	Added support for extra parameters.
%  03 05 23 Set a limit to the maximum number of iterations
%
% COMPATIBILITY: Matlab, Octave

MAX_ITERATIONS = 1000;

% Default Parameter list
if(nargin==2)
 P=[];
end

% Save the intermediate values if required.
if(nargout==2)
 x(1)=x_g;
end
niters=1;

% Initial values for the tolerance and the increment
Tol=4*eps; % 2eps leads to oscillation.
dx =eps*100;

% Set to trip the while loop
delta_x=1;

% Main loop
f_g=f(x_g,P);
% First test is to see if the function is close enough to 0.
% The second test is to see that delta_x actually modifies x_g.
while( abs(f_g)>Tol & (abs(delta_x)>abs(x_g)*eps) & niters<MAX_ITERATIONS )
 % Newton's Method
 % WARNING: small number divided by a small number.
 df=f(x_g+dx,P) - f_g;
 if(df~=0)
  dfdx=df/dx;
  delta_x = - f_g/dfdx;
 else
  delta_x=0; % not a good solution.
 end
 x_g = x_g + delta_x;

 % For the test and the next loop
 f_g=f(x_g,P);

 % Save the intermediate values if requested.
 if(nargout==2)
  x(niters)=x_g;
 end
 niters=niters+1;

 % Update dx.  We don't want it to be zero, so add eps.
 % If delta_x was big, this will give us a good average slope
 % over a large distance as well, which will hopefully get us
 % back near the zero.
 dx=delta_x/100+x_g*eps;

% disp([abs(delta_x) abs(x_g)*eps ])
end % While |delta_x|>Tol

% Store the output
x_r=x_g;

if(nargout==2)
 x=x(:); % Force to be a column vector
end

if(niters==MAX_ITERATIONS)
 disp('WARNING: maximum number of iterations reached')
end
