function [d,r]=DistanceFromPointToLine(P1,P2,P3)
% The distance from a point to a line.
%
% [d,r]=DistanceFromPointToLine(P1,P2,P3)
%
% Given a line defined by two points: P1=[x1,y1,z1] and P2=[x2,y2,z2],
% find the closest distance from the line to a point, P3.
% 
% ARGUMENTS
%  P1 & P2	Points defining a line P1=[x1,y1,z1] and P2=[x2,y2,z2]
%  P3		The point to which to find the distance.
%		P3 can be a vector of points, but must be 3 columns.
%
% RETURNS
%  d		The least distance between the point and the line.
%  r		The distance from P1 to the point on the line that is closest
%		to P3.  If r<0., then the point is on the other side of P1
%		as P2.
%
% TODO
%  Vectorise.  Presently, only one position P3 can be processed at a time.

% AUTHOR: Eric Tittley
%
% HISTORY
%  040331 First version
%
% METHOD
%  From http://astronomy.swin.edu.au/~pbourke/geometry/pointline/
%  Paul Bourke, extended to 3-D.
%  However, it is easily derived.

if(nargin~=3)
 error('syntax: DistanceFromPointToLine(P1,P2,P3)')
end

if(length(P1)~=3 | length(P2)~=3)
 error('DistanceFromPointToLine: P1 and P2 must have 3 elements');
end

if( size(P3,2)~=3 )
 error('DistanceFromPointToLine: P3 must be Nx3');
end

N=size(P3,1);

d=zeros(N,1);
r=zeros(N,1);

P2mP1 = P2-P1;
P2mP1_2=sum(P2mP1.^2);
for i=1:N
 P3mP1 = P3(i,:)-P1;
 % The distance vector from P1 to the point on the line that is closest to P3
 u =  ( (P3mP1*P2mP1') / P2mP1_2 );
 s=sign(u);
 u = u*P2mP1;

 % The distance from P to P3
 d(i) = norm(P3mP1 - u);

 % The distance from P1 to P
 r(i) = s*norm(u);
end
