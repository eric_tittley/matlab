% math: Math tools
%
%  clrNaN	Clears all NaN's from an array and replaces them with a value.
%
%  DistanceFromPointToLine	The distance from a point to a line.
%
%  GS_ortho			Gram-Schmidt orthonormalization on a set of
%				vectors
%
%  isposint: True for positive integer values.
%
%  SampFromDistribution		Create a random instance of a distribution
%				given an input distribution.
