function ortho=GS_ortho(z)
if nargin~=1, error('Incorrect number of arguments passed to GS_ortho'); end

% Syntax: ortho=GS_ortho(z)
% where ortho is an array of size [mXn], (m rows of orthonormalized vectors of 
% length, n.  Input is a matrix of the same dimensions as ortho but the vectors
% are the ones to be orthonormalized.
%
% Function to perform the orthonormalization on a set of vectors
% using the Gram-Schmidt method.
% The input array, z, is expected to be row vectors, usually with the
% lowest order vector in z(1,:)
% Written by Eric Tittley (95 06 23)

[m,n]=size(z); % m = number of vectors, n = length of vectors
ortho=0*z;     % initialize ortho to the size of z

ortho(1,:)=z(1,:)/sqrt(sum(z(1,:).*z(1,:)));
for k=2:m
% dummy=0*dummy;
% for s=1:k-1
%  dummy=dummy+sum(z(k,:).*ortho(s,:))*ortho(s,:);
% end
% ortho(k,:)=z(k,:)-dummy;
%The next line does the above stuff, only faster.
 ortho(k,:)=z(k,:)-sum((sum(((([1:k-1]&1)'*z(k,:)).*ortho(1:k-1,:))')'*(1&[1:n])).*ortho(1:k-1,:));
 ortho(k,:)=ortho(k,:)/sqrt(sum(ortho(k,:).*ortho(k,:)));
end
ortho=ortho*sqrt(n);

