function samp=SampFromDistribution(Dist,N)
%
% samp=SampFromDistribution(Dist,N);
%
% Given a probability distribution, Dist(bin), find N random values
% of bin such that hist(samp,length(bin))/N==Dist as N->inf

% AUTHOR: Eric Tittley (etittley@jca.umbc.edu)

% HISTORY
%  01 03 12 First version
%  01 04 24 Sped up considerably by:
%	1) removing all the zeros from the input distribution.
%	2) using interp1(...,'nearest')

% METHOD:
%  y=f(x) is the distribution.
%  The cumulative probability is 1. (sum(f(all x))=1)
%  So if we find the cumulative probability distribution ( cf(x)=sum(f(1:x)) )
%  then for randomly selected numbers from 0 to 1, z_i, the points, x_i, such
%  that the cumulative dist. is closest to the randomly selected value
%  z_i = cf(x_i) will give the sample of x's.

% Normalize
Dist=Dist/sum(Dist);

% The sampling
N_takes=rand([N,1]);
CumDist=cumsum(Dist);

% The zero-probability values will just slow things down.
good=find(diff(CumDist))+1;

%samp=0*N_takes;
% This can be hideously slow.
%for i=1:N
% dummy=find(CumDist(good)<=N_takes(i));
% samp(i)=length(dummy)+1;
%end
%samp=good(samp);

% The next does the previous, but faster.
% The next section simply finds the position such that pos is closest
% to the value of the takes in N_takes.
samp=good(interp1(CumDist(good),[1:length(good)],N_takes,'nearest'));
