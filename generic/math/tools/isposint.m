function f=isposint(v)
% isposint: True for positive integer values.
%
% ARGUMENTS
%  v	A value to test (array)
%
% RETURNS
%  f	True (1) for positive integer values.
%	False (0) otherwise.
%
% REQUIRES
%
% SEE ALSO

% AUTHOR Mark Beale, 11-31-97
%
% HISTORY
%  97 11 31 First version
%
% COPYRIGHT
%  Copyright 1992-2002 The MathWorks, Inc.
%
% VERSION
%  Revision: 1.7
%
% COMPATIBILITY: Matlab

f = 1;
if ~isa(v,'double') | any(size(v) ~= [1 1]) | ...
  ~isreal(v) | v<0 | round(v) ~= v
  f = 0;
end
