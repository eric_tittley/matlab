% GaussLegendreWeights: Gauss-Legendre sample points and weights
%
% [xs,w]=GaussLegendreWeights(N)
%
% ARGUMENT
%  N	The number of sample points in the interval
%
% RETURNS
%  xs	The position of the sample points in the interval [-1 1]
%  w	The weights for the sample points
%
% USAGE
%  Suppose we have a function, f(), that we want to integrate in the interval
%  between a and b.
%   [xs,w]=GaussLegendreWeights(4);
%   x = a + (b-a)*(1+xs)/2;
%   I=(b-a)/2 * sum(w.*f(x));
%
% SEE ALSO
%  GausseLegendreWeights

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab/Octave
%
% HISTORY
%  110810 First version, based on a problem-set code from 2003
%
% TOD0
%  There are more weights in GL_Weights.c in my RT code

function [xs,w]=GaussLegendreWeights(N)

switch N
 case 2
  xs=[-(3^-0.5) 3^-0.5];
  w=[1 1];
 case 3
  xs=[-(3/5)^0.5 0 (3/5)^0.5];
  w=[5/9 8/9 5/9];
 case 4
  xs=[0.3399810436 0.8611363116];
  xs=[-xs(end:-1:1), xs];
  w=[0.3478548451 0.6521451549];
  w=[w,w(end:-1:1)];
 case 5
  xs=[0 0.5384693101 0.9061798459];
  xs=[-xs(end:-1:2), xs];
  w=[0.2369268850 0.4786286705 128/225];
  w=[w,w(end-1:-1:1)];
 case 8
  xs=[0.1834346425 0.5255324099 0.7966664774 0.9602898565];
  xs=[-xs(end:-1:1), xs];
  w=[0.1012285363 0.2223810345 0.3137066459 0.3626837834];
  w=[w,w(end:-1:1)];
 case 12
  xs=[0.1252334085 0.3678314990 0.5873179543 0.7699026742 0.9041172564 ...
      0.9815606342];
  xs=[-xs(end:-1:1), xs];
  w=[0.0471753364 0.1069393260 0.1600783285 0.2031674267 0.2334925365 ...
     0.2491470458];
  w=[w,w(end:-1:1)];
 otherwise
  error('Only programmed for N=2,3,4,5,8, or 12')
end

