% math/Quadrature: Functions for integrating functions
%
% GaussLaguerreWeights		Gauss-Laguerre sample points and weights
%
% GaussLegendreWeights		Gauss-Legendre sample points and weights
%
% RichardsonExtrapolation	Extrapolate estimates to infinite sample
%				resolution.
