function integral=integrate_trap(spec);
% integrate_trap: Integrates using the trapezoidal rule.
%
% integral=integrate_trap(spec);
%
% ARGUMENTS
%  spec	The discrete function values to integrate.
%
% RETURNS
%  The integration of spec
%
% NOTES
%  spec must be evenly spaced (spacing dx).
%  The integral returned must be scaled by the spacing interval (dx).
%
% REQUIRES
%
% SEE ALSO
%  integrate_gauss

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Added comments
%
% COMPATIBILITY: Matlab, Octave

n=length(spec);
integral=sum(spec(1:n-1)+spec(2:n))/2;
