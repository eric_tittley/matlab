function [A,dev]=fit_plane(X)
% fit_plane: Fit a plane
% 
% [A,dev]=fit_plane(X)
%
% ARGUMENTS
%  X	Set of n points defining the plane to fit. [n,3]
%
% RETURNS
%  A	The fit (see NOTES)
%  dev	The standard deviation of the residuals
%
% NOTES
%  Specifically, returns the least-squares solution to X*A=I which
%  is matrix-lingo for the system of equations ax+by+cz=1 for a series
%  of points (x,y,z)_1,(x,y,z)_2,... and solution A=[a,b,c]';
%
% REQUIRES
%
% SEE ALSO
%  fitellipse fit_ellipse fit_line fit_surface

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

[m,n]=size(X);
if(n~=3), X=X'; end %X must have 3 columns of n xyz-positions

I=ones(m,1);

A=X\I;
Icalc=X*A;
dev=std(Icalc-I);
