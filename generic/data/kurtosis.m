function kurt=kurtosis(x)
% kurtosis: The kurtosis of a distribution
%
% [kurt,err]=kurtosis(x);
%
% ARGUMENTS
%  x	The values for which to derive the kurtosis
%
% RETURNS
%  The kurtosis
%
% NOTES
%  If x is an (m x n) array, then kurt will be a row vector of length n.
%
% REQUIRES
%
% SEE ALSO
%  mean, std, skewness

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%	Removed non-sensical calculation of error.
%
% COMPATIBILITY: Matlab, Octave

x_mean=mean(x);
x_std =std(x);

[M,N]=size(x);

if(N>1)
 kurt=zeros(1,N);
 for i=1:N
  kurt(i)=mean( ( (x(:,i)-x_mean(i))/x_std(i) ).^4 ) -3;
 end
else
 kurt=mean( ( (x-x_mean)/x_std ).^4 ) -3;
end
