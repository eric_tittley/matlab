% Data Analysis
%
% *** Data fitting ***
% 
% bootstrap_vect	Perform bootstrap fitting of functions to data points.
%
% bootstrap	Bootstraps the fit to y(x) of a non-linear function 'F'
% 		which is passed as a string.
%
% chisqr	The Chi-Square of a fit to some data.
%
% curvefit2	Curve fitting and plotting routine.
%
% chisqr_linear_fit The chi-square of a linear fit to a set of data.
%
% fit_ellipse	Fits an ellipse to a set of data points.
%
% fitellipse	Fits an ellipse to a set of data points.
%
% gauss_diff	The standard deviation between a Gaussian and 1-D data set.
%
% gauss_fit	Fit a 1-D Gaussian
%
% plane_fit	Fits a plane to a dataset.
%
% surface_fit	Fit a N polynomial surface to the values of Z
%
% *** Deconvolution ***
%
% lucy_damp	Damped R-L deconvolution.
%
% lucy_low	R-L deconvolution using a low-frequency filter on the
%		correction vector.
%
% opt_filt	Optimal filtering
%
% *** Distribution analysis ***
%
% clip		Clip a data set to exclude all data sigmas sigma from the mean.
%
% kurtosis	The kurtosis of a distribution.
%
% skewness	The skewness of a distribution.
%
% *** Hankel transformation ***
%
% hankel_trans	Hankel transform
%
%
% *** Histogram ***
%
%  hist2d	A 2-D histogram.
%
% *** Integration ***
%
% gaussint	Gaussian integration
%
% integrate	Integrates the area under the curve, spec, using the
% 		trapezoidal rule.
%
% *** Random distributions ***
%
% randdist	Returns random values picked from a set according
% 		to a distribution with linear interpolation between
% 		values in the set.
%
% randomdraw	Draw N non-repeating elements from a sample of M elements (M>N).
%
% *** Signal processing ***
%
% dft		Discrete Fourier Transform routine
%
% bindata_1d	Bin data, returning the mean and standard deviation within bins
%
% bindata_2d	Bin data, returning the mean and standard deviation within bins
%
% periodogram2	The periods between the events in a series.
%
% *** Smoothing ***
%
% Smooth	Smooth 1, 2, or 3-D data
%
% *** Spatial analysis ***
%
% rot3d		Rotates positions in 3-space.
%
% *** Wavelet analysis ***
%
% haar		Haar wavelet transform
%
% haar_inv	Inverse Haar wavelet transform
%
% wavelet	Colm Mulcahy's haar transform routines.
%
% s_trans	The S-transform toolkit
