% fit_line: Find the line that minimises the distances to (x,y) points.
%
% P = fit_line(x,y)
%
% ARGUMENTS
%  x,y	Lists of the positions.
%
% RETURNS
%  P = [A,B,C] such that the best-fit line is Ax + By + C = 0.
%
% NOTES
%  The solution is not the same as a least-squares fit, which fits y=f(x).
%
% REQUIRES
%
% SEE ALSO
%  fit_plane fitellipse fit_ellipse fit_surface

% AUTHOR: Eric Tittley
%
% HISTORY
%  05 03 09 First version.
%  05 03 28 Guess with a LSQ fit, since that is often close to the best guess.
%	Normalise the vectors.
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab

function P = fit_line(x,y)
P0=y/[x;ones(size(x))];
P0=[-P0(1) 1 -P0(2)];
P0=P0/norm(P0);
P = fminsearch(@fit_line_error,P0,[],x,y);
P = P/norm(P);
% END

function err=fit_line_error(P,x,y)
D = inline('abs((P(1)*x+P(2)*y+P(3))/sqrt(P(1)^2+P(2)^2))', 'x','y','P');
err=sum(D(x,y,P));
%END
