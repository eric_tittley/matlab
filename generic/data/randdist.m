function sample=randdist(value,distribution,n)
% randdist: Generate a selection of values drawn from an arbitrary distribution.
%
% sample=randdist(value,distribution,n)
%
% ARGUMENTS
%  value	The ordinates of the probability distribution
%  distribution	The probability P(values)
%  N		The number of samples to draw.
%
% RETURNS
%  N random values picked from value according
%  to the distribution and linear interpolation between
%  values.  Hence value should be monotonically
%  increasing and DISTRIBUTION should be smooth. 
%
% REQUIRES
%
% SEE ALSO
%  rand, randn, randps

% AUTHOR: Eric Tittley
%
% HISTORY
%  951108 First version
%  071217 Regularised comments
%  080911 use integrate_trap instead of integrate.
%	Check input arguments.
%
% COMPATIBILITY: Matlab, Octave

% Make columnwise
value=value(:);
distribution=distribution(:);

% Check arguments
if(numel(value)~=numel(distribution))
 error('value and distribution must have same number of elements')
end

%normalize distribution in case it hasn't been
distribution=distribution/integrate_trap(distribution);

len=length(distribution);

%find the running integral (ri)
ri=0*distribution;
for i=2:len-1
 ri(i)=integrate_trap(distribution(1:i));
end
ri(len)=1;

%initialize the sample (its values are irrelavent)
sample=[1:n]';

%calculate the sample
for i=1:n
 pick=rand(1);
 mask=(ri<pick);
 lo=find(mask(1:len-1)&~mask(2:len));
 sample(i)=value(lo)+(pick-ri(lo))*(value(lo)-value(lo+1))/(ri(lo)-ri(lo+1));
end
