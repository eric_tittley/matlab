more off
x0=0.5;
y0=1000;
a=2;
b=1;
alpha=-46/180*pi;

Npoints=6;
x=2*a*(rand(Npoints,1)-0.5);
y=b*sqrt(1-(x/a).^2).*abs(sign(rand(Npoints,1)-0.5));
%Rotate
r=rot3d([x,y],alpha,3,[0 0]);
%Translate
x=r(:,1)+x0;
y=r(:,2)+y0;


dx=0.1*(rand(Npoints,1)-0.5);
dy=0.1*(rand(Npoints,1)-0.5);
plot(x+dx,y+dy,'.');
P=fit_ellipse(x+dx,y+dy,gca)

return
xf = [-P(3):0.01:P(3)]';
yf = P(4)*sqrt(1-(xf/P(3)).^2).*sign(rand(size(xf))-0.5);
%Rotate
r=rot3d([xf,yf],P(5),3,[0 0]);
xf=r(:,1);
yf=r(:,2);
%Translate
xf=xf+P(1);
yf=yf+P(2);

if(abs(P(1))<5)
 hold on
 plot(xf,yf,'r.')
 hold off
end
