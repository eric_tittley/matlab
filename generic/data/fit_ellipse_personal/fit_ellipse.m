% Fit an ellipse to data points.
%
% P=fit_ellipse(x,y)
%
% ARGUMENTS
%  x	Vector of x positions
%  y	Vector of y positions
%
% RETURNS
%  P	Vector of ellipse parameters
%	[x0 y0 a b alpha]

% AUTHOR: Eric Tittley
%
% HISTORY
%  05 01 25 First version.
%
% From:
% Fitzgibbon, Pilu, & Fisher
%  IEEE Transactions on Pattern Analysis and Machine Intelligence
%  Vol. 21, No. 5, P. 476
% (http://www.robots.ox.ac.uk/~awf/ellipse/ellipse-pami.pdf)

function P=fit_ellipse(x,y)
% TOLERANCE
TOL = 1e-3;

% x,y, are vectors of coordinates
x=x(:);
y=y(:);

% Build design matrix
D = [ x.*x x.*y y.*y x y ones(size(x)) ];

% Build scatter matrix
S = D'*D;

% Build 6x6 constraint matrix
C(6,6)=0; C(1,3)=-2; C(2,2)=1; C(3,1)=-2;

% Solve generalised eigensystem
[gevec,geval] = eig(S,C);

% Find the only negative eigenvalue
[NegR, NegC] = find(geval<0 & ~isinf(geval));

% Get fitted parameters
A = gevec(:,NegC);
if(A(1) < 0)
 A=-A;
end

disp(A')
Pcor = [0.5 0 2 1 20/180*pi];
Ecor = fit_ellipse_min_fun(Pcor,A)
disp(' ')
P=0;
return

% Convert to useful parameters
% P = [x0 y0 a b alpha]
% Guess at values:
Pg(1)=mean(x);
Pg(2)=mean(y);
Pg(3) = sqrt(mean((x-Pg(1)).^2 + (y-Pg(2)).^2));
Pg(4) = Pg(3);
Pg(5) = 0;

P = fminsearch(@fit_ellipse_min_fun,Pg,[],A);

err = fit_ellipse_min_fun(P,A)
err1=err;

if(err > TOL)
 A=-1*A;
 P = fminsearch(@fit_ellipse_min_fun,Pg,[],A);
 err = fit_ellipse_min_fun(P,A)
end

if(err > err1)
 A=-1*A;
 P = fminsearch(@fit_ellipse_min_fun,Pg,[],A);
 err = fit_ellipse_min_fun(P,A)
end

