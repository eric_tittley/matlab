% Error between [a1 a2 a3 a4 a5 a6] & [x0 y0 a b alpha] ellipse parameters.
%
% ARGUMENTS
%  P	Vector of [x0 y0 a b alpha]
%  A	Vector of [a1 a2 a3 a4 a5 a6]
%
% RETURNS
%  err	The sum of the squares of the differences between A and A(P).
%
% NOTES
%  Two equations for translated and rotated ellipses:
%  a1*x^2 + a2*x*y + a3*Y^2 + a4*x + a5*y + a6 = 0
%  (x*cos(alpha)-y*sin(alpha)-x0)/a^2 + (x*sin(alpha)+y*cos(alpha)-x0)/b^2 = 1
%
% SEE
%  fit_ellipse

% AUTHOR: Eric Tittley
%
% HISTORY
%  05 01 25 First version

function err = fit_ellipse_min_fun(P,A)
x = P(1);
y = P(2);
a = P(3);
b = P(4);
alpha = mod(P(5),2*pi);

if(1)
D = [(cos(alpha)^2/a^2 + sin(alpha)^2/b^2) ; ...
     (2*cos(alpha)*sin(alpha)*(1/b^2 - 1/a^2)) ; ...
     (sin(alpha)^2/a^2 + cos(alpha)^2/b^2) ; ...
     -2*(x*cos(alpha)/a^2 + y*sin(alpha)/b^2) ; ...
      2*(x*sin(alpha)/a^2 - y*cos(alpha)/b^2) ; ...
     (x^2/a^2 + y^2/b^2 -1)];
else
ca=cos(alpha);
sa=sin(alpha);
ca2=ca^2;
sa2=sa^2;
a2=a^2;
b2=b^2;
D = [(ca2/a2 + sa2/b2) ; ...
     (2*ca*sa*(1/b2 - 1/a2)) ; ...
     (sa2/a2 + ca2/b2) ; ...
     -2*(x*ca/a2 + y*sa/b2) ; ...
      2*(x*sa/a2 - y*ca/b2) ; ...
     (x^2/a2 + y^2/b2 -1)];
end

disp((A-D)')
err = sum((A - D).^2);
%disp(err)
