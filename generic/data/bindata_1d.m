function [y_mean,y_std]=bindata_1d(x,y,bins)
% bindata_1d: Bin data, returning the mean and standard deviation within bins
%
% [y_mean,y_std]=bindata_1d(x,y,bins)
%
% ARGUMENTS
%  x	The position of the data
%  y	The values at (x)
%  bins	The bin edges
%
% RETURNS
%  y_mean	The mean values of the y's in the bins
%  y_std	The standard deviations of the y's in the bins
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

NumBins=length(bins);

y_mean=zeros(NumBins-1,1);
y_std=y_mean;

for i=1:NumBins-1
 elements=find( bins(i)<x & x<bins(i+1) );
 if(length(elements)>0)
  y_mean(i)=mean(y(elements));
  y_std(i) = std(y(elements));
 end
end
