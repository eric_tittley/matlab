function spec=s_invert(s_mat)
%SPEC=S_INVERT(S_MAT)

invert=sum(s_mat');

%even numbered time series only!!!!
invert=[invert,conj(invert(length(invert)-1:-1:2))];
spec=real(ifft(invert));
