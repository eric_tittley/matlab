function gauss=g_window(length,freq)
%GAUSS=G_WINDOW(LENGTH,FREQ)
%Used by the function S_TRANS

vector(1,:)=[0:length-1];
vector(2,:)=[-length:-1];
vector=vector.^2;
vector=vector*(-2*pi^2/freq^2);
gauss=sum(exp(vector));
