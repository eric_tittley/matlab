function g=hankel_trans(f)
% hankel_trans: Fast Hankel transform, order = 0.
%
% g=hankel_trans(f)
%
% ARGUMENTS
%  f	Input function
%
% RETURNS
%  g	Output function.
%
% NOTES
%  Discard g(256:512).
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Lou Lauzon (?) & Eric Tittley
%
% HISTORY
%  01 03 01 First version
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

n = (0:1:511)';
alfa =  0.01612;
r0 = 0.06349; 
r = exp(alfa.*n);
rho = r;
r1 = r(1:256);
f = [r1 .* exp(-0.5 .*(r1 .^2)); zeros(256,1)];
j = bessel(0,r);
F = fft(f.*r);
J = fft(j);
G = F .* conj(J);
g = ifft(G);
g = real(g);
