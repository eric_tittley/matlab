function P=fit_gauss(X,Y,P0)
% fit_gauss: Fit a 1-D gaussian
%
% P=fit_gauss(X,Y,P0);
%
% ARGUMENTS
%  X	The ordinal for the curve to fit.
%  Y	Y(X) is the curve to fit.
%  P0	A guess at the parameters
%
% RETURNS
%  Parameters, P, = [ amplitude, sigma, wave0 ]
%
% NOTES
%
% REQUIRES
%  gauss_diff
%
% SEE ALSO
%  gauss fit_gaussian

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 06 20 First Version.
%  00 07 19 Added comment statement explaining parameters.
%  00 12 04 Added more comments.
%  02 02 09 Played with options.  No real change.
%  02 09 20 Played with options again.  Reduces error messages.
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

options=optimset('MaxFunEvals',10000);
%options=[];
P=fminsearch('gauss_diff',P0,options,X,Y);

