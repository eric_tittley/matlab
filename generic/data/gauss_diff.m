function err=gauss_diff(P,X,Y)
% gauss_diff: The error function for gauss_fit
%
% err=gauss_diff(P,X,Y)
%
% ARGUMENTS
%  P	Parameters for the gaussian (see gauss)
%  X,Y	The data.  Y(X)
%
% RETURNS
%  The error
%
% NOTES
%  Not usually called directly.  Instead, used by gauss_fit
%
% REQUIRES
%  gauss
%
% SEE ALSO
%  gauss_fit

% AUTHOR: Eric Tittley
%
% HISTORY
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

Yfit=gauss(P(1),P(2),X,P(3));
err=sum( sqrt( (Yfit-Y).^2 ));
