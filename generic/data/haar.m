function coef=haar(vector)
% haar: Haar transform
%
% coef=harr(vector)
%
% ARGUMENTS
%  vector	Data to transform
%
% RETURNS
%  The haar transform of the data
%
% NOTES
%
% REQUIRES
%
% SEE ALSO
%  haar_inv

% AUTHOR: Eric Tittley
%
% HISTORY
%  94 09 01 First version
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

if nargin<1 error('Too few arguments passed to haar'); end
if nargin>1 error('Too many arguments passed to haar'); end
n=length(vector);

vector=vector(:);

k=0; flag=1;
while(flag)
 k=k+1;
 if n<=2^k 
  vector=[vector;0*(1&[1:2^k-n]')];
  flag=0; 
 end
end

pow=k;
coef=vector;
for k=1:pow
 avg=[1:2^(pow-k)];
 diff=[1:2^(pow-k)];
 for l=1:2^(pow-k);
  avg(l)=(coef(l*2-1)+coef(l*2))/2;
  diff(l)=(coef(l*2-1)-coef(l*2))/2;
 end
 coef(1:2^(pow-k))=avg;
 coef(2^(pow-k)+1:2^(pow-k+1))=diff;
end
