function distribution=randps(powerspec)
%RANDPS
% function distribution=randps(powerspec)
%
% returns a real random 1-d distibution from a 1-d power spectrum.
% The returned value is a column vector of twice the length as the
% input POWERSPEC minus 2.
%
% Eric Tittley (951108)
%
% See also RAND, RANDN, RANDDIST

powerspec=powerspec(:);
len=length(powerspec);
phase=i*2*pi*rand(size(powerspec));
phase(1)=0;
phase(len)=0;
ideal_long=[powerspec.*exp(phase);powerspec(len-1:-1:2).*exp(-phase(len-1:-1:2))];
distribution=real(ifft(ideal_long));
