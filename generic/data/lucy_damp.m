function reconstructed=lucy_damp(observation,profile,niterations,damping,sigma)
% lucy_damp: Deconvolve a signal using a damped Richardson-Lucy method.
%
% reconstructed=lucy_damp(observation,profile,niterations,damping,sigma)
%
% ARGUMENTS
%  observation	The observed distribution
%  profile	The kernel by which to deconvolve
%  niterations	The number of iterations to go perform.
%  damping	The damping factor. Typically, it should be an EVEN integer 4 or
%         	greater.
%  sigma	A vector of the same length as OBSERVATION giving the noise
%       	levels of the observed values at the various pixels.
%
% RETURNS
%  The deconvolution of observation using a damped Richardson-Lucy method.
%
% NOTES
%  Solves the integral (sum, in discrete mathematics):
%  observation(x)=sum_over_y(reconstructed(y)*profile(x-y))
%  for the distribution, reconstructed.
%
% See L.B.Lucy, Astronomical Journal, V 79, p. 745
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

% Check to see that observation and profile are the same length
n=length(observation);
m=length(profile);
if m~=n error("Vectors observation and profile passed to lucy must be the same length"); end

% Check to see that damping is a positive even integer.
if rem(damping,2) then error("DAMPING must be an even integer, in call to LUCY_DAMP."); end
if damping<0 then error("DAMPING must be positive, in call to LUCY_DAMP."); end

% Find the maximum of the profile.  Note, this assumes that the
% maximum actually falls in the center of a pixel~  If it doesn't,
% a shift equal to the deviation will occur.  However, the
% maximum can occur at _any_ pixel.
profile=[profile,profile,profile];
mid=find(max(profile)==profile);
midm=mid(2);

% A vector the same length as the input vectors, to be used in
% summations.
template=[1:n];

reconstructed=observation;	% Initial guess

for iteration=1:niterations	% START OF MAIN LOOP

% Calculate what would be observed if the calculated distribution
% were convolved with the profile. (eq. 14 in the ref.)  Note that this
% is a convolution, which could be done much faster with Fourier methods!!!
 for k=1:n
  implied_observation(k)=sum(reconstructed.*profile(midm+k-template));
 end

% Calculate the next guess to the distribution
% (eq. 15 in the ref.)  Note: if implied_observation==observation,
% then the sum is only of the profile, which is unity, so no
% correction is made.
% factor=(observation./implied_observation'));  %Undamped
factor=1+(observation-implied_observation)./implied_observation.*exp(-(sigma./(observation-implied_observation)).^damping); %Damped
 for k=1:n
  correction(k)=sum(profile(midm+template-k).*factor;
 end
 reconstructed=reconstructed.*correction';

end				%END OF MAIN LOOP
