function [N,X,Y,I]=hist2d(Vx,Xedges,Vy,Yedges)
% hist2d: 2-d histogram
%
% [N,X,Y,I]=hist2d(Vx,Xedges,Vy,Yedges);
%
% ARGUMENTS
%  Vx		The x-position of the events to bin
%  Xedges	The x-edges of the bins
%  Vy		The y-position of the events to bin
%  Yedges	The y-edges of the bins
%
% RETURNS
%  N	The number in each bin ([NxM] array)
%  X	The centres of the x bin edges
%  Y	The centres of the y bin edges
%  I	A NxM cell array of elements
%
% NOTES
%  Produces an array, N, with the contents being the 2-D histogram
%  of the data in Vx and Vy.
%
%  That is, N(1,1) contains the number of incidents that
%  Xedges(1) < Vx <= Xedges(2) and Yedges(1) < Vy <= Yedges(2)
%  N is size (length(Yedges)-1) x (length(Xedges)-1)
%
%  Vx and Vy must be vectors of the same length.
%  Xedges and Yedges need not be vectors of the same length, but
%  [XY]edges(i) <= [XY]edges(i+1) forall i.
%
%  X and Y are the vectors corresponding to the centres of the bins
%  so X and Y are length length(Xedges)-1
%
% REQUIRES
%
% SEE ALSO
%  hist, histc, bindata_1d, bindata_2d

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 11 10 Set things up to work on columns
%  00 11 13 Put in checks for empty slices
%	Put in check to see that Vx and Vy are the same size.
%	Put in extra usage information and LICENSE
%  00 11 29 Fixed a bug which cropped up if a slice was empty, or had
%	only one element.
%  07 12 17 Regularised comments
%  08 07 10 Added support to return indices for each cell.
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley, 2000-2007
%  The GPL applies to the contents of this file.

% Do we need to collect the indices?
if(nargout == 4)
 FlagIndices=1;
else
 FlagIndices=0;
end

% Things work fastest if they are column vectors
Vx=Vx(:);
Vy=Vy(:);
Xedges=Xedges(:);
Yedges=Yedges(:);

% Check array sizes
[Nx,dummy]=size(Vx);
[Ny,dummy]=size(Vy);
if(Nx~=Ny), error('Vx and Vy must be the same length'), end

len_x = length(Xedges);
len_y = length(Yedges);
N=zeros(len_y-1,len_x-1);
if(FlagIndices)
 I{len_y-1,len_x-1}=[];
end

if(len_x>len_y)
 Vx=Vx';
 Xedges=Xedges';
 for i=1:len_y-1
  slice=find(Yedges(i)<Vy & Vy<=Yedges(i+1));
  if(~ isempty(slice))
   if(FlagIndices)
    % Need the indices
    [dummy,bins]=histc(Vx(slice),Xedges);
    for j=1:len_x-1
     I{i,j}=slice(bins==j);
    end
   else
    % Don't need the indices
    dummy=histc(Vx(slice),Xedges);
   end
   N(i,:)=dummy(1:end-1);
  end
 end
else
 for i=1:len_x-1
  slice=find(Xedges(i)<Vx & Vx<=Xedges(i+1));
  if(~ isempty(slice)) 
   if(FlagIndices)
    % Need the indices
    [dummy,bins]=histc(Vy(slice),Yedges);
    for j=1:len_y-1
     I{j,i}=slice(bins==j);
    end
   else
    % Don't need the indices
    dummy=histc(Vy(slice),Yedges);
   end
   if(length(slice)<=1) % then dummy will end up being a column vector
    dummy=dummy';
   end
   N(:,i)=dummy(1:end-1);
  end
 end
end

X=(Xedges(1:end-1)+Xedges(2:end))/2;
Y=(Yedges(1:end-1)+Yedges(2:end))/2;
