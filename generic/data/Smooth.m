function img=Smooth(img_o,radius,method)
% Smooth: Smooth an image or array (1, 2, or 3-D)
%
% img=Smooth(img_o,radius,method)
%
% ARGUMENTS
%  img_o	The input image to be smoothed
%  radius	The radius, in pixels, over which to smooth
%  method	A string indicating which method to use which may be one of:
%   Method:			 Operates in:	 Radius:
%   'blackman'  		 Frequency space tau parameter
%   'gaussian'  		 Real space	 HWHM
%   'hamming'			 Frequency space tau parameter
%   'hann'			 Frequency space tau parameter
%   'high-frequency cut-off'	 Frequency space cut-off in pixels
%   'lanczos'			 Frequency space tau parameter
%   'low-frequency cut-off'	 Frequency space cut-off in pixels
%   'tophat'			 real space	 radius
%   The method may be selected by the first unique string that starts the label
%
% RETURNS
%  The smoothed data
%
% NOTES
%  Smoothing works for 1, 2, or 3-D arrays.
%  For 3-D arrays, the dimensions must be square.
%
%  Matlab has it's own smoothing routine, smooth3, for smoothing
%  3-D arrays.  It is both slower, and more limitted in functionality
%  since it can only do box-car and gaussian smoothing.
%  Something it can do is convolve with a non-spherically-symmetric
%  kernel.
%
%  3-D smoothing doesn't work right if the array isn't square
%
% SEE ALSO:
%  smooth (From Matlab's curvefit toolbox)
%  smooth3 (Matlab's specgraph function)
%  fft2, fft3
%  SmoothBelowThreshold

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 09 14 First Version by Eric Tittley (see copyright at end of file)
%           Many of the window definitions were taken from:
%           http://www.cg.tuwien.ac.at/studentwork/CESCG99/TTheussl/node4.html
%           (Thomas Theussl@cg.tuwien.ac.at)
%
% 01 02 01 Corrected the routine for a bug that led to a (+1,+1) shift in the
% 	smoothed image.
%	Re-wrote the section to find the distance mask.  This runs MUCH faster
%	now, and was the bottleneck for large images.
%	Changed the 'gaussian' routine to handle radius==0.
%
% 01 10 30 Moved shift of mask by -size[xy]/2 out of sub-cases.
%	Implemented 3-D smoothing. (requires fft3)
%
% 07 12 17 Regularised comments.
%
% COMPATIBILITY: Matlab, Octave
%
% LICENCE
%  Copyright (C) 2000  Eric Tittley
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
%
% BUGS
%  3-D smoothing doesn't work right if the array isn't square

if(nargin~=3)
 error('syntax: img=Smooth(img_o,radius,method)')
end

NDim=ndims(img_o);

if(NDim>3)
 error('Cannot deal with arrays with more than 3 dimensions');
end

if(NDim==2)

 [sizey,sizex]=size(img_o);

 % Find the distance from the centre
 %mask=0*img_o;
 %for i=1:sizey
 % for j=1:sizex
 %  mask(i,j)=sqrt( (i-sizey/2)^2 + (j-sizex/2)^2 );
 % end
 %end
 % shift the centre from (1,1) to (0,0)
 % This prevents a general shift of (+1,+1) in the final smoothed image
 %mask=shift(shift(mask,1,1),1,2);
 % The following does the previous, but much faster.
 %   (0.44 s vs 29 s for 1000x1000)
 y_cord=(([0:sizey-1]-sizey/2).^2)'*ones(1,sizex);
 x_cord=ones(sizey,1)*([0:sizex-1]-sizex/2).^2;
 mask=sqrt( y_cord + x_cord );

 % Shift the mask so the zero is at the corners.
 mask=shift(mask,-sizey/2,1);
 mask=shift(mask,-sizex/2,2);

else % NDim==3

 % Note the unintuitive order of the indices: y,x,z
 % I use this since I'm use to thinking of (row,column,depth)
 [sizey,sizex,sizez]=size(img_o);

 % Create a distance mask
 y_cord=(([0:sizey-1]-sizey/2).^2)'*ones(1,sizex);
 x_cord=ones(sizey,1)*([0:sizex-1]-sizex/2).^2;
 z_cord=ones(sizey,sizex,sizez);
 for i=1:sizez
  z_cord(:,:,i)=z_cord(:,:,i)*(i-1-sizez/2);
 end
 z_cord=z_cord.^2;
 x_cord=duplicate_in_depth(x_cord,sizez);
 y_cord=duplicate_in_depth(y_cord,sizez);
 
 % Mask will be an array of distances from the centre of the box array.
 mask=sqrt( y_cord + x_cord + z_cord );

 % Shift the mask so the zero is at the corners.
 mask=shift(mask,-sizey/2,1);
 mask=shift(mask,-sizex/2,2);
 mask=shift(mask,-sizez/2,3);

end % if NDim==2, else

if(length(method)<1), error('Method abbr. empty'), end
switch lower(method(1))

 case 'b' % Blackman
  mask_temp=0.42+0.5*cos(pi*mask./radius)+0.08*cos(2*pi*mask./radius);
  mask_mask=mask<radius;
  mask=mask_temp.*mask_mask;
  if(NDim==2)
   img=real( ifft2( fft2(img_o).* mask ) );
  else
   img=real( ifft3( fft3(img_o).* mask ) );
  end

 case 'g' % Gaussian smoothing
  if(radius>0)
   mask=gauss(1,radius,mask,0);
  else
   mask=mask*0+1;
  end
  mask=mask/sum(sum(sum(mask)));

  if(NDim==2)
   img=real( ifft2( fft2(img_o).* fft2(mask) ) );
  else
   img=real( ifft3( fft3(img_o).* fft3(mask) ) );
  end

 case 'h' % hamming, hann, or high-frequency cut-off
  if(length(method)<2), error('Method abbr. not unique'), end
  switch lower(method(2))
   case 'a' %hamming or hann
    if(length(method)<3), error('Method abbr. not unique'), end
    switch lower(method(3))
     case 'm' % hamming
      mask_temp=0.5*(1+cos(pi*mask./radius));
     case 'n' % hann
      mask_temp=0.54+0.46*cos(pi*mask./radius);
     otherwise
      error('Unknown Method')
    end % Switch ham or han
    % if either ham or han, do this.
    mask_mask=mask<radius;
    mask=mask_temp.*mask_mask;
   case 'i' % high-frequency cut-off
    mask=mask>radius;
   otherwise
    error('Unknown Method')
  end % Switch 'ha' or 'hi'
  % If either 'ha' or 'hi', we will finish up with this.
  if(NDim==2)
   img=real( ifft2( fft2(img_o).* mask ) );
  else
   img=real( ifft3( fft3(img_o).* mask ) );
  end

 case 'l' % Lanczos or low-frequency cut-off
  if(length(method)<2), error('Method abbr. not unique'), end
  switch lower(method(2))
   case 'a' % Lanczos
    mask_temp=sin(pi*mask./radius)./(pi*mask./radius);
    mask_mask=mask<radius;
    mask=mask_temp.*mask_mask;
   case 'o' % low-frequency cut-off
    mask=mask<=radius;
   otherwise
    error('Unknown Method')
  end % Switch 'la' or 'lo'
  % if either Lanczos or low-frequency cut-off, then we finish up with this
  if(NDim==2)
   img=real( ifft2( fft2(img_o).* mask ) );
  else
   img=real( ifft3( fft3(img_o).* mask ) );
  end

 case 't' % tophat
  mask=mask<=radius;
  mask=mask/sum(sum(sum(mask)));
  if(NDim==2)
   img=real( ifft2( fft2(img_o).* fft2(mask) ) );
  else
   img=real( ifft3( fft3(img_o).* fft3(mask) ) );
  end

 otherwise
  error('Unknown Method')

end % end SWITCH
 
 

% END OF smooth
% ------------------------------------------------------------------------- 


function A_out=duplicate_in_depth(A,N);
% take the LxM array, A, and duplicate it N times to form a LxMxN 3-D array

if(N<1)
 error('N must be >=1')
end

% We'll double the array N_doubles times, and then add on the remainder.
N_doubles=floor(log(N)/log(2));
Remainder=N-2^N_doubles;

A_out=A;
for i=1:N_doubles
 A_out=cat(3,A_out,A_out);
end
if(Remainder>0)
 A_out=cat(3,A_out,A_out(:,:,1:Remainder));
end


