function [Coef,Fit]=fit_surface(Z,mask,N)
% fit_surface: Fit a N polynomial surface to the values of Z
%
% [Coef,Fit]=fit_surface(Z,mask,N)
%
% ARGUMENTS
%  Z	The array of values.  On a regular grid.
%  mask	A mask of the same dimensions as Z, from which to sample Z for the fit.
%	1 => sample, 0 => ignore
%  N	The order of the polynomial to fit
%
% RETURNS
%  Coef	The best-fit coefficients
%  Fit	The best-fit
%
% SEE ALSO
%  fitellipse fit_ellipse fit_line fit_plane

% AUTHOR: Eric Tittley
%
% HISTORY
%  021125 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

switch N
 case 1
  Coef=mean(Z(mask==1));
  Fit=mask*Coef;
 case 2
  NX=size(Z,2);
  NY=size(Z,1);
  X=ones(NY,1)*[1:NX];
  Y=[1:NY]'*ones(1,NX);
  list=find(mask);
  x=X(list)';
  y=Y(list)';
  z=Z(list)';
  A=[ones(1,length(x));x;y];
  Coef=z/A;
  Fit=Coef(1)+Coef(2)*X+Coef(3)*Y;
 otherwise
  error(['Unable to fit a polynomial of rank ',num2str(N)])
end % switch N
