% clip: Clip a data set to exclude all data a number of sigma from the mean.
%
% [dataout,Index]=clip(data,sigmas)
%
% ARGUMENTS
%  data		a vector of data
%  sigmas	the number of sigmas to use in the clipping
%
% RETURNS
%  dataout	the elements in data that survive the clipping
%  Index	the index of elements that survived.  data(Index)==dataout
%
% NOTES
%  Clipping is an iterative procedure.
%   1) find a mean and std
%   2) elliminate all those outside the bounds
%   3) if there was any change to the data, goto 1)
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  02-02-11 First version
%  03-05-16 Add comment.
%	Return an Index.
%  05 03 07 Catch and warn if we've clipped to zero length (i.e. elliminated
%	all the data).
%
% COMPATIBILITY: Matlab, Octave

function [dataout,Index]=clip(data,sigmas)

Index=[1:length(data)];
Nelems_o=length(data)+1;
while(length(data)<Nelems_o & length(data)>0)
 M=mean(data);
 S=std(data);
 good=find(data>(M-sigmas*S) & data<(M+sigmas*S));
 Nelems_o=length(data);
 data=data(good);
 Index=Index(good);
end
if(length(data)==0)
 disp('WARNING: clip: clipped data set to zero length')
end

dataout=data;
