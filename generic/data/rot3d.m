% rot3d Rotate (x,y) or (x,y,z) positions.
%
% r_new=rot3d(r,theta,ax,r0);
%
% ARGUMENTS
%  r		List of positions (x,y) or (x,y,z) to rotate.
%  theta	The angle through which to rotate. (radians)
%  ax		The axis about which to rotate. (1=x, 2=y, 3=z)
%  r0		The point about which to rotate. (x,y) or (x,y,z)
%
% RETURNS
%  The list of points in r, rotated by theta around r0
%
% NOTES
%  If size(r)=[N,2], then the rotation axis must be 3, for z-axis.
%
%  If size(r)=[2,N] or [3,N] and N~= 2 or 3, then r is transposed
%  before processing.

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 06 19 Mature code
%  01 06 19
%   Added support for (x,y) data.
%   Added checks before transposing r.
%  05 02 15 Minor change to an error message and comments.
%
% COMPATIBILITY: Matlab, Octave

function r_new=rot3d(r,theta,ax,r0)

[mrows,ncols]=size(r);
transposeflag=0;
if( (mrows==3 | mrows==2) & ~(ncols==3 | ncols==2) ), r=r'; transposeflag=1; end
if(mrows==2), r=r'; transposeflag=1; end

[mrows,ncols]=size(r);
if(ncols==2)
 xyonlyflag=1;
 if(length(r0)~=2)
  error('r is for xy data, but r0 is not')
 end
else
 xyonlyflag=0;
 if(length(r0)~=3)
  error('r is for xyz data, but r0 is not')
 end
end

if(xyonlyflag)
 r_temp=[r(:,1)-r0(1),r(:,2)-r0(2)];
 r_new=0*r;
 r_new(:,1)=     r_temp(:,1)*cos(theta)+r_temp(:,2)*sin(theta);
 r_new(:,2)=-1.0*r_temp(:,1)*sin(theta)+r_temp(:,2)*cos(theta);
 r_new=[r_new(:,1)+r0(1),r_new(:,2)+r0(2)];
else

 r_temp=[r(:,1)-r0(1),r(:,2)-r0(2),r(:,3)-r0(3)];
 r_new=0*r;
 switch ax
  case 1,
   r_new(:,2)=     r_temp(:,2)*cos(theta)+r_temp(:,3)*sin(theta);
   r_new(:,3)=-1.0*r_temp(:,2)*sin(theta)+r_temp(:,3)*cos(theta);
   r_new(:,1)=r_temp(:,1);
  case 2,
   r_new(:,3)=     r_temp(:,3)*cos(theta)+r_temp(:,1)*sin(theta);
   r_new(:,1)=-1.0*r_temp(:,3)*sin(theta)+r_temp(:,1)*cos(theta);
   r_new(:,2)=r_temp(:,2);
  case 3,
   r_new(:,1)=     r_temp(:,1)*cos(theta)+r_temp(:,2)*sin(theta);
   r_new(:,2)=-1.0*r_temp(:,1)*sin(theta)+r_temp(:,2)*cos(theta);
   r_new(:,3)=r_temp(:,3);
  otherwise,
   error('ROTATE3D: ax must be one of 1, 2, or 3')
 end
 r_new=[r_new(:,1)+r0(1),r_new(:,2)+r0(2),r_new(:,3)+r0(3)];

end % if xy, else

if(transposeflag==1)
 r_new=r_new';
end
