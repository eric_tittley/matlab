function Cd=drag_coef(Re)
% drag_coef: The coefficient of drag, given the Reynold's number
%
% Cd=drag_coef(Re);
%
% ARGUMENTS
%  Re	Reynold's number
%
% RETURNS
%  The coefficient of drag
%
% NOTES
%  From S. W. Churchill, "Viscous Flows", Butterworths, 1988
%
% SEE ALSO
%  drag_force dynamic_viscosity impact_parameter Reffective Reynolds_number

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31: First Version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

if(Re<1e4)
 Cd = 24/Re + 3.63/Re^0.5 - (4.83e-3*Re^0.5)/(1+3e-6*Re^1.5) + 0.49;
elseif(Re<2e5)
 Cd = 0.5;
else
 Cd = 0.2;
end
