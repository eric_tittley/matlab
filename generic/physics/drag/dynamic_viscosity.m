function eta=dynamic_viscosity(T,density,A,Z)
% dynamic_viscosity: Dynamic viscosity of a medium
%
% eta=dynamic_viscosity(T,density,A);   % neutral gas
% eta=dynamic_viscosity(T,density,A,Z); % ionized gas
%
% ARGUMENTS
%  T		Temperature [K]
%  density	Density [g cm^-3]
%  A		Atomic mass of the ion [amu]
%  Z		Charge per ion [optional] [esu]
%
% RETURNS
%  The dynamical viscosity of the medium [g cm^-1 s]
%
% REQUIRES
%  impact_parameter
%
% SEE ALSO
%  drag_coef drag_force impact_parameter Reffective Reynolds_number

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31: First version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

R   = 8.314510e7; 	% erg K^-1 mol^-1
k   = 1.380658e-16;	% erg K^-1
N_A = 6.0221367e23;	% mol^-1
amu = 1.6605402e-24;	% g
e   = 4.8032068e-10;	% esu
a_o = 1e-9;		% cm

%mu(1) = 2/(3*pi*d_eff^2) * sqrt( (gam * amu * R * T)/(N_A *pi) );

if(nargin==3) %neutral gas
 eta=(3*k*T*A*amu)^0.5 / (pi * a_o^2);
elseif(nargin==4) %ionized gas
 n_e=5*density/(7*amu);
 Lambda=impact_parameter(Z,Z,T,n_e);
 eta = (6*amu)^0.5 / (4 * pi^0.5) * A^0.5 * (k * T)^(5/2) / ( (Z*e)^4 * log(Lambda) );
else
 error('need 3 or 4 arguments')
end
