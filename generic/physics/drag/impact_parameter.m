function Lambda=impact_parameter(Z1,Z2,T,n_e)
% impact_parameter: Debye radius divided by the true impact parameter for an
%                   ionized gas.
%
% Lambda=impact_parameter(Z1,Z2,T,n_e);
%
% ARGUMENTS
%  Z1, Z2	Charges on the ions [esu] [eg: 1, 2]
%  T		Plasma temperature [K]
%  n_e		Electron number density [cm^-3]
%
% RETURNS
%  The impact parameter, Lambda, which is the Debye radius divided by the
%  true impact parameter, b. (the ratio of the largest to smallest impact
%  parameters). This is for an ionized gas.
%
% NOTES
%  K. R. Lang, 'Astrophysical Formulae 2nd edition', Springer-Verlag 1980
%
% SEE ALSO
%  drag_coef drag_force dynamic_viscosity Reffective Reynolds_number

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31 First version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

e=4.8032068e-10; % esu
k=1.380658e-16;	 % erg/K

Lambda =  3/(2*Z1*Z2*e^3) * (k*T)^(3/2) / (pi * n_e)^0.5 ;
