function F=drag_force(V,R,rho,T,A,Z)
% drag_force: Force of drag on a sphere
%
% F=drag_force(V,R,rho,T,A,[Z]);
%
% ARGUMENTS
%  V		Velocity of the sphere wrt the medium [cm/s]
%  R		Diameter of the sphere [cm]
%  rho		Density of the medium [g/cm^3]
%  T		Temperature [K]
%  A		Atomic mass of the ion [amu]
%  Z		Charge per ion [optional] [esu]
%
% REQUIRES
%  drag_coef Reynolds_number
%
% SEE ALSO
%  drag_coef dynamic_viscosity impact_parameter Reffective Reynolds_number
% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31: First Version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

% The force of drag, F, for a sphere of radius R
% moving through a gas at velocity, V.
% The gas has a density of rho, temperature of T,
% a mean atomic number of A, and a (if ionized)
% a mean charge of Z

if(nargin==5) % neutral gas
 Re=Reynolds_number(R,V,rho,T,A);
elseif(nargin==6)
 Re=Reynolds_number(R,V,rho,T,A,Z);
end
Cd=drag_coef(Re);
F = 0.5*Cd*rho*V^2*pi*R^2;
