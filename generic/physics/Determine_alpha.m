function alpha = Determine_alpha(flux_l,flux_h,freq_l,freq_h);
% Determine_alpha: The spectral index given two radio fluxes and their frequencies.
%
% Determines the spectral index given two radio fluxes (in mJy)
% and their frequencies in GHz.
%
% syntax alpha = Determine_alpha(flux_l,flux_h,freq_l,freq_h);
%
% ARGUMENTS
%  flux_l	The lower-frequency flux (mJy)
%  flux_h	The higher-frequency flux (mJy)
%  freq_l	The lower frequency (GHz)
%  freq_h	The higher frequency (GHz)
%
% RETURNS
%  alpha	Spectral index.
%
% NOTES
%  The order of the fluxes and frequencies is not important since the program
%  sorts them assuming the lowest frequency has the highest flux.
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Danny Hudson
%
% HISTORY
%  020801 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

% Match frequency with flux
temp(1) = freq_l;
temp(2) = freq_h;

if temp(1) > temp(2)
 freq_l = temp(2);
 freq_h = temp(1);
 temp(1) = flux_l;
 flux_l = flux_h;
 flux_h = temp(1);
end

clear temp

% speed of light
c = 2.99792458e10; % cm/s

% Jansky
Jy = 1e-23; %ergs cm^-2 s-1 Hz-1 
mJy = Jy*1e-3;

% planck's constant
h = 4.13570111e-15; %eV*s

%SNorm = radio*mJy*(1.4e9)^(alpha-1);
%S = SNorm*(5e9)^(-(alpha-1))*A;

alpha = (log10(flux_h*mJy) - log10(flux_l*mJy))/(log10(freq_h*1e9) - log10(freq_l*1e9));

%fprintf('\n Alpha is equal to %g \n \n',alpha);
