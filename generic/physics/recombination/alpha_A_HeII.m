function alpha=alpha_A_HeII(T)
% alpha_A_HeII: HeII-->HeI recombination coefficient:   Case A
%
% alpha=alpha_A_HeII(T)
%
% m^3 s^-1

alpha = zeros(length(T),5);
i=0;

% BS60: Burgess & Seaton 1960 MNRAS 121, 471
% A table with 2 (maybe three) points.
% The data was fit by Black81, which was used by Cen92 which was used by AAZN97
% I fit the two points as such:
i=i+1;
alpha(:,i)=2.30e-16 .* T.^-0.6817; % m^3 s^-1

% Black81: Black 1981 MNRAS 197, 553
% Fits the BS60 data.  Used by Cen92 which was used by AAZN97
i=i+1;
alpha(:,i)=1.50e-16 .* T.^-0.6353; % m^3 s^-1

% VF96: Verner & Ferland 1996, ApJS 103, 467
% Error < 3% for T=3 to 10^6 K
i=i+1;
alpha(:,i)=3.294e-17 ./ ( sqrt(T/15.54).*(1+sqrt(T/15.54)).^0.309 ...
                         .*(1+sqrt(T/3.676e7)).^1.6910); % m^3 s^-1
alpha(T<3 | T>1e6,i)=NaN;

% AP73: Aldrovandi & Pequignot 1973 A&A 25, 137
% Used by Sherman79: Sherman 1979 ApJ 232, 1
i=i+1;
alpha(:,i)=2.10e-16 .* T.^-0.672; % m^3 s^-1
alpha(T<100 | T>1e4,i)=NaN;

% HS98: Hummer & Storey 1998, MNRAS 297, 1073
logT_HS=[1.0:0.2:4.4];
alpha_1_HS=[1.569 1.569 1.569 1.569 1.569 1.569 1.569 1.569 1.570 ...
            1.571 1.572 1.573 1.576 1.580 1.586 1.595 1.608 1.626] * 1e-17;
alpha_B_HS=[9.284 8.847 8.403 7.952 7.499 7.044 6.589 6.136 5.685 ...
            5.238 4.797 4.364 3.940 3.528 3.132 2.755 2.401 2.073] * 1e-17;
alpha_A_HS = alpha_1_HS + alpha_B_HS;
i=i+1;
alpha(:,i)= interp1(logT_HS,alpha_A_HS,log10(T),'spline',NaN) ./sqrt(T); % m^3 s^-1

% ET: Fit to HS98
i=i+1;
alpha(:,i)=1.27e-16 .* T.^-0.5526 .* exp(-0.006875*log(T).^2);
alpha(T>10^4.4,i)=NaN;
