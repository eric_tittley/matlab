function alpha=alpha_He1(T)
% alpha_He1: HeII-->HeI recombination coefficient
% 
% alpha=alpha_He1(T)
% 
% m^3 s^-1

a_0 = (T / 15.54).^0.5;
a_1 = (T / 3.676e7).^0.5;

% He2-->He1 recombination coefficient, CGS units
% This is the radiative term
k_2r = (3.294e-11) ./ (a_0 .* (1 + a_0).^0.309 .* (1 + a_1).^1.691);

% This is the dielectronic term
k_2d =   1.9e-3 * (1 + (0.3 * exp(-94000 ./ T))) ...
      .* exp(-470000 ./ T) .* T.^(-1.5);

k_2 = k_2r + k_2d;

% Convert to SI units, m^3 s^-1
alpha = k_2 / 1e6;
