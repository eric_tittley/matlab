function alpha=alpha_A_HII(T)
% alpha_A_HII: HII-->HI recombination coefficient:     Case A
%
% alpha=alpha_A_HII(T)
%
% m^3 s^-1
%
% Note, alpha_A_HeIII(T) = Z*alpha_A_HII(T/Z^2) where Z=2 for He

alpha = zeros(length(T),7);
i=0;

% Seaton59: Seaton, MNRAS 119, 81 (1959)
i=i+1;
alpha(:,i)=2.065e-17 ./ sqrt(T) .* (6.414 - log(T)/2. + 8.68e-3 * T.^(1./3.));
alpha(T>1e5,i)=NaN;

% HG97: Hui & Gnedin 1997 MNRAS 292, 27
i=i+1;
alpha(:,i)=2.337e-11 * T.^-1.503 .* ( 1 + 521.6*T.^-0.470 ).^-1.923;

% Cen92: Cen 1992 ApJS 78, 341
i=i+1;
alpha(:,i)=8.40e-17 ./ sqrt(T) .* (T/1e3).^(-0.2) ./ (1+(T/1e6).^0.7);

% Sherman79: Sherman 1979 ApJ 232, 1
i=i+1;
%alpha(:,i)=2.08e-17 ./ sqrt(T) .* (6.84 - log(T)/2. + 5.21e-7*T);
alpha(:,i)=1.04e-17 ./ sqrt(T) .* (13.7 - log(T) + 1.04e-6*T);
alpha(T>3.2e5,i)=NaN;

% AAZN97: Abel, Anninos, Zheng, Norman 1997 New Astronomy 2, 181
lT = log(T/11604.45);
i=i+1;
alpha(:,i)=exp( -28.6130338 - 0.72411256 *lT - 2.02604473e-2 *lT.^2 ...
                - 2.38086188e-3 *lT.^3 - 3.21260521e-4 *lT.^4 ...
		- 1.42150291e-5 *lT.^5 + 4.98910892e-6 *lT.^6 ...
		+ 5.75561414e-7 *lT.^7 - 1.85676704e-8 *lT.^8 ...
		- 3.07113524e-9 *lT.^9 ) * 1e-6;

% Spitzer78:
T_Spitz=[31.3 62.5 125 250 500 1000 2000 4000 8000 16000 32000 64000 128000 ...
         256000 512000 1e6 1e7 1e8];
phi=[4.68 4.36 4.04 3.71 3.38 3.05 2.73 2.40 2.09 1.79 1.50 1.22 .96 .73 .52 ...
     .36 .074 .011];
phi_interp=exp(interp1(log(T_Spitz),log(phi),log(T),'spline',NaN));
i=i+1;
alpha(:,i)=2.066e-17./sqrt(T).*phi_interp;

% Ferland92
logT_Fer=[0.5:0.5:10];
alpha1_Fer=[9.258e-12 5.206e-12 2.927e-12 1.646e-12 9.246e-13 5.184e-13 ...
            2.890e-13 1.582e-13 8.255e-14 3.882e-14 1.545e-14 5.058e-15 ...
	    1.383e-15 3.276e-16 7.006e-17 1.398e-17 2.665e-18 4.940e-19 ...
	    9.001e-20 1.623e-20];
alphaB_Fer=[5.758e-11 2.909e-11 1.440e-11 6.971e-12 3.282e-12 1.489e-12 ...
            6.430e-13 2.588e-13 9.456e-14 3.069e-14 8.793e-15 2.245e-15 ...
	    5.190e-16 1.107e-16 2.221e-17 4.267e-18 7.960e-19 1.457e-19 ...
	    2.636e-20 4.737e-21];
alphaA_Fer=(alpha1_Fer + alphaB_Fer)*1e-6; % m^3 s^-1
i=i+1;
alpha(:,i)= exp(interp1(logT_Fer,log(alphaA_Fer),log10(T),'spline',NaN));

% Hummer94: Hummer 1994 MNRAS 268, 109
logT_Hum=[1:0.2:7];
alpha1_Hum=[1.646 1.646 1.646 1.646 1.646 1.646 1.645 1.645 1.644 1.642 ...
            1.640 1.636 1.629 1.620 1.605 1.582 1.548 1.499 1.431 1.341 ...
	    1.227 1.093 .9454 .7920 .6427 .5058 .3866 .2877 .2089 .1485 ...
	    .1036 ] * 1e-17;
alphaB_Hum=[9.283 8.823 8.361 7.898 7.435 6.973 6.512 6.054 5.599 5.147 ...
            4.700 4.258 3.823 3.397 2.983 2.584 2.204 1.847 1.520 1.226 ...
	    .9696 .7514 .5710 .4257 .3117 .2244 .1590 .1110 .07642 .05199 ...
	    .03498] * 1e-17;
alphaA_Hum=alpha1_Hum+alphaB_Hum;
i=i+1;
alpha(:,i)= interp1(logT_Hum,alphaA_Hum,log10(T),'spline',NaN) ./sqrt(T);

