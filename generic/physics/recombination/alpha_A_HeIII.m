function alpha=alpha_A_HeIII(T)
% alpha_A_HeIII: HeIII-->HeII recombination coefficient: Case A
%
% alpha=alpha_A_HeIII(T)
%
% m^3 s^-1

% alpha_A_HeIII(T) = Z*alpha_A_HII(T/Z^2) where Z=2 for He
Z=2;
alpha = Z*alpha_A_HII(T/Z^2);

