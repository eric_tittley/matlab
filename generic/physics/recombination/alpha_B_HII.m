function alpha=alpha_B_HII(T)
% alpha_B_HII: HII-->HI recombination coefficient:     Case B
%
% alpha=alpha_B_HII(T)
%
% RETURNS
%  alpha [m^3 s^-1] for the fits:
%	Seaton 1959 MNRAS 119, 81
%	Hui & Gnedin 1997 MNRAS 292, 27
%	Spitzer 1978
%	Ferland 1992
%	Hummer 1994 MNRAS 268, 109
%
% Note, alpha_B_HeIII(T) = Z*alpha_B_HII(T/Z^2) where Z=2 for He

alpha = zeros(length(T),5);
i=0;

% AAM, presumably from 
% Seaton59: Seaton, MNRAS 119, 81 (1959)
i=i+1;
alpha(:,i)=2.065e-17 ./ sqrt(T) .* (5.62 - log(T)/2. + 8.68e-3*T.^(1./3.) + ...
                                     2.01e-5*T.^0.8);
alpha(T>1e5,i)=NaN;

% My fit to:
% Seaton59: Seaton, MNRAS 119, 81 (1959)
%i=i+1;
%alpha(:,i)=2.065e-17 ./ sqrt(T) .* (5.64 - log(T)/2. + 8.68e-3*T.^(1./3.) + ...
%                                     3e-6*T);
%alpha(T>1e5,i)=NaN;

% HG97: Hui & Gnedin 1997 MNRAS 292, 27
% Fit to Ferland92
i=i+1;
alpha(:,i)=4.881e-12 * T.^-1.5 .* ( 1 + 114.8*T.^-0.407 ).^-2.242; % m^3 s^-1
%L=2*157807./T;
%alpha(:,i)=2.753e-20*L.^1.5.*(1+(L/2.74).^0.407).^-2.242;

% Spitzer78:
T_Spitz=[31.3 62.5 125 250 500 1000 2000 4000 8000 16000 32000 64000];
phi=[3.89 3.57 3.25 2.92 2.60 2.27 1.96 1.64 1.34 1.06 .80 .59];
phi_interp=exp(interp1(log(T_Spitz),log(phi),log(T),'spline',NaN));
i=i+1;
alpha(:,i)=2.066e-17./sqrt(T).*phi_interp;

% Ferland92
logT_Fer=[0.5:0.5:10];
alphaB_Fer=[5.758e-11 2.909e-11 1.440e-11 6.971e-12 3.282e-12 1.489e-12 ...
            6.430e-13 2.588e-13 9.456e-14 3.069e-14 8.793e-15 2.245e-15 ...
	    5.190e-16 1.107e-16 2.221e-17 4.267e-18 7.960e-19 1.457e-19 ...
	    2.636e-20 4.737e-21]*1e-6; % m^3 s^-1
i=i+1;
alpha(:,i)= exp(interp1(logT_Fer,log(alphaB_Fer),log10(T),'spline',NaN));

% Hummer94: Hummer 1994 MNRAS 268, 109
logT_Hum=[1:0.2:7];
alphaB_Hum=[9.283 8.823 8.361 7.898 7.435 6.973 6.512 6.054 5.599 5.147 ...
            4.700 4.258 3.823 3.397 2.983 2.584 2.204 1.847 1.520 1.226 ...
	    .9696 .7514 .5710 .4257 .3117 .2244 .1590 .1110 .07642 .05199 ...
	    .03498] * 1e-17;
i=i+1;
alpha(:,i)= interp1(logT_Hum,alphaB_Hum,log10(T),'spline',NaN) ./sqrt(T);
