function alpha=alpha_He2(T)
% alpha_He2: HeIII-->HeII recombination coefficient
%
% alpha=alpha_He2(T)
% 
% Seaton, MNRAS 119, 81 (1959)
% m^3 s^-1

alpha = 8.260e-17 * T.^(-0.5) .* (7.107 - log(T)/2. + 5.47e-3 * T.^(1./3.));
