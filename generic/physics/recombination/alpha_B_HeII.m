function alpha=alpha_B_HeII(T)
% alpha_B_HeII: HeII-->HeI recombination coefficient:     Case B
%
% alpha=alpha_B_HeII(T)
%
% m^3 s^-1

i=0;

% BS60: Burgess & Seaton 1960 MNRAS 121, 471
% A table with 2 (maybe three) points.
% I fit the two points as such:
i=i+1;
alpha(:,i)=5.28e-16 .* T.^-0.8219; % m^3 s^-1
alpha(T>1e5,i)=NaN;

% AAM
% VF96 - alpha_1
%Range: T=3 to 10^6 K
i=i+1;
alpha(:,i)=3.294e-17 ./ ( sqrt(T/15.54).*(1+sqrt(T/15.54)).^0.309 ...
                         .*(1+sqrt(T/3.676e7)).^1.6910) ...
           - 1.32e-17*T.^-0.48; % m^3 s^-1
alpha(T<3 | T>1e6,i)=NaN;

% HG97:
% A fit to BS60
i=i+1;
alpha(:,i)=5.28e-16 .* T.^-0.8219; % m^3 s^-1
alpha(T<5e3 | T>5e5,i)=NaN;

% HS98: Hummer & Storey 1998, MNRAS 297, 1073
logT_HS=[1.0:0.2:4.4];
alpha_B_HS=[9.284 8.847 8.403 7.952 7.499 7.044 6.589 6.136 5.685 ...
            5.238 4.797 4.364 3.940 3.528 3.132 2.755 2.401 2.073] * 1e-17;
i=i+1;
alpha(:,i)= interp1(logT_HS,alpha_B_HS,log10(T),'spline',NaN) ./sqrt(T); % m^3 s^-1

% ET: Fit to HS98
i=i+1;
alpha(:,i)=1.007e-16 .* T.^-0.5095 .* exp(-0.01429*log(T).^2);
alpha(T>10^4.4,i)=NaN;
