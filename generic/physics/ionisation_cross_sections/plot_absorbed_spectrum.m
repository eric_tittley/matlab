nu=10.^[15:0.001:16.9];
sigma=ionisation_cross_section(nu);
F=1e15*nu.^-1;
absorb=F.*(1-exp(-(1e21*[4 1 1])*sigma'));
loglog(nu,F-absorb,'k')
FontSize=12;
FontName='Arial';
xlabel('\nu','fontsize',FontSize,'fontname',FontName)
ylabel('Relative flux','fontsize',FontSize,'fontname',FontName)
nu_T    = [3.282 5.933 13.13]*1e15;
for i=1:3
 line(nu_T(i)*[1 1],[0.01 1])
end
text(nu_T(1),0.8,'\nu_{HI}'  ,'fontsize',FontSize)
text(nu_T(2),0.8,'\nu_{HeI}' ,'fontsize',FontSize)
text(nu_T(3),0.8,'\nu_{HeII}','fontsize',FontSize)

print -deps absorbed_spectrum.eps
print -dpng absorbed_spectrum.png
