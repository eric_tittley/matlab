% matrix: Matrix manipulation
%
% mat2vect	Converts a matrix into a vector. (cf reshape)
%
% RebinCubeToHalf  Decrease the number of cells in a 3D matrix by 8.
%
% shift		Shifts the contents of a matrix in any given dimension.
