function A=shift(A_in,num,dir)
% shift: Shifts the contents of the matrix A_in, num cells in the dimension dir.
%
% A=shift(A_in,num,dir)
%
% ARGUMENTS
%  A_in	The array to shift.
%  num	The distance to shift. Can be a non-integer. Interpolation is linear.
%  dir	The dimension (direction) over which the shift is to be done:
%	1 first dimension (row or y)
%	2 second dimension (column or x)
%	3 third dimenstion (depth or z)
%
% RETURNS
%  An array of the same size as A_in, but with the contents shifted.
%
% Currently implemented for 3 or fewer dimensions.

% AUTHOR: Eric Tittley
%
% HISTORY:
%  00 11 27 Mature code modified to accept non-integer shifts.
%  01 10 12 Implemented 3-dimensional shifts.
%  02 05 08 Changed A=A_in*0 to A=zeros(lengths) (where lengths=size(A_in))
%	This is twice as fast.
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave

if(nargin ~= 3) error('A=shift(A_in,num,dir)'), end

Ndims=ndims(A_in);
if(Ndims>3) error('shift cannot handle more than 3 dimensions'), end
if(dir>Ndims) error('dir greater than the number of dimensions of matrix'); end
if(dir<1) error('dir must be a positive integer greater than 0'); end
if(abs(round(dir))~=dir) error('dir must be a positive integer'); end

lengths=size(A_in);

num=rem(num,lengths(dir));
if(num<0) num=lengths(dir)+num; end

if(round(num)~=num)
 % frac shift
 shift_lo=floor(num);
 shift_hi=ceil(num);
 shift_frac=num-shift_lo;
 A_lo=shift(A_in,shift_lo,dir);
 A_hi=shift(A_in,shift_hi,dir);
 A=(1-shift_frac)*A_lo+shift_frac*A_hi;
else % integer shift, much faster
 % int shift
 if(num~=0)
  if(Ndims<3)
   if(dir==1)
    A=[A_in(end-num+1:end,:);A_in(1:end-num,:)];
   elseif(dir==2)
    A=[A_in(:,end-num+1:end),A_in(:,1:end-num)];
   else
    error('In call to shift: unsupported dimension')
   end
  else % Ndims==3
   A=zeros(lengths);
   if(dir==1)
    A=[A_in(end-num+1:end,:,:);A_in(1:end-num,:,:)];
   elseif(dir==2)
    A=[A_in(:,end-num+1:end,:),A_in(:,1:end-num,:)];
   elseif(dir==3)
    A=cat(3,A_in(:,:,end-num+1:end),A_in(:,:,1:end-num));
   else
    error('In call to shift: unsupported dimension')
   end
  end % if Ndims<=3 else Ndims==3
 else
  A=A_in;
 end % if num~=0 else
end % if fractional shift else integer shift
