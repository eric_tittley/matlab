function vect=mat2vect(mat)
% mat2vect: Converts a matrix into a vector of length n*m where [n*m]=size(mat)
%
% Syntax vect=mat2vect(mat)
%
% ARGUMENTS
%  mat	The matrix to convert
%
% RETURNS
%  vect	The vector representation of the vector.
%
% NOTES
%  The periodic index is the index (n or m) which is the shortest.
%
% REQUIRES
%
% SEE ALSO
%  (:)      Matlab instrinsic which does the best job
%  reshape: Matlab built-in which does a better job

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

[n,m]=size(mat);
vect=[1:n*m];
if(n>m)
 m_temp=m;
 m=n;
 n=m_temp;
 mat=mat';
end
for i=1:n
 vect((i-1)*m+1:i*m)=mat(i,:);
end
 
