% StructCat: Concatenate the elements of structures (and structure arrays).
%
% NewStruct=StructCat(Struct1,Struct2)
%
% Contatenates the fields of structure arrays.
%
% ARGUMENTS
%  Struct1  Input structure with N elements and M fields.
%  Struct2  Input structure with N elements and M fields.
%
% RETURNS
%  NewStruct  A structure array with the same length, N, as Struct1 and Struct2,
%             but with each field the concatenation of the fields in Struct1
%             and Struct2
%
% SEE ALSO
%  Matlab's own structure concatenation routine: [Struct1,Struct2]

% AUTHOR: Eric Tittley
%
% HISTORY
%  110119 First version
%  110223 Made compatible with Matlab.  Basically, use Fields{i} instead of
%       Fields(i){}, which works in Octave but not Matlab.
%
% TODO
%  Only works with vectors, which is all I need at the moment. And it turns all
%  the vectors into column vectors.

function NewStruct=StructCat(Struct1,Struct2)

Fields1=fieldnames(Struct1);
Fields2=fieldnames(Struct2);
NFields1=length(Fields1);
NFields2=length(Fields2);

NStructElements1=length(Struct1);
NStructElements2=length(Struct2);


% Argument checks
if(NFields1 ~= NFields2)
 error('Input structures must have the same number of fields')
end
for i=1:NFields1
 if( ~isfield(Struct2,Fields1{i}) )
  error('Input structures must have the same fields')
 end
end
if(NStructElements1 ~= NStructElements2 )
 error('Input structures must have the same number of elements')
end

% Since the parameters match, don't distinguish.
Fields=Fields1;
NFields=NFields1;
NStructElements=NStructElements1;

for StructElement=1:NStructElements
 % Loop through the fields, concatenating each, assuming they are vectors.
 for field_index=1:NFields
  fieldname=Fields{field_index};
  field1=Struct1(StructElement).(fieldname);
  field2=Struct2(StructElement).(fieldname);
  NewStruct(StructElement).(fieldname)=[field1(:);field2(:)];
 end
end
