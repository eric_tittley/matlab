% struct2arr: Extract an array from a single element of a structure array.
%             Logically equivalent to Struct(:).element(:)
%
% A=struct2arr(Struct,element_label)
%
% ARGUMENTS
%  Struct		The structure array.
%  element_label	String identifying the element.
%
% RETURNS
%  A	Array containing the values of element of all the structures.
%
% Matlab is a bit weak on structure manipulation compared with IDL.
% Suppose D is a vector (length N) of structures which have a field, F
% (length M).
% In IDL, D.F is an NxM array.  Matlab doesn't permit this type of
% referencing.
% The struct2arr routine restores a bit of this functionality.
%
% NOTE: The functionality of this routine might be irrelevant.
%       A=[Struct.element_label]
%       might already do the job.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  080318 First version
%
% TODO
%  Deal with multidimensional Structures and elements.

function A=struct2arr(Struct,element_label)

Dims=size(Struct);
% Strip singleton dimensions
Dims=Dims(Dims ~= 1);
dummy=getfield(Struct, {1}, element_label);
M=length(dummy);
A=zeros([Dims,M]);
% Up to this point, we are generic with any rank of Struct and any rank of
% element.
% The following only works if Struct is rank 1 and the element is rank 1
for i=1:prod(Dims)
 size(A(i,:))
 size(getfield(Struct, {i}, element_label))
 A(i,:)=getfield(Struct, {i}, element_label);
end
