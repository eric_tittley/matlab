% For analysing the hydrostatic state of clusters.
%
% Euler		Integrates the equation of hydrostatic equilibrium for a
%		cluster.
%
% get_cutoffs	Interactively find the outer radii of clusters.
%
% virial_param	The Virial Factor (P/P_grav) at an overdensity radius.
%
% surface_script	Something to do with the surface pressure (the pressure at
%		the cutoff radius.
