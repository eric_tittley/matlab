function [r,P,dPdr,Rho_dPhi_dr,Rho_dPhi,Pres_g,P_pres,Pk]=Euler(data,unitsfile,atime,r_cut)
%[r,P,dPdr,Rho_dPhi_dr,Rho_dPhi,Pres_g,P_pres,Pk]=Euler(data,unitsfile,atime,r_cut)
%
% Where data is loaded from the outut of the Fortran routine PROFILE.
%       unitsfile is the file in which the units particular to the
%                 data set are found.
%       atime is the expansion factor for the datafile
%  OPTIONAL
%       r_cut is the upper cutoff radius (cm) for the analysis

%Units
%little_h=0.65;
%L=40;			%Mpc h^-1
%atime_o=0.012898;	%initial atime
%e_o=9.373245e-5;	%initial e
%Gamma=5/3;		%gas constant
Mo=2e33;		%Solar Mass in grams
%Mpc=3.085678e24;	%Mpc in cm
G=6.67e-8;		%Gravitational constants in CGS units
k=1.381e-16;		%Stefan-Boltzmann constant (erg/K)
mu=16/27;
m_u=1.6605e-24;		%Atomic Mass Unit in grams
%keV=11.6e6;		%Deg K per keV
%Rho_c=3*(1e7/Mpc)^2/(8*pi*G)*little_h^2;	%Critical density (g/cm^3)


eval(['load ',unitsfile]);

%Let us get rid of the cores
r       =data(:,1)*lunit*atime; %distance now in cm
if(nargin==4)
 cut_temp=find(r<r_cut);
 cut=cut_temp(end);
else
 cut=length(r);
end
r       =r(2:cut);
bin_edge=data(1:cut,2)    *lunit*atime; %bin edges in cm
numd    =data(2:cut,3);
numg    =data(2:cut,4);
Massd   =numd*mdark*munit*Mo; %mass is now in grams
Massg   =numg*mgas *munit*Mo;
rho_d   =data(2:cut,5)*munit*Mo/((lunit*atime)^3); %density now in 
rho_g   =data(2:cut,6)*munit*Mo/((lunit*atime)^3); % gm/cm^3
Th      =data(2:cut,13)*Kunit*atime^2; %temperature in K
Vrg     =data(2:cut,16)*vunit*atime; %velocity now in cm/s
Vdisp2  =data(2:cut,11).^2+data(2:cut,12).^2*(vunit*atime)^2; %velocity dispersion sqrd in (cm/s)^2
len=length(r);

%Distance
dr=(bin_edge(2:len+1)-bin_edge(1:len));

%Mass
Massdg=Massd+Massg;
Mass=0*Massd;
Mass(1)=Massdg(1);
for i=2:len
 Mass(i)=sum(Massdg(1:i));
end

%Density
rho=rho_d+rho_g;

%Velocity
dV=[Vrg(1);Vrg(2:len)-Vrg(1:len-1)];
V =[Vrg(1);Vrg(2:len)+Vrg(1:len-1)]/2;

%Pressure
P=k/(mu*m_u)*rho_g.*Th;
dP=[0;P(2:len)-P(1:len-1)];

%Kinetic pressure
Pk=rho_g.*Vdisp2;

%LHS of equation of hydrostatic equilibrium
dPdr=dP./dr;

%RHS of equation of hydrostatic equilibrium
dPhidr1=G*Mass./(r.^2);
dPhidr2=G*4*pi*r.*rho;
%dPhidr=dPhidr1-dPhidr2;
dPhidr=dPhidr1;

Rho_dPhi=rho_g.*dPhidr.*dr;
Rho_dPhi_dr=rho_g.*dPhidr;

%let's lop of those regions which are clearly not in
% hydro equilibrium
cutoff=len;
Rho_dPhi(cutoff:len)=0*Rho_dPhi(cutoff:len);

%Gravitational Pressure
Pres_g=P*0;
Pres_g(len)=Rho_dPhi(len);
for i=1:len-1
 Pres_g(i)=sum(Rho_dPhi(i:len));
end

%Pressure terms due to deccelaration of infall gas.
%dP_pres=rho_g.*r.*V.*dV./dr;  %+'ive pressure is a -'ive contribution.
rho_v_dr=rho_g.*Vrg.*dr;
drho_v_dr=[-rho_v_dr(1);rho_v_dr(2:len)-rho_v_dr(1:len-1)];
dt=0*Vrg;
dt(Vrg~=0)=dr(Vrg~=0)./Vrg(Vrg~=0);
drho_v_dr_dt=dt*0;
drho_v_dr_dt(dt~=0)=drho_v_dr(dt~=0)./dt(dt~=0);

P_pres=drho_v_dr*0;
P_pres(len)=drho_v_dr_dt(len);
for i=1:len-1
 P_pres(i)=sum(drho_v_dr_dt(i:len));
end
