function r_cut=get_cutoffs(d,unitsfile,atime)
%
%r_cut=get_cutoffs(d,unitsfile,atime)
%
%Interactively finds the cutoff radii for the clusters
% whose profiles are stored in d

[i,j,k]=size(d);

for i=1:k
 [r,P,dPdr,Rho_dPhi_dr,Rho_dPhi,Pres_g,P_pres,Pk]=Euler(d(:,:,i),unitsfile,atime);
 loglog(r,P);
 [x,y]=ginput(1);
 r_cut(i)=x;
end 
