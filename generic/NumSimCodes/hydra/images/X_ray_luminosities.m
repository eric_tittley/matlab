Lxdv=9.542e-34*dn.^2.*th.^(0.5);
A=h_proj(r(box,1),r(box,2),Lxdv(box),h(box),128);
Lx=(log(A)-min(min(log(A))))/(max(max(log(A)))-min(min(log(A))))*256;
figure(fig2),imshow(Lx,256)

Total_L=sum(sum(A))*(40*3e24)^3/128^2

Brightness=Total_L/(4*pi*(2*3e24)^2)

