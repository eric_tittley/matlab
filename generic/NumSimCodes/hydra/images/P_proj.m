function [AP,Adn]=P_proj(r,th,rm,dn,h,itype,resolution,Limits,Adn)
% [AP,Adn]=P_proj(r,th,rm,h,itype,resolution,Limits,Adn)
%
% Makes an image of a hydra simulation in which the Pressure,
% smoothed over the smoothing radius, is projected and summed onto the
% X-Y plane.  Also returned is the projected column density just 'cause we need
% to calculate it anyway.
%
% The size of the image for each frame is given by RESOLUTION.
% The Limits for the view in the X-Y plane are given by LIMITS(1,2,3,4).
% The Limits in the Z direction are given by LIMITS(5,6).
% Limits of [0 1 0 1 0 1] encompass the whole simulation box.
%
% Adn is the projected gas column density.
% At is the projected mass-weighted mean pressure (sum(rm*th*dn)/sum(rm)).
%
% See also  P_proj_movie, T_proj, h_proj

if(nargin==9)
 DN_FLAG=0;
else
 DN_FLAG=1;
end

gas=find(itype & r(:,1)>Limits(1) & r(:,1)<Limits(2) & r(:,2)>Limits(3) & r(:,2)<Limits(4) & r(:,3)>Limits(5) & r(:,3)<Limits(6) );

disp(' Finding the projected pressure')
[AP,unresolved,resolved]=h_proj(r(gas,1),r(gas,2),dn(gas).*th(gas).*rm(gas),h(gas),resolution,Limits(1:4));
if(DN_FLAG)
 disp(' Finding the projected density')
 [Adn,unresolved,resolved]=h_proj(r(gas,1),r(gas,2),rm(gas),h(gas),resolution,Limits(1:4));
end

AT=AT./Adn;
