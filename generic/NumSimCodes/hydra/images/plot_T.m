function plot_gas_temp(datafile,zoom)
% plot_T(r,th)

if (nargin==1) zoom=1; end

[rm,r,v,th,itype,dn,h,time,atime]=readdata(datafile);

if(ap3msph_flag==0) L=1; end

% Make the colour map. It will have num_shades shades.
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
max_temp=max(th);
temp_scale=num_shades/max_temp;
for i=1:num_shades
 bin(i)=(i-1)*max_temp/num_shades;
end
bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Find all the gas particles
gas_mask  =find(itype);
num_gas   =length(gas_mask);
num_gas16 =num_gas/16;
num_gas256=num_gas16/16;

%Confine particles to gas only
r=r(gas_mask,:);
th=th(gas_mask);
dn=dn(gas_mask);

% Find the point of highest density by averaging the location of the points
%  within %10 of the peak density.  Calculate the shift to add so that this
%  point lies at L/2,L/2,L/2
centre=centre_quick(r,dn,L,0);
shift=(1&gas_mask)*([L/2+L,L/2+L,L/2+L]-centre);

% Shift the points
r=rem(r+shift,L);
centre=(1&gas_mask)*[L/2,L/2,L/2];

%Confine things to the zoomed box
dL=L*(1-1/zoom)/2;
box_mask=find( (r(:,1)>dL)&(r(:,1)<(L-dL)) &(r(:,2)>dL)&(r(:,2)<(L-dL)) &(r(:,3)>dL)&(r(:,3)<(L-dL)) );
r=r(box_mask,:);
th=th(box_mask);
centre=centre(box_mask,:);
itype=itype(box_mask);

% Find all the gas particles in the new box
gas_mask  =find(itype);
num_gas   =length(gas_mask);
num_gas16 =fix(num_gas/16);
num_gas256=fix(num_gas16/16);

% Find the num_gas/16 gas particles that end up deepest in the core
distance=sqrt(sum((r-centre)'.^2));
[dummy,core_objs]=sort(distance);
core_objs=core_objs(1:num_gas/16); %core_objs is indexed for the gas particles, only.

axes_vect=[dL L-dL dL L-dL dL L-dL];

%----------------------------------------
subplot(2,2,1)
% All gas particles

% Plot the various temperatures
 Tmask=find(th>bin(2)&th<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th>bin(k)&th<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
  set(fig,'MarkerSize',1);
 end
 hold off
 title('All Gas Particles');

%----------------------------------------
subplot(2,2,3)

% Find the top 1/16 in temp
 [dummy,rankings]=sort(th);
 r16 = r(rankings(num_gas-num_gas16+1:num_gas),:);
 th16=th(rankings(num_gas-num_gas16+1:num_gas)  );

% Plot the various temperatures
 Tmask=find(th16>bin(1)&th16<bin(2));
 fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th16>bin(k)&th16<bin(k+1));
  fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
  set(fig,'Color',map(k,:));
  set(fig,'MarkerSize',1);
 end
 hold off
 title('Top 1/16 Ngas in Temp')

%----------------------------------------
subplot(2,2,2)

% Plot the core objects
 r_core= r(core_objs,:);
th_core=th(core_objs,:);

% Plot the various temperatures
 Tmask=find(th_core>bin(2)&th_core<bin(3));
 fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th_core>bin(k)&th_core<bin(k+1));
  fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
  set(fig,'Color',map(k,:));
  set(fig,'MarkerSize',1);
 end
 hold off
 title('Core Objects');

%----------------------------------------
subplot(2,2,4)

% Find the top 1/256 in temp
 [dummy16,rankings16]=sort(th16);
 th256=th16(rankings16(num_gas16-num_gas256+1:num_gas16)  );
 r256 = r16(rankings16(num_gas16-num_gas256+1:num_gas16),:);

% Plot the various temperatures
 Tmask=find(th256>bin(1)&th256<bin(2));
 fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th256>bin(k)&th256<bin(k+1));
  fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
  set(fig,'Color',map(k,:));
  set(fig,'MarkerSize',1);
 end
 hold off
 title('Top 1/256 Ngas in Temp');

%----------------------------------------
% Label the time.
 text(dL-20/zoom,dL+25/zoom,dL,num2str(13.0386*time));
