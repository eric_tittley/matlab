function [AP,Adn,AP_lim,Adn_lim,t]=P_proj_multi(run,start,spacing,finish,resolution,Limits)
% [AP,Adn,AP_lim,Adn_lim,t]=P_proj_multi(run,start,spacing,finish,resolution,Limits)
%
% Makes a series of images of a hydra simulAPion in which the Pressure,
% smoothed over the smoothing radius, is projected and summed onto the
% X-Y plane.  Also returned is the projected column density just cause we need
% to calculAPe it anyway.
%
% Both Adn and AP are scaled to the colourmap, so the units of both are
% colourmap numbers.
% AP_lim and Adn_lim are the [min max] of the projections before scaling.
% Use these with the colourbar.
%
% The run number, irun, is given by RUN.  Output times start AP START,
% are spaced every SPACING, and end at FINISH.  The size of the image
% for each frame is given by RESOLUTION.
% The Limits for the view in the X-Y plane are given by LIMITS(1,2,3,4).
% The Limits in the Z direction are given by LIMITS(5,6).
% Limits of [0 1 0 1 0 1] encompass the whole simulation box.
%
% Adn is the projected gas column density.
% AP is the projected mass-weighted mean pressure (sum(rm*th*dn)/sum(rm)).
%
% See also P_proj, h_proj, T_proj, images2movies, colourbar_log

disp('initializing')

%Initialize frames
num=(finish-start)/spacing;
A=zeros(resolution,resolution,num);
AP=A; Adn=A;

disp('Starting the main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the datafile
 file=dataname(run,i);
 disp(['File ',file]);
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(file);

%find the gas within the limiits
 gas=find(itype & r(:,1)>Limits(1) & r(:,1)<Limits(2) & r(:,2)>Limits(3) & r(:,2)<Limits(4) & r(:,3)>Limits(5) & r(:,3)<Limits(6) );

%CalculAPe the densities (these are the slow parts)
 disp(' Finding the projected pressure')
 [ AP(:,:,j),unresolved,resolved]=h_proj(r(gas,1),r(gas,2),dn(gas).*th(gas).*rm(gas),h(gas),resolution,Limits(1:4));
 disp(' Finding the projected density')
 [Adn(:,:,j),unresolved,resolved]=h_proj(r(gas,1),r(gas,2),rm(gas),h(gas),resolution,Limits(1:4));

% save the time
 t(j)=hdr.time;
 eval(['save /tmp/temp_A_P AP Adn t'])
end

%Scale the array
NumColours=64;
map=gray(NumColours);
colormap(map);
disp('Scaling the array')
AP=AP./Adn;

AP(AP<0)=0*AP(AP<0);
AP=log10(AP+min(min(min(AP(AP>0))))/10);
maxAP=max(max(max(AP)));
minAP=min(min(min(AP)));
AP=(AP-minAP)/(maxAP-minAP)*(NumColours-1)+1;
AP_lim=[minAP maxAP];

Adn(Adn<0)=0*Adn(Adn<0);
Adn=log10(Adn+min(min(min(Adn(Adn>0))))/10);
maxAdn=max(max(max(Adn)));
minAdn=min(min(min(Adn)));
Adn=(Adn-minAdn)/(maxAdn-minAdn)*(NumColours-1)+1;
Adn_lim=[minAdn maxAdn];

AP =uint8(AP );
Adn=uint8(Adn);
