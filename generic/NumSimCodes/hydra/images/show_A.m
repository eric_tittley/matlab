function [A_new,map]=show_A(A,colour_style)

if(nargin==1), colour_style='colour', end

% Make the colour map. It will have num_shades shades.
disp('Setting up the colour map')
num_shades=64;
if (colour_style(1:4)=='colo')
% colormap(hot(num_shades/2));
% map1=colormap;
% colormap(cool(num_shades/2));
% map2=colormap;
% map=[map1;map2];
 map=colourmap(num_shades);
elseif (colour_style(1:4)=='gray')
 colormap(gray(num_shades))
 map=colormap;
else
 error(['Unsupported colour map: ',colour_style])
end

max_A=max(max(A));
min_A=min(min(A));
A_new=(A-min_A)/(max_A-min_A)*(num_shades-1)+1;

img=image(A_new);
colormap(map);
set(img,'UserData',[min_A,max_A])
colorbar
