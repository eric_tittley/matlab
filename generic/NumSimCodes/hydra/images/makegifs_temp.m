function M=makegifs_temp(run,start,spacing,finish)
% makegifs_temp   makegifs_temp(run,start,spacing,finish)
%  Makes a series of GIF images for ap3msph run # RUN.
%  Only gas particles are shown and they are
%  colour-coded according to temperature.  The movie
%  starts at iteration START spaced SPACING iterations
%  apart ending at FINISH.
%
%  See also: READDATA, DENSITY_PLOT_TOPHAT, DENSITY_PLOT_TYPE,
%            MAKEMOVIE, makemovie_temp

num=(finish-start)/spacing;

mask=0;

% Load in the final state to determine the temperature
%  scale and the location of peak density
is=num2str(finish);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
command=['[r,itype,L,time,h,dn,th]=readdata(''d00',num2str(run),'.',label,''');']
eval(command)

% Find all the gas particles
gas_mask=find(itype);
num_gas   =length(gas_mask);
num_gas16 =num_gas/16;
num_gas256=num_gas16/16;

% Make the colour map. It will have num_shades shades.
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
max_temp=max(th);
temp_scale=num_shades/max_temp;
for i=1:num_shades
 bin(i)=(i-1)*max_temp/num_shades;
end
bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Find the point of highest density by averaging the location of the points
%  within %10 of the peak density.  Calculate the shift to add so that this
%  point lies at L/2,L/2,L/2
peakmask=find(dn>(max(dn)*0.9));
[MM,NN]=size(peakmask);
if(MM==1) centre=(1&gas_mask)*r(peakmask,:);
else centre=(1&gas_mask)*mean(r(peakmask,:));
end
shift=(1&gas_mask)*[L/2+L,L/2+L,L/2+L]-centre;

% Find the num_gas/16 gas particles that end up deepest in the core
distance=sqrt( (r(gas_mask,1)-centre(:,1)).^2+(r(gas_mask,2)-centre(:,2)).^2+(r(gas_mask,3)-centre(:,3)).^2 );
[dummy,core_objs]=sort(distance);
core_objs=core_objs(1:num_gas/16); %core_objs is the indexed from the gas particles, only.

axes_vect=[0 L 0 L];

j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 command=['[r_all,itype,L,time,h,dn,th_all]=readdata(''d00',num2str(run),'.',label,''');']
 eval(command)

% We only want the gas particles
 r=r_all(gas_mask,:);
 th=th_all(gas_mask);

% Shift the points
 r=rem(r+shift,L);

% All gas particles

% Plot the various temperatures
 clg
 Tmask=find((th>bin(2)&th<bin(3))&((r(:,1)<L/2+1)&(r(:,1)>L/2-1)));
 fig=plot(r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 axis(square);
 hold on
 for k=3:num_shades
  Tmask=find((th>bin(k)&th<bin(k+1))&((r(:,1)<L/2+1)&(r(:,1)>L/2-1)));
  fig=plot(r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('All Gas Particles');

% Label the time.
 text(1,1,num2str(13.0386*time));

% Grab the frame and write the gif image
 [X,map2]=getframe(gcf);
 fname=['''d00',num2str(run),'.',label,'.gif''']
 gifwrite(X,map2,fname);
end
