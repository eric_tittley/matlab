function plot_T_quick(r,th,dn,Tmax)

if(nargin==3), Tmax=0; end

% plot_T_quick(r,th,dn)

% Make the colour map. It will have num_shades shades.
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
if(Tmax), max_temp=Tmax;
else max_temp=max(th);
end
temp_scale=num_shades/max_temp;
for i=1:num_shades
 bin(i)=(i-1)*max_temp/num_shades;
end
bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Find all the gas particles
num_gas   =length(r);
num_gas16 =num_gas/16;
num_gas256=num_gas16/16;

% Find the num_gas/16 gas particles that end up deepest in the core
centre=centre_quick(r,dn);
centre=(1&dn)*centre;
distance=sqrt(sum((r-centre)'.^2));
[dummy,core_objs]=sort(distance);
core_objs=core_objs(1:num_gas/16); %core_objs is indexed for the gas particles, only.

axes_vect=[min(r(:,1)) max(r(:,1)) min(r(:,2)) max(r(:,2)) min(r(:,3)) max(r(:,3))];

%----------------------------------------
% All gas particles

% Plot the various temperatures
 Tmask=find(th>bin(2)&th<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th>bin(k)&th<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
  set(fig,'MarkerSize',1);
 end
 hold off
 title('All Gas Particles');
