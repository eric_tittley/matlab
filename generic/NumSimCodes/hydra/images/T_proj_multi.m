function [AT,Adn,AT_lim,Adn_lim,t]=T_proj_multi(run,start,spacing,finish,resolution,Limits)
% [AT,Adn,AT_lim,Adn_lim,t]=T_proj_multi(run,start,spacing,finish,resolution,Limits)
%
% Makes a series of images of a hydra simulation in which the Temperature,
% smoothed over the smoothing radius, is projected and summed onto the
% X-Y plane.  Also returned is the projected column density just cause we need
% to calculate it anyway.
%
% Both Adn and AT are scaled to the colourmap, so the units of both are
% colourmap numbers.
% AT_lim and Adn_lim are the [min max] of the projections before scaling.
% Use these with the colourbar.
%
% The run number, irun, is given by RUN.  Output times start at START,
% are spaced every SPACING, and end at FINISH.  The size of the image
% for each frame is given by RESOLUTION.
% The Limits for the view in the X-Y plane are given by LIMITS(1,2,3,4).
% The Limits in the Z direction are given by LIMITS(5,6).
% Limits of [0 1 0 1 0 1] encompass the whole simulation box.
%
% Adn is the projected gas column density.
% At is the projected temperature (sum(rm.*th)/sum(rm)).
%
% See also h_proj_movie, h_proj, T_proj

disp('initializing')

%Initialize frames
num=(finish-start)/spacing;
A=zeros(resolution,resolution,num);
AT=A; Adn=A;

%Find the base filename
is=num2str(run);
if length(is)==3 base=['d0',is];
elseif length(is)==2 base=['d00',is];
elseif length(is)==1 base=['d000',is];
else base=['d',is];
end

disp('Starting the main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the datafile
% disp(' Reading in the datafile')
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 file=[base,'.',label]
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(file);
 gas=find(itype & r(:,1)>Limits(1) & r(:,1)<Limits(2) & r(:,2)>Limits(3) & r(:,2)<Limits(4) & r(:,3)>Limits(5) & r(:,3)<Limits(6) );

 disp(' Finding the projected densities')
 [ AT(:,:,j),unresolved,resolved]=h_proj(r(gas,1),r(gas,2),th(gas).*rm(gas),h(gas),resolution,Limits(1:4));
 [Adn(:,:,j),unresolved,resolved]=h_proj(r(gas,1),r(gas,2),rm(gas),h(gas),resolution,Limits(1:4));
 t(j)=time;
 eval(['save /tmp/temp_A_t',base,' A t'])
end

%Scale the array
NumColours=64;
map=gray(NumColours);
colormap(map);
disp('Scaling the array')
AT=AT./Adn;

AT(AT<0)=0*AT(AT<0);
AT=log10(AT+min(min(min(AT(AT>0))))/10);
maxAT=max(max(max(AT)));
minAT=min(min(min(AT)));
AT=(AT-minAT)/(maxAT-minAT)*(NumColours-1)+1;
AT_lim=[minAT maxAT];

Adn(Adn<0)=0*Adn(Adn<0);
Adn=log10(Adn+min(min(min(Adn(Adn>0))))/10);
maxAdn=max(max(max(Adn)));
minAdn=min(min(min(Adn)));
Adn=(Adn-minAdn)/(maxAdn-minAdn)*(NumColours-1)+1;
Adn_lim=[minAdn maxAdn];

AT =uint8(AT );
Adn=uint8(Adn);
