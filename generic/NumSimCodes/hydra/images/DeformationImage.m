function A=DeformationImage(r_o,r_f,h,L,Limits)
% A=DeformationImage(r_o,r_f,h,L,Limits)

if nargin==4, Limits=[0 1 0 1 0 1]; end

dr=r_f-r_o;
dr(abs(dr)>0.5)=dr(abs(dr)>0.5)-1*sign(dr(abs(dr)>0.5));
dist=sqrt( sum( dr'.^2 ) );
A=h_proj(r_f(:,1),r_f(:,2),dist,h,L,Limits);
