% This package contains tools to work specifically with data produced by HYDRA
%
% clumps	For analysing the drag felt by clumps of matter
%
% data		For analysing the hydra data generally
%
% find_clusters	Routines to find clusters in Hydra simulation data
%
% gradients	Tools for analysing gradients in SPH
%
% groups	Tools to work with cluster membership data produced by SKID
%
% hydro_eq	For analysing the hydrostatic state of clusters
%
% images	Images of hydra data
%
% io		Hydra data io routines
%
% kernel	Routines to analyse the hydra kernel
%
% mex		Various tools for manipulating cosmological data
%
% movies	Routines to make MPEG movies out of Hydra data
%
% profiles	Routines to create and analyse cluster profiles
%
% scripts	Various time saving scripts
