function [handle]=gethandles(gcbf)
% get the handles for all the edit boxes
	handle.readbutton   =findobj(gcbf,'Tag','readbutton');
	handle.writebutton  =findobj(gcbf,'Tag','writebutton');
	handle.inputfile	=findobj(gcbf,'Tag','inputfile');
	handle.outputfile	=findobj(gcbf,'Tag','outputfile');
	handle.dumptimeidx	=findobj(gcbf,'Tag','dumptimeidx');
	handle.dumptime   	=findobj(gcbf,'Tag','dumptime');
	handle.itime		=findobj(gcbf,'Tag','itime');
	handle.itstop		=findobj(gcbf,'Tag','itstop');
	handle.itdump		=findobj(gcbf,'Tag','itdump');
	handle.itout		=findobj(gcbf,'Tag','itout');
	handle.time			=findobj(gcbf,'Tag','time');
	handle.atime		=findobj(gcbf,'Tag','atime');
	handle.htime		=findobj(gcbf,'Tag','htime');
	handle.dtime		=findobj(gcbf,'Tag','dtime');
	handle.Est			=findobj(gcbf,'Tag','Est');
	handle.T			=findobj(gcbf,'Tag','T');
	handle.Th			=findobj(gcbf,'Tag','Th');
	handle.U			=findobj(gcbf,'Tag','U');
	handle.Radiation	=findobj(gcbf,'Tag','Radiation');
	handle.Esum			=findobj(gcbf,'Tag','Esum');
	handle.Rsum			=findobj(gcbf,'Tag','Rsum');
	handle.irun			=findobj(gcbf,'Tag','irun');
	handle.nobj			=findobj(gcbf,'Tag','nobj');
	handle.ngas			=findobj(gcbf,'Tag','ngas');
	handle.ndark		=findobj(gcbf,'Tag','ndark');
	handle.L			=findobj(gcbf,'Tag','L');
	handle.intl			=findobj(gcbf,'Tag','intl');
	handle.nlmx			=findobj(gcbf,'Tag','nlmx');
	handle.perr		    =findobj(gcbf,'Tag','perr');
	handle.dtnorm	    =findobj(gcbf,'Tag','dtnorm');
	handle.sft0		    =findobj(gcbf,'Tag','sft0');
	handle.sftmin	    =findobj(gcbf,'Tag','sftmin');
	handle.sftmax	    =findobj(gcbf,'Tag','sftmax');
	handle.h100		    =findobj(gcbf,'Tag','h100');
	handle.box		    =findobj(gcbf,'Tag','box');
	handle.zmet		    =findobj(gcbf,'Tag','zmet');
	handle.spc0		    =findobj(gcbf,'Tag','spc0');
	handle.lcool		=findobj(gcbf,'Tag','lcool');
	handle.rmgas		=findobj(gcbf,'Tag','rmgas');
	handle.rmdark	    =findobj(gcbf,'Tag','rmdark');
	handle.rmnorm	    =findobj(gcbf,'Tag','rmnorm');
	handle.tstart	    =findobj(gcbf,'Tag','tstart');
	handle.omega0	    =findobj(gcbf,'Tag','omega0');
	handle.xlambda0	    =findobj(gcbf,'Tag','xlambda0');
	handle.h0t0		    =findobj(gcbf,'Tag','h0t0');
