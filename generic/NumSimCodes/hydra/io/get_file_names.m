function files=get_file_names( run, start, finish, data_dir )

if nargin == 3
	data_dir = 'data';
end

d = dir(data_dir);
token = '.';

is=num2str(run);
if length(is)==3 label2=['0',is];
elseif length(is)==2 label2=['00',is];
elseif length(is)==1 label2=['000',is];
else label2=is;
end

run_prefix = ['d' label2];
files = '';

for i = 1:length(d)

	file_name = ' ';
		
	if (length( d(i).name) == 10 )	
		file_name = d(i).name;
	
		%split the file name into
		%d0001
		%.0010
		[chopped, file_name] = strtok(file_name, token);

		%get rid of the .
		tmp = file_name(2:5);
		file_name = strvcat(chopped,tmp);
	
		if ( strcmp( file_name(1,:), run_prefix) )
			if ( (str2num(file_name(2,:)) >= start ) & (str2num(file_name(2,:)) <= finish ) )
				files = strvcat(files, d(i).name);
			end
		end
	end
end
