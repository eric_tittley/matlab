function [gasdn,darkdn,gash,darkh]=readdensities(datafile,mach)
%readdensities     Read in hydra density files produced by calcdens
%
%   Syntax: [gasdn,darkdn,gash,darkh]=readdensities(datafile,mach)
%            where mach is 'big' or 'little', depending on the byte
%            structure.  The default is 'native'.
%
% ARGUMENTS
%  datafile	The filename of the .density file to be read.
%  mach		(optional) The binary format of the file (see fopen).
%
% RETURNS
%  gasdn	List of the local number density for each gas particle.
%  darkdn	List of the local number density for each dark matter.
%  gash		The smoothing length for each gas particle.
%  darkh	The smoothing length for each dark matter particle.
%
% NOTE
%  The smoothing lengths are calculated to enclose exactly Nsph particles,
%  within 2h.  Nsph is set in when running calcdens.

% AUTHOR: Eric Tittley
%
% HISTORY
%  04 07 23 Very old version (at least 010301)
%	Added comments

if(nargin==1) mach='n';
else mach=mach(1);
end

fid=fopen(datafile,'rb',mach);
if(fid<=0)
 disp(['error in readdensities: unable to open file: ',datafile])
 return
end

%Header
blocksize=fread(fid,1,'uint');

%ngas
ngas=fread(fid,1,'uint');

%ndark
ndark=fread(fid,1,'uint');

blocksize=fread(fid,1,'uint');

%atime
blocksize=fread(fid,1,'uint');
atime=fread(fid,1,'float');
blocksize=fread(fid,1,'uint');

%densities
if(ngas>0)
 blocksize=fread(fid,1,'uint');
 gasdn=fread(fid,ngas,'float');
 blocksize=fread(fid,1,'uint');
else
 gasdn=0;
end
 
if(ndark>0)
 blocksize=fread(fid,1,'uint');
 darkdn=fread(fid,ndark,'float');
 blocksize=fread(fid,1,'uint');
else
 darkdn=0;
end

if(ngas>0)
 blocksize=fread(fid,1,'uint');
 gash=fread(fid,ngas,'float');
 blocksize=fread(fid,1,'uint');
else
 gash=0;
end

if(ndark>0)
 blocksize=fread(fid,1,'uint');
 darkh=fread(fid,ndark,'float');
 blocksize=fread(fid,1,'uint');
else
 darkh=0;
end

fclose(fid);
