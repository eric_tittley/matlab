function [hdr,Version]=readhydra_header(fid,Version)
% readhydra_header: Read a header for a hydra file.
%
% readhydra_header should normally be called from readhydra or readhydra_field.
%
% syntax: [hdr,Version]=readhydra_header(fid,Version)
%
% ARGUMENTS
%  fid	A file pointer returned by fopen() to the data file to be read.
%  Version	A (perhaps guess) at the Version of the data file.
%
% RETURNS
%  hdr		The header as a structure.
%  Version	The file version, as a number.
%
% SEE ALSO
%  readhydra, readhydra_field

% AUTHOR: Eric Tittley
%
% HISTORY
%  04 09 17 First version, cut from readhydra.
%  06 12 06 Modified to take Version as (over-ridable) input.

if((nargin~=1) & (nargin~=2))
 error('readhydra_header: syntax: [hdr,Version]=readhydra_header(fid,Version)')
end
 
blocksize=fread(fid,1,'uint')/4; % 4 bytes per word

% Version information
% The most recent versions of hydra store their version information within
% the first block of the header.  We must check for this.
% The version information within the file overrides any version information
% passed to the routine.
if(blocksize==3)
 Ver=fread(fid,[1,3],'uint');
 disp(['Found version: ',int2str(Ver(1)),'.',...
                         int2str(Ver(2)),'.',...
                         int2str(Ver(3)) ])
 Version=Ver(1)*100+Ver(2)*10+Ver(3);
 blocksize=fread(fid,1,'uint')/4; % Closing bocksize 
 blocksize=fread(fid,1,'uint')/4; % Blocksize of next block
 hdr.Version=Ver;
else
 Ver=Version;
end

if(blocksize~=400)
 disp('error in readhydra: mal-formed header ')
 return
end

%ibuf
hdr.times=fread(fid,[200,1],'float');

count=0;
%ibuf1
% first line 8*4 = 32 bytes
hdr.itime=fread(fid,1,'uint'); count=count+1;
hdr.itstop=fread(fid,1,'uint'); count=count+1;
hdr.itdump=fread(fid,1,'uint'); count=count+1;
hdr.itout=fread(fid,1,'uint'); count=count+1;
hdr.time=fread(fid,1,'float'); count=count+1;
hdr.atime=fread(fid,1,'float'); count=count+1;
hdr.htime=fread(fid,1,'float'); count=count+1;
hdr.dtime=fread(fid,1,'float'); count=count+1;

% second line 8*4 = 32 bytes
hdr.Est=fread(fid,1,'float'); count=count+1;
hdr.T=fread(fid,1,'float'); count=count+1;
hdr.Th=fread(fid,1,'float'); count=count+1;
hdr.U=fread(fid,1,'float'); count=count+1;
hdr.Radiation=fread(fid,1,'float'); count=count+1;
hdr.Esum=fread(fid,1,'float'); count=count+1;
hdr.Rsum=fread(fid,1,'float'); count=count+1;
hdr.cpu=fread(fid,1,'float'); count=count+1;

% third line 7*4 = 28 bytes
hdr.tstop=fread(fid,1,'float'); count=count+1;
hdr.tout=fread(fid,1,'float'); count=count+1;
hdr.icdump=fread(fid,1,'uint'); count=count+1;
padding=fread(fid,1,'float'); count=count+1;
hdr.Tlost=fread(fid,1,'float'); count=count+1;
hdr.Qlost=fread(fid,1,'float'); count=count+1;
hdr.Ulost=fread(fid,1,'float'); count=count+1;
if(Version>=400), padding=fread(fid,1,'float');  count=count+1; end

% fourth line 
if(Version>=400)
 hdr.soft2=fread(fid,1,'float'); count=count+1;
 hdr.dta=fread(fid,1,'float'); count=count+1;
 hdr.dtcs=fread(fid,1,'float'); count=count+1;
 hdr.dtv=fread(fid,1,'float'); count=count+1;
end
if(Version==211) 
% separated energies, 4*4 = 16 bytes
 hdr.Tdark=fread(fid,1,'float'); count=count+1;
 hdr.Udd=fread(fid,1,'float'); count=count+1;
 hdr.Ugg=fread(fid,1,'float'); count=count+1;
 hdr.Udg=fread(fid,1,'float'); count=count+1;
end

% fifth line, code units, 1*4+8*8 = 68 bytes
% if(Version==401 | Version==402 | Version==211)
if(rem(Version,10)>0)
 hdr.lunit=fread(fid,1,'real*8'); count=count+2;
 hdr.munit=fread(fid,1,'real*8'); count=count+2;
 hdr.tunit=fread(fid,1,'real*8'); count=count+2;
 hdr.vunit=fread(fid,1,'real*8'); count=count+2;
 hdr.nunit=fread(fid,1,'real*8'); count=count+2;
 hdr.Kunit=fread(fid,1,'real*8'); count=count+2;
 hdr.eunit=fread(fid,1,'real*8'); count=count+2;
 hdr.cunit=fread(fid,1,'real*8'); count=count+2;
end

% rest of ibuf1
for i=count+1:100 dummy=fread(fid,4,'char'); end

count=0;
%ibuf2
% first line 8*4 = 32 bytes
hdr.irun=fread(fid,1,'uint'); count=count+1;
hdr.nobj=fread(fid,1,'uint'); count=count+1;
hdr.ngas=fread(fid,1,'uint'); count=count+1;
hdr.ndark=fread(fid,1,'uint'); count=count+1;
if(Version<400)
 hdr.L=fread(fid,1,'uint'); count=count+1;
end
hdr.intl=fread(fid,1,'uint'); count=count+1;
hdr.nlmx=fread(fid,1,'uint'); count=count+1;
hdr.perr=fread(fid,1,'float'); count=count+1;

%second line 8*4 = 32 bytes
hdr.dtnorm=fread(fid,1,'float'); count=count+1;
hdr.sft0=fread(fid,1,'float'); count=count+1;
hdr.sftmin=fread(fid,1,'float'); count=count+1;
hdr.sftmax=fread(fid,1,'float'); count=count+1;
if(Version>=400)
 padding=fread(fid,1,'uint'); count=count+1;
end
hdr.h100=fread(fid,1,'float'); count=count+1;
hdr.box=fread(fid,1,'float'); count=count+1;
hdr.zmet=fread(fid,1,'float'); count=count+1;
if(Version<400);
 hdr.spc0=fread(fid,1,'float'); count=count+1;
end
hdr.lcool=fread(fid,1,'uint'); count=count+1;

%third line 6*4 = 24 bytes
hdr.rmgas=fread(fid,1,'float'); count=count+1;
if(Version<400)
 hdr.rmdark=fread(fid,1,'float'); count=count+1;
end
hdr.rmnorm=fread(fid,1,'float'); count=count+1;
if(Version>=400)
 padding=fread(fid,2,'uint'); count=count+2;
end

hdr.tstart=fread(fid,1,'float'); count=count+1;
hdr.omega0=fread(fid,1,'float'); count=count+1;
hdr.xlambda0=fread(fid,1,'float'); count=count+1;
hdr.h0t0=fread(fid,1,'float'); count=count+1;

%fourth line 2*4 = 8 bytes
if(length(Ver)~=3)
 hdr.Version=fread(fid,1,'float'); count=count+1;
end
if(Version<300)
 hdr.Version=Version/100;
end
if(Version>=400)
 % These are used for isolated boundary conditions
 hdr.rcen=fread(fid,3,'float'); count=count+3;
 hdr.rmax2=fread(fid,1,'float'); count=count+1;
end

%rest of ibuf2 (25=96/4+1)
for i=count+1:100 dummy=fread(fid,4,'char'); end


% ******************** END OF readhydra_header **********************
