function filename=dataname(irun,step)
% filename=dataname(irun,step)
%
% Returns the filename (character string) for the hydra data
% file with run number, irun, and iteration step (or time, depending
% on version), step

if nargin~=2, error('Insufficient arguments'), end
%Find the base filename
is=num2str(irun);
if length(is)==3 base=['d0',is];
elseif length(is)==2 base=['d00',is];
elseif length(is)==1 base=['d000',is];
else base=['d',is];
end

is=num2str(step);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
filename=[base,'.',label];
