function [gasdisp,darkdisp,gash,darkh]=readdispersions(datafile,mach)
%readdata     Read in hydra data files
%   Syntax: [gasdisp,darkdisp,gash,darkh]=readdispersions(datafile,mach)
%            where mach is 'big' or 'little', depending on the byte
%            structure.  The default is 'native'.

if(nargin==1) mach='n';
else mach=mach(1);
end

fid=fopen(datafile,'rb',mach);

%Header
blocksize=fread(fid,4,'char');

%ngas
ngas=fread(fid,1,'uint');

%ndark
ndark=fread(fid,1,'uint');

%atime
blocksize=fread(fid,8,'char');
atime=fread(fid,1,'float');

%dispersions
if(ngas>0)
 blocksize=fread(fid,8,'char');
 gasdisp=fread(fid,ngas,'float');
 gasdisp=gasdisp';
else
 gasdisp=[];
end
 
if(ndark>0)
 blocksize=fread(fid,8,'char');
 darkdisp=fread(fid,ndark,'float');
 darkdisp=darkdisp';
else
 darksdisp=[];
end

if(ngas>0)
 blocksize=fread(fid,8,'char');
 gash=fread(fid,ngas,'float');
end

if(ndark>0)
 blocksize=fread(fid,8,'char');
 darkh=fread(fid,ndark,'float');
end

fclose(fid);
