function r=resample(datafile_in,datafile_out,Res_out)
% resample(datafile_in,datafile_out,Res_out)
%
% the resolution of the outfile should be a factor of the 
% resolution of the infile.

[rm,r,v,th,itype,dn,h,hdr]=readhydra(datafile_in);
Res_in3=length(rm)/2;
Res_in=Res_in3^(1/3);

factor=(Res_in/Res_out)^3;

hdr.nobj=hdr.nobj/factor;
hdr.ndark=hdr.ndark/factor;
hdr.ngas=hdr.ngas/factor;
hdr.munit=hdr.munit*factor;

all=randperm(Res_in3); samp_one=all(1:Res_out^3);
samp=[samp_one,Res_in3-1+samp_one];

rm=rm(samp);
th=th(samp);
itype=itype(samp);
dn=dn(samp);
dn=dn/factor;
h=h(samp);
h=h*factor^(1/3);
r=r(samp,:);
v=v(samp,:);

writehydra(datafile_out,rm,r,v,th,itype,dn,h,hdr)
