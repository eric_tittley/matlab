function AWave=ReadWavelets(filename,dimension)
% AWave=ReadWavelets(filename,dimension);

AWave=zeros(dimension,dimension,dimension);
Infile=fopen(filename,'rt');

for i=1:dimension^3
 vector=fscanf(Infile,'%f',4);
 AWave(vector(1)+1,vector(2)+1,vector(3)+1)=vector(4);
end
