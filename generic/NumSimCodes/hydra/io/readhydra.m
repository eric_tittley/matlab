function [rm,r,v,th,itype,dn,h,hdr]=readhydra(datafile,Version)
%readdata     Read in hydra data files
%   Syntax: [rm,r,v,th,itype,dn,h,hdr]=readhydra(datafile,Version)
%
%   Version sets the hydra data version
%
% Version 2.1 is vanilla hydra2.1
%         2.11 has potential energies and units in the header
%         4.00 is vanilla
%         4.01 has units in the header
%         4.02 has constant mass per particle (hdr.rmgas), no rm array.
%	  4.03 has common mass (this should not be in the version)
%         4.04 permits galaxy merging and index resorting.
%
% SEE ALSO
%  readhydra_field

% HISTORY
% 00 05 09 Changed itype from uint to int, since 'lost' particles have type -2
%
% 00 06 30 Brought up to speed with the official release of Hydra, which has
%          Version information built in, but keeping compatibility with
%          my old files, which have a different versioning method
%
% 00 07 21 Fixed some errors due to the previous fix.
%
% 00 12 14 Added some commments.
%
% 00 12 18 Added a line such that if the Ver record is found, hdr.Version is
%	set to Ver.
% 00 12 22 Minor superficial changes
% 01 08 29 Bug fix.  rcen was being treated as a single float, when in fact
%	it is a real(3).  The only consequence is that rmax2 was being ignored.
% 02 09 16 Return empty matrices (h,dn,th) inf ngas==0.
% 04 09 17 Pulled the header reading out into its own subroutine which
%	readhydra_field can also use.
% 07 04 09 Return as the same type as read, to save memory.  Except itype,
%	which is now returned as an int4.

% there are a lot of versions of Hydra out there
if(nargin==1), Version=2; end
Version=round(Version*100);

% Open the files
fid=fopen(datafile,'rb');
if(fid<=0)
 disp(['error in readhydra: unable to open file: ',datafile])
 return
end

% Read in the header
if(nargin==1)
 [hdr,Version]=readhydra_header(fid,Version);
else
 hdr=readhydra_header(fid,Version);
end

if(Version<400)
 blocksize=fread(fid,8,'char'); rm=fread(fid,hdr.nobj,'*float');
 blocksize=fread(fid,8,'char'); r=fread(fid,[3,hdr.nobj],'*float');
 blocksize=fread(fid,8,'char'); v=fread(fid,[3,hdr.nobj],'*float');
 blocksize=fread(fid,8,'char'); h=fread(fid,hdr.nobj,'*float');
 blocksize=fread(fid,8,'char'); th=fread(fid,hdr.nobj,'*float');
 blocksize=fread(fid,8,'char'); itype=fread(fid,hdr.nobj,'*int');
 blocksize=fread(fid,8,'char'); dn=fread(fid,hdr.nobj,'*float');
else
 blocksize=fread(fid,8,'char'); itype=fread(fid,hdr.nobj,'*int');
 if(Version~=402 & Version~=422) 
  blocksize=fread(fid,8,'char'); rm=fread(fid,hdr.nobj,'*float');
 else
  rm=[];
 end
 blocksize=fread(fid,8,'char'); r=fread(fid,[3,hdr.nobj],'*float');
 blocksize=fread(fid,8,'char'); v=fread(fid,[3,hdr.nobj],'*float');
 if(hdr.ngas>0)
  blocksize=fread(fid,8,'char'); h=fread(fid,hdr.ngas,'*float');
  blocksize=fread(fid,8,'char'); th=fread(fid,hdr.ngas,'*float');
  blocksize=fread(fid,8,'char'); dn=fread(fid,hdr.ngas,'*float');
 else
  h=[];
  th=[];
  dn=[];
 end
end

fclose(fid);

%th=th/(hdr.atime^2);        % Multiply th by Kunit to get units in K
%dn=dn/(5*hdr.nobj*hdr.atime^3); % In units of Rho_critical (present)

%These have not been rescaled by the function rescaleup/down
if(Version==211)
 hdr.lunit=hdr.lunit*hdr.L;
 hdr.vunit=hdr.vunit*hdr.L;
 hdr.nunit=hdr.nunit/hdr.L^3;
 hdr.eunit=hdr.eunit*hdr.L^2;
 hdr.Kunit=hdr.Kunit*hdr.L^2;
end
