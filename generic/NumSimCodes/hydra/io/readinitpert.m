function r=readinitpert(file)
% r=readinitpert(file);
%
% Reads in an initial perturbation file as used by initial conditions
% generators such as createcosmos.
%

fp=fopen(file,'r');
blocksize=fread(fp,4,'char');
[r,N]=fread(fp,'float32');
fclose(fp);
r=r(1:end-1); %strip the trailing block marker
r=reshape(r,3,length(r)/3);
