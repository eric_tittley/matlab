function DumpPositions(r,filename)


[N,dummy]=size(r);

DataFileP=fopen(filename,'wt');

for i=1:N
 fprintf(DataFileP,'%12.8f %12.8f %12.8f\n',r(i,1),r(i,2),r(i,3));
end

fclose(DataFileP);
