function field=readhydra_field(datafile,flabel)
%readhydra_field     Read in single fields from hydra data files
%
%   Syntax: field=readhydra_field(datafile,flabel)
% ARGUMENTS
%  datafile	The name of the file to be read.
%  flabel       The field. One of 'hdr','itype','rm','r','v','h','th','dn'.
%
% RETURNS
%  field	The field requested.  An [N] or [3,N] array or a structure.
%
% SEE ALSO
%  readhydra

% AUTHOR: Eric Tittley
%
% HISTORY
% 00 12 14 Modified to cope with new hydra data files that store the version
% 	information in the first block of the header.
% 00 12 22 Many changes to bring it in line with readhydra
% 01 08 29 Bug fix.  rcen was being treated as a single float, when in fact
%	it is a real(3).  The only consequence is that rmax2 was being ignored.
% 04 09 17 Call readhydra_header to read the header.
%	Removed support for versions < 4.0.
%	Moved error checks to prior to reading.
% 07 04 09 Return as the same type as read, to save memory.  Except itype,
%	which is now returned as an int4.
%
% TODO
%  BROKEN!  Fix.  Compare with readhydra.m

if(nargin~=2 )
 disp('syntax: field=readhydra_field(datafile,flabel)')
 return
end 

fid=fopen(datafile,'rb');
if(fid<=0)
 disp(['error in readhydra_field: unable to open file: ',datafile])
 return
end

% Read in the header
[hdr,Version]=readhydra_header(fid);

if(strcmp(flabel,'hdr'))
 fflag=8;
elseif(strcmp(flabel,'itype'))
 fflag=7;
elseif(strcmp(flabel,'rm'))
 if(Version==402)
  fclose(fid);
  error('File is Version 4.02, which has no mass per particle')
 end
 fflag=6;
elseif(strcmp(flabel,'r'))
 fflag=5;
elseif(strcmp(flabel,'v'))
 fflag=4;
elseif(strcmp(flabel,'h'))
 if(hdr.ngas==0)
  fclose(fid);
  error('File has no gas particles')
 end
 fflag=3;
elseif(strcmp(flabel,'th'))
 if(hdr.ngas==0)
  fclose(fid);
  error('File has no gas particles')
 end
 fflag=2;
elseif(strcmp(flabel,'dn'))
 if(hdr.ngas==0)
  fclose(fid);
  error('File has no gas particles')
 end
 fflag=1;
else
 fclose(fid);
 disp('flabel must be one of hdr, itype, rm, r, v, h, th, dn');
end

% header
field=hdr;

%itype
if( fflag<8 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,hdr.nobj,'*int');
end

if(Version~=402)
 % rm
 if( fflag<7 )
  clear field
  blocksize=fread(fid,8,'char');
  field=fread(fid,hdr.nobj,'*float');
 end
end

% r
if( fflag<6 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,[3,hdr.nobj],'*float');
end

% v
if( fflag<5 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,[3,hdr.nobj],'*float');
end

% h
if( fflag<4 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,hdr.ngas,'*float');
end

% th
if( fflag<3 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,hdr.ngas,'*float');
end

%dn
if( fflag<2 )
 clear field
 blocksize=fread(fid,8,'char');
 field=fread(fid,hdr.ngas,'*float');
end

fclose(fid);

