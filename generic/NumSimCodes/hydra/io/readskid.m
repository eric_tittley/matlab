function clusters=readskid(file)
% syntax: clusters=readskid(file);
%
% Returns the information for the clusters found by skid and reported in the
% file, file, via a structure array, clusters.
%
% clusters has two fields, clusters(i).N = the number of members
%                          clusters(i).members = the membership list [Nx1]
%
% The number of elements in the array is the number of clusters found

[fp,message]=fopen(file,'r');
if(fp==-1)
 disp(message)
 return
end

nobj=fscanf(fp,'%d',1);

index=fscanf(fp,'%d',nobj);

fclose(fp);

Nclus=max(index);

%preliminary loop to sort the clusters by size
for i=0:Nclus
 sizes(i+1)=sum(index==i);
end

[dummy,indx]=sort(sizes);
indx=flipud(indx');

for i=1:Nclus+1
 clusters(i).members=find(index==(indx(i)-1));
 clusters(i).N=sizes(indx(i));
end
