function handle=PlotFlow2(irun,start,fin,clump,Limits,Sample)
% syntax: PlotFlow2(irun,start,fin,clump,Limits,Sample)
if(nargin<5), Limits=[-0.2 0.2 -0.2 0.2 -0.05 0.05]; end

[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,start));
centre=mean(r(clump,:));
for j=1:3
 r_o_centred(:,j)=r(:,j)-centre(j);
end

if(nargin<6)
 SampleMask=1&rm;
else
 SampleMask=0*rm;
 SampleMask(Sample)=1&Sample;
end


hold on
for i=start+1:fin
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,i));
 centre=mean(r(clump,:));
 for j=1:3
  r_f_centred(:,j)=r(:,j)-centre(j);
 end
 dr=r_f_centred-r_o_centred;
 dist=sqrt( sum( dr'.^2) )';
 box=find( r_o_centred(:,1)>Limits(1) & r_f_centred(:,1)<Limits(2) ...
         & r_o_centred(:,2)>Limits(3) & r_f_centred(:,2)<Limits(4) ...
         & r_o_centred(:,3)>Limits(5) & r_f_centred(:,3)<Limits(6) ...
         & dist<0.5 & SampleMask);
 for j=1:length(box)
  k=box(j);
  line([r_o_centred(k,1) r_f_centred(k,1)],[r_o_centred(k,2) r_f_centred(k,2)]);
 end
 r_o_centred=r_f_centred;
end
axis(Limits(1:4))
%axis equal
axis square
hold off

mean_clump_h=mean(h(clump));
mask=0*h;
mask(clump)=1&clump;
mean_gas_h=mean(h(find(~mask)));
circle(2*mean_clump_h,0,0,'g');
circle(2*mean_gas_h,0,0,'r');

set(gca,'Xticklabel',[])
set(gca,'Yticklabel',[])
