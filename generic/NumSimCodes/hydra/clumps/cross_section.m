function [area,A,Res]=cross_section(r,rm,h,rho_cut)
% syntax: [area,A,Res]=cross_section(r,rm,h,rho_cut)
%
% area ( in units of (r units)^2) of cross-section defined by
% mass/area > rho_cut.
% If rho_cut is not defined, then the FWHM surface density is used.
% length(A)*Res is the dimension (r units) of an axis.
% sum(sum(A*Res^2))=sum(rm)

min_y=min(r(:,2)); max_y=max(r(:,2));
min_z=min(r(:,3)); max_z=max(r(:,3));
centre=mean(r);
half_box=1.5*max([centre(2)-min_y,max_y-centre(2),centre(3)-min_z,max_z-centre(3),2*max(h)]);
Limits=[centre(2)-half_box centre(2)+half_box centre(3)-half_box centre(3)+half_box];
%if(nargin==3)
% L=ceil(sqrt(length(r))*4);
 L=128;
 Res=2*half_box/L;
%else
% L=ceil(half_box*2/Res);
%end

[A,unres,res]=h_proj(r(:,2),r(:,3),rm,h,L,Limits); %sum(sum(A))=sum(rm)
A=A/Res^2; %A is in units of rm units per (r units)^2.
if(nargin==3) rho_cut=max(max(A))/2; end
area=sum(sum(A>rho_cut))*Res^2; % area is in units of (r units)^2.
