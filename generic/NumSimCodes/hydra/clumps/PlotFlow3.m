function handle=PlotFlow3(irun,start,fin,clump,Limits,Sample)
% syntax: PlotFlow3(irun,start,fin,clump,Limits,Sample)
if(nargin<5), Limits=[-0.2 0.2 -0.2 0.2 -0.05 0.05]; end

[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,start));
if(nargin<6)
 SampleMask=1&rm;
else
 SampleMask=0*rm;
 SampleMask(Sample)=1&Sample;
end

clf reset
hold on
for i=start:fin
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,i));
 centre=mean(r(clump,:));
 for j=1:3
  r_centred(:,j)=r(:,j)-centre(j);
 end
 box=find( r_centred(:,1)>Limits(1) & r_centred(:,1)<Limits(2) ...
         & r_centred(:,2)>Limits(3) & r_centred(:,2)<Limits(4) ...
         & r_centred(:,3)>Limits(5) & r_centred(:,3)<Limits(6) ...
         & SampleMask);
 plot(r_centred(box,1),r_centred(box,2),'k.','markersize',1)
end
axis(Limits(1:4))
%axis equal
axis square
hold off

mean_clump_h=mean(h(clump));
mask=0*h;
mask(clump)=1&clump;
mean_gas_h=mean(h(find(~mask)));
circle(2*mean_clump_h,0,0,'g');
circle(2*mean_gas_h,0,0,'r');

set(gca,'Xticklabel',[])
set(gca,'Yticklabel',[])
