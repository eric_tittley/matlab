% A set of routines used in analysing the drag felt by clumps of matter
%
% V_solve (uses V_func and V_diff_R)
%		Calculates the effective radius of a clump
%
% clump_size	The actual size of a clump from a series of iterations
%		of a simulation.
%
% cross_section	The cross-section of a clump
%
% mean_part_sep	The mean particle separation for a clump, or any other compact
%		distribution of particles.
%
% mean_rad	The mean radius of a clump.
%
% vel_indx_trunc
%		The velocity of a clump (or any other index set of particles)
%		from a series of iterations of a simulation. The sample is
%		truncated to exclude particles that have left the clump.
%
% velocity_index
%		The velocity of a clump (or any other index set of particles)
%		from a series of iterations of a simulation.
%
% Flow2		Finds the position of the surrounding medium in the reference
%		frame of the clump for a series of simulation timesteps
%
% PlotFlow	Plots the flow of particles around the clump as seen in the
%		reference frame of the clump
%
% PlotFlow2	Plots the flow of a given sample of particles around the clump
%
% PlotFlow3	Another varient on the same theme
