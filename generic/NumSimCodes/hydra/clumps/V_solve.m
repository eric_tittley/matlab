function R=V_solve(V_actual,time_actual,Rclump_o,to,Vo,M,rho_g,T,A,Z)
% R=V_solve(V_actual,time_actual,R_max,to,Vo,M,rho_g,T,A,[Z]);

%OPTIONS=optimset;
%R=fminbnd('V_diff_R',0,R_max,OPTIONS,V_actual,time_actual,to,Vo,M,rho_g);

OPTIONS=optimset('displ','off','maxiter',200,'tolx',Rclump_o/100,'tolfun',Vo/100);
if(nargin==9) % Neutral Gas
 R=fminsearch('V_diff_R',Rclump_o,OPTIONS,V_actual,time_actual,to,Vo,M,rho_g,T,A);
else %Plasma
 R=fminsearch('V_diff_R',Rclump_o,OPTIONS,V_actual,time_actual,to,Vo,M,rho_g,T,A,Z);
end
