function handle=PlotFlow(irun,start,fin,clump)
% syntax: PlotFlow(irun,start,fin,clump)
clf
%irun=2001;
%start=0; fin=19;
%clump=[1:100];
Limits=[-0.2 0.2 -0.2 0.2 -0.05 0.05];

[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,start));
r_f=r;

hold on
for i=start+1:fin
 r_o=r_f;
  [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,i));
 r_f=r;
 dr=r_f-r_o;
 centre=mean(r(clump,:));
 for j=1:3
  r_o_centred(:,j)=r_o(:,j)-centre(j);
  r_f_centred(:,j)=r_f(:,j)-centre(j);
 end
 box=find( r_o_centred(:,1)>Limits(1) & r_o_centred(:,1)<Limits(2) ...
         & r_o_centred(:,2)>Limits(3) & r_o_centred(:,2)<Limits(4) ...
         & r_o_centred(:,3)>Limits(5) & r_o_centred(:,3)<Limits(6) );
 handle=quiver2(r_o_centred(box,1),r_o_centred(box,2),dr(box,1),dr(box,2),2);
end
axis(Limits(1:4))
hold off
