function [V,speed,time,centres,T]=vel_indx_trunc(run,start,spacing,fin,index,rad)
%[V,speed,time,centres,T]=vel_indx_trunc(run,start,spacing,fin,index,rad)

% HISTORY
%  00 09 28
%   Flipped r and v from (N,3) to (3,N) form

disp('Initializing')
search=0.1; %search_radius
num=(fin-start)/spacing+1;
V=zeros(num,3);
centres=V;
time=0*[1:num]';
T=time;

disp('Starting main loop')
j=0;
for i=start:spacing:fin
 j=j+1;

% Read in the next datafile
 file=dataname(run,i);
 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(file);
 time(j)=hdr.time;
 r=r(:,index);
 v=v(:,index);
 dn=dn(index);
 th=th(index);
 approx_centre=mean(r');
 centre=refine_centres(r,dn,approx_centre,search);
 if(isempty(centre))
  search_tmp=search;
  while(isempty(centre))
   search_tmp=search_tmp*1.1;
   centre=refine_centres(r,dn,approx_centre,search_tmp);
  end
 end
 r_temp=rem(r+([1.5; 1.5; 1.5]-centre')*ones(1,length(index)),1)-0.5;
 dist=sqrt( sum( r_temp.^2 ) );
 core=find(dist < rad);

 centres(j,:)=centre;
 
% Find the velocities
 V(j,:)=mean(v(:,core)');
 
 T(j)=mean(th(core));

end

speed=sqrt(sum(V'.^2))';
