function [V,speed,time]=velocity_index(run,start,spacing,finish,index)
%[V,speed,time]=velocity_index(run,start,spacing,finish,index)

disp('Initializing')
num=(finish-start)/spacing+1;
V=zeros(num,3);
time=0*[1:num];
speed=time;

%Find the base filename
is=num2str(run);
if length(is)==3 base=['d0',is];
elseif length(is)==2 base=['d00',is];
elseif length(is)==1 base=['d000',is];
else base=['d',is];
end

disp('Starting main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 file=[base,'.',label];

 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,time(j),atime]=readdata(file,'l');

% Find the velocities
 V(j,1)=mean(v(index,1));
 V(j,2)=mean(v(index,2));
 V(j,3)=mean(v(index,3));

end

speed=sqrt(sum((V.*V)'));
