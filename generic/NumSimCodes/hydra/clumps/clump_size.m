function [times,sizes,ncold]=clump_size(run,start,spacing,finish,radius,T_cut,centre0)
%[times,sizes,ncold,]=clump_size(run,start,spacing,finish,radius,T_cut,centre0)
%

if(nargin~=7)
 error('Syntax: [times,sizes,ncold]=clump_size(run,start,spacing,finish,radius,T_cut,centre0)')
end

num=(finish-start)/spacing+1;
sizes=zeros(num,1);
times=sizes;
ncold=times;

j=0;
for i=start:spacing:finish
 j=j+1;

%Read in the data file
 filename=dataname(run,i);
 disp(filename)
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(filename);
 gas=find(itype);
 nobj=length(gas);

%Remember the time
 times(j)=time;

%Find the number that are 'cold'
 ncold(j)=sum(th(gas)<T_cut);

%Using the previous cluster centre, find the new cluster centre
 cold_gas=find(itype & th<T_cut);
 if(length(cold_gas)==0)
  disp('No cold gas found')
 else
  centre0=refine_centres(r(cold_gas,:),dn(cold_gas),centre0,10*radius);

%centre r_temp on the centre. It becomes (0,0,0)
  r_temp=rem(r(gas,:)+ones(nobj,1)*([1.5 1.5 1.5]-centre0),1)-0.5;

%find the objects within radius of the clump centre
  dist=sqrt( sum( (r_temp').^2 ) );
  sizes(j)=sum(dist < radius);

 end

end
