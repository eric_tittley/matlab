function M=makemovie_temp(run,start,spacing,finish)
% makemovie_temp   M=makemovie_temp(run,start,spacing,finish)
%  Makes a movie for ap3msph run # RUN.
%  Only gas particles are shown and they are
%  colour-coded according to temperature.  The movie
%  starts at iteration START spaced SPACING iterations
%  apart ending at FINISH.
%
%  See also: READDATA, DENSITY_PLOT_TOPHAT, DENSITY_PLOT_TYPE,
%            MAKEMOVIE

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;

xmin=0.4;  xmax=0.6;
ymin=xmin; ymax=xmax;
zmin=xmin; zmax=xmax;

% Load in the final state to determine the temperature
%  scale and the location of peak density
is=num2str(finish);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
disp('Reading in last data file to set things up')
[rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);
disp('Setting things up')

% Find all the gas particles in the box
gas=find(itype);

% We only want the gas particles
 r= r(gas,:);
dn=dn(gas);
th=th(gas);
%th=log(th(gas));

% Make the colour map. It will have num_shades shades.
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
max_temp=max(th);
min_temp=min(th);
temp_scale=num_shades/(max_temp-min_temp);
for i=1:num_shades+1
 bin(i)=min_temp+(i-1)/temp_scale;
end
%bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Find the point of highest density by averaging the location of the points
%  within %10 of the peak density.  Calculate the shift to add so that this
%  point lies at L/2,L/2,L/2
centre=centre_quick(r,dn,0);
shift=[1.5,1.5,1.5]-centre;

% Find the num_gas/256 gas particles that end up deepest in the core
distance=sqrt( (r(:,1)-centre(:,1)).^2+(r(:,2)-centre(:,2)).^2+(r(:,3)-centre(:,3)).^2 );
[dummy,core_objs]=sort(distance);
core_objs=core_objs(1:length(gas)/256); %core_objs is the indexed from the gas particles, only.

xwidth=xmax-xmin;
ywidth=ymax-ymin;
zwidth=zmax-zmin;
axes_vect=0.5+[-xwidth/2 xwidth/2 -ywidth/2 ywidth/2 -zwidth/2 zwidth/2];

disp('Starting to loop through files')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 disp(['reading file ',label])
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);

 disp('Isolating particles')
% Shift the points and extract the gas
 r=rem(r(gas,:)+(1&gas)*shift,1);

 r_core=r(core_objs,:);     %The core objects are indexed from all the gas particles so we must
 th_core=th(core_objs);% extract them before we find just the particles in the zoom box.

% We only want the particles in the box
 gas_mask=find(r(:,1)>xmin & r(:,1)<xmax & r(:,2)>ymin & r(:,2)<ymax & r(:,3)>zmin & r(:,3)<zmax);
 r=r(gas_mask,:);
 th=th(gas_mask);
 num_gas   =length(gas_mask);
 num_gas16 =num_gas/16;
 num_gas256=num_gas16/16;


%----------------------------------------
subplot(2,2,1)
% All gas particles
disp('Plotting 1')

% Plot the various temperatures
 Tmask=find(th>bin(2)&th<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th>bin(k)&th<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('All Gas Particles');

%----------------------------------------
subplot(2,2,3)
disp('Plotting 2')

% Find the top 1/16 in temp
 [dummy,rankings]=sort(th);
 r16 = r(rankings(num_gas-num_gas16+1:num_gas),:);
 th16=th(rankings(num_gas-num_gas16+1:num_gas)  );

% Plot the various temperatures
 Tmask=find(th16>bin(1)&th16<bin(2));
 fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th16>bin(k)&th16<bin(k+1));
  fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Top 1/16 Ngas in Temp')

%----------------------------------------
subplot(2,2,2)
disp('Plotting 3')

% Plot the core objects

% Plot the various temperatures
 Tmask=find(th_core>bin(2)&th_core<bin(3));
 fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th_core>bin(k)&th_core<bin(k+1));
  fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Core Objects');

%----------------------------------------
subplot(2,2,4)
disp('Plotting 4')

% Find the top 1/256 in temp
% [dummy16,rankings16]=sort(th16);
% th256=th16(rankings16(num_gas16-num_gas256+1:num_gas16)  );
% r256 = r16(rankings16(num_gas16-num_gas256+1:num_gas16),:);
 r256 = r(rankings(num_gas-num_gas256+1:num_gas),:);
 th256=th(rankings(num_gas-num_gas256+1:num_gas)  );

% Plot the various temperatures
 Tmask=find(th256>bin(1)&th256<bin(2));
 fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th256>bin(k)&th256<bin(k+1));
  fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Top 1/256 Ngas in Temp');

%----------------------------------------
 subplot(2,2,2)
% Label the time.
 disp('Plotting time')

 command=['text(.15,0.4,0,''',num2str(13.0386*time),''')'];
 eval(command)

 disp('Grabbing frame')
% Grab the frame for the movie
 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run0',num2str(run),'.zoom.gas.mpg'')']
eval(command)
