function M=makemovie_S_prof(run,start,spacing,finish)
% makemovie   M=makemovie_S_prof(run,start,spacing,finish)
%
% Plots T/Tad and T/rho^(2/3) (entropy, s, goes as log(T/rho^(2/3)) )
% T/Tad (top) S (bottom)

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;
centre=0;

AxisV=[1e-2 1e2 1e-3 1e5];

exps=2/3*(1&[1:121]');

j=num+2;
for i=finish:-spacing:start
 j=j-1;
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 command=['[centre,time]=centre_find(''data/d00',num2str(run),'.',label,''',centre);']
 eval(command)
 command=['!profile d00',num2str(run),'.',label,' ',num2str(centre(1)),' ',num2str(centre(2)),' ',num2str(centre(3)),' 0.00021 >rho.dat']
 eval(command)
 load rho.dat
 !del rho.dat

 h=loglog(rho(:,1),[rho(:,11),rho(:,10)./(rho(:,3).^exps)] );
 set(h,'LineWidth',2);
% if(AxisV) axis(AxisV);
% else AxisV=axis;
% end
 axis(AxisV);

 command=[        'text(20,0.2,''',num2str(13.0386*time),''');'];
 command=[command,'text(20,4500,''',num2str(centre(1)),''');'];
 command=[command,'text(20,1500,''',num2str(centre(2)),''');'];
 command=[command,'text(20, 500,''',num2str(centre(3)),''');'];
 command=[command,'title(''Run',num2str(run),': T/Tad & T/rho^(2/3)'');'];
 eval(command)

 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run00',num2str(run),'.S_prof.mpg'')']
eval(command)
