function M=quick_movie(run,start,spacing,finish,Limits)
% syntax: M=quick_movie(run,start,spacing,finish,Limits)
%
% Makes a movie, centring on the densest peak

if(nargin==4), Limits=[0.45 0.55 0.45 0.55 0.45 0.55]; end

num=(finish-start)/spacing+1;
M=moviein(num,gcf);

% Load in the final state to determine the temperature
%  scale and the location of peak density
disp('Reading in last file')
filename=dataname(run,finish);
[rm,r,v,th,itype,dn,h,hdr]=readdhydra(filename);

disp('Finding centre')
centre=centre_quick(r,dn,0);

disp('Centring')
shift=[1.5,1.5,1.5]-centre;
r=rem(r+(1&itype)*shift,1);

disp('Isolating')
clus=r(:,1)>Limits(1) & r(:,1)<Limits(2) & r(:,2)>Limits(3) & r(:,2)<Limits(4) & r(:,3)>Limits(5) & r(:,3)<Limits(6);
clus_dark=find((~itype)& clus);
clus_gas =find(itype&clus);

disp('Plotting')
subplot(1,2,1)
h=plot3(r(clus_dark,1),r(clus_dark,2),r(clus_dark,3),'.');
axis(axes_vect);
title('Dark Matter')
set(h,'MarkerSize',1);

subplot(1,2,2)
h=plot3(r(clus_gas,1),r(clus_gas,2),r(clus_gas,3),'.');
axis(axes_vect);
title('Gas')
set(h,'MarkerSize',1);

% Label the time.
disp(['Labelling the time to ',num2str(13.0386*time),' Ga'])
text(centre(1)-.14,centre(2)+.00,centre(3)-0.04,[num2str(13.0386*time),' Ga']);

% Grab the frame for the movie
M(:,num+1)=getframe(gcf);

j=num+1;
for i=finish-spacing:-spacing:start
 j=j-1;

% Read in the next datafile
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end

 disp(['Reading file ',label])
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);

 disp('Finding the centre')
 centre=centre_quick(r,dn,centre);

 disp('Centring')
 shift=[1.5,1.5,1.5]-centre;
 r=rem(r+(1&itype)*shift,1);

 disp('Isolating')
 clus=r(:,1)>0.45 & r(:,1)<0.55 & r(:,2)>0.45 & r(:,2)<0.55 & r(:,3)>0.45 & r(:,3)<0.55;
 clus_dark=find((~itype)&clus);
 clus_gas =find(  itype &clus);

 disp('Plotting')
 subplot(1,2,1)
 h=plot3(r(clus_dark,1),r(clus_dark,2),r(clus_dark,3),'.');
 axis(axes_vect);
 set(h,'MarkerSize',1);
 title('Dark Matter')

 subplot(1,2,2)
 h=plot3(r(clus_gas,1),r(clus_gas,2),r(clus_gas,3),'.');
 axis(axes_vect);
 set(h,'MarkerSize',1);
 title('Gas')

% Label the time.
 disp(['Labelling the time to ',num2str(13.0386*time),' Ga'])
 text(centre(1)-.14,centre(2)+.00,centre(3)-0.04,[num2str(13.0386*time),' Ga']);

% Grab the frame for the movie
 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run0',num2str(run),'.clus.mpg'')']
eval(command)
