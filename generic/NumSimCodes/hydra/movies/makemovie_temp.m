function M=makemovie_temp(run,start,spacing,finish)
% makemovie_temp   M=makemovie_temp(run,start,spacing,finish)
%  Makes a movie for ap3msph run # RUN.
%  Only gas particles are shown and they are
%  colour-coded according to temperature.  The movie
%  starts at iteration START spaced SPACING iterations
%  apart ending at FINISH.
%
%  See also: READDATA, DENSITY_PLOT_TOPHAT, DENSITY_PLOT_TYPE,
%            MAKEMOVIE

disp('initializing')

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;

xmin=0;  xmax=1;
ymin=xmin; ymax=xmax;
zmin=xmin; zmax=xmax;
axes_vect=[xmin xmax ymin ymax zmin zmax];


% Load in the final state to determine the temperature
%  scale and the location of peak density
disp('Loading in final state');
is=num2str(finish);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
file=['d0',num2str(run),'.',label]
[rm,r,v,th,itype,dn,h,time,atime]=readdata(file);

% Find all the gas particles
disp('Finding the gas particles')
gas_mask=find(itype);
num_gas   =length(gas_mask);
num_gas16 =num_gas/16;
num_gas256=num_gas16/16;

% Make the colour map. It will have num_shades shades.
disp('Setting up the colour map')
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
max_temp=max(th);
temp_scale=num_shades/max_temp;
for i=1:num_shades
 bin(i)=(i-1)*max_temp/num_shades;
end
bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Find the point of highest density by averaging the location of the points
%  within %10 of the peak density.  Calculate the shift to add so that this
%  point lies at L/2,L/2,L/2
disp('Finding the location of the highest density')
centre=centre_quick(r,dn,0);
shift=[1.5,1.5,1.5]-centre;

% Find the num_gas/16 gas particles that end up deepest in the core
disp('Finding the core objects')
distance=sqrt( (r(gas_mask,1)-centre(:,1)).^2+(r(gas_mask,2)-centre(:,2)).^2+(r(gas_mask,3)-centre(:,3)).^2 );
[dummy,core_objs]=sort(distance);
core_objs=core_objs(1:num_gas/16); %core_objs is the indexed from the gas particles, only.

disp('Starting the main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 disp(' Reading in the datafile')
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 file=['d0',num2str(run),'.',label]
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(file);

% We only want the gas particles
 disp(' Isolating the gas and centring')
 r=r(gas_mask,:);
 th=th(gas_mask);

% Shift the points
 r=rem(r+(1&gas_mask)*shift,1);

%----------------------------------------
subplot(2,2,1)
% All gas particles
 disp(' Plotting all the gas particles')

% Plot the various temperatures
 Tmask=find(th>bin(2)&th<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th>bin(k)&th<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('All Gas Particles');

%----------------------------------------
subplot(2,2,3)
 disp(' Plotting the hottest particles')
% Find the top 1/16 in temp
 [dummy,rankings]=sort(th);
 r16 = r(rankings(num_gas-num_gas16+1:num_gas),:);
 th16=th(rankings(num_gas-num_gas16+1:num_gas)  );

% Plot the various temperatures
 Tmask=find(th16>bin(1)&th16<bin(2));
 fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th16>bin(k)&th16<bin(k+1));
  fig=plot3(r16(Tmask,1),r16(Tmask,2),r16(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Top 1/16 Ngas in Temp')

%----------------------------------------
subplot(2,2,2)
 disp(' Plotting the core particles')

% Plot the core objects
 r_core= r(core_objs,:);
th_core=th(core_objs,:);

% Plot the various temperatures
 Tmask=find(th_core>bin(2)&th_core<bin(3));
 fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=3:num_shades
  Tmask=find(th_core>bin(k)&th_core<bin(k+1));
  fig=plot3(r_core(Tmask,1),r_core(Tmask,2),r_core(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Core Objects');

%----------------------------------------
subplot(2,2,4)
 disp(' Plotting the very hottest particles')

% Find the top 1/256 in temp
 [dummy16,rankings16]=sort(th16);
 th256=th16(rankings16(num_gas16-num_gas256+1:num_gas16)  );
 r256 = r16(rankings16(num_gas16-num_gas256+1:num_gas16),:);

% Plot the various temperatures
 Tmask=find(th256>bin(1)&th256<bin(2));
 fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=2:num_shades
  Tmask=find(th256>bin(k)&th256<bin(k+1));
  fig=plot3(r256(Tmask,1),r256(Tmask,2),r256(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Top 1/256 Ngas in Temp');

%----------------------------------------
% Label the time.
 disp(' Labelling the time')
 text(-1,0.5,num2str(13.0386*time));

% Grab the frame for the movie
 disp(' Grabbing the frame')
 M(:,j)=getframe(gcf);
end

disp('End of main loop, making movie')
map=colormap;
mpgwrite(M,map,['run0',num2str(run),'.gas_temp.mpg'])
