function M=makemovie_shock(run,start,spacing,finish)
% makemovie_shock_temp   M=makemovie_shock(run,start,spacing,finish)
%
%  See also: READDATA, DENSITY_PLOT_TOPHAT, DENSITY_PLOT_TYPE,
%            MAKEMOVIE

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;

%----------------------------------
% Load in the original state to determine the T_o and Rho_o
is=num2str(start);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
[rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);

gas_mask=find(itype);

% Find T_o
T_o=mean(th(gas_mask));

% Find Rho_o
Rho_o=mean(dn(gas_mask));

%Gamma
gamma=5/3*(1&[1:length(gas_mask)])';
%----------------------------------

% Load in the final state to determine the temperature
%  scale and the location of peak density
is=num2str(finish);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
[rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);

% Find all the gas particles
num_gas   =length(gas_mask);
num_gas16 =num_gas/16;
num_gas256=num_gas16/16;

% We only want the gas particles
 r= r(gas_mask,:);
dn=dn(gas_mask);
th=th(gas_mask);

%-------------------------------

% Make the colour map. It will have num_shades shades.
num_shades=64;
colormap(hot(num_shades/2));
map1=colormap;
colormap(cool(num_shades/2));
map2=colormap;
map=[map1;map2];
max_temp=max(th);
temp_scale=num_shades/max_temp;
for i=1:num_shades
 bin(i)=(i-1)*max_temp/num_shades;
end
bin(num_shades+1)=max_temp*100; %We'll put all the big temps in one bin.

% Colourmap scaling between temperature and T/Tad
Cs=th./(T_o*(dn/Rho_o).^(gamma-1));
C_scale=max_temp/max(Cs);

%-----------------------------

% Find the point of highest density by averaging the location of the points
%  within %10 of the peak density.  Calculate the shift to add so that this
%  point lies at L/2,L/2,L/2
peakmask=find(dn>(max(dn)*0.9));
[MM,NN]=size(peakmask);
if(MM==1) centre=(1&gas_mask)*r(peakmask,:);
else centre=(1&gas_mask)*mean(r(peakmask,:));
end
shift=(1&gas_mask)*[L/2+L,L/2+L,L/2+L]-centre;

%----------------------------

axes_vect=[0 L 0 L 0 L];
%axes_vect=[L/2-4 L/2+4 L/2-4 L/2+4 L/2-4 L/2+4];

%----------------------------

j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(['d0',num2str(run),'.',label]);

% We only want the gas particles
 r=r(gas_mask,:);
 th=th(gas_mask);
 dn=dn(gas_mask);

% Shift the points
 r=rem(r+shift,L);

%----------------------------------------
subplot(1,2,1)
% Actual Gas Temperature

% Plot the various temperatures
 Tmask=find(th>bin(2)&th<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=4:num_shades %Don't plot the bins for 1-3, since they are too dark
  Tmask=find(th>bin(k)&th<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Actual Temperature');

%----------------------------------------
subplot(1,2,2)
% Degree of shockedness (T/Tadiabatic-1)
Cs=th./(T_o*(dn/Rho_o).^(gamma-1))*C_scale;

% Plot the various temperatures
 Tmask=find(Cs>bin(2)&Cs<bin(3));
 fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
 set(fig,'Color',map(1,:));
 axis(axes_vect);
 hold on
 for k=4:num_shades %Don't plot the bins for 1-3, since they are too dark
  Tmask=find(Cs>bin(k)&Cs<bin(k+1));
  fig=plot3(r(Tmask,1),r(Tmask,2),r(Tmask,3),'.');
  set(fig,'Color',map(k,:));
 end
 hold off
 title('Shocked degree');

%----------------------------------------
% Label the time.
 subplot(1,2,2)
 command=['text(-20,20,0,''',num2str(13.0386*time),''')'];
 eval(command)

% Grab the frame for the movie
 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run00',num2str(run),'.shock.gas.mpg'')']
eval(command)
