% Create an AVI movie from a scaled image array
%
% M=images2movies(A,time,moviename)
%
% ARGUMENTS
%  A          Series N images stored in an [Nx,Ny,N] array
%  time       Vector of times to be displayed for each frame
%  moviename  The movie file to generate
%
% RETURNS
%  M          Handle to the Matlab movie frames
%
% SEE ALSO
%  h_proj_movie h_proj_multi movie2avi

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: matlab, octave?
%
% HISTORY
%  130212 mpegwrite deprecated. Use movie2avi

function M=images2movies(A,time,moviename)

%movie frames
[Nx,Ny,N]=size(A);
M=moviein(N,gcf);

disp(' Plotting')
for i=1:N
 image(A(:,:,i))
 set(gca,'xticklabel',[]), set(gca,'yticklabel',[])

 if(~isempty(time) ) 
  % Label the time.
  text(0,-5,[moviename,' Time:',num2str(time(i))]);
 end

 % Grab the frame for the movie
 M(:,i)=getframe(gcf);
end

map=colormap;
movie2avi(M,moviename)
