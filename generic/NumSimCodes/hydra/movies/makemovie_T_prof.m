function M=makemovie_T_prof(run,start,spacing,finish)
% makemovie   M=makemovie_T_prof(run,start,spacing,finish)
%
% Plots Th and Th/Tad for the datafile ./data/Run##

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;
centre=0;

%AxisV=0;
AxisV=[1e-2 1e2 1e-2 1e5];

j=num+2;
for i=finish:-spacing:start
 j=j-1;
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 command=['[centre,time]=centre_find(''data/d00',num2str(run),'.',label,''',centre);']
 eval(command)
 command=['!profile d00',num2str(run),'.',label,' ',num2str(centre(1)),' ',num2str(centre(2)),' ',num2str(centre(3)),' 0.00021 >rho.dat']
 eval(command)
 load rho.dat
 !del rho.dat

 h=loglog(rho(:,1),rho(:,10:11));
 set(h,'LineWidth',2);
% if(AxisV) axis(AxisV);
% else AxisV=axis;
% end
axis(AxisV);

 command=[        'text(0.05,0.1,''',num2str(13.0386*time),''');'];
 command=[command,'text(20,45000,''',num2str(centre(1)),''');'];
 command=[command,'text(20,15000,''',num2str(centre(2)),''');'];
 command=[command,'text(20, 5000,''',num2str(centre(3)),''');'];
 eval(command)

 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run00',num2str(run),'.T_prof.mpg'')']
eval(command)
