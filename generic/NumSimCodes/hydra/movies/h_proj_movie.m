function [A,t]=h_proj_movie(run,start,spacing,finish,resolution,Limits,Format)
% [A,t]=h_proj_movie(run,start,spacing,finish,resolution,Limits,Format);
%
% Compute the image slices of a hydra simulation in which the density, smoothed
% over the smoothing radius, is projected and summed onto the X-Y plane.
%
% The run number, irun, is given by RUN.  Output times start at START,
% are spaced every SPACING, and end at FINISH.  The size of the image
% for each frame is given by RESOLUTION.  The Limits for the view in the
% X-Y plane are given by LIMITS.  Limits of [0 1 0 1] encompass the whole
% simulation box.
%
% Format (Optional): Either of the strings: 'rm', 'T', 'Lx'
%	If 'rm' (default), then the projected mass is determined.
%	If 'T', then the projected mean temperature is determined.
%	If 'Lx', then the projected X-ray luminosity is determined.
%
% BUGS
%  The projected Lx is determined by the free-free emission approximation
%  that Lx \propto rho^2 T^(1/2), which ignored line emission.

% HISTORY
%  01-08-15 Mature version
%  01-08-15 Added the ability to choose things other than 'rm' to project.
%  01-08-31 Check for gas using 'itype==1' instead of 'itype' since itype==-2
%	for `lost' particles.
%  02-01-29 Added support for rm-dark

if(nargin==6) Format='rm'; end
if(  ~ strcmp(Format,'rm') ...
   & ~ strcmp(Format,'T')  ...
   & ~ strcmp(Format,'Lx') ...
   & ~ strcmp(Format,'rm-dark') ) 
 error('Format must be one of ''rm'', ''T'', ''Lx'', or ''rm-dark'' ')
end

%movie frames
num=(finish-start)/spacing+1;
A=zeros(resolution,resolution,num);

j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the datafile
 file=dataname(run,i);
 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(file);
 L=Limits;
 gas=find(itype'==1 & r(1,:)>L(1) & r(1,:)<L(2) & r(2,:)>L(3) & r(2,:)<L(4) & r(3,:)>L(5) & r(3,:)<L(6) );

% Make this a switch-case construct
 if(strcmp(Format,'rm'))
  [A(:,:,j),unresolved,resolved]=h_proj(r(1,gas),r(2,gas),rm(gas),h(gas),resolution,Limits);
 elseif(strcmp(Format,'T'))
  [T,unresolved,resolved]=h_proj(r(1,gas),r(2,gas),th(gas),h(gas),resolution,Limits);
  [M,unresolved,resolved]=h_proj(r(1,gas),r(2,gas),rm(gas),h(gas),resolution,Limits);
  dummy=0*T;
  dummy(M~=0)=T(M~=0)./M(M~=0);
  A(:,:,j)=dummy;
 elseif(strcmp(Format,'Lx'))
  [Lx,unresolved,resolved]=h_proj(r(1,gas),r(2,gas),dn(gas).^2.*th(gas).^0.5,h(gas),resolution,Limits);
  [M,unresolved,resolved]=h_proj(r(1,gas),r(2,gas),rm(gas),h(gas),resolution,Limits);
  dummy=0*Lx;
  dummy(M~=0)=Lx(M~=0)./M(M~=0);
  A(:,:,j)=dummy;
 elseif(strcmp(Format,'rm-dark'))
  dark=find(~itype);
  if( ~ exist([file,'.densities']) )
   error(['need the file: ',file,'.densities'])
  end
  [gasdn,darkdn,gash,darkh]=readdensities([file,'.densities']);
  % Note: calcdens only processes particles that are within [0,1]
  dark=find(itype'==0 & r(1,:)>=0 & r(1,:)<=1 & r(2,:)>=0 & r(2,:)<=1 & r(3,:)>=0 & r(3,:)<=1 );
  r=r(:,dark);
  rm=rm(dark);
  % Now there is a 1-to-1 relation between the r's and the darkh's
  list=find(r(1,:)>L(1) & r(1,:)<L(2) & r(2,:)>L(3) & r(2,:)<L(4) & r(3,:)>L(5) & r(3,:)<L(6) );
  [A(:,:,j),unresolved,resolved]=h_proj(r(1,list),r(2,list),rm(list),darkh(list),resolution,Limits);
 else
  error('Format must be one of ''rm'', ''T'', ''Lx'', or ''rm-dark'' ')
 end
 t(j)=hdr.time;
end

