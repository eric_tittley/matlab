function M=simple_movie(run,start,spacing,finish,Limits)
%M=simple_movie(run,start,spacing,finish,Limits)

disp('Initializing')
num=(finish-start)/spacing+1;
M=moviein(num,gcf);

disp('Starting main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 file=dataname(run,i);

 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(file);

 disp('Plotting')
 h=plot(r(:,1),r(:,2),'.');
 set(h,'markersize',1);
 axis(Limits)

% Label the time.
 disp('Labelling the time.')
 text(0,1.02,num2str(hdr.time));

% Grab the frame for the movie
 disp('Grabbing the frame.')
 M(:,j)=getframe(gcf);
end

disp('Compiling the movie into MPEG.');
map=colormap;
command=['mpgwrite(M,map,''run',num2str(run),'.mpg'')']
eval(command)
