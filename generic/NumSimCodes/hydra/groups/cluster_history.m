function [history,time]=cluster_history(irun,start,step,fin,members,nobj)
% syntax: [history,time]=cluster_history(irun,start,step,fin,members,nobj)
%
% Finds the clustering history of the particles, members.
% The clustering history is a table of what clusters the particles
% indicated by the particle list, members, are in for each of the
% datafiles for run number irun, with output times spanning start:step:fin.
% The times of these output files are returned in time.
%
% The table, history, is an NxM array with N = number of output files
% and M = maximum number of groups in each of the output times.
% Each row corresponds to one output time.  Starting from column 1,
% each row lists the clusters in which the particles listed in members
% are found.  The rest of the column is padded with zeros.
% There is a bit of degeneracy, here, since cluster 0 is the list of unbound
% particles, which some particles are likely to be found.  This cluster is
% always listed as the first cluster.
%
% The clusters for each output time are found and numbered using
% SKID, so their really doesn't have to be a correlation between the 
% group numbers for each timestep.  I.e., cluster 34 in one timestep
% can be cluster 21 in another.

MAX_NUM_GRPS=1000;
GRP_FRACTION=0.5;

members_mask=zeros(nobj,1);
members_mask(members)=ones(length(members),1);

num_iters=(fin-start)/step+1;
history1=zeros(num_iters,MAX_NUM_GRPS);
time=zeros(num_iters,1);

k=0;
for i=start:step:fin
 k=k+1
 filename=skidname(irun,i);
 grp_indx=readgroups(filename);
 grps=grps_from_list(grp_indx,members);
% Make sure each of these groups contributes more that GRP_FRACTION 
% of themselves to the final group
 for ii=1:length(grps)
  grp_mask=grps(ii)==grp_indx;
  num_in_group=sum(grp_mask);
  num_in_members=sum(members_mask&grp_mask);
  if(num_in_members/num_in_group<GRP_FRACTION), grps(ii)=-1; end
 end %ii
 good=find(grps>-1);
% add the group list to the history table
 time(k)=i;
 if(length(good)<1001)
  history1(k,1:length(good))=grps(good);
 else
  history1(k,:)=grps(good(1:1000));
  disp('Too many groups found in iteration',num2str(i))
  disp('Increase MAX_NUM_GRPS in cluster_history.m')
 end %if
end %i

mask=sum(history1);
max_n_grps=max(find(mask));
history=history1(:,1:max_n_grps);
