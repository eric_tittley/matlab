function M=clustering_movie(irun,start,spacing,fin,history,Limits)
% syntax: M=clustering_movie(irun,start,spacing,fin,history,Limits)
%
% Creates a quick movie, M, of the clustering for the cluster formed
% according to the history given by history.
%
% Limits is a vector giving the plotting limits: [Xlo Xhi Ylo Yhi]
%
% see also: cluster_history

data_prefix='data/';

% Initialize the figure window and the movie matrix
num=(fin-start)/spacing+1;
fh=figure;
position=get(fh,'position');
position(3)=500; position(4)=500;
set(fh,'position',position);
M=moviein(num,gcf);

%find the centre of the last cluster
disp('Finding the centre of the cluster')
filename=skidname(irun,fin);
grp_indx=readgroups(filename);
grps=history(num,find(history(num,:)));
members=[];
for ii=1:length(grps)
 members=[members;grp_members(grp_indx,grps(ii))];
end
filename=[data_prefix,dataname(irun,fin)];
[rm,r,v,th,itype,dn,h,hdr]=readhydra(filename);
centre=mean(r(members,:))

% main loop
disp('starting main loop')
k=0;
for i=start:spacing:fin
 k=k+1;
% read in the group file
 filename=skidname(irun,i);
 disp(filename)
 grp_indx=readgroups(filename);
% find the list of particles that are in the clusters numbered in the history
 grps=history(k,find(history(k,:)));
 members=[];
 for ii=1:length(grps)
  new_members=grp_members(grp_indx,grps(ii));
  members=[members;new_members];
 end
% read in the datafile
 filename=[data_prefix,dataname(irun,i)];
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(filename);
% recentre
 r=recentre(r,centre);
% plot the particles
 plot(r(members,1),r(members,2),'.')
 axis(Limits)
% grab the frame
 M(:,k)=getframe(fh);
end
