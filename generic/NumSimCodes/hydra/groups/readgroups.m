function grp_indx=readgroups(file)
% syntax: grp_indx=readgroups(file)
%
% reads the group file produced by SKID, returning the group index, grp_indx
%
% See also grp_members

fp=fopen(file,'r');
nobj=fscanf(fp,'%d',1);
grp_indx=fscanf(fp,'%d',nobj);
fclose(fp);
