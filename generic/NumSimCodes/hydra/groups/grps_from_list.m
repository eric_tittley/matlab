function grps=grps_from_list(grp_indx,list)
% syntax: grps=grps_from_list(grp_indx,list)
%
% returns a list of grps for which the particles in list are members,
% where the grp_indx (produced by SKID and read in by readgroups) is the
% index to the particle groups.
%
% see also readgroups, grp_list, grp_members

n_grps=max(grp_indx);
[n,m]=hist(grp_indx(list),[0:n_grps]);
grps=m(n~=0);
