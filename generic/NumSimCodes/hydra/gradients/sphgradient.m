function [r,dn_o,bins]=sphgradient(F,Nslices,Nparticles,filename)
% syntax: [r,dn_o,bins]=sphgradient(F,Nslices,Nparticles,[filename])
%
% Creates a hydra datavolume (optionally output to filename)
% with Nslices slices each with Nparticles particles with the
% density ration between slices of F.
%
% Essentially, this is a density profile with rho = rho_o r^-1.
%
% Returned are the particle positions and the density per slice, dn.
% Note, dn will be a column vector of length Nslices.
% bins is a Nslice x 2 array with the first column containing the
% lower boundary of the slice, in the x-dir, and the 2nd column the
% upper boundary.

% Find the width of the first slice
sumF=0;
for i=0:Nslices-1
 sumF=sumF+F^i;
end
W1=1/sumF;

% Create the volume
x=zeros(Nparticles,3);
r=zeros(Nparticles*Nslices,3);
dn_o=zeros(Nslices,1);
bins=zeros(Nslices,2);
lower_edge=0;
W=W1/F;
for i=0:Nslices-1
 W=W*F;
 bins(i+1,1)=lower_edge;
 bins(i+1,2)=lower_edge+W;
 x(:,1)=rand([Nparticles,1]);
 x(:,2)=rand([Nparticles,1]);
 x(:,3)=rand([Nparticles,1]);
 r(i*Nparticles+1:(i+1)*Nparticles,1)=lower_edge+W*x(:,1);
 r(i*Nparticles+1:(i+1)*Nparticles,2)=x(:,2);
 r(i*Nparticles+1:(i+1)*Nparticles,3)=x(:,3);
 lower_edge=lower_edge+W;
 dn_o(i+1)=Nparticles/W;
end

% write output to file
if(nargin==4)

hdr.times=zeros(1,200);
hdr.times(1)=1;
% ibuf1
hdr.itime    =0;
hdr.itstop   =1;
hdr.itdump   =0;
hdr.itout    =0;
hdr.time     =0.001;
hdr.atime    =0;
hdr.htime    =0;
hdr.dtime    =0;
hdr.Est      =0;
hdr.T        =0;
hdr.Th       =0;
hdr.U        =0;
hdr.Radiation=0;
hdr.Esum     =0;
hdr.Rsum     =0;
% ibuf2
hdr.irun     =0;
hdr.nobj     =Nparticles*Nslices;
hdr.ngas     =hdr.nobj;
hdr.ndark    =0;
hdr.L        =0;
hdr.intl     =0;
hdr.nlmx     =0;
hdr.perr     =0;
hdr.dtnorm   =0;
hdr.sft0     =0;
hdr.sftmin   =0;
hdr.sftmax   =0;
hdr.h100     =1;
hdr.box      =1;
hdr.zmet     =0;
hdr.spc0     =0;
hdr.lcool    =0;
hdr.rmgas    =1;
hdr.rmdark   =0;
hdr.rmnorm   =0;
hdr.tstart   =0;
hdr.omega0   =0;
hdr.xlambda0 =0;
hdr.h0t0     =0;
% Arrays
rm=ones(hdr.nobj,1);
% r is already defined
v=0*r;
h=0.01*rm;
e=0*rm;
itype=rm;
dn=e;

writehydra(filename,rm,r,v,e,itype,dn,h,hdr);

end
