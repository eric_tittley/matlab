function [span,dn_mean_vol,dn_mean_mass,dn_std,h_mean]=meanSPHprofile(bins,r,dn,h);
% syntax: [span,dn_mean_vol,dn_mean_mass,dn_std,h_mean]=meanSPHprofile(bins,r,dn,h);
%
% Finds the mean density (dn_mean+-dn_std) of the particles with 
% positions r and densities dn using the bins established in bins.
% We need h to get a volume-weighted average.  A mass-weighted average
% will always overestimate the mass.
%
% see also: gradient

% get the number of bins
[Nbins,m]=size(bins);
if(m~=2)
 error('mean_profile: arg 1 must be a [nx2] matrix')
end

% initialize the arrays
dn_mean_vol=0*bins(:,1);
dn_mean_mass=dn_mean_vol;
dn_std =dn_mean_vol;
h_mean=dn_mean_vol;

% bin the particles
for i=1:Nbins
 span=find(r(:,1)>bins(i,1) & r(:,1)<=bins(i,2));
 total_density=sum(dn(span).*(h(span).^3));
 total_volume=sum(h(span).^3);
 dn_mean_vol(i)=total_density/total_volume;
 dn_mean_mass(i)=mean(dn(span));
 dn_std(i) = std(dn(span));
 h_mean(i)=mean(h(span));
end

% Using mean h in each of the bins, exclude bins for which
% the particles can see beyond the box boundary
span=find( bins(:,1)>h_mean & bins(:,2) < (1-h_mean) );
dn_mean_vol =dn_mean_vol(span);
dn_mean_mass=dn_mean_mass(span);
dn_std      =dn_std(span);
h_mean      =h_mean(span);
