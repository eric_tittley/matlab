function [r,dn_o,bins]=constgrad(Width,F,Nslices,filename)
%syntax: [r,dn_o,bins]=constgrad(Width,F,Nslices,filename);
%
% Creates a constant density gradient with Nslices and n=F*(x/Width) over the
% range [0:Width].

W=1/Nslices; % the width
bins=[0:W:1-W]+W/2; % the bin centres
Nperbin=floor(F*bins);
dn_o=Nperbin/W;
nobj=sum(Nperbin);
r=zeros(nobj,3);

count=0;
for i=1:Nslices
 x1=rand([Nperbin(i),1]);
 x2=rand([Nperbin(i),1]);
 x3=rand([Nperbin(i),1]);
 r(count+1:count+Nperbin(i),1)=bins(i)-W/2+W*x1;
 r(count+1:count+Nperbin(i),2)=x2;
 r(count+1:count+Nperbin(i),3)=x3;
 count=count+Nperbin(i);
end

r(:,1)=r(:,1)*Width;
bins=bins*Width;
dn_o=dn_o/Width;

% write output to file
if(nargin==4)

hdr.times=zeros(1,200);
hdr.times(1)=1;
% ibuf1
hdr.itime    =0;
hdr.itstop   =1;
hdr.itdump   =0;
hdr.itout    =0;
hdr.time     =0.001;
hdr.atime    =0;
hdr.htime    =0;
hdr.dtime    =0;
hdr.Est      =0;
hdr.T        =0;
hdr.Th       =0;
hdr.U        =0;
hdr.Radiation=0;
hdr.Esum     =0;
hdr.Rsum     =0;
% ibuf2
hdr.irun     =0;
hdr.nobj     =nobj;
hdr.ngas     =hdr.nobj;
hdr.ndark    =0;
hdr.L        =0;
hdr.intl     =0;
hdr.nlmx     =0;
hdr.perr     =0;
hdr.dtnorm   =0;
hdr.sft0     =0;
hdr.sftmin   =0;
hdr.sftmax   =0;
hdr.h100     =1;
hdr.box      =1;
hdr.zmet     =0;
hdr.spc0     =0;
hdr.lcool    =0;
hdr.rmgas    =1;
hdr.rmdark   =0;
hdr.rmnorm   =0;
hdr.tstart   =0;
hdr.omega0   =0;
hdr.xlambda0 =0;
hdr.h0t0     =0;
% Arrays
rm=ones(hdr.nobj,1);
% r is already defined
v=0*r;
h=0.01*rm;
e=0*rm;
itype=rm;
dn=e;

writehydra(filename,rm,r,v,e,itype,dn,h,hdr);

end
