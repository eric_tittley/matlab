function time=cooling_time2(cunit,dn,t)
%syntax: time=cooling_time2(cunit,dn,t)
%
% Finds the cooling time for the particles with numberdensity dn and
% temperature (in K), t.
% cunit=nunit*tunit (yes, numberdentsity units times time unit)

[ttab,ctab]=cooltab(cunit);

c1=interp1(ttab,ctab,t,'linear');
time=c1./dn;
