function [L,mag]=cluster_L(centre,r,v,mass,cutoff)
% L=cluster_L(centre,r,v,mass)
%
% Returns the total angular momentum of the cluster particles
% centred on centre with positions, r, velocities, v,
% and masses mass.  Only those particles within the distance
% given by cutoff are included.  If cutoff is undefined, all
% particles are used.

% centre all the particles on the cluster centre
centres=([1:length(mass)]&1)'*centre;
r=rem(r-centres+1.5,1)-0.5;

if(nargin==5)
 %isololate the cluster members
 mask=find(sum(r'.^2)<cutoff);
 r=r(mask,:);
 v=v(mask,:);
 mass=mass(mask);
end

%Find the momentum
m=mass*[1,1,1];
p=v.*m;

%Find Angular momentum of each particle
Ls=cross(r',p');

%Find the total angular momentum
L=sum(Ls');
mag=sqrt(L(1)^2+L(2)^2+L(3)^2);
