% A collection of tools to analyse the hydra data directly
%
%  BoostResolution	Boost the resolution of a hydra data set.
% 
%  ExtractPartParam	Extracts a parameter for an individual particle
%			from a series of simulation timesteps
% 
%  SigmaSpec		Sigma (the density variation) on a variety
%			of scales by randomly placing spheres and finding the
%			mean density.
% 
%  Units		Sets a pile of useful constants.  Requires hdr.h100 
%			already be set.
% 
%  baryon_dist		The baryon density distribution.
% 
%  cluster_L		Total angular momentum of a cluster.
% 
%  cool			The cooling table as it is implemented in hydra.
% 
%  cooltab		The cooling table as it is implemented in hydra.
% 
%  cooling_time2	The cooling time for a gas using the hydra cooling table
%  
%  dTdt			The cooling time for a gas using cooltab
% 
%  cooling_time		The cooling time for a gas assuming
%			t=2.6e-13*sqrt(T)/rho
%
%  tau_anal		The semi-analytic value of tau (see my thesis)
%
%  Lx			The x-ray luminosity of a gas.
%
%  RhoToNeNi		Determine ne and ni for a fully ionized plasma.
