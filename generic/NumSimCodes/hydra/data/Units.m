% Units: Basic astrophysical units
%
% syntax: Units
%
% Sets:
%  Constants
%   G   [dyn cm^2 g^-2 or m^3 kg^-1 s^-2]
%   kb  [erg/K  or J/K]
%   amu [g or kg]
%   m_e [g or kg]
%   h_p [erg s  or  J s]
%   c   [cm/s or km/s]
%  Conversion factors
%   AU, pc, kpc, Mpc [cm or m]
%   keV [K]
%   Ga  [s]
%   Mo  [g or kg]
% If hdr.h100 is set, then Rho_c is also calculated
%   Rho_c [g/cm^3 or kg/m^3]
%
% EXTERNAL VARIABLES
%  SI	Which unit system to use: 'MKS' (default) or 'CGS'

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: matlab & octave

if(~exist('SI'))
 disp('SI undefined.  Setting to ''MKS''.')
 SI='MKS';
end

% SI independent
yr=3600*24*365.25;	% s NOTE: Note 365.2425 days/calendar year, not Julian.
Ga=1e9*yr;		% s
keV=1.16059e7;		% K

switch lower(SI)
 case 'cgs',
  eV=1.602176634e-12;   % erg (exact)
  amu=1.6605390666e-24;	% g
  kb=1.380649e-16;	% erg/K (exact)
  G=6.67384e-8;		% dyn cm^2 g^-2
  c=29979245800;	% cm/s (exact)
  h_p = 6.62607015e-27;	% erg s (exact)
  AU = 14959787070000;  % cm (exact)
 case 'mks',
  eV=1.602176634e-19;   % J (exact)
  amu=1.6605390666e-27;	% kg
  kb=1.380649e-23;	% J/K (exact)
  G=6.67384e-11;	% m^3 kg^-1 s^-2
  c=299792458;		% m/s (exact)
  h_p = 6.62607015e-34;	% J s (exact)
  AU = 149597870700;    % m (exact)
 otherwise,
  disp(['Unknown SI unit class: ',SI])
end

m_p = 1.00727647 * amu;
m_e = m_p / 1836.15267389;

pc = 648000/pi * AU; % cm or m (exact, to precision of pi)
kpc = 1000 * pc;     % cm or m (exact, to precision of pi)
Mpc = 1000 * kpc;    % cm or m (exact, to precision of pi)

% Solar mass can be calculated exactly, only depending on AU and yr, which are
% defined to their values. The year is a Julian Year
Mo = 4*pi^2*AU^3/(G*yr^2); % g or kg (exact, depending on AU)

%% The following calculates Rho_c if hdr.h100 is defined.
% Check to see if hdr is defined
if(exist('hdr'))
 % Check to see if hdr.h100 is defined
 if( isfield(hdr,'h100') )
  switch lower(SI)
   case 'cgs',
    km=1e5;	% cm
   case 'mks',
    km=1e3;	% m
   otherwise,
    disp(['Unknown SI unit class: ',SI])
  end
  Rho_c = 3/(8*pi*G)*(100*km/Mpc*hdr.h100)^2; % g/cm^3 [CGS] or kg/m^3 [MKS]
 else
  warning('hdr.h100 not defined in hdr')
 end
else
 warning('hdr not defined. Rho_c not set.')
end
