function Tdot=dTdt(t,T,dummy,rho)

if(T<0) T=0; end
if(rho<0) rho=0; end
Tdot=-3.83e12*(rho.*T.^0.5);
