function Err=ErrInSystem(v,P)
% Find the error in the velocities of particles, given a known Ek, Mom., and
% dispersion.
%
% Used by BoostRes().
%
% v should be [3,N]

N=P(1);
V=P(2:4);
sigma=P(5);


err(1)=sum( sum(v.^2) )-N*sum(V.^2);	% Kinetic energy
err(2)=sum( v(1,:) )-N*V(1);		% Momentum - 1
err(3)=sum( v(2,:) )-N*V(2);		% Momentum - 2
err(4)=sum( v(3,:) )-N*V(3);		% Momentum - 3
dv(1,:)=v(1,:)-V(1);
dv(2,:)=v(2,:)-V(2);
dv(3,:)=v(3,:)-V(3);
err(5)=sum( sum(dv.^2) ) - N*sigma^2;	% Dispersion

Err=sqrt(sum(err.^2));
