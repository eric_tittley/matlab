function BoostResolution(file_in,factor,file_out);
% Boost the resolution of a hydra data set.
%
%  BoostResolution(file_in,factor,file_out)
%
% Reads the data from 'file_in' and increases the resolution by
% the factor, 'factor'.  The data is written to 'file_out'.
%
% factor must be a positive integer.
%
% The data in file_in must have been processed using the tool
% 'calcdens' to produce the file file_in.densities.
%
% IMPORTANT NOTE; This should not be used on initial conditions data
% or otherwise relatively smooth or unperturbed data.  The placing of
% the particles introduces a great deal of high-frequency noise and likely
% a certain amount of intermediate-frequency power.

% AUTHOR: Eric Tittley
%
% HISTORY
%  01-08-20 First version.
%  02-02-19 Partially vectorised.  3x improvement in speed.
% 	Changed radii from 1/2 h to h to avoid clumping.
%  02-09-17 Changed radii from h to 2h to reduce clumping.
%  02-09-18 Added comments
%	Pre-calculate r2 and then compare sum(dx.^2) to r2(i) instead
%	of sqrt(sum(dx.^2)) to radii(i).  Saves about 4%.
%  02-11-18 Use calcdense to find the proper h's and dn's for the gas.
%	Then put a lower bound on the h's, otherwise shsph takes eternity.
%  02-11-20 Mislabled variables on final output.
%
% TODO
%  Optimize
%
% DEPENDENCIES
%  readdensities readhydra writehydra calcdens (external)

%%% Check the arguments
if(nargin~=3)
 error('syntax: BoostResolution(file_in,factor,file_out)')
end
if(floor(factor)~=factor)
 error('factor must be an integer')
end
if(factor<0)
 error('factor must be positive')
end
if(~ exist(file_in,'file') )
 error(['file: ',file_in,'does not exist'])
end

%%% Check for the .density file
density_file=[file_in,'.densities'];
if(~ exist(density_file,'file') )
 disp(['file: ',density_file,' does not exist'])
 disp(['Please run: calcdens ',file_in,' 0.001 8 0'])
 error('exiting')
end

%% Read in the density file
[gasdn,darkdn,gash,darkh]=readdensities(density_file);

% assuming all the gas comes first in the file
radii=[gash ; darkh];
clear gasdn darkdn gash darkh
%% Read in the data file
[rm,r,v,th,itype,dn,h,hdr]=readhydra(file_in);

% The number of particles
Nreg = hdr.ngas + hdr.ndark;
Nstar = hdr.nobj - Nreg;
if(sum(itype==-1) ~= Nstar)
 warning(['Should have found ',int2str(Nstar),' star particles. Found ',...
         int2str(sum(itype==-1))])
end

%% Loop through each gas and DM particle, splitting it into (factor) particles
%% each placed randomly within a sphere centred on the position of 
%% the previous particle and with a radius for the sphere of (gash/darkh)
% This will be a CPU-killing loop that we'll vectorize once everything else
% has settled.
tic
new_r=zeros(3,factor*Nreg+Nstar);
count=0;
r2=(2*radii).^2;
for i=1:Nreg
 good=[];
 while(length(good)<=factor) % This will usually only loop once
  dx=(rand(3,2*factor)-0.5)*4*radii(i); % Create twice as many dx's as we need.
  good=find(sum(dx.^2)<=r2(i)); % Find the dx's that are within radii.
 end
 dx=dx(:,good(1:factor));
 count=[(i-1)*factor+1:i*factor];
 for j=1:3
  new_r(j,count)=r(j,i)+dx(j,:);
 end
end
toc
if(Nstar > 0)
 span = Nreg+[1:Nstar];
 new_span = factor*Nreg+[1:Nstar];
 new_r(:,new_span)=  r(:,span);
end
clear r

% Sanity check
if(count(end)~=factor*(hdr.ngas+hdr.ndark))
 error('I should have produced factor*(hdr.ngas+hdr.ndark) particle positions')
end

%% Create matrices for the rest of the data
new_rm=zeros(factor*Nreg + Nstar,1);
new_v=zeros(3,factor*Nreg + Nstar);
new_th=zeros(factor*hdr.ngas,1);
new_itype=new_rm;
new_dn=new_th;
new_h=new_th;

%% Fill in the matrices for the rest of the data, scaling where necessary.
span = [1:Nreg]; % only multiply the gas and DM particles
new_span_end = factor*(Nreg-1);
for i=1:factor
 new_rm(i:factor:new_span_end+i)=rm(span)/factor;
 % V is a problem.  All the new particles will be cold relative to one another.
 % Ideally, they should be given perturbations. But these perturbations must:
 %  1) conserve total kinetic energy
 %  2) conserve the beta factor rms(Vr)/rms(Vt)
 new_v(:,i:factor:new_span_end+i)=v(:,span);
 new_itype(i:factor:new_span_end+i)=itype(span);
 % gas only
 new_th(i:factor:end-factor + i)=th;
 new_dn(i:factor:end-factor + i)=dn*factor;
 new_h( i:factor:end-factor + i)=h/(factor^(1/3));
end
% Add the stars
if(Nstar > 0)
 span = Nreg+[1:Nstar];
 new_span = factor*Nreg+[1:Nstar];
 new_rm(new_span) = rm(span);
 new_v(:,new_span)=  v(:,span);
 new_itype(new_span)=itype(span);
end
clear rm v th itype dn h

%% Update hdr
hdr.nobj=Nreg*factor+Nstar;
hdr.ngas=hdr.ngas*factor;
hdr.ndark=hdr.ndark*factor;

%% Write the data out
writehydra(file_out,new_rm,new_r,new_v,new_th,new_itype,new_dn,new_h,hdr);

%% At this point, we should call calcdens
if(~ exist([file_out,'.densities']) )
 eval(['! ./calcdens ',file_out,' 0.001 32 1'])
end

%% Read in the .density file and apply the new h and dn (gash and gasdn)
density_file=[file_out,'.densities'];
[gasdn,darkdn,gash,darkh]=readdensities(density_file);

%% Clip small h's
minh=1/500;
gash(gash<minh)=gash(gash<minh)*0+minh;

%% Write out file again with new h's and dns
writehydra(file_out,new_rm,new_r,new_v,new_th,new_itype,gasdn,gash,hdr);


%%%% END OF FUNCTION
