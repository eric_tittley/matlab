function t=cooling_time(rho,T)
% syntax: t=cooling_time(rho,T)
%
% Returns the cooling time, t (in seconds) for the gas at density,
% rho (in g/cm^3), and temperature, T (in K).
%
% Cooling is assumed to occur via Bremstrahlung (free-free) emission,
% so the cooling time is only approximate and valid for high temperatures
% and low densities.
%
% T (in K) = th*Kunit
% rho (in g/cm^3) = dn.*rm/hdr.box^3*munit*Mo/Mpc^3)
% where Mpc=3.1e24 and Mo=2e33

t=2.6e-13*sqrt(T)./rho;

% BELOW IS A MORE PRECISE ANSWER, BUT NOT NECESSARILY ANY BETTER
% It generally produces the result t_cool = 2* t_cool (from above)
%BEGIN
%tguesses=2*(2.6e-13*sqrt(T)./rho);
%t=tguesses*0;
%for i=1:length(tguesses)
% N=0;
% tguess=tguesses(i);
% while(N==0 | N>50)
%  [times,Ts]=ode23('dTdt',[0 :tguess/100:tguess],T(i),[],rho(i));
%  tcool=find(Ts<T(i)/1e3);
%  N=length(tcool);
%  if(N==0), tguess=tguess*2; end
%  if(N>50), tguess=tguess/2; end
% end %while
%[times,Ts]=ode('dTdt',[0 :tguess/1000:tguess],T(i),[],rho(i));
% tcool=find(Ts<T(i)/1e3);
% t(i)=times(tcool(1));
%end %for
%END
