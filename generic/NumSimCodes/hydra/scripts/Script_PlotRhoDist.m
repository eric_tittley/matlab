itime=100;
[gasdn,darkdn,gash,darkh]=readdensities([directory,'/',dataname(irun,itime),'.densities']);
gasdn=gasdn*dnunit_lo;
N=hist(log10(gasdn),log10(X));
N=N/length(gasdn)*Nbins;
semilogx(X,N,'k--')
itime=1000;
[gasdn,darkdn,gash,darkh]=readdensities([directory,'/',dataname(irun,itime),'.densities']);
gasdn=gasdn*dnunit_hi;
N=hist(log10(gasdn),log10(X));
N=N/length(gasdn)*Nbins;
hold on, semilogx(X,N,'k-'), hold off
axis(Limits)
set(gca,'fontsize',FontSize)
grid
