load centres.mat
[rm,r,v,th,itype,dn,h,time,atime]=readdata('d0104.1000');
over=[200,500];

% Units particlular to the problem
num_munits=64^3;
load Units.mat

for i=1:length(centres)
 for j=2:-1:1
  [rad(i,j),index]=overdensity_radius(rm,r,centres(i,:),over(j),num_munits);
  gas=index(find(itype(index)))'; dark=index(find(~itype(index)))';
  ngas(i,j)=length(gas);  ndark(i,j)=length(dark);
  mgas(i,j)=sum(rm(gas)); mdark(i,j)=sum(rm(dark));
 end %j
% if(length(dark)>0)
%  [Ldark(i,:),Ldarkmag(i)]=cluster_L(centres(i,:),r(dark,:),v(dark,:),rm(dark));
% end
 if(length(gas)>0)
%  [ Lgas(i,:), Lgasmag(i)]=cluster_L(centres(i,:),r( gas,:),v( gas,:),rm( gas));
  cent=(1&gas)*centres(i,:);
  r_temp=rem(r(gas,:)-cent+1.5,1)-.5;
  dist=sqrt(sum(r_temp'.^2));
%  core_gas=gas(find(dist<.05));
  core_gas=gas(find(dist/rad(i,1)<.3));
  if(length(core_gas))
   Tx(i)=mean(th(core_gas));
  else
   Tx(i)=0;
  endif %if there is core_gas
 else
  disp('Warning, No gas in Overdensity=200')
  Tx(i)=0;
 end %if
end %i

mgas=mgas*munit; mdark=mdark*munit;	%Mo
rad=rad*lunit/Mpc;			%Mpc (note: corrected for h^-1)
Tx=Tx*Kunit;				%K
%Ldark=Ldark*lunit*vunit*munit*Mo;	%g*cm^2/s
%Lgas =Lgas *lunit*vunit*munit*Mo;
%Ldarkmag=Ldarkmag*lunit*vunit*munit*Mo;
%Lgasmag =Lgasmag *lunit*vunit*munit*Mo;

clear rm r v th itype dn h time atime
clear over num_munits kunit munit runit
clear i j index gas dark cent r_temp dist2 core_gas

mask=find(Tx>0);
Tx=Tx(mask)'; rad=rad(mask,:); mgas=mgas(mask,:); mdark=mdark(mask,:); ngas=ngas(mask,:); ndark=ndark(mask,:); centres=centres(mask,:);
%Ldark=Ldark(mask,:); Lgas=Lgas(mask,:);
%Ldarkmag=Ldarkmag(mask)'; Lgasmag=Lgasmag(mask)';
centres=centres(mask,:);
clear mask

%save mass_rad_Tx mgas rad Tx mdark ndark ngas centres Ldark Lgas Ldarkmag Lgasmag
save mass_rad_Tx mgas rad Tx mdark ndark ngas centres

