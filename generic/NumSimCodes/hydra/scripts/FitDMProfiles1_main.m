load densities.1000.mat
log_sigmas=FindSigmas(log10(dist_dark),log10(dn_dark),20);
[X,Xstd]= bootstrap2('NFWdiff',P_NFW,dist_dark,dn_dark,log_sigmas,iters);
X2=chisqr(log10(dn_dark),log10(NFWfit(dist_dark,10^X(1),X(2))),log_sigmas);
disp(['NFW:'])
disp(['  delta=10^(',num2str(X(1)),'+-',num2str(Xstd(1)),')'])
disp(['  r_s=',num2str(X(2)),'+-',num2str(Xstd(2))])
disp(['  ChiSqr/N=',num2str(X2/length(dn_dark))])
[X,Xstd]= bootstrap2('Herndiff',P_Hern,dist_dark,dn_dark,log_sigmas,iters);
X2=chisqr(log10(dn_dark),log10(Hernfit(dist_dark,10^X(1),X(2))),log_sigmas);
disp(['Hern:'])
disp(['  delta=10^(',num2str(X(1)),'+-',num2str(Xstd(1)),')'])
disp(['  r_s=',num2str(X(2)),'+-',num2str(Xstd(2))])
disp(['  ChiSqr/N=',num2str(X2/length(dn_dark))])
[X,Xstd]= bootstrap2('SWdiff',P_SW,dist_dark,dn_dark,log_sigmas,iters);
X2=chisqr(log10(dn_dark),log10(SWfit2(dist_dark,10^X(1),X(2),X(3),X(4))),log_sigmas);
disp(['SW:'])
disp(['  delta=10^(',num2str(X(1)),'+-',num2str(Xstd(1)),')'])
disp(['  r_s=',num2str(X(2)),'+-',num2str(Xstd(2))])
disp(['  alpha=',num2str(X(3)),'+-',num2str(Xstd(3))])
disp(['  beta=',num2str(X(4)),'+-',num2str(Xstd(4))])
disp(['  ChiSqr/N=',num2str(X2/length(dn_dark))])
[X,Xstd]= bootstrap2('ETdiff',P_ET,dist_dark,dn_dark,log_sigmas,iters);
X2=chisqr(log10(dn_dark),log10(ETfit(dist_dark,10^X(1),X(2),X(3),X(4))),log_sigmas);
disp(['ET:'])
disp(['  delta=10^(',num2str(X(1)),'+-',num2str(Xstd(1)),')'])
disp(['  r_s=',num2str(X(2)),'+-',num2str(Xstd(2))])
disp(['  alpha=',num2str(X(3)),'+-',num2str(Xstd(3))])
disp(['  beta=',num2str(X(4)),'+-',num2str(Xstd(4))])
disp(['  ChiSqr/N=',num2str(X2/length(dn_dark))])
