%Script_contour_TvsRho
cmd=['load TvsRho.',int2str(irun),'.',int2str_zeropad(itime,4),'.mat'];
eval(cmd)
contour(10.^[-32:(-24+32)/99:-24],10.^[4:(9-4)/99:9],log10(TvsRho)',bins)
set(gca,'xscale','log','yscale','log')
xlabel('\rho_{gas} [g/cm^3]')
ylabel('T [K]')
title([int2str(irun),': t=',num2str(itime/1000)])
colormap(gray)
cmd=['print -deps TvsRho.',int2str(irun),'.',int2str_zeropad(itime,4),'.eps'];
eval(cmd)
