%Read in the datafile
[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itime));
gas=find(itype);dark=find(~itype);
[gasdn,darkdn,gash,darkh]=readdensities([dataname(irun,itime),'.densities']);

%find the projections, both total and zoomed
DnGas=h_proj(r(gas,1),r(gas,2),rm(gas),gash,Res,[0 1 0 1 0 1]);
DnDM =h_proj(r(dark,1),r(dark,2),rm(dark),darkh,Res,[0 1 0 1 0 1]);
DnGas_zoom=h_proj(r(gas,1),r(gas,2),rm(gas),gash,Res,Limits);
DnDM_zoom =h_proj(r(dark,1),r(dark,2),rm(dark),darkh,Res,Limits);
DnGas=DnGas*9; % to make it the same scale as the DM
DnGas_zoom=DnGas_zoom*9;

%save the projections
cmd=['save Dns.',int2str_zeropad(itime,4),'.mat DnGas DnDM DnGas_zoom DnDM_zoom'];
eval(cmd);

%print the zoomed data
bins=[-3.4:0.4:-0.6];
Span=Limits(2)-Limits(1);
X=[Limits(1):Span/(Res-1):Limits(2)]+Span/2/Res;
Span=Limits(4)-Limits(3);
Y=[Limits(3):Span/(Res-1):Limits(4)]+Span/2/Res;

%Gas
contour(X,Y,log10(DnGas_zoom),bins)
axis('square')
title([int2str(irun),': t=',num2str(itime/1000),' gas'])
cmd=['print -depsc DnGas_zoom_contour.',int2str_zeropad(itime,4),'.eps'];
eval(cmd)
%Dark matter
contour(X,Y,log10(DnDM_zoom),bins)
axis('square')
title([int2str(irun),': t=',num2str(itime/1000),' dark'])
cmd=['print -depsc DnDM_zoom_contour.',int2str_zeropad(itime,4),'.eps'];
eval(cmd)
