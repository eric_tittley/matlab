% History:
% 00 06 21 Adopted from original file to be used with Version 4.0 files

if(~ exist('densities.1000.mat') | ~ exist('limits_r_r200.1000.mat') )

 [rm,r,v,th,itype,dn,h,hdr]=readhydra(datafile);
 clear rm v dn h
 [gasdn,darkdn,gash,darkh]=readdensities(densityfile);
 clear gash darkh
 eval(['load ',overdensityfile])
 Units

 std_cut=0.7;
 dist_cut=2;

 [list,index]=sort(mdark(:,1));
 index=flipud(index);

 NumClus=size(centres,1);

 dark=find(~itype); gas=find(itype);

 r_temp=0*r;
 dark_count=0;
 gas_count=0;

 limits_dark=zeros(NumClus,2);
 limits_gas=limits_dark;

 gas_rejected=0;
 DM_rejected=0;
 for j=1:NumClus
  disp([num2str(j),' of ',num2str(NumClus)])
  i=index(j);

  softening=hdr.sft0*hdr.lunit/Mpc/2.34/rad(i,1);

  % Find the distances from the cluster, scaled to r_200
  r_temp(1,:)=mod(r(1,:)-centres(i,1)+0.5,1)-0.5;
  r_temp(2,:)=mod(r(2,:)-centres(i,2)+0.5,1)-0.5;
  r_temp(3,:)=mod(r(3,:)-centres(i,3)+0.5,1)-0.5;

  % for the gas...
  % Find the region over which the profile is well defined
  % bin the data crudely with r_max = 2 r_200.  Note: 21 bins
  dist=sqrt(sum(r_temp( :,gas).^2))' * (hdr.lunit/rad(i,1)/Mpc);
  NminCut=NwithinR(dist,300);
  near=find(dist>NminCut & dist<dist_cut & dist>softening);
  log_min_dist=log10(min(min(dist)));
  bins=log_min_dist:(log10(dist_cut)-log_min_dist)/20:log10(dist_cut) ;
  [y_mean,y_std]=moving_std(log10(dist(near)),log10(gasdn(near)),bins);
  % if std>1, reject (log scale, so actually std > 10 times)
  span=find(y_std>std_cut); if(isempty(span)), span=21; end
  max_span=10^bins(span(1)-1);
  span=find(dist<max_span & dist>NminCut & dist>softening);
  % If the extracted profile spans less than a factor of two in radius, then
  % reject.
  if(~ isempty(span))
   if(max(dist(span))/min(dist(span))<2), span=[]; end
  end
  if(~ isempty(span))
    disp('plotting'),j,i,clf,loglog(dist(span),gasdn(span),'.','markersize',1), pause
  else
   gas_rejected=gas_rejected+1;
  end
  limits_dark(i,:)=[max(softening,NminCut),max_span];
  % density
  dist_gas(gas_count+1:gas_count+length(span))=dist(span);
  dn_gas(gas_count+1:gas_count+length(span))=gasdn(span);
  % temperature
  tau(gas_count+1:gas_count+length(span))=th(span)/(mdark(i,1)+mgas(i,1))^(2/3);
  gas_count=gas_count+length(span);

  rejected=0;
  % for the dark matter...
  % Find the distances from the cluster, scaled to r_200
  dist=sqrt(sum(r_temp(:,dark).^2))' * (hdr.lunit/rad(i,1)/Mpc);
  sphere=find(dist<0.3);
  dark_centre=refine_centres(r(:,dark(sphere)),darkdn(sphere),centres(i,:),0.5);
  r_temp(1,dark)=mod(r(1,dark)-dark_centre(1)+0.5,1)-0.5;
  r_temp(2,dark)=mod(r(2,dark)-dark_centre(2)+0.5,1)-0.5;
  r_temp(3,dark)=mod(r(3,dark)-dark_centre(3)+0.5,1)-0.5;
  dist=sqrt(sum(r_temp(:,dark).^2))' * (hdr.lunit/rad(i,1)/Mpc);
  NminCut=NwithinR(dist,300);
  near=find(dist>NminCut & dist<dist_cut & dist>softening);
  log_min_dist=log10(min(min(dist)));
  bins=log_min_dist:(log10(dist_cut)-log_min_dist)/20:log10(dist_cut) ;
  [y_mean,y_std]=moving_std(log10(dist(near)),log10(darkdn(near)),bins);
  % if std>1, reject (log scale, so actually std > 10 times)
  span=find(y_std>std_cut); if(isempty(span)), span=21; end
  max_span=10^bins(span(1)-1);
  % Extract the range between the Nmin cuttoff radius or the softening length,
  % whichever is larger, and the maximum radius
  span=find(dist>NminCut & dist>softening & dist<max_span);
  % If the extracted profile spans less than a factor of two in radius, then
  % reject.
  if(~ isempty(span))
   if(max(dist(span))/min(dist(span))<2), span=[]; end
  end
  if(~ isempty(span))
 %  disp('plotting'),clf,loglog(dist(span),darkdn(span),'.','markersize',1), pause
  else
   DM_rejected=DM_rejected+1;
  end
  limits_dark(i,:)=[max(softening,NminCut),max_span];
  dist_dark(dark_count+1:dark_count+length(span))=dist(span);
  dn_dark(dark_count+1:dark_count+length(span))=darkdn(span);
  dark_count=dark_count+length(span);

 % save densities.1000.mat i dn_gas dist_gas dist_dark dn_dark tau
 
 end 
 
 tau=tau*hdr.Kunit*1e10/keV;

 save densities.1000.mat dist_dark dn_dark dist_gas dn_gas tau
 save limits_r_r200.1000.mat limits_dark limits_gas

 disp(['rejected ',int2str(gas_rejected),' gas halos'])
 disp(['rejected ',int2str(DM_rejected),' DM halos'])

else
 if(exist('densities.1000.mat'))
  disp('densities.1000.mat exists already')
 end
 if(exist('limits_r_r200.1000.mat') )
  disp('limits_r_r200.1000.mat exists already')
 end
end
eval(BaseCmd); 

