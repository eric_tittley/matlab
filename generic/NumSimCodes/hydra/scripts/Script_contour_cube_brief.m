%Read in the  projections
cmd=['load Dns.',int2str_zeropad(itime,4),'.mat'];
eval(cmd);

%print the zoomed data
bins=[-3.4:0.4:-0.6];
Span=Limits(2)-Limits(1);
X=[Limits(1):Span/(Res-1):Limits(2)]+Span/2/Res;
Span=Limits(4)-Limits(3);
Y=[Limits(3):Span/(Res-1):Limits(4)]+Span/2/Res;

%Gas
contour(X,Y,log10(DnGas_zoom),bins)
axis('square')
title([int2str(irun),': t=',num2str(itime/1000),' gas'])
cmd=['print -depsc DnGas_zoom_contour.',int2str_zeropad(itime,4),'.eps'];
eval(cmd)
%Dark matter
contour(X,Y,log10(DnDM_zoom),bins)
axis('square')
title([int2str(irun),': t=',num2str(itime/1000),' dark'])
cmd=['print -depsc DnDM_zoom_contour.',int2str_zeropad(itime,4),'.eps'];
eval(cmd)
