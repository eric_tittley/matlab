[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itime));
Units
[gasdn,darkdn,gash,darkh]=readdensities([dataname(irun,itime),'.densities']);
gas=find(itype);
th=th(gas)*hdr.Kunit*hdr.atime^2;
dn=gasdn * hdr.munit*Mo /(hdr.lunit*hdr.atime)^3;
TvsRho=numdensity(log10(dn),log10(th),Nsrch,Res,log10(AxLimits));
cmd=['save TvsRho.',int2str(irun),'.',int2str_zeropad(itime,4),'.mat TvsRho'];
eval(cmd)
clear TvsRho
