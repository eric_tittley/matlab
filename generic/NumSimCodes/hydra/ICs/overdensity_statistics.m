function [z,maxOD]=overdensity_statistics(file,h_guess)
% overdensity_statistics: Find the statistics of a hydra IC file
%
% [z,maxOD]=overdensity_statistics(file,h_guess)

% AUTHOR: Eric TIttley
%
% HISTORY
%  101111 Comments added
%  090312 Mature

% readhydra_field is broken
%r    =readhydra_field(file,'r');

[rm,r,v,th,itype,dn,h,hdr]=readhydra(file);
dm=find(itype==0);
clear rm v th itype dn h

[h,dn]=calcdens(r(:,dm),64,h_guess);

maxOD=max(dn)/length(dn)-1;
z=1/hdr.atime-1;
