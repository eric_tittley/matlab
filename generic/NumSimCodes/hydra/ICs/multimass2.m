function multimass2(datafiles_in,centre,Boundary,MassFactor,AddGas,datafile_out)
%
% multimass2(datafiles_in,centre,Boundary,MassFactor,AddGas,datafile_out)
%
% Start with creating datafiles_in, which are the input files,
% in order of decreasing resolution. (For example, Res1, Res2, Res3, Res4)
%
% Takes the initial conditions in datafiles_in, of version, Version,
% centres the data file on centre, and, using the boundaries stored in
% Boundary, makes the following configuration: (this example presumes
% 3 boundaries)
%
% ----------------------------------
% |    /                      \    |
% |   /                        \   |
% |  /        __________        \  |
% | /        /          \        \ |
% |/        /            \        \|
% |         |    ____    |         |
% |         |   /    \   |         |
% |         |   |Res1|   |         |
% |         |   \____/   |         |
% |         |            |         |
% |\        \    Res2    /        /|
% | \        \__________/        / |
% |  \                          /  |
% |   \          Res3          /   |
% |Res4\                      /    |
% ----------------------------------
%
% That is, 3 concentric spheres in a cubic volume:
%  an inner sphere of radius Boundary(1) with Resolution Res1
%  an intermediate sphere of radius Boundary(2) with resolution Res2
%  an outer sphere of radius Boundary(3) with resolution Res3
%  and everything outside the outer sphere is at resolution Res4
%
% Masses are scaled in each regime by MassFactor.
%
% The particles should lie on the grid, (0,1)
%
% Boundary should be of length Nfiles-1
%
% AddGas is a vector of relative masses which indicate for which files the gas
% should be added and gives the relative mass of the gas to the dark matter.
% This is useful for reading in dark-matter only
% initial conditions and producing files with dark matter and gas.
% If there are three data files, then [0 0.03 0.03] will add gas to only the
% 2nd and 3rd.
% If there are already gas particles in the data, 0 will remove them
% and 1 will add them (not duplicating the dark matter)
%
% See READHYDRA for help on the Hydra Version argument

% HISTORY
% 00 05 08 Original version
%
% 00 05 09 Modified to do one file at a time
% 	Modified to take any aribtrary input
%
% 00 12 15 modifed to be able to construct systems with both dark and gas
%	from files with only dark matter.
%
% 00 12 22 Lots of changes, most notably in the manner of calculating
%	the masses of both the dark matter and gas when splitting up
%	dark matter particles into dark and gas
%
% 01 04 13 Fixed bug in which the DM masses wasn't properly set for the
%	non-gas regions.

% Because this can be memory intensive, we must do each file one at a time

boundary=[0 ,Boundary, 1];

[Nfiles,dummy]=size(datafiles_in);

rm_dark=[];
r_dark=[];
v_dark=[];
itype_dark=[];

rm_gas=[];
r_gas=[];
v_gas=[];
itype_gas=[];
th_gas=[];
dn_gas=[];
h_gas=[];

for i=1:Nfiles
% Read in the initial positions
 r=readhydra_field(datafiles_in(i,:),'r');

% Recentre the distribution and move to the origin
 r=recentre(r,centre)-0.5;

% Find the distance of each particle from the origin
 dist=sqrt( sum( r.^2 ) );

% Find those particles in each sphere/shell
 Part =find( dist>boundary(i) & dist<=boundary(i+1) )';
 clear dist

% How many particles were extracted per shell?
 Nobj(i) = length(Part);

% Read in the initial perturbed file
 clear r
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(datafiles_in(i,:));
 Version=sum(hdr.Version./[1 10 100]);

% Initialize the header
 if(i==1)
  hdr_new=hdr;
  Units
  hdr_new.ngas=0;
  hdr_new.ndark=0;
  hdr_new.nobj=0;
  hdr_new.lunit=hdr_new.box/hdr_new.h100*Mpc;
  hdr_new.tunit=aintegral(0.006,1,hdr.omega0,hdr.xlambda0)/(1e7/Mpc)/hdr_new.h100;
  hdr_new.vunit=hdr_new.lunit/hdr_new.tunit;
  hdr_new.eunit=hdr_new.vunit^2;
  hdr_new.Kunit=hdr_new.eunit*2/3*amu/kb;
 end

% Dark matter
 PartDark=Part(find(~itype(Part)));

% NOTE: THIS NEXT SECTION IS WHOLLY INCOMPATIBLE WITH 4.02 FILES
% ALSO: THE LOGIC IS WRONG IF THERE IS GAS, AND WE ONLY WANT THE DARK MATTER
%       (need to combine the gas and dark matter masses)
 if(AddGas(i)~=0) 
 % The Gas particles:  If AddGas~=0, then include the gas particles.
 % If there are no gas particles, then duplicate the dark matter particles.
 % Taking some of the dark matter's mass as the gas's mass
  PartGas=Part(find( itype(Part)));
  if(isempty(PartGas))
   % Then there are no gas particles, and we have to get the new masses for
   % each of the dark matter and gas
   dark_mass=MassFactor(i)*rm(PartDark(1))/(1+AddGas(i));
   gas_mass =MassFactor(i)*rm(PartDark(1))*AddGas(i)/(1+AddGas(i));
   PartGas=PartDark;
   Ngas=length(rm);
   To=100*(0.0015/hdr.time)^(4/3) / hdr_new.Kunit; % presumes 100K at z=75 (time=0.0015);
   dn_o= Ngas;
   h_o= (3/(4*pi) * 32/Ngas)^(1/3);
   dummy=ones(Ngas,1);
   th=To  *dummy;
   dn=dn_o*dummy;
   h =h_o *dummy;
   clear To dn_o h_o dummy Ngas
  else
   % There already are gas particles.  Just take their masses
   dark_mass=MassFactor(i)*rm(PartDark(1));
   gas_mass =MassFactor(i)*rm(PartGas(1));
  end
 else % Don't add any gas, so just scale the dark mass
  dark_mass=MassFactor(i)*rm(PartDark(1));
  PartGas=[];
 end

 hdr_new.ngas=hdr_new.ngas+length(PartGas);
 hdr_new.ndark=hdr_new.ndark+length(PartDark);

% Add to the data arrays
% The masses
 if(round(Version*100)==402)
  if(AddGas(i)~=0)
   rm_dark=[rm_dark,MassFactor(i)*hdr_new.rmgas*ones(1,Nobj(i))*(1-AddGas(i))];
   rm_gas =[rm_gas, MassFactor(i)*hdr_new.rmgas*ones(1,Nobj(i))*AddGas(i)];
  else
   rm_dark=[rm_dark,MassFactor(i)*hdr_new.rmgas*ones(1,Nobj(i))];
  end
  hdr_new.Version=4.01;
 else % Not 4.02
  rm_dark=[rm_dark;0*rm(PartDark)+dark_mass];
  if(AddGas(i)~=0)
   rm_gas =[rm_gas ;0*rm(PartGas )+gas_mass];
  end
 end

% recentre to centre at the same time
 r_dark=[r_dark,[r(1,PartDark)-centre(1);r(2,PartDark)-centre(2);r(3,PartDark)-centre(3)]];
 if(AddGas(i)~=0)
  r_gas =[r_gas,[r(1,PartGas )-centre(1);r(2,PartGas )-centre(2);r(3,PartGas )-centre(3)]];
 end
 clear r
 v_dark=[v_dark,v(:,PartDark)];
 if(AddGas(i)~=0)
  v_gas=[v_gas,v(:,PartGas)];
 end
 clear v
 if(~ isempty(PartGas)) 
  th_gas=[th_gas;th(PartGas)];
  clear th
  dn_gas=[dn_gas;dn(PartGas)];
  clear dn
  h_gas =[ h_gas; h(PartGas)];
  clear h
 end
 itype_dark=[itype_dark;zeros(length(PartDark),1)];
 if(AddGas(i)~=0)
  itype_gas=[itype_gas;ones(length(PartDark),1)];
 end
end

hdr_new.nobj=hdr_new.ngas+hdr_new.ndark;

%if( sum(Nobj) ~= hdr_new.nobj )
% error('Ooops: something is awry in multimass2')
%end

r_dark=rem(r_dark+1.5,1);
r_gas =rem(r_gas +1.5,1);

% Now, we store all gas particles first
rm_gas=[rm_gas;rm_dark]; clear rm_dark
r_gas=[r_gas,r_dark];    clear r_dark
v_gas=[v_gas,v_dark];    clear v_dark
itype_gas=[itype_gas;itype_dark]; clear itype_dark

% Finally, write the data out.
writehydra(datafile_out,rm_gas,r_gas,v_gas,th_gas,itype_gas,dn_gas,h_gas,hdr_new,0)
