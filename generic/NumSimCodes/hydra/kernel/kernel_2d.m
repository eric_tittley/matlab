% kernel_2d: SPH smoothing kernel (hyda) projected onto a plane.
%
% W=kernel_2d(x)
%
% ARGUMENTS
%  x	(r-r_i)/h  [0,2]
%  
% RETURNS
%  W    The integrated SPH weight, integrated in straight parallel lines,
%       i.e projected onto a plane, for x = r/h, in the range x=0 to 2.
%
% USAGE
%  A note about normalization.
%  Normalized such that for h=1, integral of kernel_2d(x) dA = 1
%  i.e. sum(x.*kernel_2d(x))*2*pi*dx == 1
%  x=[0:dx:2]
%  Also force min(kernel_2d(x)) == 0
%
%  More generally, this means that for x=r/h, integral of kernel_2d(x) dA = h^2
%
%  So,
%   W_i = h^-2 * kernel_2d(x)
%   where x = r/h
%
% SEE ALSO
%	kernel, kernel_1d

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

function W=kernel_2d(x)

p1=[0.476717909787056 0.020248192026407 -0.799825478212311 0.414198993939548];
p2=[0.980295287490319 -1.517509906222201 0.783354388139954 -0.134839108767931];
Fudge=1-1.100689539512700e-10;
p1=p1/Fudge;
p2=p2/Fudge;

W=0*x;
mask1=(x>=0)&(x<=1);
mask2=(x>1)&(x<=2);
W=mask1.*(p1(1)+p1(2)*x+p1(3)*x.^2+p1(4)*x.^3)+mask2.*(p2(1)+p2(2)*x+p2(3)*x.^2+p2(4)*x.^3);
W(W<0)=W(W<0)*0;
