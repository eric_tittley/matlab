function W1d=kernel_1d(x,dy)

y=[0:dy:sqrt(4-x^2)];
W1d=2*sum(kernel_2d(sqrt(x^2+y.^2)))*dy;
