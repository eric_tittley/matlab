function integral=I_dr_dphi(r,num_dtheta)
thetalo=asin(r/2);
thetahi=pi/2;
dtheta=(thetahi-thetalo)/num_dtheta;
integral=0;
for theta=thetalo:dtheta:thetahi
 integral=integral+kernel(r/sin(theta))*r/sin(theta)^2 * dtheta;
end
integral=integral*2;
