function W2d=kern_int_2d(x,dy)

y=[0:dy:sqrt(4-x^2)];
W2d=2*sum(kernel(sqrt(x^2+y.^2)))*dy;
