function hoto=aintegral(to,t,Omega_o,Lambda_o)
% hoto=aintegral(to,t,Omega_o,Lambda_o);
%
% A matlab version of Hydra's aintegral function
hoto=quad('aintegral_func',to,t,[],[],Omega_o,Lambda_o);
