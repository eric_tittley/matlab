function [CorrFunc,r_bins,Mem_bins]=PositionMemoryCorrFunc(r,MemoryPos,r_bin_edges,Mem_bin_edges)
% PositionMemoryCorrFunc: Correlation between the physical and memory distances.
%
% Find the correlation between the distance between particles and the distance in
% memory.  Used for testing the optimisation of particle positions in memory
% for N-body simulations.
%
% [CorrFunc,r_bins,Mem_bins]=PositionMemoryCorrFunc(r,MemoryPos,r_bin_edges, ...
%                                                   Mem_bin_edges);
%
% ARGUMENTS
%  r			The particle positions
%  MemoryPos		The memory postitions (can be just an index)
%  r_bin_edges		The bin edges for the physical positions.
%  Mem_bin_edges	The bin edges for the memory positions.
%
% RETURNS
%  CorrFunc		The correlation function
%  r_bins		The centres of the bins for physical positions
%  Mem_bins		The centres of the bins for the memory positions
%
% REQUIRES
%  hist2d
%
% SEE ALSO
 
% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

[dummy,N]=size(r);
if(dummy~=3)
 error('r must be 3xN');
end
r_bins=(r_bin_edges(1:end-1)+r_bin_edges(2:end))/2;
Mem_bins=(Mem_bin_edges(1:end-1)+Mem_bin_edges(2:end))/2;
CorrFunc=zeros(length(Mem_bins),length(r_bins));

r_bin_edges2=r_bin_edges.^2;

for i=1:N
 r_temp=[r(1,i+1:N)-r(1,i);r(2,i+1:N)-r(2,i);r(3,i+1:N)-r(3,i)];
 r_temp=rem(r_temp+1.5,1)-0.5;
 dist2=sum(r_temp.^2);
 [CorrFunc_temp,X,Y]=hist2d(dist2,r_bin_edges2,abs(MemoryPos(i+1:N)-MemoryPos(i)),Mem_bin_edges);
 CorrFunc=CorrFunc+CorrFunc_temp;
end

% Correct for the bias due to larger volumes in larger shells.
for i=1:length(r_bins)
 CorrFunc(:,i)=CorrFunc(:,i)/(r_bin_edges(i+1)^3-r_bin_edges(i)^3);
end

% Make it the probability per (dr*dMem)
dr=r_bin_edges(2:end)-r_bin_edges(1:end-1);
dr=0*dr+1;
dMem=Mem_bin_edges(2:end)-Mem_bin_edges(1:end-1);
dA=dMem'*dr;
CorrFunc=CorrFunc./dA;

% Normalize
CorrFunc=CorrFunc./sum(sum(CorrFunc));
