% readGrafic1: Read a GRAFIC1 file
%
% [D,hdr]=readGrafic1(fname)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%
% RETURNS
%  D       The data in the file, returned as an array
%  hdr     The header of the file, returned as a structure
%
%  Elements of hdr:
%   np     Size of the grid along the 3 directions
%   dx     Grid spacing [Mpc/h]
%   x0     Coordinates of the grid's origin
%   astart Initial scale factor
%   omegam Omega_m
%   omegav Omega_v
%   h0     Hubble Constant
%
%  Elements of D
%   (depends on the file read)
%
% SEE ALSO

% AUTHOR: Eric Tittley (based on script included in Gadget distribution)
%
% COMPATIBILITY: Matlab Octave
%
% HISTORY
%  13 01 16 First version.

function [D,hdr]=readGrafic1(fname)

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 disp(['ERROR: readgadget: unable to open file: ',fname])
 return
end

% The header
blocksize=fread(fid,1,'uint');
hdr.np     = fread(fid,3,'uint');   % Size of the grid along the 3 directions
hdr.dx     = fread(fid,1,'real*4'); % Grid spacing [Mpc/h]
hdr.x0     = fread(fid,3,'real*4'); % Coordinates of the grid's origin
hdr.astart = fread(fid,1,'real*4'); % Initial scale factor
hdr.omegam = fread(fid,1,'real*4'); % Omega_m
hdr.omegav = fread(fid,1,'real*4'); % Omega_v
hdr.h0     = fread(fid,1,'real*4'); % Hubble Constant
blocksize=fread(fid,1,'uint');

% The data
blocksize=fread(fid,1,'uint');
D = fread(fid,prod(hdr.np),'real*4');
D = reshape(D,hdr.np);
blocksize=fread(fid,1,'uint');

% Close the file
fclose(fid);
