% enzo_readField: Read a single field from a dataset. Unigrid & AMR supported
%
% D=enzo_readField(Dir,Field)
%
% ARGUMENTS
%  Dir		The directory of the redshift dump
%  Field	The Field requested. If Field='list' then dump available fields
%
% RETURNS
%  D		The data for the field
%
% SEE ALSO
%  enzo2_merge_outputs. Just use it. It does what this function does, but
%                       better.

function D=enzo_readField(Dir,Field)

Files=dir([Dir,'/RedshiftOutput*.cpu*']);

Rank=1;

switch Rank
 case 1

  D=[];

  for iFile=1:length(Files)
   File=[Dir,'/',Files(iFile).name];
   H=h5info(File);

   if(strcmp(Field,'list'))
    for i=1:length(H.Groups(1).Datasets)
     disp(H.Groups(1).Datasets(i).Name);
    end
    D=H;
    return
   end

   for i=1:length(H.Groups)
    Grid=H.Groups(i).Name;
    if(strfind(Grid,'Grid'))
     D=[D;h5read(File,[Grid,'/',Field])];
    end
   end
  end
 
 otherwise
  disp(['ERROR: Not able to handle Rank=',num2str(Rank)])
end

