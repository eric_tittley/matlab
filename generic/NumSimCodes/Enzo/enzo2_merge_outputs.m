% enzo2_merge_outputs: Build Enzo2 subgrid files into a whole grid.
%
% data=enzo2_merge_outputs(filename_base,[GridOnly],[Field])
%
% ARGUMENTS
%  filename_base	The base of the grid files.
%			Reads 'filename_base.cpu0001' to
%                       'filename_base.cpuxxxN' and 'filename_base.hierarchy'.
%  GridOnly		(optional)
%			0 => Both Gas grid and particle data (default)
%                       1 => Gas grid data only. Particle data ignored
%			2 => Particle data only. Gas grid data ignored
%			3 => Read in a single field. Requires Field argument
%
%  Field		(optional) Name of the field to read in.
%			'list' will list the available fields.
%
% RETURNS
%  A data structure containing the data for the whole grid unless a single
%  field has been requested, then it is just the field.
%
% REQUIRES
%  isoctave

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  11 07 29 First version, based on enzo_merge_outputs
%  13 04 17 Now Octave-compatible. It once was. Then it lost it. Now it is
%           found.
%  13 04 23 Bug when reading a field with Matlab that has '-' in it in the HDF5
%           file.
%  13 05 24 Added capability to deal with a data directory that is not writable

function data=enzo2_merge_outputs(filename_base,GridOnly,Field)

if(nargin<2)
 GridOnly=0;
end
if(nargin==3)
 % A field has been requested, so ignore GridOnly
 GridOnly=3;
end

ls=dir([filename_base,'*cpu*']);
N=length(ls);

if(N==0)
 error('Unable to find file')
end

if(~isoctave)
 file = [filename_base,'.cpu',int2str_zeropad(0,4)];
 info=hdf5info(file);
end

% Get the number of particles
syscmd=['grep NumberOfParticles ',filename_base,...
        '.hierarchy | awk ''{print $3;}'''];
[ierr,Ns]=system(syscmd);
Ns=str2num(Ns);
NumPart=sum(Ns);

% Get subgrid geometries
%  For any normally-behaved run, the volume is diced up into N volumes.
%  However, for some runs (eg PhotonTest runs) for reasons I don't yet
%  understand, the data is all in filename_base.cpu0000. The other files
%  filename_base.cpu0001, filename_base.cpu0002, etc are essentially empty.
%  In this case, the geometry is just [0 0 0] to [1 1 1] (i.e. 2x3) instead
%  of 2*N x 3
GeometryFile=[filename_base,'.geometry'];
DeleteGeometryFileFlag=0;
if(~exist(GeometryFile))
 % Generate a geometry file. In the path if it is writable, otherwise to /tmp
 % Extract the Data Directory from the filename_base
 BackslashPositions=strfind(filename_base,'/');
 if(isempty(BackslashPositions))
  DataDirectory='.';
 else
  DataDirectory=filename_base(1:BackslashPositions(end)-1);
 end
 % Deal with the situation where that directory is not writable
 if(~DirectoryIsWritable(DataDirectory))
  % Director is not writable, so write the geometry file to /tmp
  [status,GeometryFile]=system('mktemp --tmpdir=/tmp GeometryFile.XXXXXXXXX');
  if(status==1)
   error('Unable to create a temporary Geometry File')
  end
  if(isoctave)
   disp('Ignoring the previous warning')
  end
  % Strip the trainline new-line
  GeometryFile=GeometryFile(1:end-1);
  DeleteGeometryFileFlag=1;
 end
 if(isoctave)
  system(['grep Edge ',filename_base,'.hierarchy | cut -c 20- > ',GeometryFile]);
 else
  eval(['!grep Edge ',filename_base,'.hierarchy | cut -c 20- > ',GeometryFile])
 end
end
Geometry=load(GeometryFile);
if(DeleteGeometryFileFlag)
 delete(GeometryFile)
end

if(size(Geometry,1)==2)
 Lo=[0 0 0];
 Hi=[1 1 1];
 N=1;
else
 Lo=Geometry([1:2:2*N-1],:);
 Hi=Geometry([2:2:2*N  ],:);
end

data=[];
if(~isoctave)
 % Matlab
 N_structs = length(info.GroupHierarchy.Groups(1).Datasets);
 if(GridOnly==3) % After a field only
  if(isempty(Field))
   message('ERROR: enzo2_merge_outputs: Field undefined.')
  end
  if strcmp(lower(Field),'list')
   for i=1:N_structs
    DataSetName=info.GroupHierarchy.Groups(1).Datasets(i).Name;
    Field=EMO_Field_from_DataSetName(DataSetName);
    disp(EMO_FieldName(Field))
   end
   return
  else
   for i=1:N_structs
    DataSetName=info.GroupHierarchy.Groups(1).Datasets(i).Name;
    ThisField=EMO_Field_from_DataSetName(DataSetName);
    if strcmp(EMO_FieldName(ThisField),Field)
     Rank=info.GroupHierarchy.Groups(1).Datasets(i).Rank;
     data=EMO_ReadMatlab(Rank,filename_base,i,N,ThisField,info,Lo,Hi,NumPart);
    end
   end
   return
  end
 end
 for i=1:N_structs
  DataSetName=info.GroupHierarchy.Groups(1).Datasets(i).Name;
  Field=EMO_Field_from_DataSetName(DataSetName);
  FieldName=EMO_FieldName(Field);
  Rank=info.GroupHierarchy.Groups(1).Datasets(i).Rank;
  switch GridOnly
   case 0, % switch GridOnly: Take both the Gas and Particle data.
    raw_whole=EMO_ReadMatlab(Rank,filename_base,i,N,Field,info,Lo,Hi,NumPart);
    data=setfield(data,FieldName,raw_whole);
    clear raw_whole
   case 1, % switch GridOnly: Take the Gas Data Only
    if(isempty(findstr(DataSetName,'particle')) )
     raw_whole=EMO_ReadMatlab(Rank,filename_base,i,N,Field,info,Lo,Hi,NumPart);
     data=setfield(data,FieldName,raw_whole);
     clear raw_whole
    end
   case 2, % switch GridOnly: Take the Particle Data Only
    if(~ isempty(findstr(DataSetName,'particle')) )
     raw_whole=EMO_ReadMatlab(Rank,filename_base,i,N,Field,info,Lo,Hi,NumPart);
     data=setfield(data,FieldName,raw_whole);
     clear raw_whole
    end
   case 3, % switch GridOnly: Read only a single field
    % We should already have taken care of this situation.
    disp('Ummm.... I should not have got to this state.')
    return
   otherwise, % switch GridOnly
    message(['ERROR: enzo2_merge_outputs: Unsupported GridOnly value:',...
            num2str(GridOnly)])
  end % switch over GridOnly cases   
 end % Loop over structures
else % Octave
 for j=1:N % Load all the subfiles
  file = [filename_base,'.cpu',int2str_zeropad(j-1,4)];
  %disp(['loading ',file])
  if(GridOnly)
   switch GridOnly
    case 1,
     % Gas grid only
     Dummy=load('-hdf5',file);
     GridName=['Dummy.Grid',int2str_zeropad(j,8)];
     eval(['Dummy=',GridName,';'])
     Struct_tag=fieldnames(Dummy);
     for i=1:length(Struct_tag);
      if(~isempty(findstr(Struct_tag{i},'particle')))
       Dummy=rmfield(Dummy,Struct_tag{i});
      end
     end
     D{j}=Dummy;
     clear Dummy
    case 2,
     % Particle data only
     Dummy=load('-hdf5',file);
     GridName=['Dummy.Grid',int2str_zeropad(j,8)];
     eval(['Dummy=',GridName,';'])
     Struct_tag=fieldnames(Dummy);
     for i=1:length(Struct_tag);
      if(isempty(findstr(Struct_tag{i},'particle')))
       Dummy=rmfield(Dummy,Struct_tag{i});
      end
     end
     D{j}=Dummy;
     clear Dummy
    case 3,
     % A single field, only
     Dummy=load('-hdf5',file);
     GridName=['Dummy.Grid',int2str_zeropad(j,8)];
     eval(['Dummy=',GridName,';'])
     if strcmp(lower(Field),'list')
      % Just list the available fields and return
      disp(fieldnames(Dummy));
      return
     end
     D{j}=struct(Field,Dummy.(Field));
     clear Dummy
    otherwise,
     message(['ERROR: enzo_merge_outputs: Unknown GridOnly ',num2str(GridOnly)])
   end % of switch over GridOnly
  else
   D{j}=load('-hdf5',file);
   GridName=['D{j}.Grid',int2str_zeropad(j,8)];
   eval(['D{j}=',GridName,';'])
  end
 end
 fields=fieldnames(D{1});
 % Guess at what the total grid size is (LxLxL)
 L=round((N*prod(size(getfield(D{1},fields{1}))))^(1/3));
 % Create the master structure from the structures for each file.
 NFields=length(fields);
 for i=1:NFields
  Rank=ndims(getfield(D{1},fields{i}));
  switch Rank
   %case 1,
   % disp(['Case 1:',fields{i}])
   % raw_whole=[];
   % for j=1:N
   %  raw_whole=[raw_whole,getfield(D(j),fields{i})];
   % end
   % data=setfield(data,fields{i},raw_whole);
   case 2,
    %disp(['Case 2:',fields{i}])
    raw_whole=[];
    for j=1:N
     raw_whole=[raw_whole,getfield(D{j},fields{i})];
    end
   case 3,
    %disp(['Case 3:',fields{i}])
    raw_whole=zeros(L,L,L);
    for j=1:N
     span_x=[L*Lo(j,1)+1:L*Hi(j,1)];
     span_y=[L*Lo(j,2)+1:L*Hi(j,2)];
     span_z=[L*Lo(j,3)+1:L*Hi(j,3)];
     raw_whole(span_x, span_y, span_z) = getfield(D{j},fields{i});
    end
   otherwise,
    message(['ERROR: enzo_merge_outputs: Unknown rank ',num2str(Rank)])
  end % End of switch
  if(NFields>1)
   data=setfield(data,fields{i},raw_whole);
  else
   % Only one field has been requested, so just return that.
   data=raw_whole;
  end
 end
end % If not octave else endif

% END of enzo_merge_outputs()

function raw_whole=EMO_ReadMatlab(Rank,filename_base,i,N,Struct_tag,info,Lo,Hi,NumPart)
switch Rank
 case 1, % switch Rank
  raw_whole=zeros(NumPart,1);
  ind=0;
  for j=0:N-1
   file = [filename_base,'.cpu',int2str_zeropad(j,4)];
   DataSetName=EMO_ScanForField(file,Struct_tag);
   % Read the field
   raw=[];
   [raw,hdr]=hdf5read(file,DataSetName);
   Nsub=length(raw);
   Span=ind+[1:Nsub];
   raw_whole(Span)=raw;
   ind=Span(end);
  end
 case 3, % switch Rank
  Dims=shift(info.GroupHierarchy.Groups(1).Datasets(i).Dims,-1,2);
  L=round((N*prod(Dims))^(1/3));
  raw_whole=zeros(L,L,L);
  N_in_dims = L./Dims;
  if(prod(N_in_dims) ~= N)
   disp('Lo=')
   disp(Lo)
   disp('Hi=')
   disp(Hi)
   disp(['Dims=',num2str(Dims)])
   disp(['L=',num2str(L)])
   disp(['N_in_dims=',num2str(N_in_dims)])
   error('ERROR: enzo_merge_outputs: Inconsistent dimensions')
  end
  j=0;
  for Nx=1:N_in_dims(1)
   for Ny=1:N_in_dims(2)
    for Nz=1:N_in_dims(3)
     j=j+1;
     span_x=[L*Lo(j,1)+1:L*Hi(j,1)];
     span_y=[L*Lo(j,2)+1:L*Hi(j,2)];
     span_z=[L*Lo(j,3)+1:L*Hi(j,3)];
     file = [filename_base,'.cpu',int2str_zeropad(j-1,4)];
     DataSetName=EMO_ScanForField(file,Struct_tag);
     [raw,hdr]=hdf5read(file,DataSetName);
     raw_whole(span_x, span_y, span_z) = raw;
    end
   end
  end
 otherwise, % switch Rank
  message(['ERROR: enzo_merge_outputs: Unknown rank ',num2str(Rank)])
end % End of Rank switch
% END of EMO_ReadMatlab

function DataSetName=EMO_ScanForField(file,Field)
% Scan the file's available data sets for the one that matches the structure
% labled StructTag.
% Returns the data set name.
 % Get the info
 info=hdf5info(file);
 % Scan for the Data set name containing the requested structure tag
 numFields=length(info.GroupHierarchy.Groups(1).Datasets);
 for iField=1:numFields
  DataSetName=info.GroupHierarchy.Groups(1).Datasets(iField).Name;
  FieldForThisDataSet=EMO_Field_from_DataSetName(DataSetName);
  if( strcmp(FieldForThisDataSet,Field) )
   % If they match, return. DataSetName is the one we want.
   return; % Jump out of the scan.
  end
 end
% END of EMO_DataSetName

function Field=EMO_Field_from_DataSetName(DataSetName)
% Strip out the name of the field from the data set name
 Separators=strfind(DataSetName,'/');
 if( ~isempty(Separators) )
  % The Dataset name is of the form /Datasetnumber/Fieldname
  separator=Separators(end);
 else
  % The Field name is the entirety of the Dataset name
  separator=0;
 end
 Field=DataSetName(separator+1:end);
% END of EMO_Field_from_DataSetName

function FieldName=EMO_FieldName(Field)
% Convert an Enzo data field name, which can have '-' in it, to one that can
% be used as a Matlab stucture field name
 FieldName=Field;
 FieldName(find(Field=='-'))='_';
% END of EMO_FieldName
