function maxOD=overdensity_statistics_enzo_ic(file)
% INCOMPLETE FUNCTION

r=enzo_read_IC_particle_file(file);
[N,M]=size(r);
h_guess=1/N^(1/3);
[h,dn]=calcdens(r',32,h_guess);
maxOD=max(dn)/length(dn)-1;

% INCOMPLETE
