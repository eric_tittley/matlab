% enzo_dispacement_stats: mean and max displacements from a regular Enzo grid.
%
% [disp_mean, disp_max, max_OD] = enzo_displacement_stats(file,GF)
%
% ARGUMENTS
%  file		The file to read.
%  GF 		A grid factor which is the ratio of the ParticleDims and the
%		MaxDims. If in doubt, set to unity (1).
%
% RETURNS
%  disp_mean Mean displacement from the regular grid.
%  disp_max  Maximum displacement.
%
% REQUIRES
%  enzo_read_IC_particle_file
%
% SEE ALSO
%  enzo_merge_outputs

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  06 10 24 First version.
%  07 12 04 Comments added.
%  08 03 17 Made Octave compatible.
%  09 04 15 Making it Octave compatible has broken Matlab compatibility.
%	Annoyingly, Matlab insists on compiling code it will never run.
%	Separate out the Octave code into an octave-only code and
%	Matlab-only code.

function [disp_mean, disp_max, max_OD] = enzo_displacement_stats(file,GF)

r=enzo_read_IC_particle_file(file);

N = round(length(r(:,1))^(1./3.)) / GF;
 
dx = 1/N;
dx_half = dx/2;

disp('Calculating displacements...')
i=zeros(1,N);
disp_max=0;
disp_sum=0;
displacement=zeros(N,3);
r_o=[0;0;0]';
flagOverBoard=0;
flagInterlaced=0;
for Fx=1:GF
 for ix=0:N-1
  if(flagInterlaced==1)
   x = mod(2*ix*dx + dx_half*2,1); % vector
  else
   x = ix*dx + dx_half; % vector
  end
  for Fy=1:GF
   for iy=0:N-1
    if(flagInterlaced==1)
     y = mod(2*iy*dx + dx_half*2,1); % vector
    else
     y = iy*dx + dx_half; % vector
    end
    for Fz=1:GF
     iz=[0:N-1]';
     if(flagInterlaced==1)
      z = mod(2*iz*dx + dx_half*2,1); % vector
     else
      z = iz*dx + dx_half; % vector
     end
     i = i(end)+[1:N]; % vector
     displacement(:,1)=abs(r(i,1)-z);
     displacement(:,2)=abs(r(i,2)-y);
     displacement(:,3)=abs(r(i,3)-x);
     mask=find(displacement>0.5);
     if(~isempty(mask))
      %Mask=find(   displacement(:,1) > 0.5 ...
      %           | displacement(:,2) > 0.5 ...
      %           | displacement(:,3) > 0.5 );
      %disp([r(i(Mask),:), displacement(Mask,:) ])
      displacement(mask) = displacement(mask) - 1;
      flagOverBoard=1;
     end
     displ=sqrt(sum(displacement.^2,2));
     if(max(displ) > disp_max)
      disp_max = max(displ);
     end
     disp_sum = disp_sum + sum(displ);
    end
   end
  end
 end
end
disp_mean=disp_sum/(N*GF)^3/dx;
disp_max =disp_max/dx;
disp(['mean displacement: ',num2str(disp_mean), ' grid spaces'])
disp(['max  displacement: ',num2str(disp_max), ' grid spaces'])

if(flagOverBoard==1)
 disp('One or more particles went over the repeating BCs. This has been found to screw up Enzo.')
end

if(0)

disp('Calculating distances to nearest neighbours...')
disp(' Transposing...')
r=trans_insitu(r); % calcdens expects [3,N].
r=single(r);
disp(' Calling caldens...')
[h,dn]=calcdens(r,2,0.5/N);
clear dn

dist=2*h*N; % Units of grid spacing

disp(['min, mean, max = ',num2str(min(dist)),', ',num2str(mean(dist)),', '...
                         num2str(max(dist))])
disp('For IC data, particles should not be closer than 0.5 grid spaces.')

end

if(1 & (N^3 <= 1024^3/2) )
 disp('Calculating overdensity...')
 clear h
 r=trans_insitu(r); % calcdens expects [3,N].
 [N,M]=size(r);
 N=max(N,M);
 h_guess=1/N^(1/3);
 [h,dn]=calcdens_d(r,32,h_guess);
 % Currently, calcdens_d reports densities 8/3 too large.
 % dn=(3/8)*dn;
 max_OD=max(dn)/length(dn)-1;
else
 disp('Overdensity not calculated')
 max_OD=-1;
end
