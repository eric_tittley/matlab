% Enzo_CosmologyGetUnits: Conversion coefficients for Enzo data.
%
% [rhoU,LU,TU,tU,vU]=Enzo_CosmologyGetUnits(Omega_m,h,Boxsize,z_i,z,[SI])
%
% ARGUMENTS
%  Omega_m  OmegaMatterNow
%  h        HubbleConstantNow (Ho = h 100km/s/Mpc)
%  Boxsize  ComovingBoxSize [Mpc]
%  z_i      InitialRedshift
%  z        CurrentRedshift
%  SI       [optional] SI units, 'MKS' (default) or 'CGS'
%
% RETURNS
%  rhoU     Density unit (kg/m^3 or g/cm^3)
%  LU       Lengh unit (m or cm)
%  TU       Temperature unit (mu=1) (K)
%  tU       Time unit (s)
%  vU       Velocity unit (m/s or cm/s)
%
% NOTES
%  Get the input parameters from the Input.params file used to run Enzo.
%  TU must be multiplied by mu, where the mean molecular mass is mu*amu.
%
% REQUIRES
%  Nothing
%
% SEE ALSO

% AUTHOR: Eric Tittley
%  Taken from Greg Bryan's CosmologyGetUnits.C from Enzo-1.0.1
%
% HISTORY
%  080827 First version
%
% COMPATIBILITY: Octave, Matlab

function [rhoU,LU,TU,tU,vU]=Enzo_CosmologyGetUnits(Omega_m,h,Boxsize,z_i,z,SI)

if(nargin<6)
 SI='MKS';
 disp(['WARNING: SI not defined. Setting to ',SI])
end

TU   = 1.88e6*Boxsize^2*Omega_m*(1 + z_i);
tU   = 2.52e17/sqrt(Omega_m)/h/(1 + z_i)^1.5;
switch lower(SI)
 case 'cgs',
  rhoU = 1.88e-29*Omega_m*h^2*(1 + z)^3;          % g/cm^3
  LU   = 3.086e24*Boxsize/h/(1 + z);              % cm
  vU   = 1.225e7*Boxsize*sqrt(Omega_m*(1 + z_i)); % cm/s
 case 'mks',
  rhoU = 1.88e-26*Omega_m*h^2*(1 + z)^3;          % kg/m^3
  LU   = 3.086e22*Boxsize/h/(1 + z);              % m
  vU   = 1.225e5*Boxsize*sqrt(Omega_m*(1 + z_i)); % m/s
 otherwise,
  disp(['ERROR: SI not valid:',SI])
end % switch SI
