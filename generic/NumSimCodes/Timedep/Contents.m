% Timedep: Code to manipulate Timedep2 files.
%
% Timedep2, written by Simon Reynolds, evolves the metalicity fractions in a
% cloud of gas subjected to ionising radiation.
%
% MovieMeanFraction: Generate a movie of the mean species state from Timedep
%                    output
%
% readTimedep: Read Timedep2 output files
