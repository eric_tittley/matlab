% readTimedep: Read Timedep2 output files
%
% function D=readTimedep(filename,Nspecies,Ncells,Ntimes)
%
% ARGUMENTS
%  filename  The name of the file to read
%  Nspecies  The number of "Species".
%             length of [SPECIES] where
%             [SPECIES] = [nE1 E1S1 E1S2 E1S3 nE2 E2S1 E2S2 ... ]
%             nEi is the number density of element i
%             EiSj is state j of element i
%  Ncells    The number of cells = Nlos * NCells where
%             NLOS = number of Lines of Sight
%             NCells = Number of cells along a Line of Sight
%  Ntimes    The number of times the data are dumped.
%
% RETURNS
%  D  A structure containing the members:
%      D.Times     The output times:                        [Ntimes] doubles
%      D.Cell      The cell number along the Line-of-sight: [Ncells] integers
%      D.LOS       The Line-of-sight number:                [Ncells] integers
%      D.Fractions The fractions (including number densities)
%                  [Nspecies x Ncells x Ntimes]
%
% REQUIRES
%  Nothing.  But the relevant files must have already been generated.
%
% SEE ALSO
%  

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  100115 First version
%  100118 Comments added

% If [SPECIES] = [nE1 E1S1 E1S2 E1S3 nE2 E2S1 E2S2 ... ]
% where EiSj is state j of element i and nEi is the number density of element
% i.
%
% [LINE]=[Cell LOS time [SPECIES]]
% (two integers and 1+ length([SPECIES]) doubles)
%
% Data =
% [LINE]C1T1 [LINE]C1T2 [LINE]C1T3 ... [LINE]C1Tm
% [LINE]C2T1 [LINE]C2T2 [LINE]C2T3 ... [LINE]C2Tm
%  .
%  .
%  .
% [LINE]CnT1 [LINE]CnT2 [LINE]CnT3 ... [LINE]CnTm
%
% where n = number of cells
%       m = number of time dumps
%
% So the whole data set can be read in and parsed if  we know the length of
% [SPECIES], n, and m.
%
% Enough to read in, but not enough to know the EiSj which corresponds to a
% specific element.
%
% Everything is double except Cell and LOS, which are integers.

function D=readTimedep(filename,Nspecies,Ncells,Ntimes)

if( exist(filename,'file') ~=2 )
 D=[];
 error([filename,' not found'])
end

D.Times    = zeros(Ntimes,1);
D.Cell     = zeros(Ncells,1);
D.LOS      = zeros(Ncells,1);
D.Fractions= zeros(Nspecies,Ncells,Ntimes);

FID=fopen(filename,'r');

for i=1:Ncells
 for j=1:Ntimes
  los= fread(FID,1,'int32');
  cell = fread(FID,1,'int32');
  if(i==1)
   D.Times(j)=fread(FID,1,'double');
  else
   time= fread(FID,1,'double');
  end
  try
   D.Fractions(:,i,j) = fread(FID,Nspecies,'double');
  catch
   disp(['ERROR: unable to read ',int2str(Nspecies),' doubles at cell ' ...
         int2str(i),', time ',int2str(j)])
   i=Ncells;
   j=Ntimes;
  end
 end
 D.LOS(i)=los;
 D.Cell(i)=cell;
end

fclose(FID);
