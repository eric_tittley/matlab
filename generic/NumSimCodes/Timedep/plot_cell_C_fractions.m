function plot_cell_C_fractions(Times,Fractions,LOS,Cell)

%D=readTimedep('fracs_out-100309.dat',31,1,1000);
%D=readTimedep('fracs_out-100309.dat',15,1,500);
%mask=find(D.Times>0);
mask=[1:length(Times)-1];

c='kbrgycm';

clf
axis([min(Times(mask)) max(Times(mask)) -0.1 1.1])
hold on
for i=9:15
 plot(Times(mask),squeeze(Fractions(i,LOS,Cell,mask)),c(i-8))
end
hold off
legend('CI','CII','CIII','CIV','CV','CVI','CVII');

