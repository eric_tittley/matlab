% MovieMeanFraction: Generate a movie of the mean species state from Timedep output
%
% MovieMeanFraction(Fractions,SpeciesRange,MovieName,[PolarFlag],[Redshifts])
%
% ARGUMENTS
%  Fractions [Nspecies,NCells,NLOS,Ntimes]
%
% RETURNS
%  Nothing
%
% REQUIRES
%  movies (Ocvate only)
%  movies requires the ffmpeg system tools installed.
%
% SEE ALSO
%  movies (Octave only), addframe, aviobj (Matlab only)

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab, though the output varies
%
% HISTORY
%  100309 First version
%  100316 Added support for Octave
%  100831 Scale non-Polar images to [1 NSpecies]

function MeanState=MovieMeanFraction(Fractions,SpeciesRange,MovieName,PolarFlag,Redshifts)

if(nargin==3)
 PolarFlag=0;
 RedshiftFlag=0;
end
if(nargin==4)
 RedshiftFlag=0;
end
if(nargin==5)
 RedshiftFlag=1;
end

if(ndims(Fractions)==4)
 [NSpecies,NCells,NLOS,NTimes]=size(Fractions);
else
 error('Please pass Fractions in the form Fractions(NSpecies,NCells,NLOS,NTimes)')
end

%D=readTimedep(filename,NSpecies,NLOS*NCells,NTimes);
%F=reshape(D.Fractions,NSpecies,NLOS,NCells,NTimes);
%clear D

% Extract the range of Species we want to display
if(NSpecies ~= length(SpeciesRange))
 Fractions=Fractions(SpeciesRange,:,:,:);
end

% If any are <0, set to 0 ( usually a small fraction are slightly < 0 )
Fractions(Fractions<0)=0;

MeanState=zeros(NCells,NLOS,NTimes-1);
NSpecies = length(SpeciesRange);
% The fractions are the weights, and we're averaging the States (CI, CII, etc..)
for i=1:NSpecies
 Fractions(i,:,:,:)=Fractions(i,:,:,:)*i;
end

% Weighted means, where the fractions in each state are the weights (sum to 1)
for i=1:NTimes-1
 MeanState(:,:,i)=sum(Fractions(:,:,:,i),1);
end

% Display one frame to initialise the figure
clf
if(PolarFlag)
 ph=imagesc_polar(squeeze(MeanState(:,:,1)),[1 NSpecies]);
else
 imagesc(squeeze(MeanState(:,:,1)),[1 NSpecies])
end

% Initialise the movie
if(isoctave)
 movie('init',MovieName)
else
 aviobj = avifile(MovieName);
end

th=[];

for i=1:NTimes-1
 % Display the frame
 if(PolarFlag)
  ph=imagesc_polar(squeeze(MeanState(:,:,i)),[1,length(SpeciesRange)]);
  cb=colorbar('Ylim',[1,length(SpeciesRange)]);
  cb_im=get(cb,'child');
  set(cb_im,'Ydata',[1,length(SpeciesRange)]);
  ca=gca;
  axes(cb);
  ylabel('State')
  axes(ca);
  % Need a list of redshifts for the following to work
  %text(0,1,['z=',num2str('1.3')]);
 else
  imagesc(squeeze(MeanState(:,:,i)),[1,length(SpeciesRange)])
  cb=colorbar;
  ca=gca;
  axes(cb);
  ylabel('State')
  axes(ca);
 end
 
 if(RedshiftFlag)
  % erase the previous text
  if(~ isempty(th) )
   set(th,'Visible','off')
  end
  % print the redshift
  th=text(-1,1.05,['z=',num2str(Redshifts(i))]);
 end

 % Grab the frame for the movie
 if(isoctave)
  movie('add',MovieName);
 else
  Frame=getframe(gcf);
  aviobj = addframe(aviobj,Frame);
 end
end

% Close the movie
if(isoctave)
 movie('close',MovieName,3);
else
 aviobj = close(aviobj);
end
