% readNyxGrid		Read Nyx "plot" files
%
% [D,H]=readNyxGrid(gridFile,headerFile)

function [D,H]=readNyxGrid(gridFile,headerFile)

% Read the Header file, which tells us the number and names of fields in the
% Grid file. 
Header=readHyperCLawFile(headerFile);
nFields=length(Header.Fields);

fid=fopen(gridFile,'rb');

H=Nyx_FAB_readHeader(fid);
c=fread(fid,1,'uchar');

Header.GridDims=Header.Geom.bigEnd+1;

D=[];
for iField=1:nFields
 dummy=fread(fid,prod(Header.GridDims),'double');
 D=setfield(D,Header.Fields{iField},reshape(dummy,Header.GridDims'));
end

fclose(fid);
