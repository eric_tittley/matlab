% BL_readBox: Read a BoxLib Box structure from file stream
%
% Box=BL_readBox(fid)
function Box=BL_readBox(fid)

dummy=fscanf(fid,'%c',2); % ((
Box.smallEnd=fscanf(fid,'%i,',3);
dummy=fscanf(fid,'%c',3); % ) (
Box.bigEnd=fscanf(fid,'%i,',3);
dummy=fscanf(fid,'%c',3); % ) (
Box.type=fscanf(fid,'%i,',3);
dummy=fscanf(fid,'%c',2); % ))
