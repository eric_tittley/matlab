% Nyx_FAB_writeHeader: write a FAB file header
%
% Nyx_FAB_writeHeader(fid,H)

function Nyx_FAB_writeHeader(fid,H)

fprintf(fid,'%s (',H.Content);
BL_writeRealDescriptor(fid,H.RealDescriptor);
BL_writeBox(fid,H.Box);
fprintf(fid,' %i\n',H.nFields);
