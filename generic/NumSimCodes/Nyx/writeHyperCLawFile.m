% writeHyperCLawFile: write a HyperCLaw-format file
%
% writeHyperCLawFile(file,Header)

function writeHyperCLawFile(file,Header)

fid=fopen(file,'w');

fprintf(fid,'HyperCLaw-V1.1\n');

nFields=length(Header.Fields);
fprintf(fid,'%i\n',nFields);
for i=1:nFields
 fprintf(fid,'%s\n',Header.Fields{i});
end

fprintf(fid,'%i\n',Header.BL_SPACEDIM);
fprintf(fid,'%f\n',Header.cumTime    );
fprintf(fid,'%i\n',Header.finestLevel);
fprintf(fid,'%f %f %f \n',Header.ProbLo );
fprintf(fid,'%f %f %f \n',Header.ProbHi );
if(isempty(Header.refRatio))
 fprintf(fid,'\n');
else
 fprintf(fid,'%i\n',Header.refRatio   );
end
for i=1:Header.finestLevel+1
 BL_writeBox(fid,Header.Geom(i));
 fprintf(fid,'\n');
end
fprintf(fid,'%i\n',Header.levelSteps);
for i = 1:Header.finestLevel+1
 fprintf(fid,'%i %i %i \n',Header.CellSize(i,:));
end
fprintf(fid,'%i\n',Header.Coord);
fprintf(fid,'%i\n',Header.bndry);
fprintf(fid,'%i ',Header.level);
fprintf(fid,'%i ',Header.gridSize);
fprintf(fid,'%f\n',Header.cur_time);
fprintf(fid,'%i\n',Header.levelSteps);
for i=1:Header.BL_SPACEDIM
 fprintf(fid,'%f %f\n',Header.gridLocLo(i),Header.gridLocHi(i));
end
fprintf(fid,'%s',Header.PathNameInHeader);

fclose(fid);
