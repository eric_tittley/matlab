% readHyperCLawFile: Read a HyperCLaw-format file
%
% Header=readHyperCLawFile(file)

function Header=readHyperCLawFile(file)

fid=fopen(file,'r');

fileTypeStr=fscanf(fid,'%s',1);
if(isempty(strfind(fileTypeStr,'HyperCLaw')))
 error([file,' is not a HyperCLaw-format file. It is ',fileTypeStr]);
end

nFields=fscanf(fid,'%i',1);
for i=1:nFields
 Header.Fields{i}=fscanf(fid,'%s',1);
end

% It would be nice to know what these fields contain
Header.BL_SPACEDIM=fscanf(fid,'%i',1);
Header.cumTime=fscanf(fid,'%f',1);
Header.finestLevel=fscanf(fid,'%i',1);
Header.ProbLo=fscanf(fid,'%f',3);
Header.ProbHi=fscanf(fid,'%f',3);
Header.refRatio=fscanf(fid,'%i',Header.finestLevel);
dummy=fscanf(fid,'%c',3); % spaces
for i=1:Header.finestLevel+1
 Header.Geom(i)=BL_readBox(fid);
end
Header.levelSteps=fscanf(fid,'%i',Header.finestLevel+1);
dummy=fscanf(fid,'%f',(Header.finestLevel+1)*Header.BL_SPACEDIM);
Header.CellSize=reshape(dummy,Header.finestLevel+1,Header.BL_SPACEDIM);
Header.Coord=fscanf(fid,'%i',1);
Header.bndry=fscanf(fid,'%i',1);
Header.level=fscanf(fid,'%i',1);
Header.gridsSize=fscanf(fid,'%i',1);
Header.cur_time=fscanf(fid,'%f',1);
dummy=fscanf(fid,'%i',1); % levelSteps again
dummy=fscanf(fid,'%f',Header.gridsSize*2*Header.BL_SPACEDIM);
dummy=reshape(dummy,Header.gridsSize*2,Header.BL_SPACEDIM);
Header.gridlocLo=dummy(1,:);
Header.gridlocHi=dummy(2,:);
Header.PathNameInHeader=fscanf(fid,'%s',1);

fclose(fid);
