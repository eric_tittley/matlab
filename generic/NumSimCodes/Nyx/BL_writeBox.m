% BL_writeBox: write a BoxLib Box structure to file stream
%
% BL_writeBox(fid,Box)
function BL_writeBox(fid,Box)

fprintf(fid,'(');
fprintf(fid,'(%i,%i,%i)',Box.smallEnd);
fprintf(fid,' ');
fprintf(fid,'(%i,%i,%i)',Box.bigEnd);
fprintf(fid,' ');
fprintf(fid,'(%i,%i,%i)',Box.type);
fprintf(fid,')');
