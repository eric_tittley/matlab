% NyxReadRunlog: Read the runlog file produced by Nyx at runtime
%
% D=NyxReadRunlog(filename)
%
% ARGUMENTS
%  filename	The path to the runlog file to read
%
% RETURNS
%  D		Data structure with elements
%                step, time, dt, redshift, a,
%                maxTemp, rhoWeightedTemp, volWeightedTemp, & tempAtMeanRho

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab

function D=NyxReadRunlog(filename)

if(isempty(dir(filename)))
 error(['ERROR: NyxReadRunlog: File ',filename,' not found']);
end

FID=fopen(filename,'r');
S=fgets(FID);
[A,count] = fscanf(FID,'%f');
fclose(FID);

A=reshape(A,9,length(A)/9);

D.step      	  = A(1,:);
D.time      	  = A(2,:);
D.dt        	  = A(3,:);
D.redshift  	  = A(4,:);
D.a         	  = A(5,:);
D.maxTemp         = A(6,:);
D.rhoWeightedTemp = A(7,:);
D.volWeightedTemp = A(8,:);
D.tempAtMeanRho   = A(9,:);
