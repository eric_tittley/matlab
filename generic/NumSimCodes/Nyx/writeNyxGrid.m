% writeNyxGrid		Write Nyx gas grid file
%
% writeNyxGrid(file,D,H)
function writeNyxGrid(file,D,H)

fid=fopen(file,'w');

Nyx_FAB_writeHeader(fid,H);

Fields=fieldnames(D);
nFields=length(Fields);
for i=1:nFields
 fieldData=getfield(D,Fields{i});
 fwrite(fid,fieldData,'double');
end

fclose(fid);
