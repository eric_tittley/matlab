% Nyx_FAB_readHeader: Read n FAB file header
%
% H=Nyx_FAB_readHeader(fid)

function H=Nyx_FAB_readHeader(fid)

H.Content=fscanf(fid,'%s',1);
dummy=fscanf(fid,'%c',2); %' ('
H.RealDescriptor=BL_readRealDescriptor(fid);
H.Box=BL_readBox(fid);
H.nFields=fscanf(fid,'%i',1);
