% BL_readArray: Read a BoxLib Array structure from file stream
%
% Array=BL_readArray(fid)
function Array=BL_readArray(fid)

dummy=fscanf(fid,'%c',1); % (
nElements=fscanf(fid,'%i',1);
dummy=fscanf(fid,'%c',3); % ', ('
Array=fscanf(fid,'%f',nElements);
dummy=fscanf(fid,'%c',2); % ))
