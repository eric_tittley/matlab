% Classify        Classifies the particles into those with local spherical,
%                 planar, and filamentary structure
%
% FindMoments     Finds the mono, di, and quodropole moments or the anisotropy
%                 distribution
%
% anisotropy      Finds the anisotropy distribution (a 12xN matrix) for a set of
%                 particle positions
%
% Eric Tittley 00 01 17
