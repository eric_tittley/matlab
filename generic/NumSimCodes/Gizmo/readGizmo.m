function [Data,Header]=readGizmo(file)
% readGizmo: Read Gizmo HDF5 data files
%
% [Data,Header]=readGizmo(file)
%
% ARGUMENTS
%  file		The Gizmo HDF5 file to read
%
% RETURNS
%  Data         structure containing the particle data
%                Gas is (always?) in PartType0
%                Dark Matter is (always?) in PartType1
%  Header       structure containing simulation parameters
%
% NOTES
%  This routine may very well read Gadget HDF5 files

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab

info=h5info(file);

% Header
nAttributes=length(info.Groups(1).Attributes);
for i=1:nAttributes
 eval(['Header.',info.Groups(1).Attributes(i).Name, ...
       '=h5readatt(file,info.Groups(1).Name,info.Groups(1).Attributes(i).Name);']);
end

% Data
%  There are multiple particle types
nParticleTypes=length(info.Groups)-1;
for iGroup=2:nParticleTypes+1
 nDataSets=length(info.Groups(iGroup).Datasets);
 ParticleTypeStr=info.Groups(iGroup).Name;
 % strip leading '/' and add a trailing '.'
 StructureElementStr=[ParticleTypeStr(2:end),'.'];
 % Add trailing '/'
 ParticleTypeStr=[ParticleTypeStr,'/'];
 for iDataSet=1:nDataSets
  eval(['Data.', ...
        StructureElementStr, ...
        info.Groups(iGroup).Datasets(iDataSet).Name, ...
        '=h5read(file,[ParticleTypeStr,info.Groups(iGroup).Datasets(iDataSet).Name]);']);
 end
end
