function column=extract_column(num_files,base_file,column_number)
% column=extract_column(num_files,base_file,column_number)
% Extracts column, column_number, from the profile files numbered 1 to
%  num_files which are based onthe data file base_file

for i=1:num_files
 file_preface=['clus',int2str(i)];
 file_name=[file_preface,'.',base_file];
 eval(['load ',file_name]);
 eval(['data=',file_preface,';']);
 column(:,i)=data(:,column_number);
end
