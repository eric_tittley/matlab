function yfit=ETfit(r,delta,rs,alpha,beta)
% yfit=ETfit(r,delta,rs,alpha,beta)
%
% Returns the generalized density profile given by Eric Tittley.
% yfit is the density profile at the points given by the vector,
% r, using the parameters delta, rs, alpha, and beta.

alpha=-1*alpha;
beta=-1*beta;

delta_beta=delta*rs^(alpha-beta);
mask=r<rs;
yfit=0*r;
yfit(mask)=delta*r(mask).^alpha;
yfit(~mask)=delta_beta*r(~mask).^beta;
