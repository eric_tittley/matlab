function sqrs=SWdiff(parameters,rfit,actual,sigma)

actual_log=log10(actual);
fit=SWfit2(rfit,10^parameters(1),parameters(2),parameters(3),parameters(4));
%sqrs=mean(abs(log10(fit)-actual_log));
sqrs=chisqr(actual_log,log10(fit),sigma);
