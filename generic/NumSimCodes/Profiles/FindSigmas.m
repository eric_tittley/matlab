function sigmas=FindSigmas(X,Y,Nbins)
%
% syntax: sigmas=FindSigmas(X,Y,Nbins);

sigmas=0*Y;

% Find Bin Limits
bin_lo=min(X); bin_hi=max(X);

if(Nbins>0)
 bins=bin_lo:(bin_hi-bin_lo)/Nbins:bin_hi;
else
 error('Nbins <=0')
end

% Add a bit to the first and last bins to get all the particles
tol=(bin_hi-bin_lo)/Nbins/100;
bins(1)=bins(1)-tol;
bins(end)=bins(end)+tol;

% Find the standard deviation in the bins
[Xnew,Ynew,std_Y]=bin_profile(bins,X,Y);

% Allot Deviations to all the particles
for i=1:Nbins
 in_bin=find(X>=bins(i) & X <bins(i+1));
 sigmas(in_bin)=std_Y(i)*(1&in_bin);
end
