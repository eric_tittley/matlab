function array_out=interp_profiles(array_in)
% array_out=interp_profiles(array_in)
%
% Given the [n,m] array, array_in of profiles (m profiles of length n)
% an [n,m] array is returned (array_out) in which all the points in
% the profile for which there is no data (denoted by zeros) is has
% values linearly interpolated.

[n,m]=size(array_in);
array_out=array_in;
for i=1:m
 zeros=find(~array_in(:,i));
 for j=1:length(zeros)
  index=zeros(j);
  hi=find(array_in(index+1:n,i))+index;
  lo=find(array_in(1:index-1,i));
  hi_index=hi(1);
  if(index>1)
   lo_index=lo(length(lo));
   y1=array_in(lo_index,i);
   y2=array_in(hi_index,i);
   x1=lo_index;
   x2=hi_index;
   array_out(index,i)=y1+(y2-y1)/(x2-x1)*(index-x1);
  else
   array_out(1,i)=array_in(hi_index,i);
   array_in(1,i)=array_in(hi_index,i);
  end %if index
 end %j
end %i
