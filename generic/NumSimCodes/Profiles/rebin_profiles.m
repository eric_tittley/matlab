function new=rebin_profiles(x_new,x_old,y_old)
% new=rebin_profiles(x_new,x_old,y_old)
% Given the [n,m] array,  y_old, of profiles (m profiles of length n)
%           [n,m] array,  x_old, of profile radii (usually log10(r/r_200))
%     and the [l] vector, x_new, of new radial bins
% an [l,m] array, new, is returned which has the individual profiles
% from y_old rebinned to the same binning such that the same row of each
% column corresponds to the same bin.
%
% mean(new') returns the mean profile.
%
% Hints: Use interp_profiles, first, to get rid of zeros.

[n,num]=size(y_old);
[new_len,n]=size(x_new);
if(n>1), x_new=x_new'; new_len=n; end

%initialize arrays
new=x_new*(0*[1:num]);
max_x=max(x_old);
min_x=min(x_old);

for i=1:num
% new(:,i)=spline(x_old(:,i),y_old(:,i),x_new);
 new(:,i)=interp1(x_old(:,i),y_old(:,i),x_new);
 max_index=find(x_new<max_x(i) & [~(x_new(2:new_len)<max_x(i));1]);
 min_index=find(x_new>min_x(i) & [1;~(x_new(1:new_len-1)>min_x(i))]);
 new(max_index+1:new_len,i)=new(max_index,i)*(1&[max_index+1:new_len]');
 new(1:min_index-1,i)=new(min_index,i)*(1&[1:min_index-1]');
end
