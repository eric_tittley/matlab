function yfit=ET2fit(r,delta,rs,const)
% yfit=ET2fit(r,delta,rs,const)
%
% Returns another generalized density profile given by Eric Tittley.
% yfit is the density profile at the points given by the vector,
% r, using the parameters delta, rs, alpha, and beta.

mask=r<rs;
yfit=0*r;
yfit(mask)=const*(1&r(mask));
yfit(~mask)=delta*r(~mask).^(-log10(r(~mask))/2-1);
