function yfit=SWfit2(r,delta,rs,alpha,beta)
% yfit=SWfit(r,delta,rs,alpha,beta)
%
% Returns the generalized density profile given by Syer and White.
% yfit is the density profile at the points given by the vector, r
% using the parameters delta, rs, alpha, and beta.

yfit=delta./(r.^alpha.*(rs+r).^(beta-alpha));
