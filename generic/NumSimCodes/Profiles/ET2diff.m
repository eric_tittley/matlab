function sqrs=ETdiff(parameters,rfit,actual)

actual_log=log10(actual);
fit=ET2fit(rfit,10^parameters(1),parameters(2),parameters(3));
sqrs=mean(abs(log10(fit)-actual_log));
