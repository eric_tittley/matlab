function sqrs=Herndiff(parameters,rfit,actual,sigmas)

actual_log=log10(actual);
fit=Hernfit(rfit,10^parameters(1),parameters(2));
%sqrs=mean(abs(log10(fit)-actual_log));
sqrs=chisqr(actual_log,log10(fit),sigmas);
