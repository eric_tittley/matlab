function sqrs=Betadiff(parameters,rfit,actual,sigmas)

actual_log=log10(actual);
fit=Betafit(rfit,10^parameters(1),parameters(2),parameters(3));
sqrs=chisqr(actual_log,log10(fit),sigmas);
