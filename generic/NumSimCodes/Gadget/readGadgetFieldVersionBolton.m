% readGadgetFieldVersionBolton: Read a single field from a Gadget file with
%				Bolton's ionization fraction extensions
%
% D=readGadgetFieldVersionBolton(fname,Field)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%   Field   The name of the field to read
%            One of
%             hdr       Header
%             r         Particle positions
%             v         Particle velocities
%             id        Particle IDs
%             mass      Particle masses (if available)
%             u         Gas internal energies (if available)
%             rho       Gas densities (if available)
%             h         Smoothing length (if available)
%             X_H1	N_H1    / N_H
%             X_H2	N_HII   / N_H
%             He1	N_HeI   / N_H
%             He2	N_HeII  / N_H
%             He3	N_HeIII / N_H
%             e		N_e     / N_H
%
%  Output
%   D       The data for that field
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%
% SEE ALSO
%  readGadgetVersionBolton readgadget ReadGadgetHeader

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave

function D=readGadgetFieldVersionBolton(fname,Field)

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 error(['ERROR: readGadgetFieldVersionBolton: unable to open file: ',fname])
 return
end

% Read the header
hdr=ReadGadgetHeader(fid);

% If the header is all we need, then close file and return
if(strcmp(Field,'hdr'))
 D=hdr;
 fclose(fid);
 return
end

% The base data, position, velocity, and particle type
N=sum(hdr.npart);

% Warn if a gas field has been requested but there is no gas
Ngas=hdr.npart(1);
if( (Ngas==0) & (    strcmp(field,'u')    || strcmp(field,'rho') ...
                  || strcmp(field,'X_H1') || strcmp(field,'X_H2') ...
                  || strcmp(field,'He1') || strcmp(field,'He2') ...
                  || strcmp(field,'He3') || strcmp(field,'e') ...
                  || strcmp(field,'h') ) )
 error(['ERROR: readGadgetFieldVersionBolton: No gas in: ',fname])
 fclose(fid);
 return
end

% How many mass elements are there?
ind=find((hdr.npart > 0) & (hdr.mass == 0));
if(~isempty(ind))
 Nwithmass=sum(hdr.npart(ind));
 MassField=1;
else
 Nwithmass=0;
 MassField=0;
end
if(strcmp(Field,'mass'))
 if(isempty(ind))
  error('ERROR: readGadgetFieldVersionBolton: Field "mass" not available')
  fclose(fid);
  return
 end
end

% Determine the Offset. Each block has the following number of 4 byte values
%  + two 4-byte record header/footer + a single 4-byterecord header
%             r         3*N
%             v         3*N
%             id        N
%             mass      Nwithmass
%             u         Ngas
%             rho       Ngas
%             X_H1	Ngas
%             X_H2	Ngas
%             He1	Ngas
%             He2	Ngas
%             He3	Ngas
%             e		Ngas
%             h         Ngas
switch Field
 case 'r'
  status=fseek(fid,4,'cof');
  D = fread(fid,[3,N],'float');
 case 'v'
  status=fseek(fid,4*(            3*N          +  1*2 + 1),'cof');
  D = fread(fid,[3,N],'float');
 case 'id'
  status=fseek(fid,4*(            6*N          +  2*2 + 1),'cof');
  D = fread(fid,N,'uint32');
 case 'mass'
  status=fseek(fid,4*(            7*N          +  3*2 + 1),'cof');
  D = fread(fid,Nwithmass,'float');
 case 'u'
  status=fseek(fid,4*(Nwithmass + 7*N          +  3*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'rho'
  status=fseek(fid,4*(Nwithmass + 7*N + 1*Ngas +  4*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'X_H1'
  status=fseek(fid,4*(Nwithmass + 7*N + 2*Ngas +  5*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'X_H2'
  status=fseek(fid,4*(Nwithmass + 7*N + 3*Ngas +  6*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'He1'
  status=fseek(fid,4*(Nwithmass + 7*N + 4*Ngas +  7*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'He2'
  status=fseek(fid,4*(Nwithmass + 7*N + 5*Ngas +  8*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'He3'
  status=fseek(fid,4*(Nwithmass + 7*N + 6*Ngas +  9*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'e'
  status=fseek(fid,4*(Nwithmass + 7*N + 7*Ngas + 10*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 case 'h'
  status=fseek(fid,4*(Nwithmass + 7*N + 8*Ngas + 11*2 + 1 + MassField*2),'cof');
  D = fread(fid,Ngas,'float');
 otherwise
  error(['ERROR: readGadgetFieldVersionBolton: unrecognized field: ',Field])
end

fclose(fid);

if(status==-1)
 error('ERROR: readGadgetFieldVersionBolton: Unsuccessful read. Corrupt file or incorrect format?')
end
