% GOIVF_core: See GroupOutputIntoVolumeFiles

function GOIVF_core(file,dirlist,dirname,xlo,xhi,ylo,yhi,zlo,zhi,hdr)

if( ~ exist(file) )
 r_gas  =[];
 v_gas  =[];
 id_gas =[];
 u_gas  =[];
 rho_gas=[];
 h_gas  =[];
 r_dark  =[];
 v_dark  =[];
 id_dark =[];
 r_star  =[];
 v_star  =[];
 id_star =[];
 for i=3:length(dirlist)
  filename=[dirname,'/',dirlist(i).name];
  disp(filename)
  clear Di hdri
  [Di,hdri]=readgadget(filename);
  ngas=hdri.npart(1);
  ndark=hdri.npart(2);
  nstar=hdri.npart(5);
  dark=[ngas+1:ngas+ndark];
  star=[ngas+ndark+1:ngas+ndark+nstar];
  if(ngas>0)
   % Gas
   gas=[1:ngas];
   good=find(  Di.r(1,gas)>=xlo & Di.r(1,gas)<xhi ...
	     & Di.r(2,gas)>=ylo & Di.r(2,gas)<yhi ...
	     & Di.r(3,gas)>=zlo & Di.r(3,gas)<zhi );
   index=gas(good);
   r_gas  =[r_gas  ,Di.r(:,index)];
   v_gas  =[v_gas  ,Di.v(:,index)];
   id_gas =[id_gas ;Di.id(index) ];
   u_gas  =[u_gas  ;Di.u(index)  ];
   rho_gas=[rho_gas;Di.rho(index)];
   h_gas  =[h_gas  ;Di.h(index)  ];
   hdr.npart(1)=hdr.npart(1)+length(good);
  else
   hdr.npart(1)=0;
  end
  % DM
  good=find(  Di.r(1,dark)>=xlo & Di.r(1,dark)<xhi ...
	    & Di.r(2,dark)>=ylo & Di.r(2,dark)<yhi ...
	    & Di.r(3,dark)>=zlo & Di.r(3,dark)<zhi );
  index=dark(good);
  r_dark  =[r_dark  ,Di.r(:,index)];
  v_dark  =[v_dark  ,Di.v(:,index)];
  id_dark =[id_dark ;Di.id(index) ];
  hdr.npart(2)=hdr.npart(2)+length(good);
  % stars
  good=find(  Di.r(1,star)>=xlo & Di.r(1,star)<xhi ...
	    & Di.r(2,star)>=ylo & Di.r(2,star)<yhi ...
	    & Di.r(3,star)>=zlo & Di.r(3,star)<zhi );
  index=star(good);
  r_star  =[r_star  ,Di.r(:,index)];
  v_star  =[v_star  ,Di.v(:,index)];
  id_star =[id_star ;Di.id(index) ];
  hdr.npart(5)=hdr.npart(5)+length(good);
 end
 clear dark gas star
 D.r  =[r_gas,r_dark,r_star];
 clear r_gas r_dark r_star
 D.v  =[v_gas,v_dark,v_star];
 clear v_gas v_dark v_star
 D.id =[id_gas;id_dark;id_star];
 clear id_gas id_dark id_star
 if(ngas>0)
  D.u  =u_gas;
  clear u_gas
  D.rho=rho_gas;
  clear rho_gas
  D.h  =h_gas;
  clear h_gas
 end
 hdr.mass            = hdri.mass;
 hdr.time            = hdri.time;
 hdr.redshift        = hdri.redshift;
 hdr.sfr             = hdri.sfr;
 hdr.feedback        = hdri.feedback;
 hdr.npartTotal      = hdri.npartTotal;
 hdr.flag_cooling    = hdri.flag_cooling   
 hdr.num_files       = hdri.num_files      
 hdr.BoxSize         = hdri.BoxSize        
 hdr.Omega0          = hdri.Omega0         
 hdr.OmegaLambda     = hdri.OmegaLambda    
 hdr.HubbleParam     = hdri.HubbleParam    
 hdr.flag_stellarage = hdri.flag_stellarage
 hdr.flag_metals     = hdri.flag_metals    
 hdr.hashtabsize     = hdri.hashtabsize    
 disp('writing...')
 eval(['save -v7.3 ',file,' D hdr'])
end % if file does not exist
