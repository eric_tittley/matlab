% --------------------------------------------------------------------
% NumSimCodes/Gadget: Tools for manipulating Gadget files
% --------------------------------------------------------------------
%
% Gadget_max_overdensity      Find the statistics of a Gadget IC file
%
% GroupOutputIntoVolumeFiles  Redistribute multi-file Gadget data
%
% readgadget                  Read a Gadget file
%
% ReadGadgetField             Read a single field from a Gadget file
%
% ReadGadgetHeader            Read a Gadget header
%
% writegadget                 Write a Gadget file
%
% --------------------------------------------------------------------
% Tools to handle the Millenium Volume with Gas data
% --------------------------------------------------------------------
%
% ExtractMilleniumVolume      Extract a volume from the Millenium Data set
%
% CalcDensitiesForAllFiles    For all the re-gridded Millenium files, find the
%                             densities
%
% ExtractFromDensityFiles     Extract a list of particle densities from
%                             Millenium
%
% --------------------------------------------------------------------
% Jamie Bolton's version of Gadget with ionization fraction extensions
% --------------------------------------------------------------------
%
% readGadgetVersionBolton        Read a Gadget file with Bolton's ionization
%                                fraction extensions
%
% readGadgetFieldVersionBolton   Read a single field from a Gadget file with
%                                Bolton's ionization fraction extensions
%
% readGriddedGadgetVersionBolton Read a grid file from Bolton's GadgetToGrid
%
% gaReadGadgetDataFile           Avery's version of readGriddedGadgetVersionBolton
%
% --------------------------------------------------------------------
% Conversion to other data formats
% --------------------------------------------------------------------
%
% Gadget2HydraUnpert          Convert from Gadget native file format to Hydra
%                             unperturbed file format
