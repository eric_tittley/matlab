% GroupOutputIntoVolumeFiles: Redistribute multi-file Gadget data.
%
% GroupOutputIntoVolumeFiles(dirname,NoutputFiles,L,OutputDir)
%
% ARGUMENTS
%  dirname	The directory containing the Gadget data files.
%  NoutputFiles The number of output files.  Needs to be such that:
%		 (NoutputFiles)=N^3 where N is an integer.
%  L		The code-unit length of each side.
%  OutputDir    The directory in which to write the grouped .mat files.
%
% RETURNS
%  Nothing
%  Creates NoutputFiles Matlab V7 .mat files of the form: data_NxNyNz.mat
%  where Nx=1:N as do Ny & Nz
%
% REQUIRES
%  readgadget
%
% COMPATIBILITY: Matlab Octave

% AUTHOR: Eric Tittley
%
% HISTORY
%  071019 First version.

function GroupOutputIntoVolumeFiles(dirname,NoutputFiles,L,OutputDir)

dirlist=dir(dirname);

N=NoutputFiles^(1/3);

dx=L/N;

range=[0:dx:L];

hdr.npart=zeros(6,1);

for ixfile=1:N
 xlo=range(ixfile);
 xhi=range(ixfile+1);
 for iyfile=1:N
  ylo=range(iyfile);
  yhi=range(iyfile+1);
  for izfile=1:N
   zlo=range(izfile);
   zhi=range(izfile+1);
   file=[OutputDir,'/Data_',int2str(ixfile),int2str(iyfile),int2str(izfile),'.mat'];
   disp(['Creating ',file])
   GOIVF_core(file,dirlist,dirname,xlo,xhi,ylo,yhi,zlo,zhi,hdr)
  end
 end
end
