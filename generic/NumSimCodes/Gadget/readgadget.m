% readgadget: Read a Gadget file
%
% [D,hdr]=readgadget(fname,ICflag)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%   ICflag  1 if the file is an IC file [optional, default 0]
%
% RETURNS
%  D       The data in the file, returned as a structure
%  hdr     The header of the file, returned as a structure
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%   flag_cooling
%   num_files
%   BoxSize
%   Omega0
%   OmegaLambda
%   HubbleParam
%   flag_stellarage
%   flag_metals
%   hashtabsize
%
%  Elements of D
%   r
%   v
%   id
%   mass
%   u
%   rho
%   h
%
% SEE ALSO
%  ReadGadgetField

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave

function [D,hdr]=readgadget(fname,ICflag)

% Default behaviour
if(nargin < 2)
 ICflag=0;
end

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 disp(['ERROR: readgadget: unable to open file: ',fname])
 return
end

% Read the header
hdr=ReadGadgetHeader(fid);

% The base data, position, velocity, and particle type
N=sum(hdr.npart);

blocksize=fread(fid,1,'uint');
D.r = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

blocksize=fread(fid,1,'uint');
D.v = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

blocksize=fread(fid,1,'uint');
D.id = fread(fid,N,'uint');
blocksize=fread(fid,1,'uint');

% The mass, if necessary
ind=find((hdr.npart > 0) & (hdr.mass == 0));
if(~isempty(ind))
 Nwithmass=sum(hdr.npart(ind));
 blocksize=fread(fid,1,'uint');
 D.mass = fread(fid,Nwithmass,'float');
 blocksize=fread(fid,1,'uint');
end

% For the gas, read the thermal energy and the density
Ngas=hdr.npart(1);
if(Ngas > 0)
 blocksize=fread(fid,1,'uint');
 D.u = fread(fid,Ngas,'float');
 blocksize=fread(fid,1,'uint');
 if(ICflag ~= 1)
  % IC files don't have rho & h
  blocksize=fread(fid,1,'uint');
  D.rho = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.h = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
 end
end

% Close the file
fclose(fid);
