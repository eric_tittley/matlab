% writegadget: Write a Gadget file
%
% writegadget(fname,D,hdr,ICflag)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to write
%   D       The data in the file, a structure
%   hdr     The header of the file, a structure
%   ICflag  1 if the file is an IC file [optional, default 0]
%
% RETURNS
%  Nothing
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%   flag_cooling
%   num_files
%   BoxSize
%   Omega0
%   OmegaLambda
%   HubbleParam
%   flag_stellarage
%   flag_metals
%   hashtabsize
%
%  Elements of D
%   r
%   v
%   id
%   mass
%   u
%   rho
%   h
%
% SEE ALSO
%  readgadget writegadgetField and the Gadget User's Guide

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave
%
% HISTORY
%  11 12 07 First version.

function writegadget(fname,D,hdr,ICflag)

% Default behaviour
if(nargin < 4)
 ICflag=0;
end

% Open the file
fid=fopen(fname,'wb');
if(fid<=0)
 disp(['ERROR: writegadget: unable to open file: ',fname])
 return
end

% The header is 256 bytes
HeaderBytes = 256;
count=0; % # of words (4 bytes)
fwrite(fid,HeaderBytes,'uint32');
fwrite(fid,hdr.npart          ,'int32');   count=count+6;
fwrite(fid,hdr.mass           ,'float64'); count=count+12;
fwrite(fid,hdr.time           ,'float64'); count=count+2;
fwrite(fid,hdr.redshift       ,'float64'); count=count+2;
fwrite(fid,hdr.sfr            ,'int32');   count=count+1;
fwrite(fid,hdr.feedback       ,'int32');   count=count+1;
fwrite(fid,hdr.npartTotal     ,'int32');   count=count+6;
fwrite(fid,hdr.flag_cooling   ,'int32');   count=count+1;
fwrite(fid,hdr.num_files      ,'int32');   count=count+1;
fwrite(fid,hdr.BoxSize        ,'float64'); count=count+2;
fwrite(fid,hdr.Omega0         ,'float64'); count=count+2;
fwrite(fid,hdr.OmegaLambda    ,'float64'); count=count+2;
fwrite(fid,hdr.HubbleParam    ,'float64'); count=count+2;
fwrite(fid,hdr.flag_stellarage,'int32');   count=count+1;
fwrite(fid,hdr.flag_metals    ,'int32');   count=count+1;
fwrite(fid,hdr.hashtabsize    ,'int32');   count=count+1;

% Write the rest of the 256 byte buffer
bytesleft = 256 - count*4;
Block=zeros(1,bytesleft);
fwrite(fid,Block,'char');
fwrite(fid,HeaderBytes,'uint32');

% The base data, position, velocity, and particle type
N=sum(hdr.npart);
writefield(fid,D.r, 'float32');
writefield(fid,D.v, 'float32');
writefield(fid,D.id,'uint32' );

% The mass, if necessary
ind=find((hdr.npart > 0) & (hdr.mass == 0));
if(~isempty(ind))
 Nwithmass=sum(hdr.npart(ind));
 writefield(fid,D.mass,'float32');
end

% For the gas, write the thermal energy and the density
Ngas=hdr.npart(1);
if(Ngas > 0)
 writefield(fid,D.u,   'float32');
 if(ICflag ~= 1)
  % IC files don't have rho & h
  writefield(fid,D.rho, 'float32');
  writefield(fid,D.h,   'float32');
 end
end

% Close the file
fclose(fid);

end % writegadget

% ------- LOCAL FUNCTIONS ------

function writefield(fid,Var,Precision)

 switch Precision
  case 'float32',
   A = single(Var);
  case 'uint32',
   A = uint32(Var);
  otherwise,
   disp(['Precision ',Precision,' unknown'])
 end

 S=whos('A');
 fwrite(fid,S.bytes,'uint32');
 fwrite(fid,A,Precision);
 fwrite(fid,S.bytes,'uint32');

end %writefield
