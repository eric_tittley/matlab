% CalcDensitiesForAllFiles: For all the re-gridded Millenium files, find the
%                           densities.
%

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab

% HISTORY
%  101020 First version, based on script from 071029

function CalcDensitiesForAllFiles(MATDir)

% These should probably be parameters passed in
padding=5;
Nsph=32;
h0=0.001;
types=[{'gas'},{'DM'}];

for ix=1:5
 for iy=1:5
  for iz=1:5
   for itype=1:length(types)
    ifile=ix*100+iy*10+iz;
    file=[MATDir,'/densities_',int2str(ifile),'_',char(types(itype)),'.mat']
    if(~ exist(file))
     Range=[ix-1,ix;iy-1,iy;iz-1,iz]*100;
     Range(:,1)=Range(:,1)-padding;
     Range(:,2)=Range(:,2)+padding;
     disp(Range)
     [D,hdr]=ExtractMilleniumVolume(Range,MATDir,125,500);
     if(itype==1) %gas
      list=[1:hdr.npart(1)];
     else %DM
      list=hdr.npart(1)+[1:hdr.npart(2)];
     end
     r=D.r(:,list);
     clear D
     good=find(   (r(1,:)>=(ix-1)*100) & (r(1,:)<ix*100) ...
                & (r(2,:)>=(iy-1)*100) & (r(2,:)<iy*100) ...
                & (r(3,:)>=(iz-1)*100) & (r(3,:)<iz*100) );
     % Put r on the interval [0:1)
     r=r/500;
     centre=mean(Range'/500);
     r=recentre(r,centre); % Now r is on [0.4-padding/500,0.6+padding/500];
     r=(r-(0.4-padding/500))/(0.2+padding/250); % Now r is on [0,1)
     [h,dn]=calcdens_d(r,Nsph,h0);
%     disp('Clearing calcdens...')
%     clear fun calcdens
     clear r
     disp('Cleared')
     % Extract only the unpadded data
     %good=find( sum(r>0.05 & r<0.95)==3 );
     h=h(good);
     dn=dn(good);
     clear good
     % Scale h
     scale=500/(1-padding/50);
     h=h*scale;
     dn=dn*(scale^3);
     % Save the data
     disp('Writing...')
     size(h)
     save(file,'h','dn')
     clear h dn
    end % if density file does not exist
   end % Loop over gas and DN
  end % loop over iz
 end % loop over iy
end % loop over ix
