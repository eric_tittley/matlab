% ExtractFromDensityFiles: Extract a list of particle densities from Millenium.
%
% For a Gadget multi-file data that has been:
%  1) re-filed into a set of N^3 data sets in MAT format using
%     GroupOutputIntoVolumeFiles.
%  2) had the gas and dark matter densities and smoothing lengths re-calculated
%     using CalcDensitiesForAllFiles.
% extract the densities and smoothing lengths for the particles in the list.
% The list is usually generated by ExtractMilleniumVolume.
%
% [h_ext,dn_ext]=ExtractFromDensityFiles(list,dir,itype_str)
%
% ARGUMENTS
%  list		A structure of the form:
%		list.ifile	The 3-digit file ID
%		list.index	A cell array of indices into the files.
%		list.offset	The offset where that species begins.
%  dir		The directory containing the densities_* files.
%  itype_str	A string (either 'gas' or 'dark') indicating the particle type.
%
% RETURNS
%  h_ext	The smoothing lengths for the particles in the list.
%  dn_ext	The densities for the particles in the list.
%
% REQUIRES
%  Nothing.  But the relevant files must have already been generated.
%
% SEE ALSO
%  GroupOutputIntoVolumeFiles
%  CalcDensitiesForAllFiles

% AUTHOR: Eric Tittley
%
% HISTORY
%  071029 First version
%
% COMPATIBILITY: Matlab, Octave

function [h_ext,dn_ext]=ExtractFromDensityFiles(list,dir,itype_str)

% Set the itype
types=[{'gas'},{'DM'}];
if(itype_str(1)=='d' | itype_str(1)=='D')
 itype=2;
else
 itype=1;
end

dn_ext=[];
h_ext =[];
for i=1:length(list.ifile)
 file=[dir,'/densities_',int2str(list.ifile(i)),'_',char(types(itype)),'.mat'];
 load(file)
 index=(list.index{i})-list.offset(i);
 dn_ext=[dn_ext;dn(index)];
 h_ext =[h_ext;  h(index)];
end
