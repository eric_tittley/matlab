function writePMic(D,filename,endianess)
% writePMic: Write a PM inititial conditions file.
%
% D=writePMic(filename)
%
% ARGUMENTS
%  filename     The name of the file
%  D		A structure containing the header (hdr), positions (r),
%		and velocities (v)
%  endianess    'l', 'b', or 'n' (default)
%
% OUTPUT
%  Nothing
%
% REQUIRES
%
% SEE ALSO
%  PMRT, loadPMic, loadPMRTdata, loadPMdensity

% AUTHOR Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  090709 First version

if(nargin==2)
 endianess='n';
end

% Open the file
fid=fopen(filename,'w',endianess);
if(fid == -1)
 error(['writePMic: unable to open file ',filename,' for writing'])
end

fwrite(fid,D.hdr.eflag  ,'int32');
fwrite(fid,D.hdr.hsize  ,'int32');
fwrite(fid,D.hdr.npart  ,'int32');
fwrite(fid,D.hdr.nsph   ,'int32');
fwrite(fid,D.hdr.nstar  ,'int32');
fwrite(fid,D.hdr.aa     ,'float32');
fwrite(fid,D.hdr.softlen,'float32');

fwrite(fid,D.r,'float32');
fwrite(fid,D.v,'float32');

fclose(fid);
