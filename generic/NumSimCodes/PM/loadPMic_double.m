function D=loadPMic(filename)
% loadPMic: Read a PM inititial conditions file containing the PM (particle) info.
%
% D=loadPMic(filename)
%
% ARGUMENTS
%  filename     The name of the file
%
% OUTPUT
%  D		A structure containing the header (hdr), positions (r),
%		and velocities (v)
%
% REQUIRES
%
% SEE ALSO
%  PMRT, loadPMRTdata, loadPMdensity

% AUTHOR Eric Tittley
%
% HISTORY
%  070123 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave (?)

fid=fopen(filename,'r');

D.hdr.eflag  =fread(fid,1,'int32');
D.hdr.hsize  =fread(fid,1,'int32');
D.hdr.npart  =fread(fid,1,'int32');
D.hdr.nsph   =fread(fid,1,'int32');
D.hdr.nstar  =fread(fid,1,'int32');
D.padding    =fread(fid,1,'int32');
D.hdr.aa     =fread(fid,1,'float64');
D.hdr.softlen=fread(fid,1,'float64');

D.r=fread(fid,3*D.hdr.npart,'float64');
D.r=reshape(D.r,3,D.hdr.npart);
D.v=fread(fid,3*D.hdr.npart,'float64');
D.v=reshape(D.v,3,D.hdr.npart);

fclose(fid);
