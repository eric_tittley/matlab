function D=loadPMic(filename)
% loadPMic: Read a PM inititial conditions file containing the PM (particle) info.
%
% D=loadPMic(filename)
%
% ARGUMENTS
%  filename     The name of the file
%
% OUTPUT
%  D		A structure containing the header (hdr), positions (r),
%		and velocities (v)
%
% REQUIRES
%
% SEE ALSO
%  PMRT, loadPMRTdata, loadPMdensity

% AUTHOR Eric Tittley
%
% COMPATIBILITY: Matlab, Octave (?)
%
% HISTORY
%  070123 First version
%  071029 Modified comments
%  090709 Deal with differing endianess

% Open the file
fid=fopen(filename,'r','l');
if(fid == -1)
 error(['loadPMic: unable to open file ',filename,' for reading'])
end

% Check the byte order, closing and reopenning with reverse byte order if
% necessary.
D.hdr.eflag  =fread(fid,1,'int32');
if(D.hdr.eflag ~= 1)
 % Byte order may be wrong.  Close and reopen assuming different byte order.
 fclose(fid);
 fid=fopen(filename,'r','b');
 D.hdr.eflag  =fread(fid,1,'int32');
 if(D.hdr.eflag ~= 1)
  error(['loadPMRTdata: Unable to determine byte order in file ',filename])
 end
end

D.hdr.hsize  =fread(fid,1,'int32');
D.hdr.npart  =fread(fid,1,'int32');
D.hdr.nsph   =fread(fid,1,'int32');
D.hdr.nstar  =fread(fid,1,'int32');
D.hdr.aa     =fread(fid,1,'float32');
D.hdr.softlen=fread(fid,1,'float32');

D.r=single(fread(fid,3*D.hdr.npart,'float32'));
D.r=reshape(D.r,3,D.hdr.npart);
D.v=single(fread(fid,3*D.hdr.npart,'float32'));
D.v=reshape(D.v,3,D.hdr.npart);

fclose(fid);
