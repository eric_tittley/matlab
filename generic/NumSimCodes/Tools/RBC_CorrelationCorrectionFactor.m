% RBC_CorrelationCorrectionFactor: Repeating boundary condition correction factor for correlation functions
%
% f=RBC_CorrelationCorrectionFactor(SearchRadiusAsFractionOfBoxSize)
%
% ARGUMENTS
%  SearchRadiusAsFractionOfBoxSize  The scale over which we are searching
%
% RETURNS
%  The correction factor to divide the correlation factor by
%
% SEE ALSO
%  pscorr
%
% NOTES
%  What this function actually provides is:
%   Given a random point in a volume of size L and given a search radius, R,
%   returns the probability that another point is inside the search radius but
%   actually outside the volume.
%
% TODO:  More extensive testing!!  This function might just be a stub. A poorly
%        working stub.  Need an extensive MC test

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  130603 First version

function f=RBC_CorrelationCorrectionFactor(SearchRadiusAsFractionOfBoxSize)

if(SearchRadiusAsFractionOfBoxSize<0 | SearchRadiusAsFractionOfBoxSize>0.5)
 error('RBC_CorrelationCorrectionFactor: SearchRadiusAsFractionOfBoxSize out of range [0 0.5]')
end

R=SearchRadiusAsFractionOfBoxSize;

%f =   (1-2*R).^3 ... % Centre cube
%    + 6*(1-2*R).^2.*R * 13/16 ... % Side slabs
%    + 12*(1-2*R).*R.^2 * 0.385 ... % Side edges
%    + 8*R.^3 * (1- 3/(2*pi)) ; % Corners

f = 1-1.5374*R+0.7854*R.^2-0.2372*R.^3;
