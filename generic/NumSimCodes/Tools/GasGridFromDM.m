% GasGridFromDM: Generate a gas density and velocity grid from a dark matter
%                particle distribution.
%
% [rho_gas,v_gas]=GasGridFromDM(r,v,Res,Cosmology,Params);
%
% ARGUMENTS
%  r            Particle positions [3xM] [0, 1)
%  v            Particle velocities [3xM]
%  Res          Number of grid cells on each edge
%  Cosmology
%       Omega_m Fraction of critical density, all matter (dark+gas)
%       Omega_b Fraction of critical density, gas only
%  Params       [Optional] Structure containing gridding parameters
%       NSPH    The number of particles to smooth over
%       h_guess A guess at the smoothing length for all particles
%
% RETURNS
%  rho_gas      Grid of gas densities  [Res x Res x Res] (Fraction of Omega)
%  v_gas        Grid of gas velocities [3 x Res x Res x Res] (Units of v)
%
% REQUIRES
%  calcdens_d, h_proj_3d
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab and Octave
%
% HISTORY
%  110311 First version
%  110322 Scale the masses to something sensical
%
% TODO
%  Cosmology.Omega_m isn't used. Don't require it.

function [rho_gas,v_gas]=GasGridFromDM(r,v,Res,Cosmology,Params)

% Check input parameters and set to default values
Params.Default=0;
ParameterArgumentNumber=5;
if(nargin < ParameterArgumentNumber)
 Params.Default=1;
end

if(~isfield(Params,'Nsph'))
 Params.Nsph=64;
end
if(~isfield(Params,'h_guess'))
 Params.h_guess=length(r).^(-1/3);
end

% Ensure r is [3xM]
[N,M]=size(r);
if(N ~= 3 & M ~= 3)
 error('GasGridFromDM: r must be [3xM]');
end
if(max([N M])<Params.Nsph)
 error('GasGridFromDM: There must be more than ',int2str(Params.Nsph), ...
       ' particles.')
end
if(M==3)
 r=r';
end

% Ensure r is on the range [0,1)
if( min(r(:))<0 || max(r(:))>=1 )
 error('GasGridFromDM: r must be in the range [0,1)')
end

% Ensure r is double precision
r=double(r);

% Ensure v is [3xM]
[N,M]=size(v);
if(M==3)
 v=v';
end

% Ensure v is the same length at r
if( sum(size(r) ~= size(v)) > 0 )
 error('GasGridFromDM: r and v must be the same size')
end

% Ensure v is double precision
v=double(v);

% Cosmology parameters
if( ~ isfield(Cosmology,'Omega_m') )
 error('GasGridFromDM: Cosmology.Omega_m not defined')
end
if( ~ isfield(Cosmology,'Omega_b') )
 error('GasGridFromDM: Cosmology.Omega_b not defined')
end

% --------- End of input checks ---------

% Calculate smoothing radii
[h,dn]=calcdens_d(r,Params.Nsph,Params.h_guess);
clear dn

% Project particle numbers onto grid
z=ones(1,length(r));
rho_gas=h_proj_3d(r,z,h,Res,[0 1 0 1 0 1]);
clear z

% Project particle velocities onto grid
% Note, first index is fast index, so our assignment will be slow, but the data
% will be stored (vx,vy,vz)_i for i=1:N
v_gas=zeros(3,Res,Res,Res);
for i=1:3
 v_gas(i,:,:,:)=h_proj_3d(r,v(i,:),h,Res,[0 1 0 1 0 1]);
end

% Normalise velocities by grid density
for i=1:3
 v_gas(i,:,:,:)=squeeze(v_gas(i,:,:,:))./rho_gas;
end

% Scale the gas grid appropriately.
if(0)
% First, ensure that sum(gas(:))==length(r)
scale=length(r)/sum(rho_gas(:));
rho_gas=scale*rho_gas;
% Now for the cosmological ratios
scale=Cosmology.Omega_b/Cosmology.Omega_m;
rho_gas=scale*rho_gas;

else

% Otherwise, scale to Omega
% Since all the cells are the same size, <rho> should equal Omega_b
scale=Cosmology.Omega_b/mean(rho_gas(:));
rho_gas=scale*rho_gas;

end

% DONE!
