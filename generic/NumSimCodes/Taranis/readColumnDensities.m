% readColumnDensities: Read column densities from Taranis diagnosis output file.
%
% [NH1, NHe1, NHe2, ParticleIndices, GadgetMap]=readColumnDensities(File)

function [NH1, NHe1, NHe2, ParticleIndices, GadgetMap]=readColumnDensities(File)


FloatFlag=1;
FID=fopen(File,'rb');


LengthOfNHI   =fread(FID, 1, 'uint64');
LengthOfNHeI  =fread(FID, 1, 'uint64');
LengthOfNHeII =fread(FID, 1, 'uint64');
LengthOfPartIndices =fread(FID, 1, 'uint64');
LengthOfGadgetMap =fread(FID, 1, 'uint64');

if(FloatFlag)
 NH1  = fread(FID, LengthOfNHI   , 'float32');
 NHe1 = fread(FID, LengthOfNHeI  , 'float32');
 NHe2 = fread(FID, LengthOfNHeII , 'float32');
else
 NH1  = fread(FID, LengthOfNHI   , 'float64');
 NHe1 = fread(FID, LengthOfNHeI  , 'float64');
 NHe2 = fread(FID, LengthOfNHeII , 'float64');
end
ParticleIndices=fread(FID,LengthOfPartIndices,'uint32');
GadgetMap=fread(FID,LengthOfGadgetMap,'uint32');
fclose(FID);
