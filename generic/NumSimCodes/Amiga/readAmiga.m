% readAmiga: Read an Amiga cosmological simulation data file
%
% [D,hdr]=readAmiga(file)
%
% ARGUMENTS
%  file		The file to read
%
% RETURNS
%  D		The data [rx, vx, ry, vy, rz, vz, [m]] x N_obj
%  hdr		The header
%
% SEE ALSO
%  writeAmiga

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave and Matlab
%
% HISTORY
%  081218 First version

function [D,hdr]=readAmiga(file)

D=dir(file);
if(size(D,1)==0)
 error('readAmiga: Unable to find file')
end

% Open the Amiga file, swapping endianess if neccessary. 
[fid,SizeOfLong]=open_amiga_file(file);

% Read the header
hdr=read_amiga_header(fid,SizeOfLong);

% Read the rv data
D=read_amiga_data(fid,hdr);

% Close the file.
fclose(fid);

% END readAmiga

function [fid,SizeOfLong]=open_amiga_file(file)
% Open the Amiga file, swapping endianess if neccessary.
% Read the header, checking the byte-order
fid=fopen(file,'r','l');
if(fid == -1)
 error(['readAmiga: unable to open file ',file,' for reading'])
end
SizeOfLong=fread(fid,1,'int');
if( (SizeOfLong ~= 4) && (SizeOfLong ~= 8) )
 % Byte order may be wrong.  Close and reopen assuming different byte order.
 fclose(fid);
 fid=fopen(file,'r','b');
 SizeOfLong=fread(fid,1,'int');
 if( (SizeOfLong ~= 4) && (SizeOfLong ~= 8) )
  error(['readAmiga: Unable to determine byte order in file ',file])
  fclose(fid);
 end
end
% END open_amiga_header

function hdr=read_amiga_header(fid,SizeOfLong)

AMIGAHEADER  = 2048;
HEADERSTRING = 256;
%FILLHEADER   = AMIGAHEADER-HEADERSIZE;

if(SizeOfLong==4)
 Long='int32';
else
 Long='int64';
end
hdr.header           = fread(fid,HEADERSTRING,'char');
hdr.multi_mass       = fread(fid,1,'int');
hdr.double_precision = fread(fid,1,'int');
hdr.no_part          = fread(fid,1,Long);
hdr.no_species       = fread(fid,1,Long);
hdr.no_vpart         = fread(fid,1,'double');
hdr.timestep         = fread(fid,1,'double');
hdr.no_timestep      = fread(fid,1,'int');
if(SizeOfLong==8) dummy=fread(fid,1,'int'); end
hdr.boxsize          = fread(fid,1,'double');
hdr.omega0           = fread(fid,1,'double');
hdr.lambda0          = fread(fid,1,'double');
hdr.pmass            = fread(fid,1,'double');
hdr.cur_reflevel     = fread(fid,1,'double');
hdr.cur_frcres       = fread(fid,1,'double');
hdr.a_initial        = fread(fid,1,'double');
hdr.a_current        = fread(fid,1,'double');
hdr.K_initial        = fread(fid,1,'double');
hdr.K_current        = fread(fid,1,'double');
hdr.U_initial        = fread(fid,1,'double');
hdr.U_current        = fread(fid,1,'double');
hdr.Eintegral        = fread(fid,1,'double');
hdr.Econst           = fread(fid,1,'double');

hdr.paramNSTEPS       = fread(fid,1,'double');
hdr.paramNGRID_DOM    = fread(fid,1,'double');
hdr.paramNth_dom      = fread(fid,1,'double');
hdr.paramNth_ref      = fread(fid,1,'double');
hdr.paramE_UPDATE     = fread(fid,1,'double');
hdr.paramCELLFRAC_MAX = fread(fid,1,'double');
hdr.paramCELLFRAC_MIN = fread(fid,1,'double');
hdr.paramCA_CRIT      = fread(fid,1,'double');
hdr.paramNGRID_MAX    = fread(fid,1,'double');
hdr.paramDOMSWEEPS    = fread(fid,1,'double');
hdr.paramREFSWEEPS    = fread(fid,1,'double');

hdr.paramAHF_MINPART  = fread(fid,1,'double');
hdr.paramAHF_VTUNE    = fread(fid,1,'double');
hdr.paramAHF_RISE     = fread(fid,1,'double');
hdr.paramAHF_SLOPE    = fread(fid,1,'double');
hdr.paramAHF_MAXNRISE = fread(fid,1,'double');

hdr.paramBDM_MINPART  = fread(fid,1,'double');
hdr.paramBDM_VTUNE    = fread(fid,1,'double');
hdr.paramBDM_RISE     = fread(fid,1,'double');
hdr.paramBDM_SLOPE    = fread(fid,1,'double');
hdr.paramBDM_MAXNRISE = fread(fid,1,'double');
hdr.paramBDM_MINREF   = fread(fid,1,'double');
hdr.paramBDM_RCHANGE  = fread(fid,1,'double');
hdr.paramBDM_MRATIO   = fread(fid,1,'double');

hdr.version           = fread(fid,1,'double');
hdr.built             = fread(fid,1,'int');
if(SizeOfLong==8) dummy=fread(fid,1,'int'); end
hdr.omegab            = fread(fid,1,'double');
hdr.gamma             = fread(fid,1,'double');
hdr.H_frac            = fread(fid,1,'double');
hdr.T_init            = fread(fid,1,'double');

% Position file at the end of the header.  The +4 is for the initial SizeOfLong.
fseek(fid,AMIGAHEADER+4,'bof');

% END read_amiga_header

function D=read_amiga_data(fid,hdr)
if(hdr.multi_mass==1)
 BUFSIZE = 7;
else
 BUFSIZE = 6;
end
if(hdr.double_precision==1)
 D=fread(fid,[BUFSIZE,hdr.no_part],'double');
else
 D=fread(fid,[BUFSIZE,hdr.no_part],'float');
end
% END read_amiga_data
