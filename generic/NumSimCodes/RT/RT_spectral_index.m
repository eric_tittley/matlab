function alpha_p = RT_spectral_index(alpha,n_H1,n_He1)
% RT_spectral_index: Estimate the spectral index in a cell.  (nu/nu_0)^alpha_p
%
% alpha_p = RT_spectral_index(alpha,x,y)
%
% ARGUMENTS
%  alpha	Input spectral index				(scalar)
%  n_H1		The cumulative column density of HI (m^-2)	(Array)
%  n_He1	The cumulative column density of HeI (m^-2)	(Array)
%
% RETURNS
%  alpha_p	The approximate spectral index in the cells	(Array)
%
% REQUIRES
%
% SEE ALSO
%  PMRT

% NOTES
%  Method and constants derived in notes.  Method uses the L(nu) in the cell
%  at the threshold frequencies nu_H1 and nu_He1.

% AUTHOR: Eric Tittley
%
% HISTORY
%  040503 First version.
%  071030 Modified comments.
%
% COMPATIBILITY: Matlab, Octave.

alpha_p = alpha + (5.06e-22 * n_H1 - 7.83e-22 * n_He1) / 0.592 ;
