function density=loadPMdensity(filename,NFLUX)
% loadPMdensity: Load a density file produced by the RT part of the PM code.
%
% density=loadPMdensity(filename,NFLUX)
%
% ARGUMENTS
%  filename	The name of the file containing the density dump.
%  NFLUX	The number of cells in the z-direction.
%		The dimensions of density witll be set as NFLUX x NG x NG
%		with NG calculated from the number of elements read in and
%		NFLUX.
%
% OUTPUT
%  density	A 3D array of densities
%
% REQUIRES
%
% SEE ALSO
%  PMRT, loadPMRTdata, loadPMic

% AUTHOR Eric Tittley
%
% HISTORY
%  04 01 15 First version
%  04 02 11 Added argument NFLUX and calculate NG
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave (?)

fid=fopen(filename,'r');
[density, N] = fread(fid,inf,'float32');
fclose(fid);
NG=sqrt(N/NFLUX);
if(NG*NG*NFLUX ~= N)
 error('NFLUX inconsistent with size of density grid')
end
density=reshape(density,[NFLUX NG NG]);
