% RT_IonisationHeating: The local heating rate due to ionisation/ionizations.
%
% [G_H1,G_He1,G_He2]=RT_IonisationHeating(n_H1,n_He1,n_He2,N_H1_cum,N_He1_cum,N_He2_cum,R,z,Spectrum)
% [G_H1,G_He1,G_He2]=RT_IonisationHeating(Data,z,Spectrum)
%
% ARGUMENTS
%  n_H1         Number density of H1 [m^-3]
%  n_He2        Number density of He1 [m^-3]
%  n_He3        Number density of He2 [m^-3]
%  N_H1_cum     Cumulative column density to the cell of H1  [m^-2]
%  N_He1_cum    Cumulative column density to the cell of He1 [m^-2]
%  N_He2_cum    Cumulative column density to the cell of He2 [m^-2]
%  R            Distance from the source to the cell         [m]
%  z            Redshift (used only for providing a time ref for the source)
%  Spectrum     The spectrum of the source.  See RT_Luminosity [struct]
%
% or
%
%  Data         A structure with elements:
%                column_H1
%                column_He1
%                column_He2
%                R
%  z            Redshift (used only for providing a time ref for the source)
%  Spectrum     The spectrum of the source.  See RT_Luminosity [struct]
%  
% RETURNS
%  G_H1         Ionisation heating rate for H1  [s^-1]
%  G_He1        Ionisation heating rate for He1 [s^-1]
%  G_He2        Ionisation heating rate for He2 [s^-1]
%
% REQUIRES
%  RT_Luminosity
%
% SEE ALSO
%  RT_Luminosity

% AUTHOR: Eric Tittley
%
% HISTORY
%  110608 First version, based on RT_Gamma
%
% COMPATIBILITY: Matlab, Octave

function [G_H1,G_He1,G_He2]=RT_IonisationHeating(varargin)

switch length(varargin)
 case 3
  Data      = varargin{1};
  n_H1      = [Data.f_H1]  .* [Data.n_H] ;
  n_He1     = [Data.f_He1] .* [Data.n_He];
  n_He2     = [Data.f_He2] .* [Data.n_He];
  N_H1_cum  = [Data.column_H1];
  N_He1_cum = [Data.column_He1];
  N_He2_cum = [Data.column_He2];
  R         = [Data.R];
  z         = varargin{2};
  Spectrum  = varargin{3};
 case 9
  n_H1      = varargin{1};
  n_He1     = varargin{2};
  n_He2     = varargin{3};
  N_H1_cum  = varargin{4};
  N_He1_cum = varargin{5};
  N_He2_cum = varargin{6};
  R         = varargin{7};
  z         = varargin{8};
  Spectrum  = varargin{9};
 otherwise
  error(['RT_IonisationHeating: Incorrect number of input arguments: ', ...
        int2str(length(varargin))])
end

% Check the arguments

% We can only work with redshift being a scalar
if(~isscalar(z))
 error('RT_IonisationHeating: z must be a scalar')
end

% Now, were we given a list of cells, or a single cell?
%  No matter what they are, we only need them as vectors
%  But first remember what size they were
[NArr,MArr]=size(N_H1_cum);
n_H1      = n_H1(:);
n_He1     = n_He1(:);
n_He2     = n_He2(:);
N_H1_cum  = N_H1_cum(:);
N_He1_cum = N_He1_cum(:);
N_He2_cum = N_He2_cum(:);
R=R(:);
%  How many?
N=length(N_H1_cum);
if(length(n_H1)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
if(length(n_He1)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
if(length(n_He2)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
if(length(N_He1_cum)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
if(length(N_He2_cum)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
if(length(R)~=N)
 error('RT_IonisationHeating: n_*, N_*_cum, and R must be the same size')
end
%  If we have been given a list of cells, re-assign, since the nested functions
%  expect R & *_cum to be scalars
if(N>1)
 n_H1_vec      = n_H1;
 n_He1_vec     = n_He1;
 n_He2_vec     = n_He2;
 N_H1_cum_vec  = N_H1_cum;
 N_He1_cum_vec = N_He1_cum;
 N_He2_cum_vec = N_He2_cum;
 R_vec         = R;
end

% Globals
% Constant
h_p = 6.6260688e-34; % Planck constant. [J s]
% Ionisation thresholds
nu_0 = 3.282e15; % Hz
nu_1 = 5.933e15; % Hz
nu_2 = 1.313e16; % Hz

if(N==1)
 % Use Gauss-Kronrod
 IntFun=@quadgk;
 G_H1  = IntFun(@dG_dnu_H1_H2,  nu_0,1000*nu_2);
 G_He1 = IntFun(@dG_dnu_He1_He2,nu_1,1000*nu_2);
 G_He2 = IntFun(@dG_dnu_He2_He3,nu_2,1000*nu_2);
else
 % Allocate
 G_H1 =0*N_H1_cum;
 G_He1=0*N_H1_cum;
 G_He2=0*N_H1_cum;
 for i=1:N
  n_H1      = n_H1_vec(i);
  n_He1     = n_He1_vec(i);
  n_He2     = n_He2_vec(i);
  N_H1_cum  = N_H1_cum_vec(i);
  N_He1_cum = N_He1_cum_vec(i);
  N_He2_cum = N_He2_cum_vec(i);
  R         = R_vec(i);
  % Use Gauss-Kronrod
  IntFun=@quadgk;
  G_H1(i)  = IntFun(@dG_dnu_H1_H2,  nu_0,1000*nu_2);
  G_He1(i) = IntFun(@dG_dnu_He1_He2,nu_1,1000*nu_2);
  G_He2(i) = IntFun(@dG_dnu_He2_He3,nu_2,1000*nu_2);
 end
 % Return as the same shape as input
 G_H1  = reshape(G_H1, NArr,MArr);
 G_He1 = reshape(G_He1,NArr,MArr);
 G_He2 = reshape(G_He2,NArr,MArr);
end

% Nested functions

function y = dG_dnu_H1_H2(nu)
 J=Intensity(nu);
 y = n_H1 * (J./nu) .* (nu-nu_0) .* H1_H2(nu); 
end %dG_dnu()

function y = dG_dnu_He1_He2(nu)
 J=Intensity(nu);
 y = n_He1 * (J./nu) .* (nu-nu_1) .* He1_He2(nu); 
end %dG_dnu()

function y = dG_dnu_He2_He3(nu)
 J=Intensity(nu);
 y = n_He2 * (J./nu) .* (nu-nu_2) .* He2_He3(nu); 
end %dG_dnu()

function J=Intensity(nu)
 % externally defined
 
 L=RT_Luminosity(nu,z,Spectrum);
 
 % Optical depth to the cell
 tau = 0*nu;
 mask=nu>nu_0;
 tau(mask) =             N_H1_cum  * H1_H2(nu(mask));
 mask=nu>nu_1;
 tau(mask) = tau(mask) + N_He1_cum * He1_He2(nu(mask));
 mask=nu>nu_2;
 tau(mask) = tau(mask) + N_He2_cum * He2_He3(nu(mask));
 
 if(Spectrum.PLANE_WAVE)
  % Intensity is luminosity attentuated by 1/R_0^2 and optical depth.
  % R_0 is actually R_0 * aa * Mpc / h  The calling function needs to set this
  % correctly
  J = L./(4*pi*(Spectrum.Ro)^2) .* exp(-tau);
 else
  % Intensity is luminosity attentuated by 1/r^2 and optical depth.
  J = L./(4*pi*R^2) .* exp(-tau);
 end
 
end % Intensity()

function cs=H1_H2(nu)
 sigma = 6.3e-22;
 beta_o = 1.34;
 s = 2.99;
 cs=0*nu;
 mask=nu>nu_0;
 cs(mask)=sigma * (   beta_o*(nu(mask)/nu_0).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_0).^(-s-1) );
end

function cs=He1_He2(nu)
 sigma = 7.83e-22;
 beta_o = 1.66;
 s = 2.05;
 cs=0*nu;
 mask=nu>nu_1;
 cs(mask)=sigma * (   beta_o*(nu(mask)/nu_1).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_1).^(-s-1) );
end

function cs=He2_He3(nu)
 sigma = 1.58e-22;
 beta_o = 1.34;
 s = 2.99;
 cs=0*nu;
 mask=nu>nu_2;
 cs(mask)=sigma * (  beta_o*(nu(mask)/nu_2).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_2).^(-s-1) );
end

% End of nested functions

end % RT_IonisationHeating()
