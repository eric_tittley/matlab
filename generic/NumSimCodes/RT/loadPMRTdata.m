function [Data,Ref]=loadPMRTdata(filename)
% loadPMRTdata: Load the gas data structure output by the PMRT code
%
% [Data,Ref]=loadPMRTdata(filename)
%
% ARGUMENTS
%  filename     Name of the file to be read in.
%  NFLUX        The length of the column vector for each line of sight.
%
% OUTPUT
%  Data         An array of NLOS structures containing 29 vectors of length
%               NFLUX.  NLOS is the number of lines of sight.  NLOS is computed
%               from the file size.
%  Ref (optional) The Refinement data.  Same structure as Data, just a
%               a different number of elements.
%
% NOTES
%  Structure elements:
%       G    
%       n    
%       T    
%       L    
%       n_H  
%       n_H1 
%       n_H2 
%       n_He 
%       n_He1
%       n_He2
%       n_He3
%       R    
%       D    
%       tau_H1
%       G_H1 
%       G_He1
%       G_He2
%       I_H1
%       I_He1
%       I_He2
%       L_H1 
%       L_He1
%       L_He2
%       L_eH 
%       L_C  
%       E_H1 
%       E_He1
%       E_He2
%       column_H1
%       column_He1
%       column_He2
%       CellBufferIndex
%       Cell
%       NumCells
%
% REQUIRES
%
% SEE ALSO
%  PMRT
%  getRTdata    Extracts volumes of data from PMRT data structure.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

D=dir(filename);
if(size(D,1)==0)
 error('loadPMRTdata: Unable to find file')
end

% Read the header, checking the byte-order
fid=fopen(filename,'r','l');
if(fid == -1)
 error(['loadPMRTdata: unable to open file ',filename,' for reading'])
end
hdr=fread(fid,256,'int');
if(hdr(1) ~= 1)
 % Byte order may be wrong.  Close and reopen assuming different byte order.
 fclose(fid);
 fid=fopen(filename,'r','b');
 hdr=fread(fid,256,'int');
 if(hdr(1) ~= 1)
  error(['loadPMRTdata: Unable to determine byte order in file ',filename])
 end
end
VER = hdr(2)+hdr(3)/10.0;
NFLUX=hdr(4);
NLOS=hdr(5);

Data_VER = VER;
if( (VER==2.1) | (VER == 2.0) )
 Data_VER=1.2;
end
% The RT_DATA_SHORT should be a separate flag in the header.

Data=ReadRTStructures(Data_VER,NLOS,NFLUX,fid);

if(nargout>1)
 % Refinement data requested
 if(Data_VER~=3.6)
  warning(['loadPMRTdata: Refinements cannot be read for RT data of version ',...
          num2str(Data_VER)])
 else
  NRef=sum(sum([Data.CellBufferIndex]>0));
  if(NRef>0)
   Ref=ReadRTStructures(Data_VER,NRef,0,fid);
  else
   Ref=[];
  end
 end
end

fclose(fid);

% END of loadPMRTdata()

% Read in an RT data structure.
% Function private to loadPMRTdata
function Data=ReadRTStructures(Data_VER,NLOS,NFLUX,fid)
switch Data_VER
 case 2.3
  for los=1:NLOS
   Data(los).G            =fread(fid,NFLUX,'double');
   Data(los).n            =fread(fid,NFLUX,'double');
   Data(los).T            =fread(fid,NFLUX,'double');
   Data(los).L            =fread(fid,NFLUX,'double');
   Data(los).n_H          =fread(fid,NFLUX,'double');
   Data(los).f_H1         =fread(fid,NFLUX,'double');
   Data(los).f_H2         =fread(fid,NFLUX,'double');
   Data(los).n_He         =fread(fid,NFLUX,'double');
   Data(los).f_He1        =fread(fid,NFLUX,'double');
   Data(los).f_He2        =fread(fid,NFLUX,'double');
   Data(los).f_He3        =fread(fid,NFLUX,'double');
   Data(los).R            =fread(fid,NFLUX,'double');
   Data(los).D            =fread(fid,NFLUX,'double');
   Data(los).tau_H1       =fread(fid,NFLUX,'double');
   Data(los).G_H1         =fread(fid,NFLUX,'double');
   Data(los).G_He1        =fread(fid,NFLUX,'double');
   Data(los).G_He2        =fread(fid,NFLUX,'double');
   Data(los).I_H1         =fread(fid,NFLUX,'double');
   Data(los).I_He1        =fread(fid,NFLUX,'double');
   Data(los).I_He2        =fread(fid,NFLUX,'double');
   Data(los).L_H1         =fread(fid,NFLUX,'double');
   Data(los).L_He1        =fread(fid,NFLUX,'double');
   Data(los).L_He2        =fread(fid,NFLUX,'double');
   Data(los).L_eH         =fread(fid,NFLUX,'double');
   Data(los).L_C          =fread(fid,NFLUX,'double');
   Data(los).E_H1         =fread(fid,NFLUX,'double');
   Data(los).E_He1        =fread(fid,NFLUX,'double');
   Data(los).E_He2        =fread(fid,NFLUX,'double');
   Data(los).column_H1    =fread(fid,NFLUX,'double');
   Data(los).column_He1   =fread(fid,NFLUX,'double');
   Data(los).column_He2   =fread(fid,NFLUX,'double');
   Data(los).v_los        =fread(fid,NFLUX,'double');
   Data(los).v_x          =fread(fid,NFLUX,'double');
   Data(los).Entropy      =fread(fid,NFLUX,'double');
   Data(los).CellBufferIndex =fread(fid,NFLUX,'int64');
   Data(los).Cell         =fread(fid,1,'int32');
   Data(los).NumCells     =fread(fid,1,'int32');
  end
 case 3.2
  for los=1:NLOS
   Data(los).NumCells     =fread(fid,1,'int32');
   Data(los).Cell         =fread(fid,1,'int32');
   Data(los).NBytes       =fread(fid,1,'int32');
   NFLUX=Data(los).NumCells;
   Data(los).R            =fread(fid,NFLUX,'double');
   Data(los).D            =fread(fid,NFLUX,'double');
   Data(los).Dold         =fread(fid,NFLUX,'double');
   Data(los).Entropy      =fread(fid,NFLUX,'double');
   Data(los).T            =fread(fid,NFLUX,'double');
   Data(los).tau_H1       =fread(fid,NFLUX,'double');
   Data(los).n            =fread(fid,NFLUX,'double');
   Data(los).n_H          =fread(fid,NFLUX,'double');
   Data(los).f_H1         =fread(fid,NFLUX,'double');
   Data(los).f_H2         =fread(fid,NFLUX,'double');
   Data(los).n_He         =fread(fid,NFLUX,'double');
   Data(los).f_He1        =fread(fid,NFLUX,'double');
   Data(los).f_He2        =fread(fid,NFLUX,'double');
   Data(los).f_He3        =fread(fid,NFLUX,'double');
   % Parts not included in RT_DATA_SHORT
   Data(los).G            =fread(fid,NFLUX,'double');
   Data(los).G_H1         =fread(fid,NFLUX,'double');
   Data(los).G_He1        =fread(fid,NFLUX,'double');
   Data(los).G_He2        =fread(fid,NFLUX,'double');
   Data(los).I_H1         =fread(fid,NFLUX,'double');
   Data(los).I_He1        =fread(fid,NFLUX,'double');
   Data(los).I_He2        =fread(fid,NFLUX,'double');
   Data(los).L            =fread(fid,NFLUX,'double');
   Data(los).L_H1         =fread(fid,NFLUX,'double');
   Data(los).L_He1        =fread(fid,NFLUX,'double');
   Data(los).L_He2        =fread(fid,NFLUX,'double');
   Data(los).L_eH         =fread(fid,NFLUX,'double');
   Data(los).L_C          =fread(fid,NFLUX,'double');
   Data(los).E_H1         =fread(fid,NFLUX,'double');
   Data(los).E_He1        =fread(fid,NFLUX,'double');
   Data(los).E_He2        =fread(fid,NFLUX,'double');
   % End of long parts
   Data(los).column_H1    =fread(fid,NFLUX,'double');
   Data(los).column_He1   =fread(fid,NFLUX,'double');
   Data(los).column_He2   =fread(fid,NFLUX,'double');
   Data(los).v_z          =fread(fid,NFLUX,'double');
   Data(los).v_x          =fread(fid,NFLUX,'double');
   Data(los).NCols        =fread(fid,12,'double');
   Data(los).CellBufferIndex =fread(fid,NFLUX,'int64');
  end
 case 3.6
  for los=1:NLOS
   Data(los).NumCells     =fread(fid,1,'int32');
   Data(los).Cell         =fread(fid,1,'int32');
   Data(los).NBytes       =fread(fid,1,'int32');
   NFLUX=Data(los).NumCells;
   Data(los).R            =fread(fid,NFLUX,'double');
   Data(los).D            =fread(fid,NFLUX,'double');
   Data(los).Dold         =fread(fid,NFLUX,'double');
   Data(los).Entropy      =fread(fid,NFLUX,'double');
   Data(los).T            =fread(fid,NFLUX,'double');
   Data(los).tau_H1       =fread(fid,NFLUX,'double');
   Data(los).n            =fread(fid,NFLUX,'double');
   Data(los).n_H          =fread(fid,NFLUX,'double');
   Data(los).f_H1         =fread(fid,NFLUX,'double');
   Data(los).f_H2         =fread(fid,NFLUX,'double');
   Data(los).n_He         =fread(fid,NFLUX,'double');
   Data(los).f_He1        =fread(fid,NFLUX,'double');
   Data(los).f_He2        =fread(fid,NFLUX,'double');
   Data(los).f_He3        =fread(fid,NFLUX,'double');
   Data(los).column_H1    =fread(fid,NFLUX,'double');
   Data(los).column_He1   =fread(fid,NFLUX,'double');
   Data(los).column_He2   =fread(fid,NFLUX,'double');
   Data(los).v_z          =fread(fid,NFLUX,'double');
   Data(los).v_x          =fread(fid,NFLUX,'double');
   Data(los).NCols        =fread(fid,12,'double');
   Data(los).CellBufferIndex =fread(fid,NFLUX,'int64');
  end
 otherwise
  error(['Unknown version ',num2str(Data_VER)])
end % switch - case

% END ReadRTStructures()
