function [Data,Ref]=readRTdata(filename)
% readRTdata: Load the RT data structure output by the RT code
%
% [Data,Ref]=readRTdata(filename)
%
% ARGUMENTS
%  filename     Name of the file to be read in.
%
% OUTPUT
%  Data         An array of NLOS structures containing 29 vectors of length
%               NFLUX.  NLOS is the number of lines of sight.  NLOS is computed
%               from the file size.
%  Ref (optional) The Refinement data.  Same structure as Data, just a
%               a different number of elements.
%
% NOTES
%  Structure elements:
%       NumCells
%       Cell
%       NBytes
%       R    
%       dR    
%       D    
%       Dold
%       Entropy
%       T    
%       n_H  
%       f_H1 
%       f_H2 
%       n_He 
%       f_He1
%       f_He2
%       f_He3
%       column_H1
%       column_He1
%       column_He2
%     The following are available in some files.
%       G    
%       G_H1 
%       G_He1
%       G_He2
%       I_H1
%       I_He1
%       I_He2
%       L    
%       L_H1 
%       L_He1
%       L_He2
%       L_eH 
%       L_C  
%       E_H1 
%       E_He1
%       E_He2
%       NCols
%       CellBufferIndex
%
% REQUIRES
%
% SEE ALSO
%  loadPMRTdata For the old RT data format
%  getRTdata    Extracts volumes of data from PMRT data structure.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

D=dir(filename);
if(size(D,1)==0)
 error('readRTdata: Unable to find file')
end

% Open the file
fid=fopen(filename,'r','l');
if(fid == -1)
 error(['readRTdata: unable to open file ',filename,' for reading'])
end

% Read the 1024-byte header, checking the byte-order
% The first half is Integer, the second half floating point
hdr=fread(fid,128,'int');
if(hdr(1) ~= 1)
 % Byte order may be wrong.  Close and reopen assuming different byte order.
 fclose(fid);
 fid=fopen(filename,'r','b');
 hdr=fread(fid,128,'int32');
 if(hdr(1) ~= 1)
  error(['readRTdata: Unable to determine byte order in file ',filename])
 end
end
% The if hdr(2) is 3 and hdr(3) is 9, then VER becomes 3.9
% The if hdr(2) is 3 and hdr(3) is 11, then VER becomes 3.11
VER = hdr(2)+hdr(3)/10^floor(log10(hdr(3))+1);
NFLUX=hdr(4);
NLOS=hdr(5);

Flags.Rates       = hdr(6);
Flags.Velocities  = hdr(7);
Flags.NCols       = hdr(8);
Flags.Refinements = hdr(9);
if(hdr(3)<4)
 Flags.Dold       = 1;
else
 Flags.Dold       =0;
end
Flags.Single      = hdr(10);
Flags.EquilibriumValues = 0;
if(VER>=4.0)
 Flags.EquilibriumValues = 1;
end
Flags.Timescales  = hdr(11);

hdr=fread(fid,128,'float32');
Header.ExpansionFactor = hdr(1);
Header.Redshift        = hdr(2);
Header.Time            = hdr(3);

Data=ReadRTStructures(NLOS,NFLUX,Flags,fid);

Data.Flags  = Flags;
Data.Header = Header;

if(nargout>1)
 % Refinement data requested
 if(Flags.Refinements==0)
  warning(['readRTdata: Refinements cannot be read.'])
 else
  NRef=sum(sum([Data.CellBufferIndex]>0));
  if(NRef>0)
   Ref=ReadRTStructures(NRef,0,Flags,fid);
  else
   Ref=[];
  end
 end
end

fclose(fid);

% END of readRTdata()

% Read in an RT data structure.
% Function private to readRTdata
function Data=ReadRTStructures(NLOS,NFLUX,Flags,fid)
 if(Flags.Single==1)
  FloatPrecision = 'single';
 else
  FloatPrecision = 'double';
 end
 for los=1:NLOS
  Data(los).NumCells     =fread(fid,1,'uint64');
  Data(los).Cell         =fread(fid,1,'uint64');
  Data(los).NBytes       =fread(fid,1,'uint64');
  NFLUX=Data(los).NumCells;
  Data(los).R            =fread(fid,NFLUX,FloatPrecision);
  Data(los).dR           =fread(fid,NFLUX,FloatPrecision);
  Data(los).D            =fread(fid,NFLUX,FloatPrecision);
  if(Flags.Dold)
   Data(los).Dold         =fread(fid,NFLUX,FloatPrecision);
  end
  Data(los).Entropy      =fread(fid,NFLUX,FloatPrecision);
  Data(los).T            =fread(fid,NFLUX,FloatPrecision);
  Data(los).n_H          =fread(fid,NFLUX,FloatPrecision);
  Data(los).f_H1         =fread(fid,NFLUX,FloatPrecision);
  Data(los).f_H2         =fread(fid,NFLUX,FloatPrecision);
  Data(los).n_He         =fread(fid,NFLUX,FloatPrecision);
  Data(los).f_He1        =fread(fid,NFLUX,FloatPrecision);
  Data(los).f_He2        =fread(fid,NFLUX,FloatPrecision);
  Data(los).f_He3        =fread(fid,NFLUX,FloatPrecision);
  Data(los).column_H1    =fread(fid,NFLUX,FloatPrecision);
  Data(los).column_He1   =fread(fid,NFLUX,FloatPrecision);
  Data(los).column_He2   =fread(fid,NFLUX,FloatPrecision);
  if(Flags.Rates==1)
   % Parts not included in RT_DATA_SHORT
   Data(los).G            =fread(fid,NFLUX,FloatPrecision);
   Data(los).G_H1         =fread(fid,NFLUX,FloatPrecision);
   Data(los).G_He1        =fread(fid,NFLUX,FloatPrecision);
   Data(los).G_He2        =fread(fid,NFLUX,FloatPrecision);
   Data(los).I_H1         =fread(fid,NFLUX,FloatPrecision);
   Data(los).I_He1        =fread(fid,NFLUX,FloatPrecision);
   Data(los).I_He2        =fread(fid,NFLUX,FloatPrecision);
   Data(los).L            =fread(fid,NFLUX,FloatPrecision);
   Data(los).L_H1         =fread(fid,NFLUX,FloatPrecision);
   Data(los).L_He1        =fread(fid,NFLUX,FloatPrecision);
   Data(los).L_He2        =fread(fid,NFLUX,FloatPrecision);
   Data(los).L_eH         =fread(fid,NFLUX,FloatPrecision);
   Data(los).L_C          =fread(fid,NFLUX,FloatPrecision);
   Data(los).E_H1         =fread(fid,NFLUX,FloatPrecision);
   Data(los).E_He1        =fread(fid,NFLUX,FloatPrecision);
   Data(los).E_He2        =fread(fid,NFLUX,FloatPrecision);
  end
  if(Flags.Velocities==1)
   Data(los).v_z          =fread(fid,NFLUX,FloatPrecision);
   Data(los).v_x          =fread(fid,NFLUX,FloatPrecision);
  end
  if(Flags.EquilibriumValues)
   Data(los).n_HI_Equilibrium  =fread(fid,NFLUX,FloatPrecision);
   Data(los).n_HII_Equilibrium =fread(fid,NFLUX,FloatPrecision);
   Data(los).Gamma_HI          =fread(fid,NFLUX,FloatPrecision);
  end
  if(Flags.Timescales)
   Data(los).TimeScales   =fread(fid,NFLUX,FloatPrecision);
  end
  if(Flags.NCols==1)
   Data(los).NCols        =fread(fid,12,FloatPrecision);
  end
  if(Flags.Refinements==1)
   Data(los).CellBufferIndex =fread(fid,NFLUX,'int64');
  end
 end
% END ReadRTStructures()
