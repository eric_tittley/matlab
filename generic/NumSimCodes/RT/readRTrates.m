function Rates=readRTrates(filename)
% readRTrates: Load the RT rates produced by the RT code
%
% Rates=readRTrates(filename)
%
% ARGUMENTS
%  filename     Name of the file to be read in.
%
% OUTPUT
%  Rates        A structure containing vectors of rates.
%
% REQUIRES
%
% SEE ALSO
%  readRTdata

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

D=dir(filename);
if(size(D,1)==0)
 error('readRTrates: Unable to find file')
end

% Open the file
fid=fopen(filename,'r','l');
if(fid == -1)
 error(['readRTrates: unable to open file ',filename,' for reading'])
end

% Read the 1024-byte header, checking the byte-order
% The first half is Integer, the second half floating point
hdr=fread(fid,128,'int');
if(hdr(1) ~= 1)
 % Byte order may be wrong.  Close and reopen assuming different byte order.
 fclose(fid);
 fid=fopen(filename,'r','b');
 hdr=fread(fid,128,'int32');
 if(hdr(1) ~= 1)
  error(['readRTrates: Unable to determine byte order in file ',filename])
 end
end
NumCells      = hdr(2);
Flags.SinglePrecision = hdr(3);
Flags.Cooling = hdr(4);

hdr=fread(fid,128,'float32');

Rates=ReadRTRatesStructure(NumCells,Flags,fid);

Rates.Flags  = Flags;

fclose(fid);

% END of readRTrates()

% Read in an RT rates structure.
% Function private to readRTrates
function Rates=ReadRTRatesStructure(NumCells,Flags,fid)
 if(Flags.SinglePrecision==1)
  FloatPrecision = 'single';
 else
  FloatPrecision = 'double';
 end
 Rates.I_H1	    =fread(fid,NumCells,FloatPrecision);
 Rates.I_He1	    =fread(fid,NumCells,FloatPrecision);
 Rates.I_He2	    =fread(fid,NumCells,FloatPrecision);
 Rates.G	    =fread(fid,NumCells,FloatPrecision);
 Rates.G_H1	    =fread(fid,NumCells,FloatPrecision);
 Rates.G_He1	    =fread(fid,NumCells,FloatPrecision);
 Rates.G_He2	    =fread(fid,NumCells,FloatPrecision);
 Rates.alpha_H1     =fread(fid,NumCells,FloatPrecision);
 Rates.alpha_He1    =fread(fid,NumCells,FloatPrecision);
 Rates.alpha_He2    =fread(fid,NumCells,FloatPrecision);
 Rates.L	    =fread(fid,NumCells,FloatPrecision);
 Rates.L_H1	    =fread(fid,NumCells,FloatPrecision);
 Rates.L_He1	    =fread(fid,NumCells,FloatPrecision);
 Rates.L_He2	    =fread(fid,NumCells,FloatPrecision);
 if(Flags.Cooling==1)
  Rates.L_eH	    =fread(fid,NumCells,FloatPrecision);
  Rates.L_C	    =fread(fid,NumCells,FloatPrecision);
 end
 Rates.Gamma_HI     =fread(fid,NumCells,FloatPrecision);
 Rates.n_HI_Equilibrium  =fread(fid,NumCells,FloatPrecision);
 Rates.n_HII_Equilibrium =fread(fid,NumCells,FloatPrecision);
 Rates.TimeScales   =fread(fid,NumCells,FloatPrecision);
% END ReadRTRatesStructures()
