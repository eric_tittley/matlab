% R_ifront_estimate: The position of an ionisation front, plane wave
%
% R = R_ifront_estimate_polar(D,t,params);
%
% ARGUMENTS
%  D
%   r    distances (vector) [m]
%   n_H  number densities of HI in the interval r(i) to r(i+1) (vector) [m^-3]
%  t     times at which to calculate the I-front position, R (vector) [s]
%  F     Flux of ionising photons from the source (scalar) [s^-1 m^2]
%
% RETURNS
%  R	Position of the ionization front.
%
% NOTES
%  Assumes only Hydrogen
%
% SEE ALSO
%  R_ifront_estimate_polar, R_ifront_estimate_spherical

function R = R_ifront_estimate(D,t,F)

r0=D.R(1);
[tout,R]=ode45(@f,t,r0);

 % ---------- NESTED FUNCTIONS ------------
 % D and params are shared
 function dRdt=f(t,r)
  n_H_r=number_density_H(r);
  dRdt = F/n_H_r;
  dRdt(dRdt>299792458) = 299792458;
 end
 % END function Ifront_progression_rate()

 function n_H_r=number_density_H(r)
  % Number density of neutral Hydrogen
  ind=[1:length(D.R)];
  n_H_r=0*r;
  mask = ( (r>=D.R(1)) && (r<=D.R(end)) );
  index = floor(interp1(D.R,ind,r(mask)));
  n_H_r(mask) = D.n_H(index);
  mask = (r > D.R(end));
  n_H_r(mask) = D.n_H(end);
  mask = (r < D.R(1));
  n_H_r(mask) = 0;
 end
 % End of local function, number_density_H()

 % ---------- END OF NESTED FUNCTIONS ------------

end
% END of R_ifront_estimate()
