% RT_Luminosity: The source spectrum.
%
% L=RT_Spectrum(nu,z,Spectrum)
%
% ARGUMENTS
%  nu		Frequencies at which to sample the spectrum [Hz] 	(vector)
%  z            current redshift                                        (scalar)
%  Spectrum     Structure detailing the spectrum
%   Elements:
%    .Type ('PL','SB',or 'HY' for Power-law, Star-burst, and hybrid)
%    .PL.Lo                                             [W/Hz]          (scalar)
%    .PL.alpha                                                          (scalar)
%      such that L = PL.Lo*(nu/nu_0).^PL.alpha
%    .SB.Lo                                             [W/Hz]          (scalar)
%    .SB.SB_L_nu_0                                      [Hz]            (scalar)
%    .SB.Table.nu                                       [Hz]            (vector)
%    .SB.Table.L                                        [W/Hz]          (vector)
%      such that L = SB.Lo/SB.SB_L_nu_0 * interp1(SB.Table.nu,SB.Table.L,nu);
%    .HY.z_trans_start                                                  (scalar)
%    .HY.z_trans_finish                                                 (scalar)
%
% RETURNS
%  L	The energy spectrum of photons [W/Hz]	(vector of length(nu))
%
% REQUIRES
%
% SEE ALSO
%  PMRT, RT_Gamma, RT_Spectrum
%
% USAGE
%  nu = [3.282e15:1e13:3e16]; % [Hz]
%
%  SB_N_photons    = 0.1047;
%  QSO_SB_L_factor = 0.5;
%
%  SpecFile = '/home/ert/PMRT/Spectra/StarBurst.dat';
%  Spec = load(SpecFile);
%  Table.nu = Spec(:,1);  % [Hz]
%  Table.L  = Spec(:,2);  % [W/Hz]
%  SB.Lo = 1.00e+21;      % [W/Hz]
%  SB.SB_L_nu_0 = 5.3e10; % [W/Hz]
%  SB.Table = Table;
%
%  PL.alpha = -8;
%  PL.Lo = SB.Lo * QSO_SB_L_factor * (-PL.alpha * SB_N_photons);
%
%  HY.z_trans_start  = 3.5;
%  HY.z_trans_finish = 2.5;
%
%  Spectrum.Type='HY';
%  Spectrum.PL = PL;
%  Spectrum.SB = SB;
%  Spectrum.HY = HY;
%
%  z = 4.0;
%  L=RT_Luminosity(nu,z,Spectrum);
%  loglog(nu,L,'k');
%  hold on
%  z = 3.0;
%  L=RT_Luminosity(nu,z,Spectrum);
%  loglog(nu,L,'r');
%  z = 2.0;
%  L=RT_Spectrum(nu,z,Spectrum);
%  loglog(nu,L,'b');
%  hold off

% AUTHOR: Eric Tittley
%
% HISTORY
%  090629 First version. Based on RT_Spectrum.
%
% COMPATIBILITY: Matlab, Octave

function L=RT_Luminosity(nu,z,Spectrum)

% Ionisation thresholds */
nu_0 = 3.282e15;
nu_1 = 5.933e15;
nu_2 = 1.313e16;

method = 'cubic';

% The input spectrum
switch lower(Spectrum.Type(1))
 case 'p', % Power Law
  L=Spectrum.PL.Lo*(nu/nu_0).^Spectrum.PL.alpha;
 case 's', % Star Burst
  L= Spectrum.SB.Lo/Spectrum.SB.SB_L_nu_0 ...
    *interp1(Spectrum.SB.Table.nu,Spectrum.SB.Table.L,nu,method,0);
 case 'h', % Hybrid
  if(z > Spectrum.HY.z_trans_start)
   L= Spectrum.SB.Lo/Spectrum.SB.SB_L_nu_0 ...
     *interp1(Spectrum.SB.Table.nu,Spectrum.SB.Table.L,nu,method,0);
  elseif(z < Spectrum.HY.z_trans_finish)
   L=Spectrum.PL.Lo*(nu/nu_0).^Spectrum.PL.alpha;
  else
   L_SB = Spectrum.SB.Lo/Spectrum.SB.SB_L_nu_0 ...
         *interp1(Spectrum.SB.Table.nu,Spectrum.SB.Table.L,nu,method,0);
   L_PL = Spectrum.PL.Lo*(nu/nu_0).^Spectrum.PL.alpha;
   fraction_of_z_trans = (z-Spectrum.HY.z_trans_finish)...
                        /(  Spectrum.HY.z_trans_start ...
                          - Spectrum.HY.z_trans_finish);
   L = fraction_of_z_trans*L_SB + (1.-fraction_of_z_trans)*L_PL;
  end
 otherwise,
  disp(['Unknown spectrum unit class: ',Spectrum.Type])
end % switch

end % RT_Luminosity()
