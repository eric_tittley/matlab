Files=dir('*P00.data');
N=length(Files);
zs=zeros(N,1);
for i=1:N
 zs(i)=sscanf(Files(i).name,'RT_Z%f_P00.data');
end
zs=flipud(sort(zs));

%Span=129:192; % For 128x128 with 5 layers
Span=257:384; % For 256x256 with 5 layers

dummy=Merge_EnzoRT_RT_files(zs(1),[0 5 6 7],Span,'D');

D=zeros([size(dummy),N]);

for i=1:N
 D(:,:,i)=Merge_EnzoRT_RT_files(zs(i),[0 5 6 7],Span,'D');
end

DRange=[-1 1];
for i=1:N
 imagesc(T(:,:,i),TRange);
 axis square
 cbh=colorbar;
 ax=gca;
 axes(cbh);
 ylabel('kK');
 axes(ax);
 %set(gca,'ylim',[1 1e6],'xlim',[0 130])
 % text(225,-5,num2str(zs(i),'z=%6.3f')) % For 256x256
 text(110,-5,num2str(zs(i),'z=%6.3f'))
 %M(i)=getframe();
 print('-dtiff',['/tmp/matlab-',int2str_zeropad(i,4),'.tiff'])
end


%movie2gifmov(M,'T_movie.gif',1,0.05)

CWD=pwd;
cd /tmp
Files=dir('matlab*.tiff');
for i = 1:length(Files)
 cmd=['set giffile=`basename ', ...
      Files(i).name, ...
      ' .tiff`.gif;', ...
      ' tifftogif ', ...
      Files(i).name, ...
      ' $giffile;'];
 system(cmd);
end
cmd=['gifmerge -0.05 matlab*.gif > ',CWD,'/Density_Movie.gif']
system(cmd);
system('rm matlab*.tiff matlab*.gif');
cd(CWD)
