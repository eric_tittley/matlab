% Combine_EnzoRT_RT: Combine RT Data from a set of EnzoRT RT files.
%
% D = Combine_EnzoRT_RT(z, grids, span, Dir)
%
% EnzoRT generates multiple RT files for the same timestep, each corresponding
% to a node. Combine_EnzoRT_RT() merges the data from the multiple RT files.
%
% ARGUMENTS
%  z          Redshift
%  grids      The layout of the grids. See note
%  span       If only a subset is required. Otherwise set to 0.
%             This is a tricky one, depending on the subgrid layout
%             and the total grid size
%             Set it to the range of LOSs in each Subgrid that you want.
%             For example, a 512^3 grid on 8 processors will be split into
%             8 256^3 grids
%             If there are three slices, and you only want the middle slice from
%             each, then slice=257:512
%             
%  Dir        The directory the files are located in [optional]
%
% RETURNS
%  RT Data structure array
%
% NOTES
%  Grid layout
%   grids will change depending on the LOSs used.  But here are some guesses
%   to get you going.
% case 8
%  grids=[0 4; 3 7] or [0 5; 6 7]
% case 16
%  grids=[0 12 8 4; 15 11 7 3];
%
% REQUIRES
%  loadPMRTdata, int2str_zeropad, StructCat
%
% SEE ALSO
%  loadPMRTdata, enzo_merge_outputs, Extract_EnzoRT_RT_field

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  11 01 19 First version.

function D = Combine_EnzoRT_RT(z, grids, span, Dir)

if(nargin==3)
 Dir='./';
end

% Get the size of grids. The number of rows is the depth of the LOSs, while
% the number of columns is the number of groups of LOSs
[NLOSStacks,NLOSGroups]=size(grids);

% Read in the data files
CWD=pwd;
cd(Dir)
for i=1:NLOSStacks
 for j=1:NLOSGroups
  file=['RT_Z',num2str(z,'%.3f'),'_P',int2str_zeropad(grids(i,j),2),'.data'];
  DataIn(i,j).D=loadPMRTdata(file);
 end
end
cd(CWD)

% Apply the span if needed
if( ~ (isscalar(span) & (span==0)) )
 for i=1:NLOSStacks
  for j=1:NLOSGroups
   DataIn(i,j).D=DataIn(i,j).D(span);
  end
 end
end

% Merge the Data.
%  Major assumptions, here:
%   The start of the LOS's will be in the first row of "grids"
%   Every subsequent row of "grids" will correspond to an extension of
%   the LOSs in the first row.
for Cell_group_index=1:NLOSStacks
 % First concatenate the row, initialising with the first LOS_group
 Drow=DataIn(Cell_group_index,1).D;
 for LOS_group_index=2:NLOSGroups
  Drow=[Drow DataIn(Cell_group_index,LOS_group_index).D];
 end
 % Second: concatenate the new row to the previous row
 if(Cell_group_index==1)
  D=Drow;
 else
  D=StructCat(D,Drow);
 end
end
