% User supplied parameters:
Grids = [0 6; 7 5];
TRange=[0 5e4]/1e3;
%Span=129:192; % For 128x128 with 5 layers
Span=257:384; % For 256x256 with 5 layers


Files=dir('*P00.data');
N=length(Files);
zs=zeros(N,1);
for i=1:N
 zs(i)=sscanf(Files(i).name,'RT_Z%f_P00.data');
end
zs=flipud(sort(zs));

dummy=Extract_EnzoRT_RT_field(zs(1),Grids,Span,'T');

T=zeros([size(dummy),N]);

for i=1:N
 T(:,:,i)=Extract_EnzoRT_RT_field(zs(i),Grids,Span,'T');
end

for i=1:N
 imagesc(T(:,:,i)'/1e3,TRange);
 axis square
 set(gca,'visible','off');
 colormap(hot);
 cbh=colorbar;
 ax=gca;
 axes(cbh);
 ylabel('kK');
 axes(ax);
 XLim=get(gca,'XLim');
 text(11/13 * XLim(2),-5,num2str(zs(i),'z=%6.3f'))
 print('-dtiff',['/tmp/matlab-',int2str_zeropad(i,4),'.tiff'])
end

%movie2gifmov(M,'T_movie.gif',1,0.05)

CWD=pwd;
cd /tmp
Files=dir('matlab*.tiff');
for i = 1:length(Files)
 cmd=['set giffile=`basename ', ...
      Files(i).name, ...
      ' .tiff`.gif;', ...
      ' tifftogif ', ...
      Files(i).name, ...
      ' $giffile;'];
 system(cmd);
end
cmd=['gifmerge -0.05 matlab*.gif > ',CWD,'/T_Movie.gif']
system(cmd);
system('rm matlab*.tiff matlab*.gif');
cd(CWD)
