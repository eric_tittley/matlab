function centre=centre_quick(r,dn,last_centre)
% centre=centre_quick(r,dn,last_centre)
% r is assumed to range 0..1

if(nargin==2) last_centre=0; end
if(length(last_centre)==1) last_centre=0; end

tol=0.1;

if(last_centre==0)
 fudge=1.0; MM=0;
 while (MM==0)
  fudge=0.9*fudge;
  peakmask=find(dn>(max(dn)*fudge));
  [MM,NN]=size(peakmask);
 end
 [n,x]=hist(r(peakmask,1));
 if(std(r(peakmask,1))>tol)
  max_x=x(find(n==max(n)));
  peakmask=peakmask(find((r(peakmask,1)>max_x(1)-0.1)&(r(peakmask,1)<max_x(1)+0.1)));
 end
 [MM,NN]=size(peakmask);
 if(MM==1) centre=r(peakmask,:);
 else centre=mean(r(peakmask,:));
 end

else
 r=rem(r+([1:length(r)]&1)'*([1.5 1.5 1.5]-last_centre),1);  %now we're centred on the previous density max location.
 boxmin=0.5-tol; boxmax=0.5+tol;
 boxmask=find(r(:,1)>boxmin & r(:,1)<boxmax & r(:,2)>boxmin & r(:,2)<boxmax & r(:,3)>boxmin & r(:,3)<boxmax);
 peakmask=boxmask(find(dn(boxmask)>(max(dn(boxmask))*0.4)));   %find the density peak within a box around the previous density max
 [n,x]=hist(r(peakmask,1));
 if(std(r(peakmask,1))>1)
  max_x=x(find(n==max(n)));
  peakmask=peakmask(find((r(peakmask,1)>max_x(1)-0.1)&(r(peakmask,1)<max_x(1)+0.1)));
 end
 [MM,NN]=size(peakmask);
 if(MM==1) centre=r(peakmask,:);
 else centre=mean(r(peakmask,:));
 end
%newcentre=lastcentre+deltacentre
%deltacentre=centre-0.5
%newcentre=rem(newcentre,1)
%=>newcentre=rem(lastcentre+centre-0.5,1)
%==newcentre=rem(lastcentre+centre-0.5+1,1)
%=>newcentre=rem(lastcentre+centre+0.5,1)
 centre=rem(last_centre+centre+[0.5 0.5 0.5],1);
end

