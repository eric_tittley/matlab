function [centre,time]=centre_find(filename,last_centre)
% centre_find: Find the centre of a cluster using a guess at the position.
%
% [centre,time]=centre_find(filename)
%  Find the centre of the density maximum in the file, filename.
%
% [centre,time]=centre_find(filename,last_centre)
%  Same as above, but look for the density maximum within 0.2 of last_centre.
%
% ARGUMENTS
%  filename	The file from which to read the density data.
%  last_centre (optional) The guess at the location of the centre.
%
% METHOD
%  Searches for the density maximum for the gas particles.
%
% BUGS
%  Not sure how this ever worked before.  L is not defined.

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 03 01 Mature version
%  04 07 28 Added comments.
%	Modified to deal with readhydra (not readdata) and the 3xN dimensions
%	for r & v. Noted L is not defined.  How did this ever work before?!?

if(nargin==1) last_centre=0; end
if(length(last_centre)==1) last_centre=0; end

tol=0.2;

[rm,r,v,th,itype,dn,h,time,atime]=readhydra(filename);

if(last_centre==0)
 peakmask=find(dn>(max(dn)*0.4));
 [n,x]=hist(r(1,peakmask));
 if(std(r(1,peakmask))>1)
  max_x=x(find(n==max(n)));
  peakmask=peakmask(find(( (r(1,peakmask)>max_x(1)-L/10)) ...
                          &(r(1,peakmask)<max_x(1)+L/10)) );
 end
 [MM,NN]=size(peakmask);
 if(MM==1) centre=r(:,peakmask);
 else centre=mean(r(:,peakmask));
 end

else
 % Centre on the previous density max location.
 r=rem(r+([1:length(r)]&1)'*(L*[1.5 1.5 1.5]-last_centre),L);
 boxmask=find(  r(1,:)>L/2-tol & r(1,:)<L/2+tol ...
              & r(2,:)>L/2-tol & r(2,:)<L/2+tol ...
              & r(3,:)>L/2-tol & r(3,:)<L/2+tol);
 % Find the density peak within a box around the previous density max
 peakmask=boxmask(find(dn(boxmask)>(max(dn(boxmask))*0.4)));
 [n,x]=hist(r(1,peakmask));
 if(std(r(1,peakmask))>1)
  max_x=x(find(n==max(n)));
  peakmask=peakmask(find(( (r(1,peakmask)>max_x(1)-L/10)) ...
                          &(r(1,peakmask)<max_x(1)+L/10)) );
 end
 [MM,NN]=size(peakmask);
 if(MM==1) centre=r(:,peakmask);
 else centre=mean(r(:,peakmask));
 end
 centre=rem(last_centre+(centre+L*[0.5 0.5 0.5])-[0.5 0.5 0.5],L)+[0.5 0.5 0.5];
end

