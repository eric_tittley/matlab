% CentreOfPositionList: Given a list of positions, find the meaningful centre.
%
% Centre=CentreOfPositionList(r)
%
% ARGUMENTS
%  r    List of positions [3,N], on [0,1) assuming repeating boundary
%       conditions.
%
% RETURNS
%  Centre  The meaningful centre (i.e. peak in the density)
%
% NOTES
%  The method is expecting a list of positions that are all or mostly members
%  of a distinct clump. Outliers which are not in the clump are elliminated
%  prior to estimate of the centre.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab
%
% HISTORY
%  120808 First version


function Centre=CentreOfPositionList(r)

%%% Get the number of objects, transpose if necessary
nPos=size(r,2);
if(nPos==3)
 if(size(r,1)~=3)
  r=r';
  nPos=size(r,2);
 end
end

if(nPos<5)
 Centre=mean(r');
else % More than 5 positions

 % Find the particle in the highest density
 % Nsph should be between 5 and 64
 nSPH = max([5 min([64 ceil(nPos/4)])]);
 h_guess=nNSPH^(-1/3);
 [h,dn]=calcdens_d(r,nSPH,h_guess);
 iMaxRho = find(dn==max(dn));

 % Centre on the density maximum
 DensityMaxPos=r(:,iMaxRho);
 r_recentred=recentre(r,DensityMaxPos);

 % Clip out data far from the density peak
 sigmas=1.5;
 [dataout,Index1]=clip(r_recentred(1,:),sigmas);
 [dataout,Index2]=clip(r_recentred(2,Index1),sigmas);
 [dataout,Index3]=clip(r_recentred(3,Index1(Index2)),sigmas);
 Index=Index1(Index2(Index3));

 % Find the mean position (should be around [0.5 0.5 0.5] => DensityMaxPos)
 CentredCentre=mean(r(:,Index)');
 
 % Shift to where it belongs
 Centre=rem(CentredCentre+1.5,1);
end
