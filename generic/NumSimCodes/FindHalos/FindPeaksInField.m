% FindPeaksInField: Find the positions of peaks in an array
%
% Pos=FindPeaksInField(A)
%
% ARGUMENTS
%  A  An array of numeric values
%
% RETURNS
%  Pos  The index positions of the peaks in A
%
% USAGE
%  For multi-dimensional data, you can extract the dimensional positions as
%   A=rand([5 5 5]);
%   Pos=FindPeaksInField(A);
%   [iX,iY,iZ]=ind2sub(size(A),Pos);

% TODO
%  Case 2 could be treated like case 3 to conserve memory
%  Move to a more generic function location, like data or arrays

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function Pos=FindPeaksInField(A)

NumberOfDimensions=length(size(A));

switch NumberOfDimensions
 case 1
  A=A(:);
  Aleft  = shift(A,-1,1);
  Aright = shift(A, 1,1);
  ispeak = (A>Aleft & A>Aright);
  Pos = find(ispeak);
 case 2
  Aleft      =shift(A     ,-1,1);
  Aright     =shift(A	  , 1,1);
  Aup	     =shift(A	  , 1,2);
  Adown      =shift(A	  ,-1,2);
  Aleft_up   =shift(Aleft , 1,2);
  Aleft_down =shift(Aleft ,-1,2);
  Aright_up  =shift(Aright, 1,2);
  Aright_down=shift(Aright,-1,2);
  ispeak=(A>Aleft & A>Aright & A>Aup & A>Adown & A>Aleft_up & A>Aleft_down & A>Aright_up & A>Aright_down );
  Pos = find(ispeak);
 case 3
  ispeak=ones(size(A));
  for di=[-1:1]
   for dj=[-1:1]
    for dk=[-1:1]
     if( (di~=0) | (dj~=0) | (dk ~=0) )
      Ashift=shift(     A,di,1);
      Ashift=shift(Ashift,dj,2);
      Ashift=shift(Ashift,dk,3);
      ispeak=ispeak & (A>Ashift);
     end
    end
   end
  end
  Pos = find(ispeak);
 otherwise
  disp(['ERROR: FindPeaksInField: Can not handle ',int2str(NumberOfDimensions),' dimensions'])
end
%%%% END of FindPeaksInField()
