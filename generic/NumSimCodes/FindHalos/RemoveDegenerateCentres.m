function CleanedCentres=RemoveDegenerateCentres(centres,tolerance)
% RemoveDegenerateCentres : Remove overlapping (x,y,z) positions from a list
%
% ARGUMENTS
%  centres (3xN) list of centres (or simply (x,y,z) positions)
%  tolerance     The distance less than which two positions are considered
%                degenerate and removed.
% OUTPUT
%  CleanedCentres (3xM) the list of centres, with duplicates removed.
%
% NOTES: Algorithm is N^2, which can be annoying

% AUTHOR: Eric Tittley
%
% HISTORY
%  110215 First version

NCentres=size(centres,2);
KeepFlag=ones(1,NCentres);

tol2=tolerance^2;

for i=1:NCentres
 %centre=centres(:,i);
 %for j=i+1:NCentres
 % if(KeepFlag(j)==1)
 %  dist2 = sum( (centres(:,j)-centre).^2 );
 %  if(dist2<tol2)
 %   KeepFlag(j)=0;
 %  end
 % end
 %end
 % The following does the previous, much faster
 centre=centres(:,i)*ones(1,NCentres-i);
 dist2=sum( (centres(:,i+1:NCentres)-centre).^2 );
 mask=find(dist2<tol2);
 if(~isempty(mask))
  KeepFlag(i+mask)=0;
 end
end

CleanedCentres=centres(:,KeepFlag==1);
