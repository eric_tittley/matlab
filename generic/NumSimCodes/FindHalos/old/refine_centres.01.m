function new_centres=refine_centres(r,dn,old_centres,rad)
% new_centres=refine_centres(r,dn,old_centres,rad)

% HISTORY
% 00 05 09
%  Flipped r(N,3) to r(3,N)

%initialize the matrix
new_centres=0*old_centres;

[nc,nobj]=size(r);

[nr,nc]=size(old_centres);
for i=1:nr
 disp(['In refine_centres: refining centre number ',num2str(i),' of ',num2str(nr)])
 %centre r_temp on the estimated centre. It becomes (0,0,0)
% r_temp=rem(r+ones(nobj,1)*([1.5 1.5 1.5]-old_centres(i,:)),1)-0.5;
 r_temp=recentre(r,old_centres(i,:))-0.5;

 %find the objects within rad of the estimated cluster centre
 dist=sqrt( sum( r_temp.^2 ) );
 spheremask=find(dist < rad);
 r_clus=r_temp(:,spheremask);

 if(~isempty(spheremask))

  %find the density peak within a sphere around the estimated cluster centre
  %first find the objects
  peakmask=spheremask( dn(spheremask)>(max(dn(spheremask))*0.4) );
  %find the location of the peak in the x-y-z direction
  [nx,x]=hist(r_temp(1,peakmask));
  [ny,y]=hist(r_temp(2,peakmask));
  [nz,z]=hist(r_temp(3,peakmask));
  max_x=x(nx==max(nx)); max_x=max_x(1);
  max_y=x(ny==max(ny)); max_y=max_y(1);
  max_z=x(nz==max(nz)); max_z=max_z(1);

  %find those particles in all three of these peaks
  dx=rad/3; %dy=dx, dz=dx
  core=find(  r_clus(1,:)>(max_x-dx) & r_clus(1,:)<(max_x+dx) ...
            & r_clus(2,:)>(max_y-dx) & r_clus(2,:)<(max_y+dx) ...
            & r_clus(3,:)>(max_z-dx) & r_clus(3,:)<(max_z+dx) );

  if(~isempty(core))
   %find the mean location of this core.
   centre=mean(r_clus(:,core)');
   %recentre and save
   new_centres(i,:)=rem(old_centres(i,:)+centre+1,1);
  end

 end %i

end %is spheremask not empty

new_centres=new_centres(find(sum(new_centres')),:);
