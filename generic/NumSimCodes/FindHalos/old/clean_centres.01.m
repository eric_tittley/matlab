function [centres,index]=clean_centres(centres,tol,L_MAX)
% Remove the degenereate (repeated) centres in a list of centres.
%
% syntax: [centres,index]=clean_centres(centres,tol,[L_MAX]);
%
% ARGUMENTS
%  centres	A (Nx3) array of centres.
%  tol		The tolerance.  If two centres are separated by less
%		than tol, then they are considered the same cluster.
%  L_MAX	(optional) Maximum number of chaining cells per dimension.
%
% OUTPUT
%  centres	The new list of cleaned centres.
%  index	The index into the original list of centres from which
%		the new list is derived.
%
% This is an order N^2 problem, so beware of large lists of objects.
%
% To help optimize the speed, two extra parameters may be passed:
%  L_MAX = maximum number of chaining cells per dimension
%  A large value of L_MAX means lots of individual cells
%  There will be an optimum value for L_MAX.  Too large a value and 
%  the routine will spend too much time finding particles in a cell.
%  Particularly if many of the cells are empty. Time goes as L^3 (or L^2
%  for 2D data).
%  Too small a value and there will be too many positions per cell for the
%  core routine to process, which goes as N_positions_per_cell^2

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 04 05 Version mature
%  01 04 05 Fixed bug (for [i,j,k]=1:L changed to for [i,j,k]=0:L) which
%   prevented the lowest positions to be cleaned.
%  01 04 05 Sped up significantly by looping through each cell, instead of
%   the possible (LxLxL) postitions of a cell.
%  02 04 30 Added comments.

%MAX_NUM=1024;
if(nargin<3)
 L_MAX=32;
end
if(nargin<4)
 OptFlag=0;
end

[Num,Ndim]=size(centres);
if(Ndim~=2 & Ndim~=3)
 error(' I do not know what to do with other than 2 or 3 dimensions')
end

good=ones(1,Num);

if(0)
%if(Num<MAX_NUM)
% dist=zeros((Num^2-Num)/2,1);
% count=0;
% for i=1:Num-1
%  span=Num-i;
%  centre=centres(i,:);
%  centre_long=ones(span,1)*centre;
%  dist(count+1:count+span)=sqrt(sum(((centres(i+1:Num,:)-centre_long).^2)')');
%  count=count+span;
% end
%else
 % Index the particles into cells
 mins=min(centres);
 maxs=max(centres);
 if(Ndim==3)
  cells=[(centres(:,1)-mins(1))/tol,...
         (centres(:,2)-mins(2))/tol,...
         (centres(:,3)-mins(3))/tol];
 elseif(Ndim==2)
  cells=[(centres(:,1)-mins(1))/tol,...
         (centres(:,2)-mins(2))/tol];
 else
  error(' I do not know what to do with other than 2 or 3 dimensions')
 end
 tic
 for i=1:Num-1
  if(~ rem(i-1,floor(Num/1000)) )
   disp([int2str(i),' of ',int2str(Num)])
   toc,tic
  end
  if(good(i))
   if(Ndim==3)
    cell=[(centres(i,1)-mins(1))/tol,...
          (centres(i,2)-mins(2))/tol,...
          (centres(i,3)-mins(3))/tol];
    neighbours=find(  cells(:,1)>=(cell(1)-1) & cells(:,1)<=(cell(1)+1) ...
                    & cells(:,2)>=(cell(2)-1) & cells(:,2)<=(cell(2)+1) ...
                    & cells(:,3)>=(cell(3)-1) & cells(:,3)<=(cell(3)+1) );
   else % Assume 2-D
    cell=[(centres(i,1)-mins(1))/tol,...
          (centres(i,2)-mins(2))/tol];
    neighbours=find(  cells(:,1)>=(cell(1)-1) & cells(:,1)<=(cell(1)+1) ...
                    & cells(:,2)>=(cell(2)-1) & cells(:,2)<=(cell(2)+1) );
   end
%   span=Num-i;
   centre=centres(i,:);
   centre_long=ones(length(neighbours),1)*centre;
   dist=sqrt(sum(((centres(neighbours,:)-centre_long).^2)')');
   match=find(dist<tol);
   if(isempty(match))
    error('I should have at least found at least one match')
   end
   if(length(match)>1)
    centres(i,:)=mean([centres(i,:);centres(neighbours(match),:)]);
    good(neighbours(match))=~good(neighbours(match));
   end % if more than one match
  end % if good
 end % loop through centres
%end % if too big
end

if(1) % Index the particles into boxes and process one box at a time
 % Index the particles into cells
 mins=min(centres);
 maxs=max(centres);
 if(Ndim==3)
  max_span=ceil(max( [(maxs(1)-mins(1))/tol,(maxs(2)-mins(2))/tol,(maxs(3)-mins(3))/tol] ));
 else
  max_span=ceil(max( [(maxs(1)-mins(1))/tol,(maxs(2)-mins(2))/tol] ));
 end
 if(max_span<=L_MAX)
  L=max_span;
 else
  L=L_MAX;
 end
 divisor=tol*max_span/L;
 if(Ndim==3)
  cells=ceil([(centres(:,1)-mins(1))/divisor,...
              (centres(:,2)-mins(2))/divisor,...
              (centres(:,3)-mins(3))/divisor] );
 else
  cells=ceil([(centres(:,1)-mins(1))/divisor,...
              (centres(:,2)-mins(2))/divisor] );
 end

 if(Ndim==3)
  % Now loop through each cell;  loop through all particles in a cell;
  %  loop through neighbour particles
  for c=1:size(cells,1)
   i=cells(c,1);
   j=cells(c,2);
   k=cells(c,3);
   particles_in_cell=find(cells(:,1)==i & cells(:,2)==j & cells(:,3)==k ); 
   if(~ isempty(particles_in_cell) ) % if there are any positions in the cell
    % find all the neighbouring positions
    neighbours=find(  cells(:,1)>=(i-1) & cells(:,1)<=(i+1) ...
                    & cells(:,2)>=(j-1) & cells(:,2)<=(j+1) ...
                    & cells(:,3)>=(k-1) & cells(:,3)<=(k+1) );
    for n=1:length(particles_in_cell) % loop through positions in cell
     if(good(particles_in_cell(n))) % if this positions is unique
      centre=centres(particles_in_cell(n),:);
      centre_long=ones(length(neighbours),1)*centre;
      dist=sqrt(sum(((centres(neighbours,:)-centre_long).^2)')');
      match=find(dist<tol);
      if(isempty(match))
       error('I should have at least found at least one match')
      end
      if(length(match)>1)
       centres(particles_in_cell(n),:)...
        =mean(centres(neighbours(match),:));
       good(neighbours(match))=~good(neighbours(match));
       good(particles_in_cell(n))=1;
      end % if more than one match
     end % if unique
    end % loop through positions in cell
   end % if not empty cell
  end % loop through cells

 else % Ndim==2
  % Now loop through each cell;  loop through all positions in a cell;
  %  loop through neighbour positions
  for c=1:size(cells,1)
   i=cells(c,1);
   j=cells(c,2);
   particles_in_cell=find(cells(:,1)==i & cells(:,2)==j );
   if(~ isempty(particles_in_cell) ) % if there are any positions in the cell
    % find all the neighbouring positions
    neighbours=find(  cells(:,1)>=(i-1) & cells(:,1)<=(i+1) ...
                    & cells(:,2)>=(j-1) & cells(:,2)<=(j+1) );
    for n=1:length(particles_in_cell) % loop through positions in cell
     if(good(particles_in_cell(n))) % if this positions is unique
      centre=centres(particles_in_cell(n),:);
      centre_long=ones(length(neighbours),1)*centre;
      dist=sqrt(sum(((centres(neighbours,:)-centre_long).^2)')');
      match=find(dist<tol);
      if(isempty(match))
       error('I should have at least found one match')
      end
      if(length(match)>1)
       centres(particles_in_cell(n),:)...
        =mean(centres(neighbours(match),:));
       good(neighbours(match))=0*good(neighbours(match));
       good(particles_in_cell(n))=1;
      end % if more than one match
     end % if unique
    end % loop through positions in cell
   end % if not empty cell
  end % Loop through cells

 end % if Ndim==3, else, end
 
end % end dummy control if
index=find(good);
centres=centres(index,:);
