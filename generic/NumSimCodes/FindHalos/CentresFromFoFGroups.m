% CentresFromFoFGroups: calculate halo centres from FoF group list
%
% ARGUMENTS
%  r            Positions [3,N], in the range [0 1)
%  GroupNum     List of the group number for each of the N particles
%  NGroups      Number of Groups
%
% OUTPUT
%  centres      Centres of each group [3,NGroups]
%
% SEE ALSO
%  fof

% AUTHOR: Eric Tittley
%
% HISTORY
%  120723 First version
%  120806 Return centres as 3xNGroups instead of NGroupsx3

function centres=CentresFromFoFGroups(r,GroupNum,NGroups);

% A simple enough problem, except when you consider particles that have gone
% over the repeating boundary conditions.

% Check dimensions of r
[N,M]=size(r);
if(N~=3)
 if(M==3)
  r=r';
 else
  error('CentredFromFoFGroups: r must be [3,N]')
 end
end

centres=zeros(3,NGroups);

for i=1:NGroups
 Index=find(GroupNum==i);
 rGroup=r(:,Index);
 % Take the first particle as an approximate centre
 r_recentred=recentre(rGroup,rGroup(:,1));
 % Find the mean of the positions (which should be around (0.5,0.5,0.5) which is
 % where the first particle is, now), subtract (0.5,0.5,0.5), then add the
 % first particle's position.
 centres(:,i)=mean(r_recentred')'-0.5+rGroup(:,1);
end

% Put on 0 to 1. The centres can drift over the edges because we centred each
% group on its first particle as an approximation.
centres=rem(centres+1,1);
