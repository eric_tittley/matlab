% HaloCatalogue: Construct a halo catalogue from n-body positions
%
% [NHalos,HaloMass]=HaloCatalogue(r,HaloFindingMethod,MassBins,Options)
%
% WARNING: This is currently a horribly mis-named function.  It actually
%          constructs the halo catalogue, but then returns the number of halos
%          in mass bins (akin to the mass function).
%
% [NHalos,HaloMass]=HaloCatalogue(r,HaloFindingMethod,MassBins,Options)
%
% ARGUMENTS
%  r                   Positions [3,N]
%  HaloFindingMethod   One of:
%                       fof  Friends-of-Friends
%  MassBins            Bins of particle number, not mass
%  Options             Arguments for the Halo Finding Method
%                       Options.EPS        [fof]
%                       Options.MinMembers [fof]
%                       Options.Period     [fof]
%                       Options.Verb_flag  [fof]
%
% RETURNS
%  NHalos    Number of halos in the mass bin
%  HaloMass  The mass bin (actually particle number bin)
%
% SEE ALSO
%  fof

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  13 02 13 First version
%
% TODO
%  Actually make this the halo catalogue part by separating away halo-number
%  and mass-function finding parts.
%  The "mass" and "mass bin" are actually particle number.  Correct.

function [NHalos,HaloMass]=HaloCatalogue(r,HaloFindingMethod,MassBins,Options)

% Work on the data, which should be just r=[3,N]
switch lower(HaloFindingMethod)
 case 'fof'
  disp('fof')
  [GroupNum,NGroups]=fof(r,Options.EPS,Options.MinMembers,Options.Period, ...
                         Options.Verb_flag);
 otherwise
  disp(['Unknown method: ',HaloFindingMethod])
  return
end

% Each method should return a Group Number for each particle

% Create the Mass Function
% GroupMass=zeros(NGroups,1);
% disp([int2str(0),' of ',int2str(NGroups)])
% for i=1:NGroups
%  if(rem(i,1000)==0)
%   disp([int2str(i),' of ',int2str(NGroups)])
%  end
%  GroupMass(i)=length(find(GroupNum==i));
% end

% The above, but much much faster ( > 1000x )
GroupNumSorted=sort(GroupNum);
Diff=GroupNumSorted(2:end)-GroupNumSorted(1:end-1);
% Diff will be a bunch of zeros with 1s at the edges
Diff=[Diff(:);1]; % Append a 1, for the last group.
OnesAt=find(Diff); % The positions of each '1'
GroupMass=diff(OnesAt);
% diff Conveniently discards the first group, which is the group of un-grouped
% positions.

[NHalos,HaloMass]=histc(GroupMass(2:end),MassBins);
