function [radius,index]=overdensity_radius(rm,r,centre,over_density,num_munits)
% overdensity_radius: Finds the overdensity radius for a cluster.
%
% [radius,index]=overdensity_radius(rm,r,centre,over_density,num_munits)
%
% ARGUMENTS
%  rm		Particle masses. (length N)
%  r		Particle positions. Span the range (0,1]. (3xN)
%  centre	The position of the centre of the sphere. (length 3)
%  over_density	The overdensity, like 200 or 500 or 180.
%  num_munits	Number of mass units in the total volume for a mean density
%		in the volume equal to an overdensity of 1. Usually
%		this is sum(rm).
%
% OUTPUT
%  radius	The radius of the overdensity.
%  index	An index of particles within the overdensity radius of the
%		centre.
%
% ERROR MESSAGES
%  "Warning: overdensity_radius: cluster not dense enough. over_density=500"
%	The cluster is too small.  It doesn't have any part that is more dense
%	than an overdensity of 500.
%
% SEE ALSO
%  overdensities

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 06 06 Modified to use r(3,N) convention.
%  02 04 30 Updated comments.
%  02 04 30 Changed: centre=centre'*([1:length(rm)]&1);
%	         to: centre=centre'*ones(1,length(rm));
%  02 05 01 Added comments
%  04 07 29 Minor changes to the comments

% This routine is iterative.

coef=num_munits*4*pi/3;
centre=centre*ones(1,length(rm));
dist2=sum((rem(r-centre+1.5,1)-0.5).^2);
radius=0.1;
over_rho=10*over_density;
%NumNotDenseEnough=0;
while(abs(over_rho/over_density-1)>(2/num_munits))
 index=find(dist2<=radius^2);
 if(radius>0)
  over_rho=sum(rm(index))/(coef*radius^3);
 else
  %NumNotDenseEnough=NumNotDenseEnough+1;
  radius=0;
  index=[];
  over_rho=over_density;  % This kills the loop.
 end
 radius=radius*(over_rho/over_density)^(1/3);
end

%if(NumNotDenseEnough>0)
% disp(['Warning: overdensity_radius: cluster not dense enough for ',...
%       int2str(NumNotDenseEnough),' centres'])
%end
