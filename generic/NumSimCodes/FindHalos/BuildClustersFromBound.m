function Structures=BuildClustersFromBound(centres,r,v,rm,hdr,MeshRes)
% BuildClustersFromBound: Find cluster members by building them up one at a time
% from bound particles.
%
% Structures=BuildClustersFromBound(centres,r,v,rm,hdr,MeshRes);
%
% ARGUMENTS
%  centres	cluster centres 	[Ncentres x 3]  Range=[0,1)
%  r		particle positions 	[3 x Nobj]      Range=[0,1)
%  v		particle velocities	[3 x Nobj]
%  rm		particle masses		[Nobj x 1]
%  hdr		Header containing required parameters:
%		hdr.lunit Converts code units to cm
%		hdr.tunit Converts code units to time (s)
%		hdr.lunit/hdr.tunit Converts code units to velocity (cm/s)
%		hdr.munit Converts code units to Mo
%		hdr.sft0  Gravitational softening (code units).
%  MeshRes	# mesh cells per side
%
% OUTPUT
%  Structures	Members of the structures [Ncentres cell array]
%
% USAGE
%
% SEE ALSO
%  BuildClusterFromBound  readhydra  LL_Create  LL_ElementsInCell
%  find_centres  SortCentresByCoreMass

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 09 30 Added argument info for MeshRes.  What are the limits to its value?
%  02 05 11 First version
%  02 05 16 Took out the bulk of the routine and stuck it in
%	BuildClusterFromBound which works on a single cluster
%	at a time.
%  04 07 29 Minor comment changes.

% For each of the centres build the cluster.
%
% But first, index all the particles into cells so we don't have
% to process all the particles for each centre.
[Start,ll]=LL_Create(r,MeshRes);

N=length(rm);
available=ones(N,1);

Ncentres=size(centres,1);
for i=1:Ncentres
 disp([int2str(i),' of ',int2str(Ncentres)])

 % Get the list of particles from the 27 surrounding cells
 Cell=floor(centres(i,:)*MeshRes)+1;
 SpanI=mod(Cell(1)+[-2:0],MeshRes)+1;
 SpanJ=mod(Cell(2)+[-2:0],MeshRes)+1;
 SpanK=mod(Cell(3)+[-2:0],MeshRes)+1;
 Elements=[];
 for I=SpanI
  for J=SpanJ
   for K=SpanK
    if(Start(I,J,K)>0)
     Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
    end
   end
  end
 end

 % Get the list of neighbouring particles that are available
 ElementsAvail=Elements(find(available(Elements)));
% disp([' ',num2str(length(ElementsAvail)),' available'])
 % Get the list of particles that are in the structure.
 % Note that struct will index into the list of available particles
 struct=BuildClusterFromBound(centres(i,:),r(:,ElementsAvail),...
                          v(:,ElementsAvail),rm(ElementsAvail),hdr);

 % Store the list in a cell
 if(~ isempty(struct) )
  Structures{i}=ElementsAvail(struct);
  available(Structures{i})=zeros(length(Structures{i}),1);
 else
  Structures{i}=[];
 end

end % loop over all centres
