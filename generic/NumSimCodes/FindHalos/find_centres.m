function centres=find_centres(varargin)
% find_centres: Find clusters using tomographic deprojection.
%
% syntax: centres=find_centres(irun,step,Res)
%     or: centres=find_centres(r,rm,dn,h,Res)
%
%
% centres=find_centres(irun,itime,Res)
%  Read the data from a file.  The gas is used for finding clusters.
%
%  ARGUMENTS
%   irun	Run number.				See: dataname (irun)
%   itime	Iteration label.			See: dataname (step)
%   Res		The resolution of the projected masses.	See: h_proj (L)
%
%
% centres=find_centres(r,rm,dn,h,Res)
%  Use the data supplied.
%
%  ARGUMENTS
%   r            The (x,y,z) position of each particle.
%   rm           The mass of each particle.
%   dn           The local density of each particle.     See: refine_centres (dn)
%   h            The smoothing radii.                    See: h_proj (h)
%   Res          The resolution of the projected masses. See: h_proj (L)
%
%
% SEE ALSO:
%  readhydra dataname h_proj deproj refine_centres

% AUTHOR; Eric Tittley
%
% HISTORY:
% 00 05 05
%  Updated to use r(3,N) arrays instead of r(N,3)
%  and to be compatible with hydra4.0
% 00 11 12 Removed A=flipud(A) since that line was removed from h_proj
% 02 04 29 Redo to use variable argument lists.
% 04 07 27 Minor changes to the comments.

if(length(varargin)==3)
 irun =varargin{1};
 itime=varargin{2};
 Res  =varargin{3};
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itime));
% A mistake in hydra means that all particles that are 'lost' have their itype
% changed to -2.
% In Hydra 4.0, all gas particles must be found at the start of arrays.
 gas=find(itype(1:hdr.ngas) & dn>10*hdr.ngas); %dn==hdr.ngas => dn==rho_c
 clear v th itype
 r=r(:,gas);
 rm=rm(gas);
 dn=dn(gas);
 h=h(gas);
 clear gas
elseif(length(varargin)==5)
 r=  varargin{1};
 rm= varargin{2};
 dn= varargin{3};
 h=  varargin{4};
 Res=varargin{5};
else
 disp('syntax: centres=find_centres(irun,step,Res)')
 disp('    or: centres=find_centres(r,rm,dn,h,Res)')
 error('find_centres: invalid syntax')
end

max_tol=round(Res*0.01);
if(~ exist('PreDeproject.mat') )

 tic

 %xy
 A=h_proj(r(1,:),r(2,:),rm,h,Res,[0 1 0 1 0 1]);
 Aleft      =shift(A     ,-1,1);
 Aright     =shift(A     , 1,1);
 Aup        =shift(A     , 1,2);
 Adown      =shift(A     ,-1,2);
 Aleft_up   =shift(Aleft , 1,2);
 Aleft_down =shift(Aleft ,-1,2);
 Aright_up  =shift(Aright, 1,2);
 Aright_down=shift(Aright,-1,2);
 ispeak=(A>Aleft & A>Aright & A>Aup & A>Adown & A>Aleft_up & A>Aleft_down & A>Aright_up & A>Aright_down );
 [Y1,X1]=ind2sub(size(A),find(ispeak));
 Axy=A;

 %xz
 A=h_proj(r(1,:),r(3,:),rm,h,Res,[0 1 0 1 0 1]);
 Aleft      =shift(A     ,-1,1);
 Aright     =shift(A     , 1,1);
 Aup        =shift(A     , 1,2);
 Adown      =shift(A     ,-1,2);
 Aleft_up   =shift(Aleft , 1,2);
 Aleft_down =shift(Aleft ,-1,2);
 Aright_up  =shift(Aright, 1,2);
 Aright_down=shift(Aright,-1,2);
 ispeak=(A>Aleft & A>Aright & A>Aup & A>Adown & A>Aleft_up & A>Aleft_down & A>Aright_up & A>Aright_down );
 [Z2,X2]=ind2sub(size(A),find(ispeak));
 Axz=A;

 %yz
 A=h_proj(r(2,:),r(3,:),rm,h,Res,[0 1 0 1 0 1]);
 Aleft      =shift(A     ,-1,1);
 Aright     =shift(A     , 1,1);
 Aup        =shift(A     , 1,2);
 Adown      =shift(A     ,-1,2);
 Aleft_up   =shift(Aleft , 1,2);
 Aleft_down =shift(Aleft ,-1,2);
 Aright_up  =shift(Aright, 1,2);
 Aright_down=shift(Aright,-1,2);
 ispeak=(A>Aleft & A>Aright & A>Aup & A>Adown & A>Aleft_up & A>Aleft_down & A>Aright_up & A>Aright_down );
 [Z3,Y3]=ind2sub(size(A),find(ispeak));
 Ayz=A;

 toc

 clear A Aleft Aright Aup Adown Aleft_up Aleft_down Aright_up Aright_down
 clear Axy Axz Ayz

 %save -v6 PreDeproject.mat X1 Y1 X2 Z2 Y3 Z3 max_tol
else
 if(~ exist('centres_approx.mat') )
  disp('loading projections...')
  load PreDeproject.mat
 end
end

if(~ exist('centres_approx.mat') )
 disp('deprojecting...')
 tic
 [X,Y,Z,tol]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol);
 toc
 clear X1 Y1 X2 Z2 Y3 Z3 max_tol
 centres_approx=[X,Y,Z]/Res;
 %save centres_approx.mat centres_approx r dn Res
else
 disp('loading approximate centres...')
 load centres_approx.mat
end

disp('refining...')
rad=3/Res;
new_centres=refine_centres(r,dn,centres_approx,rad);
centres=new_centres;

%save centres.mat centres
