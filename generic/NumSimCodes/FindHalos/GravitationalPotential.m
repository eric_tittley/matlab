% GravitationalPotential: The gravitational potential of a series particles
%
% [Potential,Phi,h]=GravitationalPotential(r,rm,hdr,Method,[h])
%
% ARGUMENTS
%  centre	cluster centre	 	[1 x 3] 
%  r		particle positions 	[3 x Nobj] [0 1)
%  rm		particle masses		[Nobj x 1]
%  hdr		Header containing required parameters:
%		hdr.lunit Converts code units to cm (code unit 1 => hdr.lunit)
%		hdr.munit Converts code units to Mo
%		hdr.sft0  Gravitational softening (code units).
%               hdr.GridDimension
%               hdr.Nsph
%               hdr.h_guess
%               hdr.time  Expansion factor
%  Method       The method to calculate the potential
%                'PP'   Particle-Particle (isolated)
%                'PM'   Particle Mesh (repeating boundary condition)
%  h            Smoothing lengths [optional, calculated if not passed in]
%               
% RETURNS
%  Potential    The gravitational potential of each particle. [Nobj x 1]
%  Phi		The potential field per mass

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: matlab
%                Methods fft3 and ifft3 SegFault with complex data in octave
%
% HISTORY
%  12 08 17 First version

function [Potential,Phi,h]=GravitationalPotential(r,rm,hdr,Method,h)

if(nargin<5)
 h=[];
end

% Initialise the units
SI='CGS';
Units, G=6.6732e-8; % cm^3 g^-1 s^-2
veryLargeNumber=1e10; % distance in code units

% r must be in the interval [0 1)
if( min(r(:))< 0 | max(r(:))>=1 )
 error('r must be on the interval [0 1]')
end

n=size(r,2);

switch upper(Method)
 case 'PP'
  % Direct summation. VERY slow.  Good for testing.
  Potential=zeros(n,1);
  for i=1:n
   r_centre=recentre(r,r(:,i))-0.5; % Centred on the current particle
   dist=sqrt(sum(r_centre.^2)); % Code units
   % Hack to exclude adding the particle's own contribution at r=0;
   dist(dist==0)=veryLargeNumber;
   Potential(i)=sum(rm./(dist+hdr.sft0));
  end
  % erg
  Potential = Potential(:) .* rm(:) * (G*(hdr.munit*Mo)^2/(hdr.lunit*hdr.time));
 case 'PM'
  % Grid particles
  %  Options available:
  %   1) Write a gridding routine, like Cloud-in-Cell
  %   2) Use h_proj_3d and h from calcdensities
  %  We don't need exceptionally high resolution, since the densest particles will
  %  be clearly bound.
  if(isempty(h))
   tic
   [h,dn]=calcdens_d(r,hdr.Nsph,hdr.h_guess);
   clear dn
   toc
  end
  Limits=[0 1 0 1 0 1];
  tic
  Rho=h_proj_3d(r,rm,h,hdr.GridDimension,Limits);
  toc
  Rho=Rho*(hdr.munit*Mo/(hdr.lunit*hdr.time)^3); % g/cm^3
  % Mass distribution is the density * cell volume
  dR=hdr.lunit*hdr.time/hdr.GridDimension;
  dV=dR^3;
  M=Rho*dV;
  mean(M(:))
  clear Rho

  % Grid Greens function
  % 1/r
  V = [0:hdr.GridDimension/2,hdr.GridDimension/2-1:-1:1];
  V = V/hdr.GridDimension * hdr.lunit*hdr.time; % cm (should it be hdr.GridDimension-1?)
  [X,Y,Z]=meshgrid(V,V,V);
  R=sqrt(X.^2+Y.^2+Z.^2+(hdr.sft0*hdr.lunit*hdr.time)^2);
  if(hdr.sft0==0)
   R(1,1,1)=1;
  end
  Greenfn=G./R;
  clear X Y Z R

  % Convolve Density and Greens function to give potential
  tic
  MFT         = fft3(M);
  GreenfnFT   = fft3(Greenfn);
  if(isreal(MFT))
   MFT=complex(MFT);
  end
  if(isreal(GreenfnFT))
   GreenfnFT=complex(GreenfnFT)
  end
  PhiFT       = MFT.*GreenfnFT;
  % ifft3 fails, but fft3 is fine.
  Phi         = real(ifft3(PhiFT));
  disp(['Phi: ',num2str([min(Phi(:)) mean(Phi(:)) max(Phi(:))],'%e ')])
  % Remove the background constant potential.
  % This is a hack.  There must be a better solution.
  Phi=Phi-min(Phi(:));
  toc

  % The potential of each particle is its mass * Phi(r) where r is the position
  % of the particle.
  X = floor(r*hdr.GridDimension+1);
  % The following index order may look like a bug. It isn't
  % It arrised because matrices are [Row,Col,Depth]
  ind = sub2ind(hdr.GridDimension*[1 1 1],X(2,:),X(1,:),X(3,:));
  Potential = (rm .* Phi(ind)) * hdr.munit*Mo;

% WHY DO I NEED THIS STEP?
  Potential = Potential * (hdr.GridDimension^3);
  disp(['Potential: ',num2str([min(Potential(:)) mean(Potential(:)) max(Potential(:))],'%e ')])

 otherwise
  error(['Unknown method: ',Method])
end
