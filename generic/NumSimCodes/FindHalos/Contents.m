% FindHalos: Routines to find halos in N-body simulation data
%
%  BuildClusterFromBound Find cluster members by building them up one at
%                        a time from bound particles.  Use this for one cluster.
%
%  BuildClustersFromBound Find cluster members by building them up one at
%			  a time from bound particles.  Use this when you have a
%			  list of clusters.
%
%  centre_search	Finds the peak number density in a sample of
%			particle positions.  Good for zeroing in on the
%			centre of a pre-chosen cluster.
% 
%  CentresFromFoFGroups Calculate halo centres from FoF a group list
%
%  clean_centres	Removes the degenerate (repeated) centres in
%			the list of centres.
% 
%  find_centres		Find the clusters in a volume using tomographic
%			deprojection.
%
%  FindHalosInGriddedData Given gridded density field, break it up into halos
%
%  FindPeaksInField     Find the positions of peaks in an array
%
%  GravitationalPotential The gravitational potential of a series particles
%
%  HaloCatalogue        Construct a halo catalogue from n-body positions
%
%  LL_Create		Create a Link-List.
%
%  LL_ElementsInCell	Get the elements in a cell from a link list.
% 
%  MatchPositions       Match a list of positions with another list
%
%  overdensities	Returns that details of the overdensity radius (at
%			overdensities of 200 and 500).
% 
%  overdensity_radius	Finds the overdensity radius for a cluster.
% 
%  ParticlesInSphere    List of particles within a distance of a position
%
%  recentre		Returns the r's centred on a new position.
% 
%  refine_centres	From a list of approximate centres, calculate better
% 			centres.  Good for zeroing in if you have a list of
%			centres.
% 
%  RemoveHaloStructure	Sort clusters into primary clusters and halo
%			substructure.
%
%  SortCentresByCoreMass	Sort a list of centres by the mass of the cores.
%
% ______________________________________________________________________________
% OLD and Broken routines (see find_clusters/Broken)
%
%  centre_find		Finds the centre of a cluster in a file using
%			the last reported position of the centre
% 
%  centre_quick		Same thing as centre_find.  What's the diff?
%
%  cluster_in_box	Finds the cluster with the maximum density using
%			tomographic deprojection.	
% 
%  find_centres_saveflag
%			Same as find_centres, but have the option to save
%			intermediate steps.
% 
%  getcentres		Returns a list of n centres interactively selected.
%
%  max_den		Finds the centre of a pre-selected cluster by
%			calculating the maximum density after interpolating
%			onto a mesh.
