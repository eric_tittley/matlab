function header=FITSsetkey(header,key,value)
  
% FITSSETKEY -  Sets keyvalue in header.
%
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%
% Modifies an already existing "key" (line)
% i a header char matrix.
%
% newheader=FITSsetkey(header,key,value)
%
% Argument:
% header    Existing char matrix to modify.
%
% key       String specifying key name or number of
%           line in header to modify.
%
% Example:
% header=FITSsetkey(header,'MYKEY',34);
% header=FITSsetkey(header,'MYKEY','This is a text string');
%

% HISTORY
%  02-03-12 Changed %21G%s to %21.13G%s

  if ischar(key),
    keyname=key;
    key=FITSfindkey(header,key);
    if key<1,
      header=FITSaddkey(header,keyname,value,0);
      return;
    end
  end
  
  if ischar(value)==0,
    newline=sprintf('%21.13G%s',value,blanks(70));
  else
    newline=[' ',char(39),value,char(39),blanks(70)];
  end
  header(key,10:80)=newline(1:71);
