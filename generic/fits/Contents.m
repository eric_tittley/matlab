% FITS Toolbox for FITS I/O.
%
% FITS is the Flexible Image Transport System file format.
%
% Reads and writes FITS files.
% Modifies FITS headers.
% Not all features in the FITS standard are implemented.
%
% Basic I/O
%  FITSload		Loads data and header.
%  FITSwrite		Writes data and header data to FITS file.
% (FITSwrite_2		Another implementation of FITSwrite.  It may work better)
%
% Input of header values
%  FITSfindkeyvalue	Returns numeric value associated to keys.
%  FITSfindkeystring	Returns text string associated to key.
%  FITSfindkeycomment	Returns comment string from key in header list.
%  FITSfindkey		Finds the order number of a key.
%  FITSfindnextkey	Returns number of the next key with the same name as the
%			key whose number is supplied.
%
% Header modification
%  FITSaddkey		Adds new key to header.
%  FITSremovekey	Removes one or more keys in header.
%  FITSsetkey		Sets key value in header.
%
%
% **************************** Toolbox list END *************************
%
% Numerous modifications made by Eric Tittley.  See each file individually.
%
%  Version 1.0 Beta 99-09-25 Peter Rydesäter, GNU General Public License.
%
%  Tested under MATLAB 5.1+ Linux platform.
%
%  Copyright (C) Peter Rydesäter 1998 - 1999, Mitthögskolan, SWEDEN
%
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
%
%  Please, send me an e-mail if you use this and/or improve it.
%
%  Peter Rydesäter
%  Mitthögskolan
%  83125 ÖSTERSUND
%  SWEDEN
%
%  e-mail: Peter.Rydesater@ite.mh.se
%
%  http://alis.irf.se
%  http://www.irf.se
%
%  I hope this will be usefull for your application.
%
%  /Peter R
