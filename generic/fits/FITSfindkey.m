function i = FITSfindkey(V,s)
% FITFINDKEY   - finds a key in a FITS header matrix.
%
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%
%  Number = FITSfindkey(Header,KeyWord)
% KeyWord = FITSfindkey(Header,Number)
%
% Two ways of using this; in the first way it seeks for a key in the
% header and returns it's header possition number for it.
% The second way is to give the Number and return the name of the key
% at that possition.
% Header is the header that returned from FITSload(...) function.
% KeyWord is a string of maximum 8 characters with all leading
% and ending spaces removed.
% Number is the number of the the first found matching keyword.
% If their is't any keyword Number will be 0.

% History:
%  00 09 12: Modified to not neglect the lasts line of the header
%
%  01 04 04 Fixed bug when called using the second form.
  if ischar(s),
    if size(V,2)<80,
      i=0;
      return;
    end
%    i=strmatch(s,V(1:end-1,1:8),'exact');
% Modified by E Tittley (00 09 12) since the above line neglects the final
% line of the header.  END has already been stripped, so this is not necessary.
    i=strmatch(s,V(1:end,1:8),'exact');
    if length(i)==0,
      i=0;
    else
      i=i(1);
    end
  else
    if size(V,2)<80,
      i='';
      return;
    end
    if s>size(V,1),
      i='';
    else
      i=V(s,1:8);
      i=deblank(i(end:-1:1));
      i=deblank(i(end:-1:1));
    end
  end
  return;
