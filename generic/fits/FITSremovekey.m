function header=FITSremovekey(header,key)
  
% FITSremovekey(header,key) - Removes key from FITS header.
%
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%
% newheader=FITSremovekey(header,key)
%
% Arguments:
%      header         valid matlab formated FITS header.
%      key            keyname to be removed.
%
% Returns:
%      newheader      Modified header.
%
  
  if ischar(key),
    num=1;
    while num,
      num=FITSfindkey(header,key);
      if num>0,
        header=FITSremovekey(header,num);
      end
    end
  else
    header=strvcat(header(1:key-1,:),header(key+1:end,:));
  end
  

