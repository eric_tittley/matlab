function str=FITSfindkeystring(Header,KeyWord)
% FITSFINDKEYSTRING Returns the string that is assigned to KeyWord in header
%  Header.
%
% str=FITSfindkeystring(Header,KeyWord)
% 
% If key KeyWord does't exits or nothing is placed between ' ' (not string
% datatype) then an empty string is returned.
% KeyWord could also be numeric and tell about what number of key to access.
%
% SEE ALSO
%  FITSfindkeycomment FITSfindkey FITSfindkeyvalue FITSfindnextkey

% HISTORY
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%  03 06 07 Modified comments.  Added "SEE ALSO"

str='';
if ischar(KeyWord),
 num=double(FITSfindkey(Header,KeyWord));
else
 num=double(KeyWord);
end

if num>length(Header),
 return
elseif num<1,
 return
end
s=Header(num,10:end);
s=deblank(s(end:-1:1));
s=deblank(s(end:-1:1));
str=strtok(s,char(39));
