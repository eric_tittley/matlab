function [header,data] = FITSload(name,rec,onlyheader,unsigned)
% FITSLOAD Loads some data, including the header, in FITS format.
% You get to choose the record to load.
%
%   [header, data] = FITSload(file, [rec],[onlyheader],[unsigned]);
%
% Arguments:
%
% file 		File name.
% 		If directory or empty then input interactively.
%
% rec		Record number ( default 0 )
%
% onlyheader	Read in only the header? (0=no, otherwise=yes) (default 0) 
%
% unsigned	Is the integer image data unsigned?
%		(0=no, otherwise=yes) (default 0)
%		The FITS standard has no support for unsigned integer data,
%		Other than 8 bit data.
%		Some FITS image data, such as produced by SBIG cameras, is
%		unsigned.
%		This flag has no effect if the data is not an image, and not
%		either 16, 32, or 64 bit integer data.
%
% Return values:
%
% header            Header list. Use FITSfindkey() to parse it.
%
% data              The data stored in record
%
% Notes: 1) Record 0 is the first record
%        2) If there is scaling to be done with BZERO and BSCALE present,
%           then they are done WHILE READING.  DON'T RESCALE!!! 
%
% SEE ALSO: FITSwrite, FITSfindkeyvalue, FITSfindkey, FITSfindkeycomment,
%           FITSfindkeystring, FITSfindnextkey

%*****************************************************************************
%
% History: 
%
%  Based on FITSLOAD by Peter Rydes�ter. (99 09 25) GNU General Public License
% 
% TODO:
% Add support for ASCII table extension
%		Complex data


% Default settings
filerequester=0;
  
%%%%%%%%%%%%%%%%%%%%%%% Check arguments. %%%%%%%%%%%%%%%%%%%%%%%%%%%
  
if(nargin==0 | nargin >4)
 disp('syntax: [header, data] = FITSload(file, [rec],[onlyheader],[unsigned])')
 return
end

if(nargin<2),
 rec=0;
end
if(nargin<3),
 onlyheader=0;
end
if(nargin<4),
 unsigned=0;
end


%%%%%%%%%%%%%%%%%%%%%%% END Check arguments. %%%%%%%%%%%%%%%%%%%%%%%%%%%
  

% Open the file  
fid = fopen(name,'r','ieee-be');
if(fid == -1)
 disp('Can�t find/open FITS file.');
 return;
end

% Read in and discard records until we get to the record we want 
for record=1:rec 
 % The header
 header=local_read_header(fid);

 % Get the dimensions of the data  
 naxis=FITSfindkeyvalue(header,'NAXIS');
 % naxis == 0 means there is only a header, no data.

 if(naxis>0)
  s=zeros(naxis,1);
  if(naxis>0), s(1)=FITSfindkeyvalue(header,'NAXIS1'); end
  if(naxis>1), s(2)=FITSfindkeyvalue(header,'NAXIS2'); end
  if(naxis>2), s(3)=FITSfindkeyvalue(header,'NAXIS3'); end
  if(naxis>3), s(4)=FITSfindkeyvalue(header,'NAXIS4'); end
  if(naxis>4)
   disp('I do not know what to do with more than 4 axes')
   return
  end
  % read the data in

  % determine the datatype
  bitpix=FITSfindkeyvalue(header,'BITPIX');
  switch bitpix
   case 8,   datatype='uchar';   BytesPerWord=1;
   case 16,  datatype='int16';   BytesPerWord=2;
   case 32,  datatype='int32';   BytesPerWord=4;
   case 64,  datatype='int64';   BytesPerWord=8;
   case -32, datatype='float32'; BytesPerWord=4;
   case -64, datatype='float64'; BytesPerWord=8;
   otherwise
    disp(['I do not know what to do with the BITPIX value of ',num2str(bitpix)])
    return
  end % end switch bitpix

  % the data are stored in 2880 byte fields
  NBytes=prod(s)*BytesPerWord;
  % If there is a heap (for variable size arrays, for example), then
  % we must add the length of the heap to NBytes
  % If there is a heap, then the keyword PCOUNT must be set to the length
  % of the heap, in bytes.  Otherwise, PCOUNT may or may not be there,
  % but if it is, it must equal 0.
  if(FITSfindkey(header,'PCOUNT')>0)
   PCount=FITSfindkeyvalue(header,'PCOUNT');
   NBytes=NBytes+PCount;
  end
  NBytes=2880*(floor((NBytes-1)/2880)+1);
  fseek(fid,NBytes,0);

 end % end if naxis >0
 % finished reading in record to discard

end % end for record = 1:rec
% finished reading in all records to discard

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% ----------------  Now get the record we want --------------- %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The header
header=local_read_header(fid);
if(isempty(header))
 error('Requested record beyond number of records')
end

data=[];

% If only the header is requested, then return now
if(onlyheader)
 fclose(fid);
 return;
end

% What type of data is in the extension?
% (In the primary HDU, only IMAGE data is permitted.
%  But IMAGE data can be multidimensional.)
if(rec~=0)
 DataType=FITSfindkeystring(header,'XTENSION');
else
 DataType='IMAGE   ';
end

switch DataType
 case 'IMAGE   '
  % Get the dimensions of the data  
  naxis=FITSfindkeyvalue(header,'NAXIS');

  % naxis == 0 means there is only a header, no data.
  if(naxis==0)
   disp('WARNING: HDU contains IMAGE with no data.')
   return
  end

  % Get the size of the image array
  s=zeros(1,naxis);
  for i=1:naxis
   KeyWordStr=['NAXIS',int2str(i)];
   s(i)=FITSfindkeyvalue(header,KeyWordStr);
  end

  % Determine the datatype by converting the bitpix character string into a
  % precision Matlab understands.
  bitpix=FITSfindkeyvalue(header,'BITPIX');
  datatype=local_datatype_from_bitpix(unsigned,bitpix);

  % Read the data in.
  % "Arrays of more than one dimension shall consist of a sequence such that
  %  the index along axis 1 varies most rapidly, that along axis 2 next most
  %  rapidly, and those along subsequent axes progressively less rapidly,
  %  with that along axis m, where m is the value of NAXIS, varying least
  %  rapidly."
  [dummy,count]=fread(fid,prod(s),datatype);
  data=reshape(dummy,s);
  clear dummy
  
  % For 2-d Images, it makes more sense to return the transpose.
  if(naxis==2); data=data'; end

  % This is a bit of a contentious section.  It is convenient to have the
  % FITS reader scale the data independently of the user, but then the
  % user must know not to do it again
  if( FITSfindkey(header,'BZERO')>0 & FITSfindkey(header,'BSCALE')>0 )
   data=  FITSfindkeyvalue(header,'BZERO') ...
        + FITSfindkeyvalue(header,'BSCALE') * data;
  end
  % End of scaling

 %%%% end case IMAGE

 case 'BINTABLE'
  disp('Reading in a table')

% A binary table can have 2 formats:
% 1) A table of data with N rows and M Columns with N*M cells
%    The data is laid out in the binary files as:
%    ColA1,ColB1,ColC1,...,ColM1,ColA2,ColB2,ColC2,...,ColMN
% 2) A collection of data fields of (potentially) different lengths
%    For example, 100 integers, followed by 50 floats, followed by
%    25 characters.
%    The data is laid out in the binary file in this manner
% Logically, these two forms are identical if you interpret the second form
% as a regular data table with only one Row but each cell having
% many elements within it.
% This is the interpretation used here, since it permits regular tables to
% have more than one element per cell.
% Which is probably what they wanted out of the standard.

% Some basic checks.  These should not need to be done.  They should always
% be OK.
  naxis=FITSfindkeyvalue(header,'NAXIS');
  if(naxis~=2)
   disp('Binary tables must have exactly 2 axes');
   return
  end
  bitpix=FITSfindkeyvalue(header,'BITPIX');
  if(bitpix~=8)
   disp('Binary tables must have exactly 8-bit bytes');
   return
  end

% Size of the array
  % Get the number of bytes in each row of data
  nBPR=FITSfindkeyvalue(header,'NAXIS1');
  if(nBPR<0)
   disp(['Binary tables cannot have ',num2str(nBPR),' bytes per row'])
   return
  end
  % Get the number of rows of data
  nrow=FITSfindkeyvalue(header,'NAXIS2');
  if(nrow<0)
   disp(['Binary tables cannot have ',num2str(nrow),' rows'])
   return
  end
  % Get the number of fields
  nfields=FITSfindkeyvalue(header,'TFIELDS');
  if(nfields<0)
   disp(['Binary tables cannot have ',num2str(nfields),' fields'])
   return
  elseif(nfields==0)
   disp(['Empty binary table.  Returning just the header'])
   return
  end

  % Create the Format Strings
  fmtstr=cell(nfields,1);
  for i=1:nfields
   % read in the type label, stripping off trailing whitespaces
   TypeLabel=sscanf(FITSfindkeystring(header,['TFORM',int2str(i)]),'%s');
   % TypeLabel can either be just a letter, like I or D
   % or be a number-of-type combination, like 100I or 16D
   % Each will require a different method of reading the file
   ArrayDescriptorFlag(i)=0;
   if(length(TypeLabel)~=1) % Array  
    if( sum(TypeLabel=='P')==1 ) % Array Descriptor rPt(emax)
     % Form: rPt(emax) where the P indicates the amount of space occupied by
     % the array descriptor  in the data record (64 bits), the element count
     % r should be 0, 1, or absent, t is a character denoting the datatype of
     % the array data (L, X, B, I, J, etc., but not P), and emax is a quantity
     % guaranteed to be equal to or greater than the maximum number of elements
     % of type t actually stored in a table record. There is no built-in upper
     % limit on the size of a stored array; emax merely reflects the size of
     % the largest array actually stored in the table, and is provided to avoid
     % the need to preview the table when, for example, reading a table
     % containing variable length elements into a database that supports only
     % fixed size arrays. There may be additional characters in the TFORMn
     % keyword following the emax.
     P_pos=find(TypeLabel=='P');
     if(length(P_pos)>1), error('Invalid TFORM entry'); end
     if(P_pos==2)
      r=sscanf('%i',TypeLabel(1)); % Parse it out, even though we really don't
                                   % know what to do with it
      if(r~=0 & r~=1), error('Invalid TFORM entry'); end
     end
     TypeLabel=TypeLabel(P_pos+1:end); % Shift it to strip off everything up to
                                     % and including the P
     Type=TypeLabel(1);  % Get the datatype character, t
     if(length(TypeLabel)>=4) % in P<type>(<max num>) format
      NElems(i)=sscanf(TypeLabel(3:end),'%i',1); % Parse out maximum number of
                                                 % elements from the string
                                                 % 't(NElems)'
     else % not in P<type>(<max num>) format
      % This is not crucial, since we effectively set the size of the array
      % when we scan in the descriptor data.
      % The ASCA RMF's do this, for example.
      NElems(i)=2; % This will flag it so that we will know to scan
     end       
     ArrayDescriptorFlag(i)=1;
    else % of the form nType (like 100I)
     NElems(i)=sscanf(TypeLabel,'%i',1);
     Type=TypeLabel(length(int2str(NElems(i)))+1);
    end % if an array descriptor or nType form
   else % single element
    NElems(i)=1; % of the form Type (just I)
    Type=TypeLabel;
   end
   switch Type
    case 'L', fmt='uchar  '; % 1-byte logical
    case 'X', % Bit.  This is tricky, since Matlab can't read bits.
     % If the bits are stored in a grouping of 8, 16, 32, or 64, then
     % read them in as integers
     if(NElems(i)== 8)
      fmt='uint8  ';
      NElems(i)=1;
     elseif(NElems(i)==16)
      fmt='uint16 ';
      NElems(i)=1;
     elseif(NElems(i)==32)
      fmt='uint32 ';
      NElems(i)=1;
     elseif(NElems(i)==64),
      fmt='uint64 ';
      NElems(i)=1;
     else
      disp('Sorry, Matlab can not read bits unless they are in blocks of 8, 16, 32, or 64');
      return;
     end
    case 'B', fmt='uchar  '; % unsigned byte
    case 'A', fmt='uchar  '; % 1-byte character
    case 'I', fmt='int16  '; % 2-byte integer
    case 'J', fmt='int32  '; % 4-byte integer
    case 'E', fmt='float32'; % 4-byte real
    case 'D', fmt='float64'; % 8-byte double
    case 'C', disp('Sorry, I can not yet read complex'); return; % Complex
    case 'M', disp('Sorry, I can not yet read complex'); return; % Complex Double
    otherwise
     disp(['I do not know what to do with the TFORM ',Type])
     return
   end % Switch
   fmtstr{i}=sscanf(fmt,'%s');
  end % end of loop through fields

  % Now we have the format string, create the data structure and the 
  % string for the fscanf command
%  DataStr=char(uint8(zeros(nfields,8)));
  DataStr=cell(nfields,1);
  for i=1:nfields
   % read in the column label, stripping off trailling whitespaces.
   % we will use the column label as the variable name.
   ColLabel=sscanf(FITSfindkeystring(header,['TTYPE',int2str(i)]),'%s');
%   DataStr(i,1:length(ColLabel))=ColLabel;
   DataStr{i}=sscanf(ColLabel,'%s');
% The next two lines are equivalent
%   eval(['data.',ColLabel,'=zeros(nrow,NElems(i));'])
   % *** I don't think I need to predfine data anymore ***
   % data=setfield(data,ColLabel,zeros(nrow,NElems(i)));
   % *****************************************************
  end

% ##### NOTE: The next loop(s) suck(s) up all the time
  if(sum(NElems>1)>0) % Some of the fields have more than one element.
%   disp('Some of the fields have more than one element')
   % The loop is very inefficient, but the best I can do with MATLAB at the moment.
   % A FSCANF that worked on binary data would make things MUCH better.
   % Sounds like a fun project.
   % The problem is that the FITS table data is stored as mixed type in binary
   % format.  So one has something like:
   % [4 words integer][4 words real][4 words integer][8 words double]
   % [4 words integer][4 words real][4 words integer][8 words double]
   % ...
   % The format would have been much easier to read if it stored tabular data 
   % COLUMNWISE, instead of rowwise.
   %Now read the data
   if( floor(nrow/25) >0 )
    step=min(floor(nrow/25),1000);
   else
    step=1;
   end

% Predefine the data fields
   for i=1:nfields
    data=setfield(data,DataStr{i},zeros(nrow,NElems(i)));
   end
%    % For debugging, this skips rows
%     sx=FITSfindkeyvalue(header,'NAXIS1');
%     dummy=fread(fid,80000*sx,'uchar');
%   tic
%   for row=80001:nrow
   for row=1:nrow
    if(~rem(row,step) & nrow>5000)
     disp([int2str(row),' of ',int2str(nrow)])
    end
    ArrayDescriptor=0;
   % Speed up things a bit if there are no array descriptors
    if(sum(ArrayDescriptorFlag)==0)
     for i=1:nfields
      % This line is a real killer, to have to do this for every field in every
      % row.  It takes about 0.025 seconds.
%      data=setfield(data,DataStr{i},{row,1:NElems(i)}, ...
%                    fread(fid,NElems(i),fmtstr{i})     );
% The next line does the previous, but more than 10 times faster.
       eval(['data.',DataStr{i}, ...
             '(row,:)=fread(fid,NElems(i),fmtstr{i});'])
     end
    else
     for i=1:nfields
      if(ArrayDescriptorFlag(i)==0) % Just a regular entry
%       data=setfield(data,DataStr{i},{row,1:NElems(i)}, ...
%                     fread(fid,NElems(i),fmtstr{i})     );
% The next line does the previous, but more than 10 times faster.
       eval(['data.',DataStr{i}, ...
             '(row,:)=fread(fid,NElems(i),fmtstr{i});'])
      else % Then the data is not stored here, but the number of elements and
           % the heap offset are.
       ArrayDescriptor=ArrayDescriptor+1;
       dummy=fread(fid,2,'int32');
       NElems_actual(row,ArrayDescriptor)=dummy(1);
       Offset(row,ArrayDescriptor)=dummy(2);
      end % if not an Array Descriptor, else
     end % loop over fields
    end % if there are no Array Descriptors, else
   end % loop over rows

  else % none of the fields have more than one element
%   disp('none of the fields have more than one element')
   % Note, I'm making an assumption that there are no array descriptors
   % in this case, though it is conceivable.  If it should ever be that case,
   % then it would not be difficult to add the necessary code below.

% This is a faster version of the prevous loop.
% This loop does not work if some of the data fields contain more than one
% entry.
% USE ONE OR THE OTHER
% BEWARE: if the numbers in each column are on vastly different scales,
%         then roundoff errors could occur.
   % If there are fewer than 116950872 elements, we can read to a single
   % array.  Otherwise we need to read in as a cell array, which has a larger
   % memory limit.
   if(nrow*nfields < 116950872)
    Data=zeros(nrow,nfields);
    for row=1:nrow
     if(nrow>5000)
      if(~rem(row,floor(nrow/20)))
       disp([int2str(row),' of ',int2str(nrow)])
      end
     end
     for i=1:nfields
      Data(row,i)=fread(fid,1,fmtstr{i});
     end
    end
    for i=1:nfields
     data=setfield( data, DataStr{i}, Data(:,i) );
    end
   else
    disp('WARNING: Large table.  Requires slow read.')
    disp([int2str(nfields),' fields'])
    disp([int2str(nrow),' rows'])
    memused=0;
    for i=1:nfields
     %disp([DataStr{i},' ',fmtstr{i}])
     switch fmtstr{i}
      case 'uint16', fmt='uint16'; memused=memused+nrow*2;
      case 'uint32', fmt='uint32'; memused=memused+nrow*4;
      case 'int16',  fmt='int16';  memused=memused+nrow*2;
      case 'int32',  fmt='int32';  memused=memused+nrow*4;
      case 'float32',fmt='single'; memused=memused+nrow*4;
      case 'float64',fmt='double'; memused=memused+nrow*8;
      otherwise, disp(['Unknown fmtstr: ',fmtstr{i}]), return
     end % switch
     Data{i}=zeros(nrow,1,fmt);
     %disp([' Memory used: ',int2str(memused),' bytes'])
    end
    disp([' Memory used: ',int2str(memused/2^20),' Mb'])
    for row=1:nrow
     if(nrow>5000)
      if(~rem(row,floor(nrow/20)))
       disp([int2str(row),' of ',int2str(nrow)])
      end
     end
     for i=1:nfields
      Data{i}(row)=fread(fid,1,fmtstr{i});
     end
    end
    for i=1:nfields
     data=setfield( data, DataStr{i}, Data{i} );
     Data{i}=0;
    end
   end
% End of time-consuming loop  

  end % if-else any of the fields have more than one entry

  if(sum(ArrayDescriptorFlag)>0)
% Read in the HEAP, which should only exist (?) if there are variable length
% arrays.

% A binary table containing variable length arrays consists of three principal
% segments, as follows: 
%   [table_header] [record_storage_area] [heap_area] 
% The table header consists of one or more 2880-byte FITS logical records with
% the last record indicated by the  keyword END somewhere in the record. The
% record storage area begins with the next 2880-byte logical record following
% the last header record and is NAXIS � NAXIS2 bytes in length. The  zero
% indexed byte offset of the heap measured from the start of the record
% storage area is given by the THEAP keyword  in the header. If this keyword
% is missing the heap is assumed to begin with the byte immediately following
% the last data record, otherwise there may be a gap between the last stored
% record and the start of the heap. If there is no gap the value of the heap
% offset is NAXIS � NAXIS2. The total length in bytes of the heap area
% following the last stored record (gap plus heap) is given by the PCOUNT
% keyword  in the table header. 
  disp('Reading in Arrays given by Descriptors')
  HeapOffset=FITSfindkey(header,'THEAP'); % This returns 0 if there is no
                                       %  keyword 'THEAP', which is want we
                                       %  want it to be in that case, anyway
  if(HeapOffset<0), error('THEAP negative.  Bad THEAP.  Bad.'), end
  status=fseek(fid,HeapOffset,'cof');  % Move the distance, HeapOffSet, from
                                       %  the current file position.
  if(status==-1), error('Could not move to the start of the heap'), end
  HeapStart=ftell(fid); % The position of the start of the heap.
  DescriptorFields=find(ArrayDescriptorFlag==1);
  NDescriptors=length(DescriptorFields);
  for i=1:nrow
   for j=1:NDescriptors
    k=DescriptorFields(j);
    status=fseek(fid,HeapStart+Offset(i,j),'bof');
    if(status==-1)
     error('Could not move to the requested offset')
    end
    if(NElems_actual(i,j) > 0)
%     dummy=fread(fid,NElems_actual(i,j),sscanf(fmtstr(k,:),'%s'));
     dummy=fread(fid,NElems_actual(i,j),fmtstr{k});
%     eval([sscanf(DataStr(k,:),'%s'),'(i,1:NElems_actual(i,j))=dummy;']);
%     data=setfield(data,sscanf(DataStr(k,:),'%s'),{i,1:NElems_actual(i,j)},dummy);
     data=setfield(data,DataStr{k},{i,1:NElems_actual(i,j)},dummy);
    end % read in only if there are elements to be read in
   end % loop through Descriptors
  end % loop through all rows of the table
 
 end % if there are Array Descriptors  

 % end case 'BINTABLE
 otherwise
  disp(['I have not been programmed to deal with records of the type ',DataType])
  return

% ###### End of slow stuff

end
% End of switch DataType
fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% End of programme FITSload %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function header=local_read_header(fid)
header=[];
while(feof(fid)==0)
 % Read in 2880 blocks at a time (36 lines)
 long_str=char(fread(fid,2880,'uchar')');
 % If we have an "END" followed by 77 blanks in the block just read,
 % then assume it is the end of the header.
 pos=findstr(long_str,['END',32*ones(1,77)]);
 if(isempty(pos))
  % Not a final block of 36 lines.
  % Reshape it into 36 rows of 80 characters and append
  header=[header;reshape(long_str,80,36)'];
 else % A final block of 36 lines
  Nlines=(pos-1)/80;
  % Sanity check
  if(rem(Nlines,1)>0)
   error('the END of the header is not n*80+1 characters into the header.')
  end
  header=[header;reshape(long_str(1:80*Nlines),80,Nlines)'];
  break
 end % if not the final set of 36 card images, else, end
 % Since we've been reading in 2880 characters at a time, and headers
 % must end on a multiple of 2880, we don't have to scan ahead for the
 % beginning of the the data section.
end %while
%%%% End of local_read_header

function datatype=local_datatype_from_bitpix(unsigned,bitpix)
% Convert the bitpix character string into a precision Matlab understands
switch bitpix
 case 8,   datatype='uchar';
 case 16,
  if(unsigned==0)
   datatype='int16';
  else
   datatype='uint16';
  end
 case 32,
  if(unsigned==0)
   datatype='int32';
  else
   datatype='uint32';
  end
 case 64,
  if(unsigned==0)
   datatype='int64';
  else
   datatype='uint64';
  end
 case -32, datatype='float32';
 case -64, datatype='float64';
 otherwise
  error(['I do not know what to do with the BITPIX value of ',num2str(bitpix)])
end
%%%% End of local_datatype_from_bitpix
