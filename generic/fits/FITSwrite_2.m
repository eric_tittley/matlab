function FITSwrite(header,data,name)
  
% FITSwrite - function to write FITS files.
%
% function FITSwrite(header,data,name)
%
%
%   header    A char matrix holding all FITS file header
%             "keywords". All chars after position 80
%             are truncated. Header char matrix is easily
%             created with FITSaddkey.
%             If you don't have any keywords to put in to
%             your header, then just put in '' or [] as an
%             argument.
%
%             Image will be stored in datatype specified by
%             BITPIX keyword. Remember to set that keyword
%             to wanted datatype.
%             Valid values are:
%              8 Character or unsigned binary integer
%             16 16-bit twos-complement binary integer
%             32 32-bit twos-complement binary integer
%            -32 IEEE single precision floating point
%            -64 IEEE double precision floating point
%
%             The minimum header should depends on the form
%             of the data.
%
%   data      Data.  Currently supported: Image data and table
%             data.
%
%   name      Path, name and extention for file to store in.
%
% Note: 1) The full set of possibilities described by the FITS
%          standard (http://archive.stsci.edu/FITS/FITS_standard/)
%          is yet to to be implemented.
%       2) Values for BSCALE and BZERO will be applied to the data
%          before writing.
%
% SEE ALSO: FITSload, FITSaddkey, FITSremovekey

% AUTHOR: Eric Tittley
% HISTORY
%       Original version Bjorn Gustavsson 98-5-25 wFITS.m
% Modified         Peter Rydesaeter 98-5-26 FITSwrite version
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%

% We are going to either create the file if it does not exist or append
% to the end.
if(exist(name))
 rec=1;
else
 rec=0;
end

%In FITS format, the primary data array shall consist of a single data array of
% 0-999 dimensions.  Here, we call the array, 'image'.
% Needless to say, it is not an extension.
DataType=FITSfindkeystring(header,'XTENSION');
% We can only accept absent 'XTENSION' keywords if it is record 0
if(length(DataType)==0)
 if( rec==0 )
  if(length(data)==0)
   DataType='EMPTY';
  elseif(length(data)>1)
   DataType='IMAGE';
  else
   % Presume that if size(data)=[1,1], then it is a structure, not an array.
   % This is potentially false and constitutes a bug.  But we need a way to
   % test if something is a structure.
   error('Record 0 must be empty or a single array')
  end
 else % Not record 0
  if(length(data)==0)
   DataType='EMPTY';
  elseif(length(data)>1)
   DataType='IMAGE';
  else
   % Presume that if size(data)=[1,1], then it is a structure, not an array.
   % This is potentially false and constitutes a bug.  But we need a way to
   % test if something is a structure.
   error('Keyword XTENSION must be set if not an empty record or image record')
  end
 end
elseif(rec==0)
 error('Record 0 cannot be an XTENSION')
end

% If the array is empty, then overide the wishes of the user. 
if(isempty(data)), DataType='EMPTY', end

% Open/Create the file as necessary
if(exist(name))
 fid = fopen(name,'a','ieee-be');
 if(fid == -1)
  error('Can not find/open FITS file.');
 end
else
 fid = fopen(name,'w','ieee-be');
 if(fid == -1)
  error('Can not create FITS file.');
 end
end

switch sscanf(DataType,'%s')
 case 'EMPTY'
  local_write_empty(fid,header,rec)

 case 'IMAGE',
  local_write_image(fid,data,header,rec)

 case 'BINTABLE',
  local_write_bintable_orig(fid,data,header,rec)
 
 otherwise,
  error(['I do not know how to deal with records of the form ',DataType])
end

fclose(fid);

% End of main programme

%-------------------------------------------------------------------------

function local_write_header(fid,header,rec)
 
header(end+1,1:80)=['END',blanks(77)];  % Add end line.
header=header(:,1:80);     % Cut width of text matrix to 80.

if(rec==0)
 % Is it necessarily true that the Primary header cannot exceed 36 rows?
 % The standard states:
 %  The header of a primary HDU  shall consist of a series of card images  in
 %  ASCII  text. All header records shall consist of 36 card images. Card
 %  images without information shall be filled  with ASCII blanks
 %  (hexadecimal 20). 
 %% Ensure that the Primary header is composed of 36 rows of 80 characters
 [nrows,ncols]=size(header);
 %if(nrows>36)
 % error('more than 36 lines in primary header')
 %elseif(nrows<36)
 padding=rem(nrows,36);
 if(padding==0), padding=36; end
 header=[header;char(32*ones(36-padding,80))];
 %end
end

% Write header
fprintf(fid,'%s',header');

% Zero pad ( This shouldn't be called since 36*80=2880 )
local_zero_pad_ascii(fid);

% End of subroutine local_write_header


%------------------------------------------------------------------------------


function local_zero_pad_ascii(fid)
% Find number of bytes left to end of this 2880 block.
% (this sorta repeats the step done previously for the primary header to ensure
% that the header is composed of 36 x 80 characters
i=mod(2880-mod(ftell(fid),2880),2880);
fwrite(fid,char(32*ones(i,1)),'uchar');

%End of function local_zero_pad_ascii


%------------------------------------------------------------------------------


function local_zero_pad_binary(fid)
% Find number of bytes left to end of this 2880 block.
% (this sorta repeats the step done previously for the primary header to ensure
% that the header is composed of 36 x 80 characters
i=mod(2880-mod(ftell(fid),2880),2880);
fwrite(fid,char(0*ones(i,1)),'uchar');

%End of function local_zero_pad_binary


%------------------------------------------------------------------------------


function local_write_empty(fid,header,rec)

% Remove old keywords
header=FITSremovekey(header,'SIMPLE');
header=FITSremovekey(header,'BITPIX');
header=FITSremovekey(header,'NAXIS');
header=FITSremovekey(header,'NAXIS1');
header=FITSremovekey(header,'NAXIS2');
header=FITSremovekey(header,'EXTEND');
header=FITSremovekey(header,'END');

%Add new keywords.
header=FITSaddkey(header,'SIMPLE','T',1);
header=FITSaddkey(header,'BITPIX',8  ,2);
header=FITSaddkey(header,'NAXIS' ,0  ,3);
header=FITSaddkey(header,'EXTEND','T',4);

% Write header
local_write_header(fid,header,rec);

%%%% END OF FUNCTION local_write_empty %%%%


%------------------------------------------------------------------------------

function local_write_image(fid,data,hdr,rec)

bitpix=FITSfindkeyvalue(hdr,'BITPIX');
if(isempty(bitpix)), bitpix=0; end
switch bitpix
 case 0,
  disp('Warning: BITPIX not set. Assuming double')
  image=double(data);
  bitpix=-64;
 case 8,
  image=uint8(data);
 case 16,
  image=int16(data);
 case 32,
  image=int32(data);
 case -32,
  image=single(data);
 case -64,
  image=double(data);
 otherwise,
  error('I do not know what to do with the given value of BITPIX')
end
datatypesize=abs(bitpix)/8;

si=size(data);

% Remove old keywords
hdr=FITSremovekey(hdr,'SIMPLE');
hdr=FITSremovekey(hdr,'BITPIX');
hdr=FITSremovekey(hdr,'NAXIS');
hdr=FITSremovekey(hdr,'NAXIS1');
hdr=FITSremovekey(hdr,'NAXIS2');
hdr=FITSremovekey(hdr,'END');

%Add new keywords.
hdr=FITSaddkey(hdr,'SIMPLE','T',1);
hdr=FITSaddkey(hdr,'BITPIX',bitpix,2);
hdr=FITSaddkey(hdr,'NAXIS' ,2,     3);
hdr=FITSaddkey(hdr,'NAXIS1',si(2), 4);
hdr=FITSaddkey(hdr,'NAXIS2',si(1), 5);

local_write_header(fid,hdr,rec);

% This is a bit of a contentious section.  It is convenient to have the
% FITS writer scale the data independently of the user, but then the
% user must know not to do it first
if( FITSfindkey(hdr,'BZERO')>0 & FITSfindkey(hdr,'BSCALE')>0 )
 data=   data / FITSfindkeyvalue(hdr,'BSCALE')  ...
       - FITSfindkeyvalue(hdr,'BZERO');
end
% End of scaling

% Write data.
switch bitpix,
 case 8,   fwrite(fid,data','uint8');
 case 16,  fwrite(fid,data','int16');
 case 32,  fwrite(fid,data','int32');
 case -32, fwrite(fid,data','float32');
 case -64, fwrite(fid,data','float64');
 otherwise,
  error('Impossible state.  This should have been caught.')
end

%Pad out to a 2880 boundary
local_zero_pad_binary(fid)


%%%%% END OF FUNCTION local_write_image %%%%%


%------------------------------------------------------------------------------

function local_write_bintable_orig(fid,data,hdr,rec)

% A row of a binary table is a collection of data fields of (potentially)
% different lengths For example, 100 integers, followed by 50 floats, followed
% by 25 characters.  The data is laid out one row at a time.

% ARRAY DESCRIPTORS ARE NOT YET SUPPORTED

% We will presume that the table data is in a structure.
% Each field of the structure will have Nrows rows, which will be the
% same for each field.
% Each field can have Ncols columns, and this can be different for each
% field.
% However, if none of the fields is an array, then we can forget them, and
% just use the singleton dimension.

% A crude check to see if data is of a size other than 1x1, which is what
% is reported by size for structures.
[dummyx,dummyy]=size(data);
if(dummyx~=1 | dummyy~=1)
 error('data must be a structure if to be written to a BINTABLE')
end
% I wish there were a way to determine if a variable represents a structure.
% The next line can cause an unclean error if data is an array of size 1x1.
fields=fieldnames(data);
[ncols,dummy]=size(fields);
[nrows,dummy]=size(getfield(data,fields{1}));

% Sanity check
% Here we will find the master_index.  If it is 1, then the rows of the tables
% will correspond to rows of the fields.  If it is 2, then table rows will
% correspond to columns of the fields.
for i=1:ncols
 [field_rows(i),field_cols(i)]=size(getfield(data,fields{i}));
end
if(ncols>1) % if more than one field
 if(sum(field_rows==1)==ncols) % the 1st index is singleton for every field
  nrows=field_cols(1);
  master_index=2;
  % All the other field 2nd index lengths must be the same, otherwise there
  % can only be one row
  if(sum(field_cols(2:end)==nrows)~=(ncols-1))
   nrows=1;
   master_index=1;
  end
 elseif(sum(field_cols==1)==ncols) % the 2nd index is singleton for every field
  nrows=field_rows(1);
  master_index=1;
  % All the other field 1st index lengths must be the same, otherwise there
  % can only be one row
  if(sum(field_rows(2:end)==nrows)~=(ncols-1))
   nrows=1;
   master_index=2;
  end
 else % Neither index is singleton
  % If neither is singleton, then either the rows or the columns must be the
  % same length for each field, and this will become the master_index
  nrows=field_rows(1);
  master_index=1;
  % All the other field 1st index lengths must be the same, otherwise try the
  % second index
  if(sum(field_rows(2:end)==nrows)~=(ncols-1))
   nrows=field_cols(1);
   master_index=2;
   % All the other field 2nd index lengths must be the same, otherwise we
   % are out of options
   if(sum(field_cols(2:end)==nrows)~=(ncols-1))
    error('I cannot make a table out of the data structure without a common dimension')
   end
  end % if the 1st index lengths are not the same
 end % Primary case test
end % if there is more than one field

%END of the sanity check to determine the correct layout of the table

disp(['table with ',int2str(nrows),' row(s). Master index:',int2str(master_index)])

% Get the number of elements in each field.
for i=1:ncols
 dummy=getfield(data,fields{i});
 if(master_index==1)
  NElems(i)=length(dummy(1,:));
 else
  NElems(i)=length(dummy(:,1));
 end
end

%Get the data type for each field.  Default to double.
% Add up the bytes per table row, at the same time.
fmtstr=char(uint8(zeros(ncols,7)));
NBytes=0;
for i=1:ncols
 % Read in the type label, stripping off trailing whitespaces
 TypeLabel=sscanf(FITSfindkeystring(hdr,['TFORM',int2str(i)]),'%s');
 % Default to double.
 if(isempty(TypeLabel)), TypeLabel='D'; end
 Type=TypeLabel(end); % This is not exactly strict, since 100Integer is also
                      % acceptable, according to the standard.
 switch Type
  case 'L', % 1-byte logical
   fmt='uchar  '; 
   NBytes=NBytes+1*NElems(i);
  case 'X', % Bit.  This is tricky, since Matlab can't read bits.
   % If the bits are stored in a grouping of 8, 16, 32, or 64, then
   % read them in as integers
   if(NElems(i)== 8)
    fmt='uint8  ';
    NElems(i)=1;
    NBytes=NBytes+1;
   elseif(NElems(i)==16)
    fmt='uint16 ';
    NElems(i)=1;
    NBytes=NBytes+2;
   elseif(NElems(i)==32)
    fmt='uint32 ';
    NElems(i)=1;
    NBytes=NBytes+4;
   elseif(NElems(i)==64),
    fmt='uint64 ';
    NElems(i)=1;
    NBytes=NBytes+8;
   else
    disp('Sorry, Matlab can not write bits unless they are in blocks of 8, 16, 32, or 64');
    return;
   end
  case 'B', % unsigned byte
   fmt='uchar  ';
   NBytes=NBytes+1*NElems(i);
  case 'A', % 1-byte character
   fmt='uchar  ';
   NBytes=NBytes+1*NElems(i);
  case 'I', % 2-byte integer
   fmt='int16  ';
   NBytes=NBytes+2*NElems(i);
  case 'J', % 4-byte integer
   fmt='int32  ';
   NBytes=NBytes+4*NElems(i);
  case 'E', % 4-byte real
   fmt='float32';
   NBytes=NBytes+4*NElems(i);
  case 'D', % 8-byte double
   fmt='float64';
   NBytes=NBytes+8*NElems(i);
  case 'C', disp('Sorry, I can not yet write complex'); return; % Complex
  case 'M', disp('Sorry, I can not yet write complex'); return; % Complex Double
  otherwise
   error(['I do not know what to do with the TTYPE ',Type])
 end % Switch
 fmtstr(i,:)=fmt;
 key=['TTYPE',int2str(i)];
 hdr=FITSremovekey(hdr,key);
 hdr=FITSaddkey(hdr,key,fields{i},0);
 key=['TFORM',int2str(i)];
 hdr=FITSremovekey(hdr,key);
 hdr=FITSaddkey(hdr,key,[int2str(NElems(i)),Type],0);
end % end of loop through fields

% Remove old keywords
hdr=FITSremovekey(hdr,'SIMPLE');
hdr=FITSremovekey(hdr,'XTENSION');
hdr=FITSremovekey(hdr,'BITPIX');
hdr=FITSremovekey(hdr,'NAXIS');
hdr=FITSremovekey(hdr,'NAXIS1');
hdr=FITSremovekey(hdr,'NAXIS2');
hdr=FITSremovekey(hdr,'PCOUNT');
hdr=FITSremovekey(hdr,'GCOUNT');
hdr=FITSremovekey(hdr,'TFIELDS');
hdr=FITSremovekey(hdr,'END');

%Add new keywords.
if(rec==0)
 hdr=FITSaddkey(hdr,'SIMPLE','T',   1);
else
 hdr=FITSaddkey(hdr,'XTENSION','BINTABLE',1);
end
hdr=FITSaddkey(hdr,'BITPIX' ,8,     2);
hdr=FITSaddkey(hdr,'NAXIS'  ,2,     3);
hdr=FITSaddkey(hdr,'NAXIS1' ,NBytes,4);
hdr=FITSaddkey(hdr,'NAXIS2' ,nrows, 5);
hdr=FITSaddkey(hdr,'PCOUNT' ,0,     6);
hdr=FITSaddkey(hdr,'GCOUNT' ,1,     7);
hdr=FITSaddkey(hdr,'TFIELDS',ncols, 8);

% Write the header
local_write_header(fid,hdr,rec);

% Now to the data
step=max(floor(nrows/25),1);
for row=1:nrows
 if(~rem(row,step) & nrows>5000)
  disp([int2str(row),' of ',int2str(nrows)]),
 end
 for i=1:ncols
  if(master_index==1)
   dummy=getfield(data,fields{i},{row,1:NElems(i)});
  else
   dummy=getfield(data,fields{i},{1:NElems(i),row});
  end
  fwrite(fid,dummy,sscanf(fmtstr(i,:),'%s'));
 end % loop over fields
end % loop over rows

local_zero_pad_binary(fid);

%%%%% END OF FUNCTION local_write_bintable_orig %%%%%


%------------------------------------------------------------------------------

%%%% THIS NEXT FUNCTION HAS YET TO BE TESTED.
function local_write_bintable(fid,data,hdr,rec)

%%% Some basic checks to the header
% XTENSION
xtension=FITSfindkeystring(hdr,'XTENSION');
if(isempty(xtension))
 disp('WARNING: XTENSION keyword not set. Setting...')
 xtension='BINTABLE';
 hdr=FITSaddkey(hdr,'XTENSION',xtension,1);
end
if(~ strcmp(xtension,'BINTABLE'))
 xtension='BINTABLE';
 hdr=FITSremovekey(hdr,'XTENSION');
 hdr=FITSaddkey(hdr,'XTENSION',xtension,1);
 disp('WARNING: XTENSION ~= BINTABLE. Setting...')
end
% BITPIX
bitpix=FITSfindkeyvalue(hdr,'BITPIX');
if(isempty(bitpix))
 disp('WARNING: BITPIX keyword not set. Setting...')
 bitpix=8;
 hdr=FITSaddkey(hdr,'BITPIX',bitpix,2);
end
if(bitpix~=8)
 disp('WARNING: BITPIX ~= 8. Setting...')
 bitpix=8;
 hdr=FITSremovekey(hdr,'BITPIX');
 hdr=FITSaddkey(hdr,'BITPIX',bitpix,2);
end
% NAXIS
naxis=FITSfindkeyvalue(hdr,'NAXIS');
if(isempty(naxis))
 disp('WARNING: NAXIS keyword not present.  Setting to 2...')
 naxis=2;
 hdr=FITSaddkey(hdr,'NAXIS',naxis,3);
end
if(naxis~=2)
 disp('WARNING: NAXIS ~= 2. Setting to 2...');
 naxis=2;
 hdr=FITSremovekey(hdr,'NAXIS');
 hdr=FITSaddkey(hdr,'NAXIS',naxis,3);
end
% NAXIS1 goes here.
naxis1=FITSfindkeyvalue(hdr,'NAXIS1');
if(isempty(naxis1))
 disp('WARNING: NAXIS1 not set. Setting to 0...')
 naxis1=0;
 hdr=FITSaddkey(hdr,'NAXIS1',naxis1,4);
else
 hdr=FITSremovekey(hdr,'NAXIS1');
 hdr=FITSaddkey(hdr,'NAXIS1',naxis1,4);
end
% NAXIS2
names=fieldnames(data);
naxis2=size(getfield(data,names{1}),1);
naxis2_orig=FITSfindkeyvalue(hdr,'NAXIS2');
if(isempty(naxis2_orig))
 disp('WARNING: NAXIS2 not set. Setting...')
 hdr=FITSaddkey(hdr,'NAXIS2',naxis2,5);
else
 if(naxis2_orig~=naxis2)
  disp('WARNING: NAXIS2 not equal to the number of rows in the first table column.')
  disp('         Resetting...')
 end
 hdr=FITSremovekey(hdr,'NAXIS2');
 hdr=FITSaddkey(hdr,'NAXIS2',naxis2,5);
end
% PCOUNT goes here.
pcount=FITSfindkeyvalue(hdr,'PCOUNT');
if(isempty(pcount))
 disp('WARNING: pcount not set. Setting to 0...')
 naxis2=0;
 hdr=FITSaddkey(hdr,'PCOUNT',pcount,6);
else
 hdr=FITSremovekey(hdr,'PCOUNT');
 hdr=FITSaddkey(hdr,'PCOUNT',pcount,6);
end
% GCOUNT goes here.
gcount=FITSfindkeyvalue(hdr,'GCOUNT');
if(isempty(gcount))
 disp('WARNING: GCOUNT keyword not present.  Setting to 1...')
 naxis=2;
 hdr=FITSaddkey(hdr,'GCOUNT',gcount,7);
end
if(gcount~=1)
 disp('WARNING: gcount ~= 1. Setting to 1...');
 gcount=1;
end
hdr=FITSremovekey(hdr,'GCOUNT');
hdr=FITSaddkey(hdr,'GCOUNT',gcount,7);
% TFIELDS, the number of fields in the data structure.
nfields=size(names,1);
pos=FITSfindkey('TFIELDS');
if(pos==0)
 disp('WARNING: No TFIELDS keyword found.  Setting...')
 pos=FITSfindkey('GCOUNT')+1;
end
hdr=FITSremovekey(hdr,'TFIELDS');
hdr=FITSaddkey(hdr,'TFIELDS',nfields,pos);

%%% Get the Format String
[fmtstr,NElems,ArrayDescriptorFlag,NBytes]=local_Create_fmtstr(hdr);

%%% Set NAXIS1, which is the # bytes per row and add it after the 'NAXIS' keyword
pos=FITSfindkey(hdr,'NAXIS1');
hdr=FITSremovekey(hdr,'NAXIS1');
hdr=FITSaddkey(hdr,'NAXIS1',NBytes,pos);

%%% Now we are ready to write things.

%% First the header.  We will assume that the primary (0) header has already
%% been written. This will pad out as necessary.
local_write_header(fid,hdr,rec);

%% The data
%% There are two distinctions: 1) there are array descriptors
%%                             2) there are no descriptors
step=max(floor(nrow/25),1);
for row=1:nrow
 if(~rem(row,step) & nrow>5000)
  disp([int2str(row),' of ',int2str(nrow)])
 end
 for i=1:nfields
  eval(['fwrite(fid,data.',names{i},'(row,:),fmtstr{i});'])
 end
end % loop through rows

%% Pad out the data storage area to end on a 2880-byte offset from the start
%% of the file.
local_zero_pad_binary(fid);

%%% END OF FUNCTION local_write_bintable %%%


%------------------------------------------------------------------------------


function [fmtstr,NElems,ArrayDescriptorFlag,NBytes]=local_Create_fmtstr(header);

%%% Get the number of fields
nfields=FITSfindkeyvalue(header,'TFIELDS');
if(isempty(nfields))
 error('TFIELDS not set in header.  It should state the number of fields.')
end

%%% Create the Format Strings
fmtstr=cell(nfields,1);
NBytes=0;
for i=1:nfields
 % read in the type label, stripping off trailing whitespaces
 TypeLabel=sscanf(FITSfindkeystring(header,['TFORM',int2str(i)]),'%s');
 if(isempty(TypeLabel))
  error(['No TFORM entry for field ',int2str(i)])
 end
% TypeLabel can either be just a letter, like I or D
% or be a number-of-type combination, like 100I or 16D
% Each will require a different method of reading the file
 ArrayDescriptorFlag(i)=0;
 if(length(TypeLabel)~=1) % Array  
  if( sum(TypeLabel=='P')==1 ) % Array Descriptor rPt(emax)
   % Form: rPt(emax) where the P indicates the amount of space occupied by
   % the array descriptor  in the data record (64 bits), the element count
   % r should be 0, 1, or absent, t is a character denoting the datatype of
   % the array data (L, X, B, I, J, etc., but not P), and emax is a quantity
   % guaranteed to be equal to or greater than the maximum number of elements
   % of type t actually stored in a table record. There is no built-in upper
   % limit on the size of a stored array; emax merely reflects the size of
   % the largest array actually stored in the table, and is provided to avoid
   % the need to preview the table when, for example, reading a table
   % containing variable length elements into a database that supports only
   % fixed size arrays. There may be additional characters in the TFORMn
   % keyword following the emax.
   NBytes=NBytes+8; % All descriptors use 2, 4-byte integers (?)
   P_pos=find(TypeLabel=='P');
   if(length(P_pos)>1), error('Invalid TFORM entry'); end
   if(P_pos==2)
    r=sscanf('%i',TypeLabel(1)); % Parse it out, even though we really don't
                                 % know what to do with it
    if(r~=0 & r~=1), error('Invalid TFORM entry'); end
   end
   TypeLabel=TypeLabel(P_pos+1:end); % Shift it to strip off everything up to
                                   % and including the P
   Type=TypeLabel(1);  % Get the datatype character, t
   if(length(TypeLabel)>=4) % in P<type>(<max num>) format
    NElems(i)=sscanf(TypeLabel(3:end),'%i',1); % Parse out maximum number of
                                               % elements from the string
                                               % 't(NElems)'
   else % not in P<type>(<max num>) format
    % This is not crucial, since we effectively set the size of the array
    % when we scan in the descriptor data.
    % The ASCA RMF's do this, for example.
    NElems(i)=2; % This will flag it so that we will know to scan
   end       
   ArrayDescriptorFlag(i)=1;
  else % of the form nType (like 100I)
   NElems(i)=sscanf(TypeLabel,'%i',1);
   Type=TypeLabel(length(int2str(NElems(i)))+1);
  end % if an array descriptor or nType form
 else % single element
  NElems(i)=1; % of the form Type (just I)
  Type=TypeLabel;
 end
 switch Type
  case 'L', fmt='uchar  '; % 1-byte logical
  case 'X', % Bit.  This is tricky, since Matlab can't read bits.
   % If the bits are stored in a grouping of 8, 16, 32, or 64, then
   % read them in as integers
   if(NElems(i)== 8)
    fmt='uint8  ';
    NElems(i)=1;
    NBytes=NBytes+1;
   elseif(NElems(i)==16)
    fmt='uint16 ';
    NElems(i)=1;
    NBytes=NBytes+2;
   elseif(NElems(i)==32)
    fmt='uint32 ';
    NElems(i)=1;
    NBytes=NBytes+4;
   elseif(NElems(i)==64),
    fmt='uint64 ';
    NElems(i)=1;
    NBytes=NBytes+8;
   else
    disp('Sorry, Matlab can not read bits unless they are in blocks of 8, 16, 32, or 64');
    return;
   end
  case 'B',
   fmt='uchar  '; % unsigned byte
   NBytes=NBytes+1*NElems(i);
  case 'A',
   fmt='uchar  '; % 1-byte character
   NBytes=NBytes+1*NElems(i);
  case 'I',
   fmt='int16  '; % 2-byte integer
   NBytes=NBytes+2*NElems(i);
  case 'J',
   fmt='int32  '; % 4-byte integer
   NBytes=NBytes+4*NElems(i);
  case 'E',
   fmt='float32'; % 4-byte real
   NBytes=NBytes+4*NElems(i);
  case 'D',
   fmt='float64'; % 8-byte double
   NBytes=NBytes+8*NElems(i);
  case 'C', disp('Sorry, I can not yet write complex'); return; % Complex
  case 'M', disp('Sorry, I can not yet write complex'); return; % Complex Double
  otherwise
   disp(['I do not know what to do with the TFORM ',Type])
   return
 end % Switch
 fmtstr{i}=sscanf(fmt,'%s');
end % end of loop through fields
