% Misc routines that will be catalogued
%
%  *** Time ***
% 
%  Greg2Jul	A simplistic (i.e. perhaps erroneous at times) Gregorian
%		to Julian data converter
% 
%  *** Unit Conversion ***
% 
%  deg2dms	Converts Degrees to Degrees, Minutes, and Seconds
%		or Hours to H, M, S
%
%  dms2deg	Converts (d,m,s) to degrees, or (H,M,S) to Hours
%
%  *** Misc ***
% 
%  monty_windows
%		Monty Carol simulation of the Monty Hall windows
%		probability problem
