clear

%The minimum contour level

load -ascii MinLevel.dat
ContourLevels=2.^[MinLevel:0.5:0];

% The object name
fid=fopen('ObjName','r');
 Cluster=fscanf(fid,'%s');
fclose(fid);

more off
load Image.mat

% The instrument for each observation
load Inst.mat
DecTicks=[-90:2/60:90];
RATicks=[-90:5/60:90];

figure(1)
clf
Xaxis = (Xaxis-mean(Xaxis))*deg_per_hr;
Yaxis = Yaxis-mean(Yaxis);
axis([Xaxis([end,1])-mean(Xaxis),Yaxis([1,end])])
ah=gca;
c=contourc(Xaxis,Yaxis,Image,ContourLevels);
set(gca,'ydir','nor','xdir','rev')
set(gca,'dataaspectratio',[1 1 1])
set(gca,'Ytick',DecTicks);
set(gca,'yTickLabel',DEC2str(DecTicks,2))
set(gca,'Xtick',RATicks);
set(gca,'xTickLabel',DEC2str(RATicks,2))

Nelem=size(c,2);

i=1;
j=1;
while( i< Nelem )
 npoints=c(2,i);
 if(npoints>5)
  level(j)=c(1,i);
  line(c(1,i+1:i+npoints),c(2,i+1:i+npoints));
  ellipse_t(j) = fit_ellipse(c(1,i+1:i+npoints),c(2,i+1:i+npoints),ah);
  if(strcmp(ellipse_t(j).status,''))
   j=j+1;
  end
 end
 i=i+npoints+1;
end

% Parse out the largest areas at each level
N = length(level);
j=1;
i=1;
while( i<=N )
 levels(j)=level(i);
 match = find(level==levels(j));
 Area=zeros(length(match),1);
 for k=1:length(match)
  Area(k)=ellipse_t(match(k)).a*ellipse_t(match(k)).b;
 end
 IndMaxArea = find( Area==max(Area));
 ellip(j) = ellipse_t(match(IndMaxArea));
 j=j+1;
 i=i+length(match);
end

Nlevels=j-1;

for i=1:Nlevels
 X(i)=ellip(i).X0_in;
 Y(i)=ellip(i).Y0_in;
 a(i)=ellip(i).a;
 b(i)=ellip(i).b;
 Phi(i)=ellip(i).phi;
end

eval(['save ',Cluster,'_Bisector.mat Cluster X Y a b Phi'])
