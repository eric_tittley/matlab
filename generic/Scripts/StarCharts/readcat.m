function [ra,dec,mag]=readcat
%catalogue='/usr/local/lib/xephem/edb/basic.edb';
catalogue='basic.edb';

fid=fopen(catalogue,'r');
S=''; type=''; class='';

for i=1:1779

while S~='|', S=fscanf(fid,'%1s',1); end, S=''; %skip name
type=fscanf(fid,'%1s',1);
S=fscanf(fid,'%1s',1); %skip '|'
class=fscanf(fid,'%3s',1);
hour=fscanf(fid,'%2i',1);
S=fscanf(fid,'%1s',1); %skip ':'
minute=fscanf(fid,'%2i',1);
S=fscanf(fid,'%1s',1); %skip ':'
second=fscanf(fid,'%2i',1);
ra(i)=hour+minute/60+second/3600;

S=fscanf(fid,'%1s',1); %skip ','
hour=fscanf(fid,'%3i',1);
S=fscanf(fid,'%1s',1); %skip ':'
minute=fscanf(fid,'%2i',1);
dec(i)=hour+sign(hour)*minute/60;

S=fscanf(fid,'%1s',1); %skip ','
mag(i)=fscanf(fid,'%4f',1);

S=fscanf(fid,'%1s',1); %skip ','
epoch=fscanf(fid,'%4i',1);

end
