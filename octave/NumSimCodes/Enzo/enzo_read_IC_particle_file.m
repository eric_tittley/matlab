% enzo_read_IC_particle_file: The contents of an enzo IC particle file (r or v)
%
% r=enzo_read_IC_particle_file(file)
%
% ARGUMENTS
%  file		The file to read.
%
% RETURNS

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave
%
% HISTORY
%  09 04 15 First version.

function r=enzo_read_IC_particle_file(file)

D=load('-hdf5',file);
r=getfield(D,fieldnames(D){1});
